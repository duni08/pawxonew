//
//  UnFollowAlertVC.swift
//  Pawxo
//
//  Created by 42works on 07/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class UnFollowAlertVC: UIViewController {

    
    
    @IBOutlet weak var profileImage: UIImageView!
    
    
    var nameStr   : String?
    var imageStr  : String?
    
    var searchCell : SearchCollectionCell?
    var followDelegate : FollowVC?
    var profileDelegate : ProfileVC?
    var postLikeDelegate : PostLikesListVC?
    @IBOutlet weak var usernameLabel: RegularLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let name = nameStr{
            self.usernameLabel.text = name
        }
        
        if let imageString = imageStr {
            self.profileImage.setImage(fromUrl: imageString, defaultImage: DefaultImage.defaultProfileImage)
        }
        // Do any additional setup after loading the view.
    }
    
    
    
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    

    @IBAction func unFollowButtonPressed(_ sender: Any) {
        if let searchCell = searchCell {
            self.dismiss(animated: true, completion: nil)
            searchCell.unFollowRequest!()
        }
        
        if let followDelegate = followDelegate{
            self.dismiss(animated: true, completion: nil)
            followDelegate.unFollowRequest!()
        }
        
        if let profileDelegate  = profileDelegate{
            self.dismiss(animated: true, completion: nil)
            profileDelegate.unFollowRequest!()
        }
        
        if let postLikeDelegate  = postLikeDelegate{
            self.dismiss(animated: true, completion: nil)
            postLikeDelegate.unFollowRequest!()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
