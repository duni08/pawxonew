//
//  SearchMainVC.swift
//  Pawxo
//
//  Created by 42works on 07/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class SearchMainVC: UIViewController {
  
    @IBOutlet weak var searchTF: RegularTextField!
    
    
    @IBOutlet weak var searchCollectionView: UICollectionView!
    
    @IBOutlet weak var indicatorView: UIView!
    
    @IBOutlet weak var tagsLabel: RegularLabel!
    @IBOutlet weak var userLabel: RegularLabel!
    @IBOutlet weak var indicatorConstrait: NSLayoutConstraint!
    
    @IBOutlet weak var underlineView: UIView!
    
    @IBOutlet weak var tabView: UIView!
    
    let cellIdentifier = "SearchCollectionCell"
    
    
    var userList = [Userlist]()
    var hashTagList = [HashTag]()
    
    var isUser = true
    
    
    var search_VM : Search_VM?
    
    
    @IBOutlet weak var hideKeyBoardButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.indicatorConstrait.isActive = false
        indicatorView.translatesAutoresizingMaskIntoConstraints = false
        // Do any additional setup after loading the view.
        self.setUpIndicatorView(first: true, animate: false, scroll: false)
        searchCollectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
//        searchCollectionView.register(SearchCollectionCell.self, forCellWithReuseIdentifier: cellIdentifier)
        self.searchTF.becomeFirstResponder()
        self.searchCollectionView.isHidden = true
        //self.underlineView.isHidden = true
        self.tabView.isHidden = true
    }
    
    @IBAction func hideKeyboardButtonPressed(_ sender: Any) {
        self.view.endEditing(true)
        self.hideKeyBoardButton.isHidden = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.searchTF.text!.count > 0 {
            self.setPopOver(textToSearch: self.searchTF.text!)
        }
    }

    @IBAction func backButtonPressed(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setPopOver(textToSearch :String){
        let userLogin = CommonFunctions.getuserFromDefaults()
                search_VM = Search_VM()
                
                search_VM?.userId = userLogin.userId
                search_VM?.perPage = "10"
                search_VM?.page = "1"
                search_VM?.nameStr = textToSearch
               
        
        
                search_VM?.updateLoadingStatus = { [weak self] () in
                    DispatchQueue.main.async {
                        if self?.search_VM?.isLoading ?? false {
                            LoaderView.shared.showLoader()
                        } else {
                            LoaderView.shared.hideLoader()
                        }
                    }
                }
        
                search_VM?.responseRecieved = {
                    
                    self.userList = self.search_VM!.userList
                    self.hashTagList = self.search_VM!.hashTagList
                    // refresh content
                    self.isUser = true
                   
                    self.showTableView(show: true)
                    
                    //self.setPetViewHeight()
                }
                
                search_VM?.showAlertClosure = {[weak self] () in
                    DispatchQueue.main.async {
                        if let message = self?.search_VM?.alertMessage {
//                            self?.showAlert(self ?? UIViewController(), message: message)
//                            self?.userList.removeAll()
//                            self?.showTableView(show: false)
                        }
                    }
                }
         search_VM?.callGetSearchListService = true
    }
    
    
    func showTableView(show:Bool){
        self.searchCollectionView.isHidden = !show
        self.tabView.isHidden = !show
        //self.underlineView.isHidden = !show
        self.searchCollectionView.reloadData()
    }
    
    func setUpIndicatorView(first:Bool, animate:Bool,scroll:Bool){
        self.indicatorConstrait.constant = 0
        if first {
            self.indicatorConstrait.constant = -(self.view.center.x / 2)
            self.userLabel.textColor = AppColor.baseColor
            self.tagsLabel.textColor = AppColor.colorGreySubHeading
            if scroll {
                self.searchCollectionView.scrollToTop()
            }
        }else{
            self.indicatorConstrait.constant = (self.view.center.x / 2)
            self.userLabel.textColor = AppColor.colorGreySubHeading
            self.tagsLabel.textColor = AppColor.baseColor
            if scroll {
                self.searchCollectionView.scrollToBottom()
            }
        }
        if animate {
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    @IBAction func UserButtonTapped(_ sender: UIButton) {
        self.setUpIndicatorView(first: true, animate: true, scroll: true)
        //self.searchTF.placeholder = "Search your friends"
    }
    
   
    @IBAction func TagButtonPressed(_ sender: UIButton) {
        //self.indicatorView.removeConstraint(self.indicatorView!.centerXAnchor)
        self.setUpIndicatorView(first: false, animate: true, scroll: true)
        //self.searchTF.placeholder = "Search your Tags"
    }
}



extension SearchMainVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let searchCell : SearchCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! SearchCollectionCell
        if indexPath.item == 1{
            searchCell.isTagView = true
        }else{
            searchCell.isTagView = false
        }
        searchCell.userList = self.userList
        searchCell.hashTagList = self.hashTagList
        searchCell.setListing()
        searchCell.delegate = self
        return searchCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height =  self.view.bounds.size.height - (self.view.safeAreaInsets.top + self.view.safeAreaInsets.bottom + 101)
        let width = collectionView.frame.width
        
        return CGSize(width: self.view.frame.width, height: height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let visibleRect = CGRect(origin: searchCollectionView.contentOffset, size: searchCollectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = searchCollectionView.indexPathForItem(at: visiblePoint)
        // print(visibleIndexPath?.item)
        if(visibleIndexPath?.item ==  1){
            self.setUpIndicatorView(first: false, animate: true, scroll: false)
        }else{
            self.setUpIndicatorView(first: true, animate: true, scroll: false)
        }
        
    }
}


extension UICollectionView {
    
    func scrollToBottom(){
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(
                row: self.numberOfItems(inSection:self.numberOfSections - 1) - 1,
                section: self.numberOfSections - 1)
            self.scrollToItem(at: indexPath, at: .right, animated: true)
            
        }
    }
    
    func scrollToTop() {
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.scrollToItem(at: indexPath, at: .left, animated: true)
        }
    }
}


extension SearchMainVC : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.hideKeyBoardButton.isHidden = false
        if textField.text == "" {
            self.searchCollectionView.isHidden = true
        }else{
            self.searchCollectionView.isHidden = false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField.text == "" {
            self.searchCollectionView.isHidden = true
        }else{
            self.searchCollectionView.isHidden = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var updatedText = ""
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
           
        }
        print(updatedText)
        if updatedText.count == 0 {
            self.showTableView(show: false)
        }else if (updatedText.count >= 1){
            self.setPopOver(textToSearch: updatedText)
            
        }else{
            
        }
        return true
    }
    
}



extension SearchMainVC : SearchCollectionCellProtocol{
    func profileButtonPressed(user: Userlist) {
        let storyboardProfile = UIStoryboard(name: "Profile", bundle: nil)
        let vc_Profile = storyboardProfile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc_Profile.isFromOtherScreen = true
        vc_Profile.userId = "\(user.id!)"
        self.navigationController?.pushViewController(vc_Profile, animated: true)
    }
    
    func alert(message: String) {
        self.showAlert(self, message: message)
    }
    
    
    func tagPressed(hashTag:String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_PostHashTag = storyboard.instantiateViewController(withIdentifier: "HashTagPostsVC") as! HashTagPostsVC
        vc_PostHashTag.hashTag = hashTag
        self.navigationController?.pushViewController(vc_PostHashTag, animated: true)
    }
    
    
    func alertForUnFollow(cell:SearchCollectionCell,user:Userlist){
       
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let unfollow = storyboard.instantiateViewController(withIdentifier: "UnFollowAlertVC") as! UnFollowAlertVC
        unfollow.nameStr = user.name ?? ""
        unfollow.imageStr = user.userImage ?? ""
        unfollow.searchCell = cell
        unfollow.modalPresentationStyle = .overCurrentContext
        unfollow.modalTransitionStyle = .crossDissolve
        self.present(unfollow, animated: true, completion: nil)
        //     cell.unFollowRequest!()
       
    }
}
