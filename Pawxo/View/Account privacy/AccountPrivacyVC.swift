//
//  AccountPrivacyVC.swift
//  Pawxo
//
//  Created by Apple on 17/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class AccountPrivacyVC: UIViewController {

    @IBOutlet weak var privacySwitch: UISwitch!
    
    var accountPrivateStatus = 0
    var isUpdated = false
    
    var account_VM = AccountPrivacy_VM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        privacySwitch.isOn = false
        fetchAccountPrivacy()
    }
    
    
    
    
    
    @IBAction func privacyChanged(_ sender: UISwitch) {
        
        if isUpdated {
            self.updateAccountPrivacy(status: sender.isOn)
        }else{
            privacySwitch?.setOn(false, animated: true)
            let message = "Setings not Updated from Account, PLease Refresh"
            let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "Refresh", style: .default, handler: { action in
                self.fetchAccountPrivacy()
            }))
            alertController.view.tintColor = UIColor.black
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    
    func fetchAccountPrivacy(){
        let userLogin = CommonFunctions.getuserFromDefaults()
        account_VM.userId = userLogin.userId
        account_VM.responseRecieved = {
            self.accountPrivateStatus = self.account_VM.account_private_status ?? 0
            // refresh content
            self.isUpdated = true
            self.updateStatus()
        }
        
        
        account_VM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if self.account_VM.isLoading {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        account_VM.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.account_VM.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        account_VM.callGetAccountStatusService = true
    }
    
    
    func updateAccountPrivacy(status:Bool)
    
    {
        let userLogin = CommonFunctions.getuserFromDefaults()
        account_VM.userId = userLogin.userId
        
        
        if status {
            account_VM.account_private_status = 1
        }else{
            account_VM.account_private_status = 0
        }
        
        
        
        account_VM.responseRecieved = {
            
            self.accountPrivateStatus = (self.accountPrivateStatus == 0) ? 1 : 0
            // refresh content
            self.isUpdated = true
            self.updateStatus()
            
        }
        
        account_VM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if self.account_VM.isLoading {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        account_VM.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.account_VM.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
                self?.updateStatus()
            }
        }
        
        account_VM.callUpdatePrivacyService = true
    }
    
    func updateStatus(){
        self.privacySwitch.isOn = self.accountPrivateStatus.boolValue
        
    }
    
    @IBAction func backButtonTapped(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
