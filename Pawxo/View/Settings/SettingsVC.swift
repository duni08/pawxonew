//
//  SettingsVC.swift
//  Pawxo
//
//  Created by Apple on 16/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {

    @IBOutlet weak var settings_TBL: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}


extension SettingsVC:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Settings.titleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:SettingsCell = (tableView.dequeueReusableCell(withIdentifier: "Cell") as? SettingsCell)!

        cell.title_Lbl.text = Settings.titleArr[indexPath.row]
        
        
        
        if indexPath.row == 0 || indexPath.row == 1{
            cell.description_Lbl.text = Settings.descriptionArr[indexPath.row]
            cell.nameTopConstraints.constant = 8
        }else{
            cell.description_Lbl.text = ""
            cell.nameTopConstraints.constant = 18
        }
        
        if indexPath.row == Settings.titleArr.count - 1{
            cell.dividerView.isHidden = true
        }else{
            cell.dividerView.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
         DispatchQueue.main.async {
        
        let storyboardPet = UIStoryboard(name: "Settings", bundle: nil)
        var viewController = UIViewController()
        
        let selectItem = Settings.titleArr[indexPath.row]
        
        if selectItem == "Push Notifications" {
            
            viewController = storyboardPet.instantiateViewController(withIdentifier: "PushNotificationVC") as! PushNotificationVC
            
        }else if selectItem == "Change Password" {
            
            viewController = storyboardPet.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            
        }else if selectItem == "Linked Accounts" {
            
            viewController = storyboardPet.instantiateViewController(withIdentifier: "LinkedAccountVC") as! LinkedAccountVC
            
        }else if selectItem == "Privacy Policy" {
            
            viewController = storyboardPet.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
            
        }else if selectItem == "Account Privacy" {
            
            viewController = storyboardPet.instantiateViewController(withIdentifier: "AccountPrivacyVC") as! AccountPrivacyVC
            
        }else if selectItem == "Logout" {
            
            let alertController = UIAlertController(title: "Are you sure you want to logout?", message: "", preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
               
                    
                    kAppDelegate.logoutFromApp()
                
            }))
            
            let cancelAction = UIAlertAction(title: "CANCEL", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)

            alertController.view.tintColor = UIColor.black
            self.present(alertController, animated: true, completion: nil)
            
            return
            
        }
        
        self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    
}
