//
//  BasicCell.swift
//  MyPagerCollView
//
//  Created by Leela Prasad on 08/03/18.
//  Copyright © 2018 Leela Prasad. All rights reserved.
//

import UIKit

class BasicCell: UICollectionViewCell {
    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.JosefinSansSemiBold(size: 16.0)
        lbl.textColor = AppColor.colorGreySubHeading
        return lbl
    }()
    
    var indicatorView: UIView!
    var underlineView : UIView!
    override var isSelected: Bool {
        
        didSet{
            //UIView.animate(withDuration: 0.30) {
            self.indicatorView.backgroundColor = self.isSelected ? AppColor.baseColor : UIColor.clear
               // self.layoutIfNeeded()
            self.titleLabel.textColor = self.isSelected ? AppColor.baseColor : AppColor.colorGreySubHeading
            //}
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        titleLabel.textAlignment = .center
        addSubview(titleLabel)
        addConstraintsWithFormatString(formate: "H:|[v0]|", views: titleLabel)
        addConstraintsWithFormatString(formate: "V:|[v0]|", views: titleLabel)
        
        addConstraint(NSLayoutConstraint.init(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint.init(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        setupIndicatorView()

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = ""
    }
   
    func setupIndicatorView() {
        underlineView = UIView()
        underlineView.backgroundColor = AppColor.baseColor
        addSubview(underlineView)
        
        addConstraintsWithFormatString(formate: "H:|[v0]|", views: underlineView)
        addConstraintsWithFormatString(formate: "V:[v0(0.8)]|", views: underlineView)
        
        
        indicatorView = UIView()
        addSubview(indicatorView)
        
        addConstraintsWithFormatString(formate: "H:|[v0]|", views: indicatorView)
        addConstraintsWithFormatString(formate: "V:[v0(2.5)]|", views: indicatorView)
        
        
    }
    
}
