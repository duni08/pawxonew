//
//  NewChatVC.swift
//  Pawxo
//
//  Created by 42works on 20/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class NewChatVC: UIViewController, UITextFieldDelegate {
    // MARK: - PROPERTIES
    
    @IBOutlet var searchTF: RegularTextField!
    @IBOutlet var newChatTableView: UITableView!
    
    var cellIdentifier = "NewChatCell"
    var staticCount = 7
    var search_VM: Search_VM?
    var userList = [Userlist]()
    var hashTagList = [HashTag]()
    
    var isUser = true
    
    @IBOutlet var hideTableButton: UIButton!
    
    // MARK: - LIFECYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//          self.searchTF.becomeFirstResponder()
        self.hideTableButton.isHidden = true
        newChatTableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        searchListApi()
//          if self.searchTF.text!.count > 0 {
//                    self.setPopOver(textToSearch: self.searchTF.text!)
//                }
    }
    
    @IBAction func hideTableButtonPressed(_ sender: Any) {
        hideTableButton.isHidden = true
        view.endEditing(true)
    }
    
    //  MARK: - WEBSERVICES
    
    // SEARCH API
    
    func searchListApi() {
        let userLogin = CommonFunctions.getuserFromDefaults()
        search_VM = Search_VM()
        
        search_VM?.userId = userLogin.userId
        search_VM?.perPage = "10"
        search_VM?.page = "1"
        search_VM?.nameStr = ""
        search_VM?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.search_VM?.isLoading ?? false {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        search_VM?.responseRecieved = {
            self.userList = self.search_VM!.userList
            print(self.userList)
            self.hashTagList = self.search_VM!.hashTagList
            // refresh content
            self.isUser = true
            
            self.showTableView(show: true)
            
            // self.setPetViewHeight()
        }
        
        search_VM?.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.search_VM?.alertMessage {
                    //                            self?.showAlert(self ?? UIViewController(), message: message)
                    //                            self?.userList.removeAll()
                    //                            self?.showTableView(show: false)
                }
            }
        }
        search_VM?.callGetSearchListService = true
    }
    
    // MARK: HELPER FUNCTION  SERCH TEXTFILED
    
    func setPopOver(textToSearch: String) {
        let userLogin = CommonFunctions.getuserFromDefaults()
        search_VM = Search_VM()
        
        search_VM?.userId = userLogin.userId
        search_VM?.perPage = "10"
        search_VM?.page = "1"
        search_VM?.nameStr = textToSearch
        
        search_VM?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.search_VM?.isLoading ?? false {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        search_VM?.responseRecieved = {
            self.userList = self.search_VM!.userList
            print(self.userList)
            self.hashTagList = self.search_VM!.hashTagList
            // refresh content
            self.isUser = true
            
            if !self.userList.isEmpty {
                self.showTableView(show: true)
            } else {
                self.showTableView(show: false)
            }
            
            // self.setPetViewHeight()
        }
        
        search_VM?.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.search_VM?.alertMessage {
                    //                            self?.showAlert(self ?? UIViewController(), message: message)
                    //                            self?.userList.removeAll()
                    //                            self?.showTableView(show: false)
                }
            }
        }
        search_VM?.callGetSearchListService = true
    }
    
    // MARK: - UITEXTFILED DELEGATE
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        hideTableButton.isHidden = false
    }
    
//
//    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
//        if textField.text == "" {
//            self.newChatTableView.isHidden = true
//        }else{
//            self.newChatTableView.isHidden = false
//        }
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var updatedText = ""
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            updatedText = text.replacingCharacters(in: textRange,
                                                   with: string)
        }
        print(updatedText)
        if updatedText.isEmpty {
            showTableView(show: false)
        } else if updatedText.count >= 1 {
            setPopOver(textToSearch: updatedText)
            
        } else {}
        return true
    }
    
    // MARK: - TABLE VIEW HELPER FUNCTION
    
    func showTableView(show: Bool) {
        newChatTableView.isHidden = !show
//        self.tabView.isHidden = !show
//        self.underlineView.isHidden = !show
        newChatTableView.reloadData()
    }
    
    // MARK: - UIBUTTON ACTION'S
    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITABLE VIEW DATASOURCE & DELEGATE

extension NewChatVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NewChatCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! NewChatCell
        cell.userName.text = userList[indexPath.row].name
        cell.userDesc.text = userList[indexPath.row].username
        cell.userImg.setImage(fromUrl: userList[indexPath.row].userImage ?? "", defaultImage: DefaultImage.defaultProfileImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboardChat = UIStoryboard(name: "Chat", bundle: nil)
        let vc_ChatView = storyboardChat.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        vc_ChatView.imgUserStr = userList[indexPath.row].userImage ?? ""
        vc_ChatView.nameUserStr = userList[indexPath.row].name ?? ""
        vc_ChatView.descUserStr = userList[indexPath.row].username ?? ""
        vc_ChatView.idFrnd = "\(userList[indexPath.row].id ?? 0)"
        print(userList)
        vc_ChatView.chatId = "\(userList[indexPath.row].chatid ?? 0)"
        
        if userList[indexPath.row].is_user_blocked == 1 {
            vc_ChatView.isBlockedChat = true
        }
        
        navigationController?.pushViewController(vc_ChatView, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
}
