//
//  ChatListVC.swift
//  Pawxo
//
//  Created by 42works on 20/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage

class ChatListVC: UIViewController {
    
   // MARK:- PROPERTIES
    
    @IBOutlet weak var searchTF: RegularTextField!
    @IBOutlet weak var chatListTableView: UITableView!
    @IBOutlet weak var vewPopup: UIView!
    
    var cellIdentifier = "ChatListCell"
    var staticCount = 7
    var ResultString = String()
    var chatArr = [NSDictionary]()

   var timer : Timer? = nil
    var dictList = NSDictionary()
    var refresh = UIRefreshControl()
    var Handle:DatabaseHandle?
    
    var ref : DatabaseReference?

    
    // MARK:- LIFECYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        refresh.tintColor = UIColor.clear
        refresh.backgroundColor = UIColor.clear
        refresh.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refresh.addTarget(self, action:#selector(refreshTable), for: UIControl.Event.valueChanged)
        // Add Custom Loader
        var loader = UIImageView()
        let gif = UIImage.gifImageWithName("gif")
        loader.center = self.view.center
        loader = UIImageView(frame: CGRect(x: self.view.frame.size.width/2 - 20, y: refresh.center.y, width: 40, height: 40))
        loader.image = gif
        refresh.insertSubview(loader, at: 0)
        
        
        
        self.chatListTableView.addSubview(refresh)
        ref = Database.database().reference()
        chatListTableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "notificationName"), object: nil)
    }
    
    
    @objc private func refreshTable() {
        UserFrndDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
      setUpUI()
    }
    
     // MARK:- SETUP UI HELPER FUNCTION
    
    func setUpUI(){
       
       
        vewPopup.isHidden = false
        UserFrndDetails()
       }
    
    @objc func showSpinningWheel(_ notification: NSNotification) {
             UserFrndDetails()
       }
    
    // MARK:- FIREBASE FUNCTION'S
    
    @objc func UserFrndDetails(){
            
        LoaderView.shared.showLoader()
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
            self.refresh.endRefreshing()
            LoaderView.shared.hideLoader()
        }
             self.chatArr.removeAll()
            self.chatListTableView.reloadData()
            let userLogin = CommonFunctions.getuserFromDefaults()
            
                ref?.child("users").child(userLogin.userId ?? "")
                    .observe(.childAdded, with: { (snapshot) in
                            if let dict = snapshot.value as? NSDictionary {
                                  print(dict)
                                self.refresh.endRefreshing()
                            let timestemp = dict["timestamp"] as! Int
                            print(timestemp)
                            
                            
                                var add = true
                                
                                if let isBlocked = dict["is_blocked"] as? Bool,let blockedby = dict["blocked_by"] as? String,(isBlocked == true && blockedby != (userLogin.userId ?? "")) {
                                    //
                                    add = false
                                    print("isblocked and should be hiden")
                                }
                                
                                if add {
                                
                                if let index = self.chatArr.lastIndex(where: {$0["chat_id"] as! String == dict["chat_id"] as! String}){
                                    self.chatArr[index] = dict
                                }else{
                                    self.chatArr.append(dict)
                                }
                                }
                                
                                
                                
                                let array = self.chatArr.sorted{
                                    guard let s1 = $0["timestamp"] as? Int64,let s2 = $1["timestamp"] as? Int64 else{
                                        return false
                                    }

                                    return s1 > s2
                                }
                                
                                
                                self.chatArr = array
                            }
                            
                        if self.chatArr.count > 0 {
                            self.vewPopup.isHidden = true
                        }
                            
                            DispatchQueue.main.async {
                            self.chatListTableView.reloadData()
                             LoaderView.shared.hideLoader()
                            }
                           
                     })
        
        ref?.child("users").child(userLogin.userId ?? "")
            .observe(.childChanged, with: { (snapshot) in
                if let dict = snapshot.value as? NSDictionary {
                    print(dict)
                    self.refresh.endRefreshing()
                    let timestemp = dict["timestamp"] as! Int
                    print(timestemp)
                    self.vewPopup.isHidden = true
                    
                    
                    
                    
                    
                    if let isBlocked = dict["is_blocked"] as? Bool, isBlocked == false{
                        if let index = self.chatArr.lastIndex(where: {$0["chat_id"] as! String == dict["chat_id"] as! String}){
                            self.chatArr[index] = dict
                        }else{
                            self.chatArr.append(dict)
                        }
                    }else{
                        if dict["blocked_by"] as? String != (userLogin.userId ?? ""){
                            print("isblocked and should be hiden")
                            
                            if let index = self.chatArr.lastIndex(where: {$0["chat_id"] as! String == dict["chat_id"] as! String}){
                                self.chatArr.remove(at: index)
                            }
                            
                            
                            
                        }
                    }
                    
                    let array = self.chatArr.sorted{
                        guard let s1 = $0["timestamp"] as? Int64,let s2 = $1["timestamp"] as? Int64 else{
                            return false
                        }

                        return s1 > s2
                    }
                    
                    
                    self.chatArr = array
                    
                    
                    if self.chatArr.count > 0 {
                        self.vewPopup.isHidden = true
                    }else{
                        self.vewPopup.isHidden = false
                    }
                    //self.chatArr.append(dict)
                    
                }
                
                
                DispatchQueue.main.async {
                    self.chatListTableView.reloadData()
                    LoaderView.shared.hideLoader()
                }
                
            })
       
  
    }
    
    
    
     // MARK:- UIBUTTON ACTIONS

    @IBAction func newChatPressed(_ sender: Any) {
        let storyboardChat = UIStoryboard(name: "Chat", bundle: nil)
         let vc_newChat = storyboardChat.instantiateViewController(withIdentifier: "NewChatVC") as! NewChatVC
      
        self.navigationController?.pushViewController(vc_newChat, animated: true)
    }
    

}

  // MARK:- UITABLE VIEW DTASOURCE & DELEGATE

extension ChatListVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ChatListCell =  tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ChatListCell
       
        var time = Int()
        time = (((chatArr[indexPath.row] as! NSDictionary)["timestamp"] as? Int) ?? 0)
        let myTimeInterval = TimeInterval(time)
        let times = NSDate(timeIntervalSince1970: TimeInterval(myTimeInterval))
        let currentDateStr = timeAgoSinceDate(times as Date, numericDates: true)
        cell.timeLbl.text = currentDateStr
         cell.userMsg.text = ((chatArr[indexPath.row] as! NSDictionary)["last_message"] as? String)
          cell.userName.text = ((chatArr[indexPath.row] as! NSDictionary)["user_name"] as? String)
        let url = NSURL(string: (chatArr[indexPath.row] as! NSDictionary)["user_image"] as? String ?? "")
        cell.userImg?.sd_setImage(with: url as URL?, placeholderImage: UIImage(named: "TempProfile"), options: .transformAnimatedImage, progress: nil, completed: nil)
        print((chatArr[indexPath.row] as! NSDictionary)["allread"] as? Bool as Any)
       
        if ((chatArr[indexPath.row] as! NSDictionary)["allread"] as? Bool) == true{
            cell.inlineOffline.layer.cornerRadius = cell.inlineOffline.frame.width/2
            cell.inlineOffline.layer.masksToBounds = true
            cell.inlineOffline.isHidden = true
        }else{
            cell.inlineOffline.layer.cornerRadius = cell.inlineOffline.frame.width/2
            cell.inlineOffline.layer.masksToBounds = true
            cell.inlineOffline.isHidden = false
        }
      
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        timer = nil
        
        
        
        
        
        
        
        
        
        
        let storyboardChat = UIStoryboard(name: "Chat", bundle: nil)
         let vc_ChatView = storyboardChat.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        vc_ChatView.imgUserStr = (chatArr[indexPath.row] as! NSDictionary)["user_image"] as? String ?? ""
        vc_ChatView.nameUserStr = (chatArr[indexPath.row] as! NSDictionary)["user_name"] as? String ?? ""
        vc_ChatView.descUserStr = ((chatArr[indexPath.row] as! NSDictionary)["last_message"] as? String)!
        vc_ChatView.idFrnd = ((chatArr[indexPath.row] as! NSDictionary)["user_id"] as? String ?? "")
        vc_ChatView.timenew = (((chatArr[indexPath.row] as! NSDictionary)["timestamp"] as? Int) ?? 0)
        
        
        let userLogin = CommonFunctions.getuserFromDefaults()
        if userLogin.userId ?? "" > vc_ChatView.idFrnd {
        
                print("first_value is greater than second_value")
            self.ResultString = "\("chat")_\(vc_ChatView.idFrnd)_\(userLogin.userId ?? "")"
            print(self.ResultString)
            }
            else {
                print("first_value is not greater than second_value ")
            self.ResultString = "\("chat")_\(userLogin.userId ?? "")_\(vc_ChatView.idFrnd)"
            print(self.ResultString)
            }
        
        
        let ref : DatabaseReference!
        ref = Database.database().reference()
        ref.child("users").child(userLogin.userId ?? "").child(self.ResultString).child("allread").setValue(true)
        
        let dict = chatArr[indexPath.row] as? NSDictionary
        
        if dict!["is_blocked"] as? Bool == true{
            vc_ChatView.isBlockedChat = true
        }
        
        
        
        self.navigationController?.pushViewController(vc_ChatView, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
   func getCurrentMillis()->Int64 {
       return Int64(Date().timeIntervalSince1970)
   }
    
}

func timeAgoSinceDate(_ date:Date, numericDates:Bool = false) -> String {
    let calendar = NSCalendar.current
    let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
    let now = Date()
    let earliest = now < date ? now : date
    let latest = (earliest == now) ? date : now
    let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)

    if (components.year! >= 2) {
        return "\(components.year!) years ago"
    } else if (components.year! >= 1){
        if (numericDates){
            return "1 year ago"
        } else {
            return "Last year"
        }
    } else if (components.month! >= 2) {
        return "\(components.month!) months ago"
    } else if (components.month! >= 1){
        if (numericDates){
            return "1 month ago"
        } else {
            return "Last month"
        }
    } else if (components.weekOfYear! >= 2) {
        return "\(components.weekOfYear!) weeks ago"
    } else if (components.weekOfYear! >= 1){
        if (numericDates){
            return "1 week ago"
        } else {
            return "Last week"
        }
    } else if (components.day! >= 2) {
        return "\(components.day!) days ago"
    } else if (components.day! >= 1){
        if (numericDates){
            return "1 day ago"
        } else {
            return "Yesterday"
        }
    } else if (components.hour! >= 2) {
        return "\(components.hour!) hours ago"
    } else if (components.hour! >= 1){
        if (numericDates){
            return "1 hour ago"
        } else {
            return "1 hour ago"
        }
    } else if (components.minute! >= 2) {
        return "\(components.minute!) min ago"
    } else if (components.minute! >= 1){
        if (numericDates){
            return "1 min ago"
        } else {
            return "1 min ago"
        }
    } else if (components.second! >= 3) {
        return "\(components.second!) sec ago"
    } else {
        return "Just now"
    }

}
