//
//  RightTextCell.swift
//  Pawxo
//
//  Created by 42works on 20/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class RightTextCell: UITableViewCell {
    
    
    @IBOutlet weak var bubbleHeight: NSLayoutConstraint!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var bubbleRight: UIImageView!
    
    @IBOutlet weak var bubblewidth: NSLayoutConstraint!
    @IBOutlet weak var messageTextLabel: UILabel!
    
    @IBOutlet weak var textHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var textWidthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    func setUpCell(chatObj:TempChat){
//        self.timeLabel.text = chatObj.time
//        self.messageTextLabel.text = chatObj.text
//        self.textWidthConstraint.constant = chatObj.bubbleSize.width
//        self.textHeightConstraint.constant = chatObj.bubbleSize.height
//        
//    }
    
}
