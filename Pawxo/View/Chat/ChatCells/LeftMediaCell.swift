//
//  RightMediaCell.swift
//  Pawxo
//
//  Created by 42works on 20/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class LeftMediaCell: UITableViewCell {
    
    
    @IBOutlet weak var rightImageView: UIImageView!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var vew: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
//    func setUpCell(chatObj:TempChat){
//        self.timeLabel.text = chatObj.time
//        self.rightImageView.image = chatObj.img
//        
//    }
}
