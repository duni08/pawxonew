//
//  Chat__Vew_Model.swift
//  Pawxo
//
//  Created by 42Works on 26/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation
import UIKit



class notificationChat_viewModel: Codable {
       
    var data =  [notificationChat]()
       var message : String  = ""
       var success : Bool?
       
       enum CodingKeys: String, CodingKey {
           case data = "data"
           case message = "message"
           case success = "success"
       }
       
       
    required init(from decoder: Decoder) throws {
           let values = try decoder.container(keyedBy: CodingKeys.self)
           data = try values.decodeIfPresent([notificationChat].self, forKey: .data) ?? []
           message = try values.decodeIfPresent(String.self, forKey: .message) ?? ""
           success = try values.decodeIfPresent(Bool.self, forKey: .success)
       }

}

struct notificationChat : Codable {
}













