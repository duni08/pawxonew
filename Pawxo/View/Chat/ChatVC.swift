//
//  ChatVC.swift
//  Pawxo
//
//  Created by 42works on 20/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Alamofire
import Firebase
import IQKeyboardManager
import UIKit

enum chatCellPosition {
    case Left
    case Right
}

enum chatCellType {
    case Text
    case Image
    case Video
}

class TempChat {
    var text: String = "" {
        didSet {
            bubbleSize = self.text.rectForText(withConstrainedWidth: 300, font: UIFont.JosefinSansRegular(size: 15.0))
        }
    }
    
    var padding: CGFloat = 5
    var time: String = ""
    var position: chatCellPosition = .Left
    var type: chatCellType = .Text
    var bubbleSize: CGSize = .zero
    var img: UIImage = UIImage()
}

class ChatVC: UIViewController {
    // MARK: - PROPERTIES
    
    @IBOutlet var boxView: UIView!
    @IBOutlet var placeHolderLabel: LightLabel!
    @IBOutlet var chatTextView: UITextView!
    @IBOutlet var chatTableView: UITableView!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var user_Img: UIImageView!
    @IBOutlet var userName: RegularLabel!
    @IBOutlet var userdes: LightLabel!
    
    var cellIdentifierLeftText = "LeftTextCell"
    var cellIdentifierRightText = "RightTextCell"
    var cellIdentifierRightMedia = "RightMediaCell"
    var cellIdentifierLeftMedia = "LeftMediaCell"
    
    var staticCount = 27
    // var chatArray = [TempChat]()
    var tempBool = false
    
    var imgUserStr = String()
    var nameUserStr = String()
    var descUserStr = String()
    var idFrnd = String()
    var idChatUser = ""
    var ResultString = String()
    var theId = String()
    var chatId = String()
    var frendChatArr = [NSDictionary]()
    var imgarr: NSMutableArray = []
    let objChat = TempChat()
    var text: String = ""
    var timer: Timer!
    var imgUserStrr = String()
    var timenew = Int()
    
    var isBlockedChat = false
    
    var isYouBlocked = false
    
    var isFromNew = false
    
    // MARK: - LIFECYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(idFrnd)
        
//        if notifyDta == false{
//            print(notifyDta)
//          UIApplication.shared.unregisterForRemoteNotifications()
//        }
        isChat = true
        userName.text = nameUserStr
        user_Img.setImage(fromUrl: imgUserStr, defaultImage: DefaultImage.defaultProfileImage, isCircled: true, borderColor: nil, borderWidth: nil, isCache: false, showIndicator: false)
        
        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        // Do any additional setup after loading the view.
        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
        if !chatTextView.text.isEmpty {
            placeHolderLabel.alpha = 0
        } else {
            placeHolderLabel.alpha = 1
        }
        
        chatTableView.register(UINib(nibName: cellIdentifierLeftText, bundle: nil), forCellReuseIdentifier: cellIdentifierLeftText)
        
        chatTableView.register(UINib(nibName: cellIdentifierRightText, bundle: nil), forCellReuseIdentifier: cellIdentifierRightText)
        
        chatTableView.register(UINib(nibName: cellIdentifierRightMedia, bundle: nil), forCellReuseIdentifier: cellIdentifierRightMedia)
        
        chatTableView.register(UINib(nibName: cellIdentifierLeftMedia, bundle: nil), forCellReuseIdentifier: cellIdentifierLeftMedia)
        chatTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        
        let userLogin = CommonFunctions.getuserFromDefaults()
        
        if userLogin.userId ?? "" > idFrnd {
            print("first_value is greater than second_value")
            ResultString = "\("chat")_\(idFrnd)_\(userLogin.userId ?? "")"
            print(ResultString)
        } else {
            print("first_value is not greater than second_value ")
            ResultString = "\("chat")_\(userLogin.userId ?? "")_\(idFrnd)"
            print(ResultString)
        }
        
        LoaderView.shared.showLoader()
        let ref: DatabaseReference!
        ref = Database.database().reference()
        ref.child("chats").child(ResultString).observeSingleEvent(of: .value) { snapshot in
            LoaderView.shared.hideLoader()
            if let dict = snapshot.value as? NSDictionary {
                print("Chat at viewdidLoad")
                print(dict)
                self.frendChatArr.removeAll()
                for (keys, value) in dict {
                    self.frendChatArr.append(value as! NSDictionary)
                }
                
                let array = self.frendChatArr.sorted {
                    guard let s1 = $0["timestamp"] as? Int64, let s2 = $1["timestamp"] as? Int64 else {
                        return false
                    }
                    let times1 = NSDate(timeIntervalSince1970: TimeInterval(s1)) as Date
                    let times2 = NSDate(timeIntervalSince1970: TimeInterval(s2)) as Date
                    return times1 < times2
                }
                
                self.frendChatArr = array
                self.chatTableView.reloadData()
                let indexPath = IndexPath(
                    row: self.chatTableView.numberOfRows(inSection: self.chatTableView.numberOfSections - 1) - 1,
                    section: self.chatTableView.numberOfSections - 1)
                self.chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
            self.fetchChatUserData()
        }
    }
    
    func setUPObserver() {
        if isBlockedChat {}
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // fetchChatUserData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        isChat = false
        
        let userLogin = CommonFunctions.getuserFromDefaults()
        if userLogin.userId ?? "" > idFrnd {
            print("first_value is greater than second_value")
            ResultString = "\("chat")_\(idFrnd)_\(userLogin.userId ?? "")"
            print(ResultString)
        } else {
            print("first_value is not greater than second_value ")
            ResultString = "\("chat")_\(userLogin.userId ?? "")_\(idFrnd)"
            print(ResultString)
        }
        
        if !frendChatArr.isEmpty {
            let ref: DatabaseReference!
            ref = Database.database().reference()
            ref.child("users").child(userLogin.userId ?? "").child(ResultString).child("allread").setValue(true)
        }
        
        Database.database().reference().child("chats").child(ResultString).removeAllObservers()
    }
    
    // MARK: - FIREBASE HELPER FUNCTION
    
    func fetchChatUserData() {
        let userLogin = CommonFunctions.getuserFromDefaults()
        
        if userLogin.userId ?? "" > idFrnd {
            print("first_value is greater than second_value")
            ResultString = "\("chat")_\(idFrnd)_\(userLogin.userId ?? "")"
            print(ResultString)
        } else {
            print("first_value is not greater than second_value ")
            ResultString = "\("chat")_\(userLogin.userId ?? "")_\(idFrnd)"
            print(ResultString)
        }
        
        Database.database().reference().child("chats").child(ResultString)
            .observe(.childAdded, with: { snapshot in
                if let dict = snapshot.value as? NSDictionary {
                    print(dict)
                    
                    if let index = self.frendChatArr.lastIndex(where: { $0["timestamp"] as! NSNumber == dict["timestamp"] as! NSNumber }) {
                        
                    } else {
                        self.frendChatArr.append(dict)
                        if self.frendChatArr.isEmpty {
                            self.chatTextView.text = ""
                        } else {
                            let array = self.frendChatArr.sorted {
                                guard let s1 = $0["timestamp"] as? Int64, let s2 = $1["timestamp"] as? Int64 else {
                                    return false
                                }
                                let times1 = NSDate(timeIntervalSince1970: TimeInterval(s1)) as Date
                                let times2 = NSDate(timeIntervalSince1970: TimeInterval(s2)) as Date
                                return times1 < times2
                            }
                            
                            self.frendChatArr = array
                            let indexpath = IndexPath(row: self.frendChatArr.count - 1, section: 0)
                            self.chatTableView.insertRows(at: [indexpath], with: .none)
                            self.chatTableView.scrollToBottom()
                            self.chatTextView.text = ""
                        }
                        
                    }
                    
                }
        })
        
        
        
        
        Database.database().reference().child("users").child(userLogin.userId ?? "").child(ResultString).child("is_blocked")
            .observe(.value, with: { snapshot in
                if let isBlocked = snapshot.value as? Bool, isBlocked == true {
                    print(isBlocked)
                    
                    self.isYouBlocked = true
                    Database.database().reference().child("chats").child(self.ResultString).removeAllObservers()
                    
//                if let isBlocked = dict["is_blocked"] as? Bool, isBlocked == true{
//                    self.isYouBlocked = true
//
//                    Database.database().reference().child("chats").child(self.ResultString).removeAllObservers()
//                }
                }
        })
        
//        if self.frendChatArr.count == 0{
//            self.chatTextView.text = ""
//              }else{
//         let indexpath = IndexPath(row: self.frendChatArr.count - 1, section: 0)
//               self.chatTableView.insertRows(at: [indexpath], with: .none)
//               self.chatTableView.scrollToBottom()
//               self.chatTextView.text = ""
//        }
    }
    
    // MARK: - UIBUTTON ACTIONS
    
    @IBAction func backButtonPressed(_ sender: Any) {
        UIApplication.shared.registerForRemoteNotifications()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        var message = AppMessages.unblockUserText
        
        if isYouBlocked {
            message = AppMessages.isYouBlockedText
            showAlert(self, message: message)
            return
        }
        
        if isBlockedChat == true {
            showAlert(self, message: message)
            return
        }
        
        if chatTextView.text == "" {
            return
        }
        
        let currentTime = getCurrentMillis()
        print(currentTime)
        let userLogin = CommonFunctions.getuserFromDefaults()
        if userLogin.userId ?? "" > idFrnd {
            ResultString = "\("chat")_\(idFrnd)_\(userLogin.userId ?? "")"
            print(ResultString)
        } else {
            ResultString = "\("chat")_\(userLogin.userId ?? "")_\(idFrnd)"
            print(ResultString)
        }
        let ref = Database.database().reference().child("chats").child(ResultString)
        let childRef = ref.childByAutoId()
        let values = ["attachment": "", "message": chatTextView.text as Any, "sender_id": userLogin.userId ?? "", "timestamp": currentTime] as [String: Any]
        print(values)
        childRef.updateChildValues(values)
        print(imgUserStr)
        if imgUserStr == "" {
            imgUserStr = "https://cuddl.app/admin/public/uploads/user_image/\(idFrnd)\("_user_image.jpg")"
            print(imgUserStr)
        } else {}
        
        Database.database().reference().child("users").child(userLogin.userId ?? "").child(ResultString).updateChildValues(["allread": true, "chat_id": ResultString, "last_message": chatTextView.text as Any, "user_id": idFrnd, "timestamp": currentTime, "user_name": nameUserStr, "user_image": imgUserStr, "is_blocked": false, "blocked_by": "NA"] as [String: Any])
        
        // user
        if userLogin.userImage ?? "" == "" {
            imgUserStrr = "https://cuddl.app/admin/public/uploads/user_image/ \(userLogin.userId ?? "")\("_user_image.jpg")"
            print(imgUserStrr)
        } else {
            imgUserStrr = userLogin.userImage ?? ""
        }
        Database.database().reference().child("users").child(idFrnd).child(ResultString).updateChildValues(["allread": false, "chat_id": ResultString, "last_message": chatTextView.text as Any, "user_id": userLogin.userId ?? "", "timestamp": currentTime, "user_name": userLogin.userName ?? "", "user_image": imgUserStrr, "is_blocked": false, "blocked_by": "NA"] as [String: Any])
        
        if frendChatArr.isEmpty {
            chatTableView.reloadData()
        } else {
            let indexPath = IndexPath(row: frendChatArr.count - 1, section: 0)
            chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
        placeHolderLabel.alpha = 1
        // api call
        notificationChatWS()
    }
    
    @IBAction func attachMedia(_ sender: Any) {
        view.endEditing(true)
        
        var message = AppMessages.unblockUserText
        
        if isYouBlocked {
            message = AppMessages.isYouBlockedText
            showAlert(self, message: message)
            return
        }
        
        if isBlockedChat == true {
            showAlert(self, message: message)
            return
        }
        
        photoManager = PhotoManager(navigationController: navigationController!, allowEditing: true, videoType: false, callback: { pickedImage, urlVideo in
            print(urlVideo)
            // self.questionaireModel.profileImage = pickedImage ?? #imageLiteral(resourceName: "TempProfile")
            if let image = pickedImage {
                let chatTemp = TempChat()
                chatTemp.text = "Sent an image."
                chatTemp.time = "10:20 Pm"
                chatTemp.type = .Image
                chatTemp.position = .Right
                chatTemp.img = image
                self.imgarr.add(image)
                print(image)
                self.UploadImgesWebservices()
            }
            
         })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func getChatBubbleFrame(text: String, padding: CGFloat, maxWidth: CGFloat) -> CGSize {
        let rect = text.rectForText(withConstrainedWidth: maxWidth, font: UIFont.JosefinSansRegular(size: 15.0))
        return rect
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration: TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve: UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            var scroll = false
            if endFrameY >= UIScreen.main.bounds.size.height {
                bottomConstraint?.constant = 0.0
            } else {
                bottomConstraint?.constant = ((endFrame?.size.height)! - view.safeAreaInsets.bottom)
                scroll = true
            }
            
            UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations: {
                self.view.layoutIfNeeded()
            }) { _ in
                if scroll {
                    if self.frendChatArr.isEmpty {
                        self.chatTableView.reloadData()
                    } else {
                        self.chatTableView.scrollToBottom()
                    }
                }
            }
        }
    }
    
    func getCurrentMillis() -> Int64 {
        return Int64(Date().timeIntervalSince1970)
    }
    
    // MARK: - WEB SERVICES
    
    func UploadImgesWebservices() {
        let defaultHeaders = ["X-custom-header": "7890abcdefghijkl"]
        weak var weakSelf = self
        print(imgarr)
        Alamofire.upload(multipartFormData: { multipartFormData in
            for i in 0..<self.imgarr.count {
                multipartFormData.append(((self.imgarr[i] as? UIImage)?.jpegData(compressionQuality: 0.1)!)!, withName: "photos[]", fileName: "file.jpg", mimeType: "image/jpeg")
            }
         }, usingThreshold: UInt64(), to: WebServiceConstants.uploadimageFirebase, method: .post, headers: defaultHeaders) { result in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { progress in
                    let value = Float(progress.fractionCompleted) * 100
                    print(progress.fractionCompleted)
                    print(value)
        })
                upload.responseJSON { response in
                    print("Succesfully uploaded = \(response)")
                    if let dic = response.result.value as? [String: Any] {
                        print(dic)
                        let dataImg = dic["data"] as? NSArray
                        print(dataImg ?? [])
                        var strimg = ""
                        for i in 0..<dataImg!.count {
                            let url = (dataImg![i] as AnyObject)
                            print(url)
                            strimg = url as! String
                            print(strimg)
                        }
                        
                        let currentTime = self.getCurrentMillis()
                        print(currentTime)
                        let userLogin = CommonFunctions.getuserFromDefaults()
                        if userLogin.userId ?? "" > self.idFrnd {
                            print("first_value is greater than second_value")
                            self.ResultString = "\("chat")_\(self.idFrnd)_\(userLogin.userId ?? "")"
                            print(self.ResultString)
                        } else {
                            print("first_value is not greater than second_value ")
                            self.ResultString = "\("chat")_\(userLogin.userId ?? "")_\(self.idFrnd)"
                            print(self.ResultString)
                        }
                        let ref = Database.database().reference().child("chats").child(self.ResultString)
                        let childRef = ref.childByAutoId()
                        let values = ["attachment": strimg, "message": "", "sender_id": userLogin.userId ?? "", "timestamp": currentTime] as [String: Any]
                        print(values)
                        childRef.updateChildValues(values)
                        if self.imgUserStr == "" {
                            self.imgUserStr = "https://cuddl.app/admin/public/uploads/user_image/\(self.idFrnd)\("_user_image.jpg")"
                            print(self.imgUserStr)
                        } else {}
                        
                        Database.database().reference().child("users").child(userLogin.userId ?? "").child(self.ResultString).updateChildValues(["allread": true, "chat_id": self.ResultString, "last_message": "Sent an image.", "user_id": self.idFrnd, "timestamp": currentTime, "user_name": self.nameUserStr, "user_image": self.imgUserStr] as [String: Any])
                        
                        // user
                        if userLogin.userImage ?? "" == "" {
                            self.imgUserStrr = "https://cuddl.app/admin/public/uploads/user_image/ \(userLogin.userId ?? "")\("_user_image.jpg")"
                            print(self.imgUserStrr)
                        } else {
                            self.imgUserStrr = userLogin.userImage ?? ""
                        }
                        Database.database().reference().child("users").child(self.idFrnd).child(self.ResultString).updateChildValues(["allread": false, "chat_id": self.ResultString, "last_message": "Sent an image", "user_id": userLogin.userId ?? "", "timestamp": currentTime, "user_name": userLogin.userName ?? "", "user_image": self.imgUserStrr] as [String: Any])
                        
                        if self.frendChatArr.isEmpty {
                            self.chatTableView.reloadData()
                        } else {
                            let indexPath = IndexPath(row: self.frendChatArr.count - 1, section: 0)
                            self.chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                            self.chatTextView.text = ""
                        }
                    }
                }
            case .failure(let encodingError):
                print("Error in upload: \(encodingError.localizedDescription)")
            }
        }
    }
    
    func notificationChatWS() {
        let userLogin = CommonFunctions.getuserFromDefaults()
        let str = "\("chat")_\(userLogin.userId ?? "")_\(idFrnd)"
        print(ResultString)
        var param = [String: Any]()
        param = ["chat_id": ResultString, "message": chatTextView.text ?? "", "sender_id": userLogin.userId ?? "", "receiver_id": idFrnd]
        print(param)
        
        WebServices.shared.apiDataPostMethod(url: WebServiceConstants.notificationchat, parameters: param) { response, error in
            switch error {
            case nil:
                do {
                    let data = try JSONDecoder().decode(notificationChat_viewModel.self, from: response!)
                    print(data)
                    switch data.success {
                    case true: break
                        
                    default:
                        print(data.message)
                        // showMessage("Info:", signInData.message)
                    }
                } catch {
                    print(error)
                    // self.showMessage("Info:", error.localizedDescription)
                }
            default: break
                self.showNoConnection(error?.localizedDescription ?? "", completion: { refresh in
                    if refresh {
                        self.notificationChatWS()
                    }
                       })
            }
            //                Indicator.shared().stop()
        }
    }
}

// MARK: - UITextViewDelegate

extension ChatVC: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if !newText.isEmpty {
            placeHolderLabel.alpha = 0
            
        } else {
            placeHolderLabel.alpha = 1
        }
        return true
    }
}

// MARK: - UITABLE VIEW DATASOURCE & DELEGATE

extension ChatVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return frendChatArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userLogin = CommonFunctions.getuserFromDefaults()
        theId = (frendChatArr[indexPath.row] as! NSDictionary)["sender_id"] as! String
       // print(theId)
       // print(ResultString)
        if userLogin.userId ?? "" == theId {
            let cell: RightTextCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifierRightText, for: indexPath) as! RightTextCell
            var time = Int()
            time = (((frendChatArr[indexPath.row] as! NSDictionary)["timestamp"] as? Int) ?? 0)
            let myTimeInterval = TimeInterval(time)
            let times = NSDate(timeIntervalSince1970: TimeInterval(myTimeInterval))
            let currentDateStr = timeAgoSinceDate(times as Date)
            cell.timeLabel.text = currentDateStr
            cell.messageTextLabel.text = ((frendChatArr[indexPath.row] as! NSDictionary)["message"] as? String)
          //  print(objChat.bubbleSize.width)
            let MsgString = ((frendChatArr[indexPath.row] as! NSDictionary)["message"] as? String)
            objChat.bubbleSize = (MsgString?.rectForText(withConstrainedWidth: 300, font: UIFont.JosefinSansSemiBold(size: 15.0)))!
            //print(objChat.bubbleSize)
            // cell.textWidthConstraint.constant = objChat.bubbleSize.width + 60
            // cell.textHeightConstraint.constant = objChat.bubbleSize.height + 5
            
            cell.messageTextLabel.text = ((frendChatArr[indexPath.row] as! NSDictionary)["message"] as? String)
            
            if ((frendChatArr[indexPath.row] as! NSDictionary)["attachment"] as? String) != "" {
                let cell: RightMediaCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifierRightMedia, for: indexPath) as! RightMediaCell
                cell.rightImageView.setImage(fromUrl: (frendChatArr[indexPath.row] as! NSDictionary)["attachment"] as? String ?? "", defaultImage: DefaultImage.defaultChatImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
                var time = Int()
                time = (((frendChatArr[indexPath.row] as! NSDictionary)["timestamp"] as? Int) ?? 0)
                let myTimeInterval = TimeInterval(time)
                let times = NSDate(timeIntervalSince1970: TimeInterval(myTimeInterval))
                let currentDateStr = timeAgoSinceDate(times as Date)
                cell.timeLabel.text = currentDateStr
                cell.rightImageView.roundCorners([.topRight, .topLeft], radius: 10)
                cell.rightImageView.layer.masksToBounds = true
                return cell
            }
            return cell
        } else {
            let cell: LeftTextCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifierLeftText, for: indexPath) as! LeftTextCell
          //  print(frendChatArr)
            var time = Int()
            time = (((frendChatArr[indexPath.row] as! NSDictionary)["timestamp"] as? Int) ?? 0)
            let myTimeInterval = TimeInterval(time)
            let times = NSDate(timeIntervalSince1970: TimeInterval(myTimeInterval))
            let currentDateStr = timeAgoSinceDate(times as Date)
            // print(currentDateStr)
            cell.timeLabel.text = currentDateStr
            
            let MsgString = ((frendChatArr[indexPath.row] as! NSDictionary)["message"] as? String)
            objChat.bubbleSize = (MsgString?.rectForText(withConstrainedWidth: 300, font: UIFont.JosefinSansSemiBold(size: 15.0)))!
            // cell.textWidthConstraint.constant = objChat.bubbleSize.width + 60
            // cell.textHeightConstraint.constant = objChat.bubbleSize.height + 5
            cell.messageTextLabel.text = ((frendChatArr[indexPath.row] as! NSDictionary)["message"] as? String)
            if ((frendChatArr[indexPath.row] as! NSDictionary)["attachment"] as? String) != "" {
                let cell: LeftMediaCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifierLeftMedia, for: indexPath) as! LeftMediaCell
                cell.rightImageView.setImage(fromUrl: (frendChatArr[indexPath.row] as! NSDictionary)["attachment"] as? String ?? "", defaultImage: DefaultImage.defaultChatImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
                cell.rightImageView.roundCorners([.topRight, .topLeft], radius: 10)
                cell.rightImageView.layer.masksToBounds = true
                var time = Int()
                time = (((frendChatArr[indexPath.row] as! NSDictionary)["timestamp"] as? Int) ?? 0)
                let myTimeInterval = TimeInterval(time)
                let times = NSDate(timeIntervalSince1970: TimeInterval(myTimeInterval))
                let currentDateStr = timeAgoSinceDate(times as Date)
                cell.timeLabel.text = currentDateStr
                return cell
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
