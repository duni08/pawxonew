//
//  PawxoTabBar.swift
//  testProject
//
//  Created by 42works on 07/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation
import UIKit

class PawxoTabBar: UIViewController {
    
    var stackView: UIStackView!
    var emptyView: UIView!
    var containerView : UIView!
    
    var selectedTabColor : UIColor = UIColor.orange
    var unselectedTabColor : UIColor = UIColor.darkGray
    var tabSize : CGFloat = 32
    var tabTopInset : CGFloat = -3
    var tabBackGroundColor : UIColor = UIColor.init(red: 247, green: 246, blue: 246, alpha: 1.0)
    var dividerColor : UIColor = AppColor.colorGreyTime
    
    var tabBarImages = [UIImage]() 
//    var tabBarImages : [UIImage]? = [
//        UIImage(named: "ic_music")!,
//        UIImage(named: "ic_play")!,
//        UIImage(named: "ic_star")!,
//        UIImage(named: "ic_star")!,
//        UIImage(named: "ic_star")!,
//        UIImage(named: "ic_star")!
//    ]
    
    
    func setButtonConstraints(button:UIButton, toView:UIView){
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: self.tabSize).isActive = true
        button.heightAnchor.constraint(equalToConstant: self.tabSize).isActive = true
        button.centerXAnchor.constraint(equalTo: toView.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: toView.centerYAnchor, constant: tabTopInset).isActive = true
    }
    
    var controllers = [UIViewController](){
        didSet {
            for i in 0...controllers.count - 1 {
                if i == 6{
                    break
                }
                let view = UIView()
                let button = UIButton()
                button.setTitleColor(.black, for: .normal)
                button.setImage(tabBarImages[i], for: .normal)
                view.addSubview(button)
                stackView.addArrangedSubview(view)
                button.addTarget(self, action: #selector(buttonAction), for:.touchUpInside)
                button.tag = i
               // button.backgroundColor = .gray
                self.setButtonConstraints(button: button, toView:view)
            }
            
            self.setChildViewController(childVC: self.controllers.first!)
            
            self.setSelectedForButton(button: (stackView.arrangedSubviews.first?.subviews.first as! UIButton), first: true)
            for button in stackView.arrangedSubviews {
                let btn =  button.subviews.first as! UIButton
                if btn.tag != 0 {
                    self.setUnSelectedForButton(button: btn)
                }
            }
            
        }
    }
    
    func setChildViewController(childVC:UIViewController){
        self.configureChildViewController(childController: childVC, onView: self.view)
        self.view.bringSubviewToFront(self.containerView)
    }
    
    
    func setSelectedTAB(index:Int){
        for button in stackView.arrangedSubviews {
            let btn =  button.subviews.first as! UIButton
            if btn.tag == index{
                self.buttonAction(sender: btn)
            }
        }
    }
    
    
    @objc func buttonAction(sender:UIButton){
        children.forEach({ $0.willMove(toParent: nil); $0.view.removeFromSuperview(); $0.removeFromParent() })
        self.setChildViewController(childVC: self.controllers[sender.tag])
        
        self.setSelectedForButton(button: sender, first: false)
        
        for button in stackView.arrangedSubviews {
            let btn =  button.subviews.first as! UIButton
            if btn.tag != sender.tag{
                self.setUnSelectedForButton(button: btn)
            }
        }
        
    }
    
    
    
    func setSelectedForButton(button:UIButton, first:Bool){
//        let image = button.currentImage?.withRenderingMode(.alwaysTemplate)
//        button.setImage(image, for: .normal)
//        button.tintColor = self.selectedTabColor
        button.setImage(BottomImages.selectedImages[button.tag], for: .normal)
        if !(first) {
            playBounceAnimation(button)
        }else{
            if button.tag == 2 {
            button.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }else{
                button.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            }
        }
    }
    
    func playBounceAnimation(_ button: UIButton) {
        
        UIView.animate(withDuration: 0.6,
                       animations: {
                        if button.tag == 2 {
                            button.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        }else{
                            button.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                        }
                        
        }, completion: nil)
        
    }
    
    
    
    func setUnSelectedForButton(button:UIButton){
        //let image = button.currentImage?.withRenderingMode(.alwaysTemplate)
        button.setImage(BottomImages.unSelectedImages[button.tag], for: .normal)
        //button.tintColor = AppColor.homeIconUnselected
        UIView.animate(withDuration: 0.6) {
            
            if button.tag == 2 {
            button.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
            }else{
                    button.transform = CGAffineTransform.identity
            }
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView = UIView()
        emptyView = UIView()
        emptyView.backgroundColor = .black
        stackView   = UIStackView()
        stackView.axis  = NSLayoutConstraint.Axis.horizontal
        stackView.distribution  = UIStackView.Distribution.fillEqually
        
        let lineView = UIView()
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        emptyView.translatesAutoresizingMaskIntoConstraints = false
        lineView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(stackView)
        
        lineView.backgroundColor = dividerColor
        lineView.alpha = 0.5
        containerView.addSubview(lineView)
        
        self.view.addSubview(containerView)
        self.view.addSubview(emptyView)
        
        //Constraints
        stackView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        containerView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        containerView.backgroundColor = tabBackGroundColor
        
        containerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        stackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        
        lineView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        lineView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        lineView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        lineView.topAnchor.constraint(equalTo: self.containerView.topAnchor).isActive = true

    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func addHeightConstraintToTabbar() -> Void {
        
        let heightConstant:CGFloat = self.view.safeAreaInsets.bottom + 49
        containerView.heightAnchor.constraint(equalToConstant: heightConstant).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        stackView.heightAnchor.constraint(equalToConstant: 49).isActive = true
        stackView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0).isActive = true
        emptyView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        emptyView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        emptyView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        
    }
    
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        addHeightConstraintToTabbar()
        //  layoutViewsforViewControllers()
        if controllers.count >  0  {
            emptyView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }else{
            emptyView.heightAnchor.constraint(equalTo: self.view.heightAnchor).isActive = true
        }
    }
    
    
    
    func configureChildViewController(childController: UIViewController, onView: UIView?) {
        var holderView = self.view
        if let onView = onView {
            holderView = onView
        }
        addChild(childController)
        holderView?.addSubview(childController.view)
        constrainViewEqual(holderView: holderView!, view: childController.view)
        childController.didMove(toParent: self)
        childController.willMove(toParent: self)
    }
    
    
    func constrainViewEqual(holderView: UIView, view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        //pin 100 points from the top of the super
        let pinTop = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal,
                                        toItem: holderView, attribute: .top, multiplier: 1.0, constant: 0)
        let pinBottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal,
                                           toItem: self.containerView, attribute: .top, multiplier: 1.0, constant: 0)
        let pinLeft = NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal,
                                         toItem: holderView, attribute: .left, multiplier: 1.0, constant: 0)
        let pinRight = NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal,
                                          toItem: holderView, attribute: .right, multiplier: 1.0, constant: 0)
        
        holderView.addConstraints([pinTop, pinBottom, pinLeft, pinRight])
    }
    
}
