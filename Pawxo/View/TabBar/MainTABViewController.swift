//
//  MainTABViewController.swift
//  Pawxo
//
//  Created by 42works on 07/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class MainTABViewController: PawxoTabBar {

    override func viewDidLoad() {
        super.viewDidLoad()
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let storyboardSearch = UIStoryboard(name: "Search", bundle: nil)
        let storyboardForum = UIStoryboard(name: "Forum", bundle: nil)
        let storyboardProfile = UIStoryboard(name: "Profile", bundle: nil)
        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
        let storyboardChat = UIStoryboard(name: "Chat", bundle: nil)
        let vc_feed = storyboardPet.instantiateViewController(withIdentifier: "FeedVC") as! FeedVC
        let vc_Home = storyboardMain.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC

        let vc_chatList = storyboardChat.instantiateViewController(withIdentifier: "ChatListVC") as! ChatListVC

        let vc_ListForum = storyboardForum.instantiateViewController(withIdentifier: "ForumsListVC") as! ForumsListVC
        
        let vc_Profile = storyboardProfile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC

        
        let homeImage = UIImage(named: "TABHome")
        let groomingImage = UIImage(named: "TABForum")
        let chatImage = UIImage(named: "TABChat")
        let pawImage = UIImage(named: "TABPawxo")
        let profileImage = UIImage(named: "TABProfile")
        self.tabBarImages = [homeImage!,groomingImage!,chatImage!,pawImage!,profileImage!]
        
        self.controllers = [vc_Home,vc_ListForum,vc_chatList,vc_feed,vc_Profile]
        // Do any additional setup after loading the view.
        tabBar = self
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setSelectedTABForNotification(index:Int){
        self.setSelectedTAB(index:index)
    }

}
