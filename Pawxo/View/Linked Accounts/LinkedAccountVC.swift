//
//  LinkedAccountVC.swift
//  Pawxo
//
//  Created by Apple on 15/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

import TwitterKit
import TwitterCore


class LinkedAccountVC: UIViewController {
    
    
    
    @IBOutlet weak var tickView: UIView!
    
    var showTick = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
            if let session = TWTRTwitter.sharedInstance().sessionStore.session() {
                let client = TWTRAPIClient()
                client.loadUser(withID: session.userID) { (user, error) -> Void in
                    if user != nil
                    {
                        if let user = user {
                            // self.twitterUserName.text = user.screenName
                        }
                    }
                    else{
                        print("error: \(error?.localizedDescription)");
                    }
                }
                if let authToken = session.authToken as? String
                {
                    showTick = true
                    // self.obj.twitterAccessToken = authToken
                    // self.obj.isShare = "1"
                }
                if let secretAuthToken = session.authTokenSecret as? String
                {
                    showTick = true
                }
                
                
            }
            
            
            
            
            
            
            
        }
        
        self.showTickOnScreen(show: showTick)
        
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action:#selector(self.longPressed(sender:)) )
        self.tickView.addGestureRecognizer(longPressRecognizer)

    }
    
    
    @IBAction func longPressed(sender: UILongPressGestureRecognizer)
    {
        let alertController = UIAlertController(title: AppMessages.unlinkAccount, message: nil, preferredStyle: UIAlertController.Style.alert)
         
         
        
         let somethingAction = UIAlertAction(title: "OK", style: .default, handler: {(alert: UIAlertAction!) in
             
            if let session = TWTRTwitter.sharedInstance().sessionStore.session() {
                TWTRTwitter.sharedInstance().sessionStore.logOutUserID(session.userID)
                self.showTick = false
                self.showTickOnScreen(show: self.showTick)
            }
            
             
             })
             
             let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
             
             alertController.addAction(somethingAction)
             alertController.addAction(cancelAction)
             alertController.view.tintColor = UIColor.black
             DispatchQueue.main.async {
                 self.present(alertController, animated: true, completion:{})
             }
        //Different code
    }
    
    func showTickOnScreen(show:Bool){
      //  self.tickView.isHidden = !show
    }
    
    
    @IBAction func backButtonTapped(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
