//
//  PrivacyPolicyVC.swift
//  Pawxo
//
//  Created by Apple on 15/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import WebKit

class PrivacyPolicyVC: UIViewController {

    @IBOutlet weak var policy_text: UITextView!
    
    @IBOutlet weak var privacyWebview: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let link = URL(string:"https://cuddl.app/privacy-policy/")!
        let request = URLRequest(url: link)
        self.privacyWebview.load(request)
        
        
    }
    
    @IBAction func backButtonTapped(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
