//
//  editProfileVC.swift
//  Pawxo
//
//  Created by 42works on 10/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import Kingfisher

class EditProfileVC: UIViewController {
    @IBOutlet weak var nameTF: RegularTextField!
    @IBOutlet weak var userNameTF: RegularTextField!
    @IBOutlet weak var emailTF: RegularTextField!
    @IBOutlet weak var phoneNumberTF: RegularTextField!
    @IBOutlet weak var locationTF: RegularTextField!
    @IBOutlet weak var bioTV: RegularTextView!
    @IBOutlet weak var maleBtn: UIButton!
      @IBOutlet weak var femaleBtn: UIButton!
      @IBOutlet weak var maleRadioImg: UIImageView!
      @IBOutlet weak var femaleRadioImg: UIImageView!
    
    var currentLat : Double?
    var currentLong : Double?
    var currentAddress : String?
    
    var user: Userlist?
    
    var isMale : Bool?
    var imageChanged = false
    
    var profile_VM : Profile_VM?
    
    @IBOutlet weak var placeHolderLabel: RegularLabel!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var dropDownArrow: UIImageView!
    
    @IBOutlet weak var phoneOkImage: UIImageView!
    
    var profileUpdated:(()->())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profile_VM = Profile_VM()
        self.userNameTF.isUserInteractionEnabled = false
        self.emailTF.isUserInteractionEnabled = false
        self.userNameTF.isUserInteractionEnabled = false
        self.setUIChanges(user: user!)
        
        let degreesToRadians: (CGFloat) -> CGFloat = { (degrees: CGFloat) in
            return degrees / 180.0 * CGFloat.pi
        }
        self.dropDownArrow.transform = CGAffineTransform(rotationAngle:degreesToRadians(-90))
        // Do any additional setup after loading the view.
    }
    
    
    func setUIChanges(user:Userlist){
        self.nameTF.text = user.name ?? ""
        self.emailTF.text  = user.email ?? ""
        self.userNameTF.text = user.username ?? ""
        
        if let location = user.location {
            self.locationTF.text = location
        }else{
            self.locationTF.text = ""
        }
        
        self.phoneNumberTF.text =  (user.phone != nil) ? user.phone : ""
        
        if self.phoneNumberTF.text?.count != 10 {
            self.phoneOkImage.isHidden = true
        }
        
        self.profileImageView.setImage(fromUrl: user.userImage ?? "", defaultImage: DefaultImage.defaultProfileImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: false, showIndicator: false)
        
        if let gender = user.gender {
            if gender == "Male"{
                maleRadioImg.image = UIImage.init(named: "radioSelected")
                femaleRadioImg.image = UIImage.init(named: "radioUnselected")
                isMale = true
            }else{
                femaleRadioImg.image = UIImage.init(named: "radioSelected")
                maleRadioImg.image = UIImage.init(named: "radioUnselected")
                isMale = false
            }
        }else{
            femaleRadioImg.image = UIImage.init(named: "radioUnselected")
            maleRadioImg.image = UIImage.init(named: "radioUnselected")
        }
        
        self.bioTV.text = user.bio ?? ""
        if self.bioTV.text == "" {
            self.placeHolderLabel.alpha = 1
        }else{
            self.placeHolderLabel.alpha = 0
        }
    }
    
    
    
    @IBAction func radioButton(sender: UIButton) {
        if sender == maleBtn {
          //  strInterested = "yes"
            maleBtn.isSelected = true
            femaleBtn.isSelected = false
            maleRadioImg.image = UIImage.init(named: "radioSelected")
            femaleRadioImg.image = UIImage.init(named: "radioUnselected")
            isMale = true

        } else if sender == femaleBtn{
           // strInterested = "no"
            maleBtn.isSelected = false
            femaleBtn.isSelected = true
            femaleRadioImg.image = UIImage.init(named: "radioSelected")
            maleRadioImg.image = UIImage.init(named: "radioUnselected")
            isMale = false

        }
    }
    @IBAction func back(sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func editImagePressed(_ sender: UIButton) {
        photoManager = PhotoManager(navigationController: self.navigationController!, allowEditing: true, videoType: false, callback: { (pickedImage,urlVideo)  in
            //self.questionaireModel.profileImage = pickedImage ?? #imageLiteral(resourceName: "TempProfile")
            if let image = pickedImage {
                self.profileImageView.image = image
                self.imageChanged = true
            }
        })
    }
    
    
    @IBAction func goToLocation(_sender : UIButton){
        if let vc = UIStoryboard(name: StoryboardName.Settings, bundle: nil).instantiateViewController(withIdentifier: "EditLocationVC") as? EditLocationVC{
            vc.isEdit = true
            self.navigationController?.pushViewController(vc, animated: true)
            
            vc.delegate = self
        }
    }
    
    
    @IBAction func saveButtonPressed(_sender : UIButton){
        
        if self.nameTF.text == ""{
            self.showAlert(self , message: "Please enter name")
            return
        }
        
        if self.userNameTF.text == ""{
            self.showAlert(self , message: "Please enter username")
            return
        }
        
        if self.emailTF.text == ""{
            self.showAlert(self , message: "Please enter email")
            return
        }
        
        if self.phoneNumberTF.text == ""{
            self.showAlert(self , message: "Please enter phone no")
            return
        }
        
        
        if self.nameTF.text == ""{
            self.showAlert(self , message: "Please enter name")
            return
        }
        
        if isMale == nil{
            self.showAlert(self , message: "Please select gender")
            return
        }
        
        if self.locationTF.text == ""{
            self.showAlert(self , message: "Please choose location")
            return
        }
        
        if self.bioTV.text == ""{
            self.showAlert(self , message: "Please enter bio")
            return
        }
        
        profile_VM?.name = self.nameTF.text
        profile_VM?.bio = self.bioTV.text
        
        profile_VM?.userId = "\((self.user?.id)!)"
        if imageChanged {
            profile_VM?.imageData = self.profileImageView.image?.pngData()
        }
        profile_VM?.phone = self.phoneNumberTF.text
        if let isMale = isMale{
            profile_VM?.gender = isMale ? "Male" : "Female"
        }
        
        if let currentAddress = self.currentAddress {
            profile_VM?.location = currentAddress
        }
        
        if let lat = self.currentLat {
            profile_VM?.latitude = "\(lat)"
        }
        
        if let long = self.currentLong {
            profile_VM?.longitude = "\(long)"
        }
            
        profile_VM!.responseRecieved = {
            
            if let message = self.profile_VM!.alertMessage{
                DispatchQueue.main.async {
                    self.showAlert(self , message: message)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        
                        self.profileUpdated?()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
            
            let cache = ImageCache.default
            // cache.clearMemoryCache()
            //cache.clearDiskCache { print("Done") }
            //https://cuddl.app/admin/public/uploads/user_image/421_user_image.jpg
            
            cache.removeImage(forKey: self.user!.userImage!)
            
        }
               
        profile_VM!.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self!.profile_VM!.alertMessage {
                    self!.showAlert(self!, message: message)
                }
            }
        }
               
        
        
        profile_VM!.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self!.profile_VM!.isLoading ?? false{
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
               profile_VM!.callUpdateProfileService = true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension EditProfileVC : EditLocationVCProtocol {
    func locationDetails(lat: Double, long: Double, address: String) {
        self.currentLat = lat
        self.currentLong = long
        self.currentAddress = address
        self.locationTF.text = address
    }
    
}


extension EditProfileVC : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if newText.count > 0 {
            placeHolderLabel.alpha = 0
        }else{
            placeHolderLabel.alpha = 1
        }
        return newText.count < 121
    }
    
}


extension EditProfileVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var updatedText = ""
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
           
        }
        
        print(updatedText)
        if textField == phoneNumberTF{
            
            if updatedText.count < 10{
              self.phoneOkImage.isHidden = true
            }else if updatedText.count == 10{
              self.phoneOkImage.isHidden = false
            }else if updatedText.count > 10 {
                return false
            }
            
        }
     
        return true
    }
    
   
    
    
}
