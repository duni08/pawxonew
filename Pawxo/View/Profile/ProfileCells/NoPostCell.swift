//
//  NoPostCell.swift
//  Pawxo
//
//  Created by 42works on 05/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class NoPostCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var noPostImageView: UIImageView!
    
    @IBOutlet weak var heightConstraintImage: NSLayoutConstraint!
    @IBOutlet weak var widthConstraintImage: NSLayoutConstraint!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
