//
//  ProfilePostCell.swift
//  Pawxo
//
//  Created by 42works on 16/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit


protocol ProfilePostCellDelegate {
    func setheight(height:CGFloat)
    func gotoPostDetailScreen(post:PostList)
}

class ProfilePostCell: UITableViewCell {
    
    
    @IBOutlet weak var profilePostCollectionView: UICollectionView!
    
    let cellIdentifier = "MediaAddCell"
    var staticCount = 30
    var delegate : ProfilePostCellDelegate?
    var padding : CGFloat = 2
    
    var postList = [PostList]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profilePostCollectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
       
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: padding, bottom: 10, right: padding)
        //layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        layout.minimumInteritemSpacing = padding
        layout.minimumLineSpacing = padding
        profilePostCollectionView!.collectionViewLayout = layout
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPost(postList:[PostList]){
        self.postList = postList
        self.profilePostCollectionView.reloadData()
    }
    
    
    @objc func changeHeight(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { 
            self.delegate?.setheight(height: self.profilePostCollectionView.contentSize.height)
        }
    }
}


extension ProfilePostCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.postList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let mediaCell : MediaAddCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! MediaAddCell
        
        let post = self.postList[indexPath.item]
       // print(indexPath.item/3)
        if post.type == "image"{
            mediaCell.videoButton.isHidden = true
            
            mediaCell.imgView.setImage(fromUrl: post.media?.first ?? "", defaultImage: DefaultImage.defaultPostImage)
        }else{
            mediaCell.videoButton.isHidden = false
           
            mediaCell.imgView.setImage(fromUrl: post.thumbnail?.first ?? "", defaultImage: DefaultImage.defaultPostImage)
        }
        
        return mediaCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //let height =  self.bounds.size.height - (self.view.safeAreaInsets.top + self.view.safeAreaInsets.bottom + 101)
        
//        let width = collectionView.frame.size.width -
        let width = (collectionView.frame.size.width - CGFloat(4 * padding))/3 - 0.02
        return CGSize(width: width, height: width)
    }
    
    
    
       // return CGSize(width: 100, height: 100)
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print(indexPath.item)
        let post = self.postList[indexPath.item]
        delegate?.gotoPostDetailScreen(post:post)
    }
    
}
