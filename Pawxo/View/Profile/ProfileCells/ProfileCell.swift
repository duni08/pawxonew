//
//  ProfileCell.swift
//  Pawxo
//
//  Created by 42works on 16/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit


enum profileFollowStatus {
    case requested
    case follow
    case following
    case edit
}

protocol ProfileCellDelegate {
    func editProfileButtonPressed(status:profileFollowStatus,cell:ProfileCell)
    func editProfileImagePressed(cell:ProfileCell)
    func followerPressed(cell:ProfileCell)
    func followingPressed(cell:ProfileCell)
    func petPressed(pet:PetList)
}



class ProfileCell: UITableViewCell {
    
    @IBOutlet weak var petCollectionView: UICollectionView!
    
    @IBOutlet weak var profileImageEditButton: UIButton!
    
    @IBOutlet weak var descriptionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var editProfileButton: RegularButton!
    
    @IBOutlet weak var followersLabel: RegularLabel!
    @IBOutlet weak var followingLabel: RegularLabel!
    
    @IBOutlet weak var numberOfPostsLabel: RegularLabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var nameLabel: RegularLabel!
    
    var delegate : ProfileCellDelegate?
    
    var user: Userlist?
    
    var profileStatus = profileFollowStatus.requested
    
    @IBOutlet weak var descriptionLabel: RegularLabel!
    
    @IBOutlet weak var petWidthConstraint: NSLayoutConstraint!
    
    var showPets = false
    var isMyProfile = false
    
    let cellIdentifier = "PetProfileCell"
    var staticCount = 3
    var isPrivate = false
    var showFollowerFollowing = true
    
    
    @IBOutlet weak var bottomDividerView: DividerView!
    @IBOutlet weak var descriptionBottomConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var petCollectionTopConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        petCollectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    
    func setStaticCount(count:Int){
        self.staticCount = count
        
        // calculate width
        let calculatedWidth = (50 * count) + ((count - 1) * 10)
        if Int((self.frame.size.width - 40)) >= calculatedWidth {
            self.petWidthConstraint.constant = CGFloat(calculatedWidth)
        }else{
            self.petWidthConstraint.constant = self.frame.size.width - 40
        }
        self.petCollectionView.reloadData()
    }
    
    func showDividerView(show:Bool){
        self.bottomDividerView.isHidden = !show
    }
    
    func setConstraintsForCell(caseForCheck:Int){
        // case 1 own account with description only  1 0
        // case 2 own account with description and pets 11
        // case 3 own accouunt with no  desciption no pets 00
        // case 4 own  with no description with pets 01
        
        switch caseForCheck {
        case 1:
        self.descriptionBottomConstraint.constant =  15
        case 2:
            self.descriptionBottomConstraint.constant = 80
        case 3:
            self.descriptionBottomConstraint.constant = 0
        default:
            self.descriptionBottomConstraint.constant = 65
            self.petCollectionTopConstraint.constant = 0
        }
        
    }
    
    
    
    func setData(labelText:String,status : profileFollowStatus){
        self.descriptionLabel.text = labelText
        let textHeight = labelText.height(withConstrainedWidth: self.descriptionLabel.frame.width , font: self.descriptionLabel.font)
        //self.descriptionHeightConstraint.constant = textHeight
        self.setUpButtonStatus(profileStatus: status)
    }
    
    
    func setUpButtonStatus(profileStatus:profileFollowStatus){
        switch profileStatus {
        case .requested:
            print("requested")
            self.editProfileButton.setTitle("Requested", for: .normal)
            self.profileImageEditButton.isHidden = true
            self.setProfileButtonFilled(fill: false)
        case .follow:
            print("follow")
            self.editProfileButton.setTitle("Follow", for: .normal)
            self.profileImageEditButton.isHidden = true
            self.setProfileButtonFilled(fill: true)
        case .following:
            print("following")
            self.editProfileButton.setTitle("Following", for: .normal)
            self.profileImageEditButton.isHidden = true
            self.setProfileButtonFilled(fill: false)
        default:
            print("Edit")
            self.editProfileButton.setTitle("Edit Profile", for: .normal)
            self.profileImageEditButton.isHidden = false
            self.setProfileButtonFilled(fill: false)
        }
    }
    
    func setProfileButtonFilled(fill:Bool){
        if !fill{
            self.editProfileButton.backgroundColor = AppColor.baseColor
            self.editProfileButton.setTitleColor(.white, for: .normal)
            self.editProfileButton.borderWidth = 0
        }else{
            self.editProfileButton.backgroundColor = .white
            self.editProfileButton.setTitleColor(AppColor.baseColor, for: .normal)
            self.editProfileButton.borderWidth = 1.5
            self.editProfileButton.borderColor = AppColor.baseColor
        }
    }
    
    
    
    
    func setUser(user:Userlist,showPets:Bool,isMyProfile:Bool){
            // refresh content of user
        self.user = user
        self.showPets = showPets
        self.isMyProfile = isMyProfile
        self.nameLabel.text = user.name
        
        
        if let blocked = self.user?.is_user_blocked,blocked.boolValue == true {
            self.numberOfPostsLabel.text =  "0"
        }else{
           self.numberOfPostsLabel.text = (user.total_post != nil) ? "\(user.total_post!)" : "0"
        }
        
        
        
        self.followersLabel.text = (user.followerCount != nil) ? "\(user.followerCount!)" : "0"
        self.followingLabel.text = (user.followingCount != nil) ? "\(user.followingCount!)" : "0"
        self.descriptionLabel.text = user.bio ?? ""
        self.profileImage.setImage(fromUrl: user.userImage ?? "" , defaultImage: DefaultImage.defaultProfileImage)

        if isMyProfile {
            profileStatus = .edit
        }else{
            // check for user
            if let isPrivate = user.isProfilePrivate, isPrivate.boolValue == true {
                if user.isFollowed!.boolValue{
                    profileStatus = .following
                }else if !(Int(user.isRequested!)!.boolValue) {
                    profileStatus = .follow
                }else{
                    profileStatus = .requested
                }
                showFollowerFollowing = false
            }else{
                showFollowerFollowing = true
                // public check for follow unfoolow
                if user.isFollowed!.boolValue{
                    profileStatus = .following
                }else{
                    profileStatus = .follow
                }
            }
        }
        
        
        self.setData(labelText: self.descriptionLabel.text!, status: profileStatus)
        
        self.petCollectionView.isHidden = !showPets
        self.petCollectionView.reloadData()
        
        if let pet = self.user?.pets{
            self.setStaticCount(count: pet.count)
        }
        
        self.setDescription(desc: user.bio ?? "", showPets: showPets)
        
    }
    
    func setDescription(desc:String,showPets:Bool){
        
        // case 1 own account with description only  1 0
        // case 2 own account with description and pets 11
        // case 3 own accouunt with no  desciption no pets 00
        // case 4 own  with no description with pets 01
        
        if desc == ""{
            if showPets{
                self.setConstraintsForCell(caseForCheck: 4)
            }else{
                self.setConstraintsForCell(caseForCheck: 3)
            }
        }else{
            if showPets{
                self.setConstraintsForCell(caseForCheck: 2)
            }else{
                self.setConstraintsForCell(caseForCheck: 1)
            }
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImageProfile(image:UIImage){
        self.profileImage.image = image
    }
    
    @IBAction func editProfilePressed(_ sender: Any) {
        delegate?.editProfileButtonPressed(status: self.profileStatus ,cell:self)
    }
    
    @IBAction func editProfileImagePressed(_ sender: Any) {
        delegate?.editProfileImagePressed(cell: self)
        
    }
    
    @IBAction func followersButtonPressed(_ sender: Any) {
        if showFollowerFollowing{
            delegate?.followerPressed(cell: self)
        }
    }
    
    @IBAction func followingButtonPressed(_ sender: Any) {
        if showFollowerFollowing{
            delegate?.followingPressed(cell: self)
        }
    }
    
}

extension ProfileCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let pet = self.user?.pets{
            return pet.count
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        let pet = self.user?.pets![indexPath.item]
        let petProfileCell : PetProfileCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! PetProfileCell
        
        
        
        petProfileCell.petImageView.setImage(fromUrl: pet?.petImages ?? "" , defaultImage: DefaultImage.defaultPetImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
        return petProfileCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //let height =  self.bounds.size.height - (self.view.safeAreaInsets.top + self.view.safeAreaInsets.bottom + 101)
        return CGSize(width: 50, height: 50)
    }
       // return CGSize(width: 100, height: 100)
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print(indexPath.item)
        if let pets = self.user?.pets{
            let pet = pets[indexPath.item]
            DispatchQueue.main.async {
                self.delegate?.petPressed(pet: pet)
            }
        }
    }
    
}
