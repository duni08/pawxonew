//
//  profileVC.swift
//  Pawxo
//
//  Created by 42works on 07/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

import Firebase
import Kingfisher

class ProfileVC: UIViewController {
    
    
    @IBOutlet weak var profileTableView: UITableView!
    
    @IBOutlet weak var noResultsView: UIView!
    @IBOutlet weak var noInternetView: UIView!
    
    @IBOutlet weak var backButton: UIButton!
    
    var staticCount = 2
    
    @IBOutlet weak var settingButtonTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: SemiBoldLabel!
    
    var toPostNotification = false
    
    var profileCellIdentifier = "ProfileCell"
    var profilePostCellIdentifier = "ProfilePostCell"
    var privateCellIdentifier = "PrivateAccountCell"
    var noPostCellIdentifier = "NoPostCell"
    var cellHeight : CGFloat = 500
  
    var isFromOtherScreen = false
    
    var isForPullToRefresh = false
    
    var dataArrayMain = [String:Any]()
    
    @IBOutlet weak var settingButton: UIButton!
    
    var heightRefreshed = false
    
    var isPrivate = false
    var isMyProfile = true
    
    var isBlocked = false
    
    var hasPet = true
    var padding  : CGFloat = 20
    var profileCellHeight : CGFloat = 0
    var descriptionText = "this is a temp text this is a temp text this is a temp text/n this is a temp text this is a temp text this is a temp text\n this is a temp text this is a temp text"
    
    var profile_VM = Profile_VM()
    
    var user : Userlist?
    var postList : [PostList]?
    
    var userId = CommonFunctions.getuserFromDefaults().userId!
    
    var unFollowRequest: (()->())?
    
    
    var page = 1
       var refresh = UIRefreshControl()
       var total_page = 0
    
    var IsloadedPosts = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backButton.isHidden = !isFromOtherScreen
        self.settingButton.isHidden = isFromOtherScreen
        profileTableView.register(UINib(nibName: profileCellIdentifier, bundle: nil), forCellReuseIdentifier: profileCellIdentifier)
        profileTableView.register(UINib(nibName: profilePostCellIdentifier, bundle: nil), forCellReuseIdentifier: profilePostCellIdentifier)
        profileTableView.register(UINib(nibName: privateCellIdentifier, bundle: nil), forCellReuseIdentifier: privateCellIdentifier)
        profileTableView.register(UINib(nibName: noPostCellIdentifier, bundle: nil), forCellReuseIdentifier: noPostCellIdentifier)
        
        self.profileTableView.isHidden = false
        
       // isPrivate = !isMyProfile
        
            refresh.tintColor = UIColor.clear
           refresh.backgroundColor = UIColor.clear
           refresh.attributedTitle = NSAttributedString(string: "Pull to refresh")
           refresh.addTarget(self, action:#selector(refreshTable), for: UIControl.Event.valueChanged)
           // Add Custom Loader
           var loader = UIImageView()
           let gif = UIImage.gifImageWithName("gif")
           loader.center = self.view.center
           loader = UIImageView(frame: CGRect(x: self.view.frame.size.width/2 - 20, y: refresh.center.y, width: 40, height: 40))
           loader.image = gif
           refresh.insertSubview(loader, at: 0)
           
           
           
           self.profileTableView.addSubview(refresh)

           let emptyLabel = UILabel.init(frame: self.profileTableView.frame)
           var msg = ""
        
           msg = "No latest posts yet."
           
           
           emptyLabel.isHidden = true
           emptyLabel.text = msg
           emptyLabel.textAlignment = .center
           emptyLabel.numberOfLines = 0
           emptyLabel.textColor = UIColor.darkGray
           self.profileTableView.addSubview(emptyLabel)
        
        
        self.getDataForProfileVC()
        
    }
    
    
    
    @IBAction func tryAgainPressed(_ sender: Any) {
        self.refreshTable()
        
    }
    
    @objc private func refreshTable() {
        page = 1
        self.isForPullToRefresh = true
        self.getDataForProfileVC()
    }
    
    @IBAction func reloadProfile(_ sender: Any) {
        self.getUserProfile(userId: userId)
    }
    
    
    func getDataForProfileVC(){
        let myId = CommonFunctions.getuserFromDefaults().userId!
        
        if myId == userId {
            isMyProfile = true
        }else{
            isMyProfile = false
        }
        
        self.getUserProfile(userId: userId)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        if let val = dataArrayMain["posts"]{
//            dataArrayMain.removeValue(forKey: "posts")
//        }
//
//        if let val = dataArrayMain["Nodata"]{
//                dataArrayMain.removeValue(forKey: "Nodata")
//        }
        
        self.profileTableView.reloadData()
        
      //  self.getDataForProfileVC()
        
        if self.toPostNotification{
            NotificationCenter.default.post(name: Notification.Name("RefreshHomeList"), object: nil, userInfo: nil)
            self.toPostNotification = false
        }
    }
    
    func getUserProfile(userId:String){
        
        
        if !(WebServices().isInternetWorking()){
            self.noInternetView.isHidden = false
            self.dataArrayMain.removeAll()
            self.profileTableView.isHidden = false
            self.profileTableView.addSubview(self.noInternetView)
            self.profileTableView.reloadData()
            self.isForPullToRefresh = false
            return
        }else{
            self.noInternetView.isHidden = true
        }
        
        
        profile_VM.userId = userId
        profile_VM.perPage = "50"
        profile_VM.page = "\(page)"
        
        
        profile_VM.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.profile_VM.isLoading ?? false{
                    if self?.isForPullToRefresh == false{
                        LoaderView.shared.showLoader()
                    }
                } else {
                    if self?.isForPullToRefresh == false{
                        LoaderView.shared.hideLoader()
                    }else{
                        self?.isForPullToRefresh = false
                    }
                }
            }
        }
        
        profile_VM.responseRecieved = {
             self.refresh.endRefreshing()
            if let message = self.profile_VM.alertMessage{
                DispatchQueue.main.async {
                    self.showAlert(self , message: message)
                }
            }else{
                self.user = self.profile_VM.userList!
                
                    if !self.isMyProfile{
                        
                        self.settingButton.isHidden = false
                        self.settingButton.setImage(DefaultImage.DotImage, for: .normal)
                        self.settingButtonTrailingConstraint.constant = 8
                        
                        if let blocked =  self.user?.is_user_blocked, blocked.boolValue == true{
                            self.isBlocked = true
                        }else{
                            self.isBlocked = false
                        }
                        
                        
                        
                    if let isPrivate = self.user?.isProfilePrivate {
                        if isPrivate == 0 {
                            self.postList = self.profile_VM.postList
                        }else{
                            if let follow = self.user?.isFollowed {
                                if follow == 0{
                                    self.isPrivate = true
                                }else{
                                    self.isPrivate = false
                                    self.postList = self.profile_VM.postList
                                }
                            }
                        }
                    }
                        self.titleLabel.text = self.user?.username
                    }else{
                        self.titleLabel.text = "My Profile"
                        self.postList = self.profile_VM.postList
                    }
            
                self.setProfile()
            }
            
            
            
        }
        
        profile_VM.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self!.profile_VM.alertMessage {
                    self!.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        
        profile_VM.callGetUserProfileService = true
    }
    
    func bottomRefresh(_ scrollView : UIScrollView)
       {
           let scrollViewHeight = scrollView.frame.size.height;
           let scrollContentSizeHeight = scrollView.contentSize.height;
           let scrollOffset = scrollView.contentOffset.y;
           if (scrollOffset >  scrollContentSizeHeight - scrollViewHeight)
           {
               if IsloadedPosts {
                   page = page + 1
                   
                   if page <= self.total_page{
                       self.getDataForProfileVC()
                   }
               }
           }
       }
    
    func setProfile(){
        self.profileTableView.isHidden = false
        self.dataArrayMain["profile"] = self.user
        
        if self.isBlocked{
            self.dataArrayMain["Nodata"] = "No data"
            self.dataArrayMain.removeValue(forKey: "posts")
        }else{
        
        if let postlist = self.postList, postlist.count > 0  {
            self.dataArrayMain["posts"] = postlist
            self.dataArrayMain.removeValue(forKey: "Nodata")
        }else{
            self.dataArrayMain["Nodata"] = "No data"
        }
        }
        self.setHeightForProfileView()
        self.profileTableView.reloadData()
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let _ = scrollView as? UITableView {
            bottomRefresh(scrollView)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            if let _ = scrollView as? UITableView {
                bottomRefresh(scrollView)
            }
        }
    }
    
    
    @IBAction func refreshProfileView(){
        
    }
    
    
    func setHeightForProfileView(){
        // calculateHeight for descriptionView
        
        // case 1 own account with description only  1 0
        // case 2 own account with description and pets 11
        // case 3 own accouunt with no  desciption no pets 00
        // case 4 own  with no description with pets 01
        
//        switch caseForCheck {
//        case 1:
//        self.descriptionBottomConstraint.constant =  15
//        case 2:
//            self.descriptionBottomConstraint.constant = 80
//        case 3:
//            self.descriptionBottomConstraint.constant = 0
//        default:
//            self.descriptionBottomConstraint.constant = 65
//            self.petCollectionTopConstraint.constant = 0
//        }
        
        
        
        self.descriptionText = self.user?.bio ?? ""
        var textHeight = descriptionText.height(withConstrainedWidth: self.view.frame.width - (padding * 2), font: UIFont.JosefinSansRegular(size: 16.0))
        
        if  isFromOtherScreen{
            // show pets if exits
            
            if let pet = self.user?.pets,((!isMyProfile && !isPrivate) || isMyProfile) == true {
                if pet.count > 0 {
                    
                    if self.descriptionText != ""{
                    textHeight += 153 + 80
                    }else{
                        textHeight = 153 + 65
                    }
                }
            }else{
                if self.descriptionText != ""{
                textHeight += 165
                }else{
                    textHeight = 165
                }
            }
        }else{
            
            
            if self.descriptionText != ""{
                textHeight += 165
            }else{
                textHeight = 165
            }
        }
        
        profileCellHeight = textHeight
    }
    
    
    func gotoEditProfile(){
        let storyboardProfile = UIStoryboard(name: "Profile", bundle: nil)
         let vc_editProfile = storyboardProfile.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        vc_editProfile.user = self.user
        //vc_editProfile.profile_VM = self.profile_VM
        vc_editProfile.profileUpdated = {
            self.refreshTable()
        }
        self.navigationController?.pushViewController(vc_editProfile, animated: true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func settingScreensPressed(_ sender: Any) {
        
        if self.isMyProfile{
        
        let storyboardSettings = UIStoryboard(name: "Settings", bundle: nil)
        let vc_setting = storyboardSettings.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        self.navigationController?.pushViewController(vc_setting, animated: true)
        }else{
            self.showAlertForBlock()
        }
    }
    
    
    func showAlertForBlock(){
        let alertController = UIAlertController(title:  self.isBlocked == false ? AppMessages.blockUser : AppMessages.UnBlockUser, message: nil, preferredStyle: UIAlertController.Style.alert)
        
        
        
        let somethingAction = UIAlertAction(title: "OK", style: .default, handler: {(alert: UIAlertAction!) in
            
            let profile_VM = Profile_VM()
            //                            profile_VM.userId = userId
            
            let uid = CommonFunctions.getuserFromDefaults().userId
            profile_VM.userId = uid
            profile_VM.blocker_id = "\((self.user?.id)!)"
            
            
            profile_VM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if profile_VM.isLoading {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            profile_VM.responseRecieved = {
                DispatchQueue.main.async {
                    if let message = profile_VM.alertMessage {
                        self.showAlert(self, message: message)
                       // self.popVC()
                    }
                    
                    if self.isBlocked{
                        self.resetChatIfFound(blocked: false)
                    }else{
                        self.resetChatIfFound(blocked: true)
                    }
                    
                    self.isBlocked = !self.isBlocked
                    //self.setProfile()
                    self.getDataForProfileVC()
                }
            }

            profile_VM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = self!.profile_VM.alertMessage {
                        self!.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            
            profile_VM.callBlockUserProfileService = true
            
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
        
        alertController.addAction(somethingAction)
        alertController.addAction(cancelAction)
        alertController.view.tintColor = UIColor.black
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
    }
    
    
    func resetChatIfFound(blocked:Bool){
        let userLogin = CommonFunctions.getuserFromDefaults()
        
        var ResultString = ""
        
        if userLogin.userId ?? "" > "\((self.user?.id!)!)" {
               
                    print("first_value is greater than second_value")
                   ResultString = "\("chat")_\((self.user?.id!)!)_\(userLogin.userId ?? "")"
                   print(ResultString)
                   }
                   else {
                       print("first_value is not greater than second_value ")
                   ResultString = "\("chat")_\(userLogin.userId ?? "")_\((self.user?.id!)!)"
                   print(ResultString)
                   }
               
        
        
        
        var blockedby = userLogin.userId ?? ""
        
        if !blocked {
            blockedby = "NA"
        }
        
        
               let ref : DatabaseReference!
               ref = Database.database().reference()
               ref.child("users").child(userLogin.userId ?? "").child(ResultString).child("is_blocked").setValue(blocked)
                ref.child("users").child(userLogin.userId ?? "").child(ResultString).child("blocked_by").setValue(blockedby)
        
                ref.child("users").child("\((self.user?.id!)!)").child(ResultString).child("is_blocked").setValue(blocked)
                ref.child("users").child("\((self.user?.id!)!)").child(ResultString).child("blocked_by").setValue(blockedby)
        
        
    }
    
    func gotoFollowerScreen(){
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let vc_Follow = storyboardMain.instantiateViewController(withIdentifier: "FollowVC") as! FollowVC
        self.navigationController?.pushViewController(vc_Follow, animated: true)
        self.toPostNotification = true
    }
    
}
    

extension ProfileVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataArrayMain.keys.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if indexPath.row == 0 {
            
            let user = dataArrayMain["profile"] as? Userlist
            
            let cell : ProfileCell =  tableView.dequeueReusableCell(withIdentifier: profileCellIdentifier, for: indexPath) as! ProfileCell
            cell.delegate = self
            
            
            var showPets = false
            if  isFromOtherScreen{
                // show pets if exits
                if let pet = self.user?.pets, ((!isMyProfile && !isPrivate) || isMyProfile) == true {
                    if pet.count > 0 {
                        showPets = true
                    }
                }else{
                    showPets = false
                }
                
            }
            cell.isPrivate = self.isPrivate
            cell.setUser(user: user!, showPets: showPets, isMyProfile: self.isMyProfile)
            if let post = self.postList, post.count > 0 {
                cell.showDividerView(show:false)
            }else{
                cell.showDividerView(show:true)
            }
            //cell.calculateWidthForCateogry()
            return cell
        }else{
            
            if isPrivate {
                let cell : NoPostCell =  tableView.dequeueReusableCell(withIdentifier: noPostCellIdentifier, for: indexPath) as! NoPostCell
                cell.titleLabel.text = "This account is private."
                cell.descriptionLabel.text = "Follow this account to see their\nphotos and videos"
                return cell
//                let cell : PrivateAccountCell =  tableView.dequeueReusableCell(withIdentifier: privateCellIdentifier, for: indexPath) as! PrivateAccountCell
//
//                return cell
            }else if let postlist = self.postList, postlist.count > 0 && !isBlocked {
                let cell : ProfilePostCell =  tableView.dequeueReusableCell(withIdentifier: profilePostCellIdentifier, for: indexPath) as! ProfilePostCell
                
                let posts = dataArrayMain["posts"] as? [PostList]
                //                    if isMyProfile{
                //                        cell.staticCount = 30
                //                    }else{
                //                        cell.staticCount = 0
                //                    }
                cell.setPost(postList: posts!)
                cell.delegate = self
                if !self.heightRefreshed {
                    cell.changeHeight()
                }
                //cell.changeHeight()
                
                
                return cell
                
            }else{
                let cell : NoPostCell =  tableView.dequeueReusableCell(withIdentifier: noPostCellIdentifier, for: indexPath) as! NoPostCell
                cell.noPostImageView.image = UIImage(named:"NoResults")
                
                
                if self.isMyProfile{
                    cell.descriptionLabel.text = AppMessages.myProfileNoPost
                }else{
                    cell.descriptionLabel.text = AppMessages.otherProfileNoPost
                }
                
                //cell.heightConstraintImage.constant = 200
                //cell.widthConstraintImage.constant = 200
                return cell
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc_PostDetail = storyboard.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
        //self.navigationController?.pushViewController(vc_PostDetail, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return  UITableView.automaticDimension //profileCellHeight
        }else{
            
            //profileCellHeight =  self.tableView(self.profileTableView, heightForRowAt: (IndexPath(row: 0, section: 0)))
            
            print(self.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0 )).bounds.size.height)
            print("Height comes")
            
            var NewHeight = self.view.frame.height  - self.view.safeAreaInsets.top - 44 - profileCellHeight
            if !self.isMyProfile{
             NewHeight = self.view.frame.height  - self.view.safeAreaInsets.top - 44 - profileCellHeight - self.view.safeAreaInsets.bottom - 49
            }
            if (cellHeight > NewHeight){
                return cellHeight
            }else{
                return NewHeight
            }
        }
    }
}


extension ProfileVC : ProfilePostCellDelegate {
    
    func setheight(height: CGFloat) {
        if !self.heightRefreshed {
        self.cellHeight = height
        self.heightRefreshed = true
        self.profileTableView.reloadData()
        }
    }
    
    
    func gotoPostDetailScreen(post:PostList){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_PostDetail = storyboard.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
        vc_PostDetail.post = post
        vc_PostDetail.isFromProfileOrPet = true
        self.toPostNotification = true
        self.navigationController?.pushViewController(vc_PostDetail, animated: true)
    }
}


extension ProfileVC : ProfileCellDelegate {
    func petPressed(pet:PetList) {
        
        if self.isBlocked {
                   self.showAlert(self, message: AppMessages.unblockUserText)
                   return
               }
        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
        let vc_feed = storyboardPet.instantiateViewController(withIdentifier: "FeedVC") as! FeedVC
        vc_feed.isFromOtherScreen = true
        vc_feed.petObject  = pet
        vc_feed.isMYPet = self.isMyProfile
        self.toPostNotification = true
        self.navigationController?.pushViewController(vc_feed, animated: true)
    }
    
    func followerPressed(cell:ProfileCell) {
        
        if self.isBlocked {
            self.showAlert(self, message: AppMessages.unblockUserText)
            return
        }
        
        
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let vc_Follow = storyboardMain.instantiateViewController(withIdentifier: "FollowVC") as! FollowVC
        
        vc_Follow.userId = CommonFunctions.getuserFromDefaults().userId!
        vc_Follow.followersId = "\((cell.user?.id)!)"
        self.toPostNotification = true
        vc_Follow.isFollowing =  false
        self.navigationController?.pushViewController(vc_Follow, animated: true)
    }
    
    func followingPressed(cell:ProfileCell) {
        
        if self.isBlocked {
            self.showAlert(self, message: AppMessages.unblockUserText)
            return
        }
        
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let vc_Follow = storyboardMain.instantiateViewController(withIdentifier: "FollowVC") as! FollowVC
        
        vc_Follow.userId = CommonFunctions.getuserFromDefaults().userId!
        vc_Follow.followersId = "\((cell.user?.id)!)"
        vc_Follow.isFollowing = true
        self.toPostNotification = true
        self.navigationController?.pushViewController(vc_Follow, animated: true)
    }
    
   
    func editProfileButtonPressed(status:profileFollowStatus,cell:ProfileCell) {
        
        
        
        if self.isBlocked {
            self.showAlert(self, message: AppMessages.unblockUserText)
            return
        }
        
       // self.gotoEditPet()
        var follow_VM = Follow_VM()
        follow_VM.userId = CommonFunctions.getuserFromDefaults().userId
        follow_VM.followerId = "\((cell.user?.id)!)"
        if self.isMyProfile {
            gotoEditProfile()
            return
        }
        
         let user = cell.user!
        
        if status == .following {
            //Show Alert
            
            
            
           
                       let storyboard = UIStoryboard(name: "Main", bundle: nil)
                       let unfollow = storyboard.instantiateViewController(withIdentifier: "UnFollowAlertVC") as! UnFollowAlertVC
                       unfollow.nameStr = user.name ?? ""
                       unfollow.imageStr = user.userImage ?? ""
                       unfollow.profileDelegate = self
                       unfollow.modalPresentationStyle = .overCurrentContext
                       unfollow.modalTransitionStyle = .crossDissolve
                       self.present(unfollow, animated: true, completion: nil)
            
            
            self.unFollowRequest = {
            
            
            follow_VM.responseRecieved = {
                let user = cell.user!
                user.isFollowed = 0
                cell.setUser(user: user, showPets: cell.showPets, isMyProfile: cell.isMyProfile)
            }
            follow_VM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = follow_VM.alertMessage {
                        self!.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            
            follow_VM.updateLoadingStatus = { [weak self] () in
                DispatchQueue.main.async {
                    if follow_VM.isLoading ?? false {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            follow_VM.callFollowerService = true
            }
        }else if status == .requested{
            
            follow_VM.responseRecieved = {
                let user = cell.user!
                 user.isRequested = "0"
                cell.setUser(user: user, showPets: cell.showPets, isMyProfile: cell.isMyProfile)
            }
            
            follow_VM.updateLoadingStatus = { [weak self] () in
                DispatchQueue.main.async {
                    if follow_VM.isLoading ?? false {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            follow_VM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = follow_VM.alertMessage {
                        self!.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            
            if let isPrivate = user.isProfilePrivate, isPrivate.boolValue == true {
                follow_VM.callRequestUserFollowService = true
            }else{
                // public check for follow unfoolow
                follow_VM.callFollowerService = true
            }
            
        }
        else
        {
            
            follow_VM.responseRecieved = {
                let user = cell.user!
                
                if let isPrivate = user.isProfilePrivate, isPrivate.boolValue == true {
                    user.isRequested = "1"
                }else{
                    // public check for follow unfoolow
                    user.isFollowed = 1
                }
                cell.setUser(user: user, showPets: cell.showPets, isMyProfile: cell.isMyProfile)
            }
            
            follow_VM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if follow_VM.isLoading {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            follow_VM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = follow_VM.alertMessage {
                        self!.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            
            if let isPrivate = user.isProfilePrivate, isPrivate.boolValue == true {
                follow_VM.callRequestUserFollowService = true
            }else{
                // public check for follow unfoolow
                follow_VM.callFollowerService = true
            }
        }
        
    }
    
    
    
    
    func editProfileImagePressed(cell:ProfileCell) {
        
        photoManager = PhotoManager(navigationController: self.navigationController!, allowEditing: true, videoType: false, callback: { (pickedImage,urlVideo)  in
            //self.questionaireModel.profileImage = pickedImage ?? #imageLiteral(resourceName: "TempProfile")
            if let image = pickedImage {
                
                
                let profile_VM = Profile_VM()
                profile_VM.userId = CommonFunctions.getuserFromDefaults().userId!
                profile_VM.name = self.user?.name!
                profile_VM.imageData = image.pngData()
                
                profile_VM.updateLoadingStatus = {
                    DispatchQueue.main.async {
                        if profile_VM.isLoading {
                            LoaderView.shared.showLoader()
                        } else {
                            LoaderView.shared.hideLoader()
                        }
                    }
                }
                
                profile_VM.responseRecieved = {
                                  
                                  if let message = profile_VM.alertMessage{
                                      DispatchQueue.main.async {
                                          self.showAlert(self , message: message)
                                      }
                                  }
                    let cache = ImageCache.default
                    // cache.clearMemoryCache()
                     //cache.clearDiskCache { print("Done") }
                     //https://cuddl.app/admin/public/uploads/user_image/421_user_image.jpg
                     
                        cache.removeImage(forKey: self.user!.userImage!)
                                cell.setImageProfile(image: image)
                              }
                              
                profile_VM.showAlertClosure = {[weak self] () in
                                  DispatchQueue.main.async {
                                      if let message = profile_VM.alertMessage {
                                       self!.showAlert(self!, message: message)
                                      }
                                  }
                              }
                              
                profile_VM.callUpdateProfileService = true
                
            }
            
        })
    }
}


