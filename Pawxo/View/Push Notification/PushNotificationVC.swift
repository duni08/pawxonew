//
//  PushNotificationVC.swift
//  Pawxo
//
//  Created by Apple on 15/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class PushNotificationVC: UIViewController {
    
    var pushCellIdentifier = "PushNotificationCell"

    @IBOutlet weak var pushNotification_tbl: UITableView!
    
    var settings = NotificationSetting()
    
    var push_VM = PushNotification_VM()
    
    var pushSettings = [PushSetting]()
    
    var isUpdated = false
    
    var titleArr = ["Likes your post","Comment on your post","Mention you in a post","Mention you in a post comment","Sends you follow request","Start following you","Likes your forum","Comment on your forum","Mention you in a forum comment"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSettings(settings: settings)
        fetchPushNotificationSetting()
    }
    
    
    func setUpSettings(settings:NotificationSetting){
        self.pushSettings.removeAll()
        let likesSetting = PushSetting()
        likesSetting.title = titleArr[0]
        likesSetting.isEnabled = settings.likePush ?? 0
        self.pushSettings.append(likesSetting)
        
        let commentSetting = PushSetting()
        commentSetting.title = titleArr[1]
        commentSetting.isEnabled = settings.commentPush ?? 0
        self.pushSettings.append(commentSetting)
        
        let tagPostPushSetting = PushSetting()
        tagPostPushSetting.title = titleArr[2]
        tagPostPushSetting.isEnabled = settings.tagPostPush ?? 0
        self.pushSettings.append(tagPostPushSetting)
        
        let tagCommentPushSetting = PushSetting()
        tagCommentPushSetting.title = titleArr[3]
        tagCommentPushSetting.isEnabled = settings.tagCommentPush ?? 0
        self.pushSettings.append(tagCommentPushSetting)
        
        let receive_follow_request_pushSetting = PushSetting()
        receive_follow_request_pushSetting.title = titleArr[4]
        receive_follow_request_pushSetting.isEnabled = settings.receive_follow_request_push ?? 0
        self.pushSettings.append(receive_follow_request_pushSetting)
        
        let receiveFollowPushSetting = PushSetting()
        receiveFollowPushSetting.title = titleArr[5]
        receiveFollowPushSetting.isEnabled = settings.receiveFollowPush ?? 0
        self.pushSettings.append(receiveFollowPushSetting)
        
        let like_forum_pushSetting = PushSetting()
        like_forum_pushSetting.title = titleArr[6]
        like_forum_pushSetting.isEnabled = settings.like_forum_push ?? 0
        self.pushSettings.append(like_forum_pushSetting)
        
        let comment_forum_pushSetting = PushSetting()
        comment_forum_pushSetting.title = titleArr[7]
        comment_forum_pushSetting.isEnabled = settings.comment_forum_push ?? 0
        self.pushSettings.append(comment_forum_pushSetting)
        
        let mention_commentSetting = PushSetting()
        mention_commentSetting.title = titleArr[8]
        mention_commentSetting.isEnabled = settings.mention_comment ?? 0
        self.pushSettings.append(mention_commentSetting)
        
       
        
        self.pushNotification_tbl.reloadData()
        
    }
    
    func fetchPushNotificationSetting(){
        let userLogin = CommonFunctions.getuserFromDefaults()
        push_VM.userId = userLogin.userId
        
        push_VM.responseRecieved = {
            self.settings = self.push_VM.settings!
            // refresh content
            self.isUpdated = true
            self.setUpSettings(settings: self.settings)
            self.pushNotification_tbl.reloadData()
        }
        
        push_VM.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.push_VM.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        
        push_VM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if self.push_VM.isLoading {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        push_VM.callGetSettingService = true
    }
    
    func setUIEnable(){
        
    }
    
    
    
    @IBAction func backButtonTapped(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension PushNotificationVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pushSettings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PushNotificationCell =  tableView.dequeueReusableCell(withIdentifier: pushCellIdentifier, for: indexPath) as! PushNotificationCell
        //cell.homeCellDelegate = self
        //cell.indexNumber = indexPath
        
        cell.setSettings(setting: pushSettings[indexPath.row])
        
        cell.index = indexPath.row
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
}


extension PushNotificationVC : PushNotificationCellProtocol {
    func switchChanged(cell: PushNotificationCell) {
        // do stuff
        
        let statusSwitch = cell.swicth_Btn
        
        if self.isUpdated {
            
            let userLogin = CommonFunctions.getuserFromDefaults()
            push_VM.userId = userLogin.userId
            
            
            switch statusSwitch?.tag {
            case 0:
                push_VM.likePush = "\((statusSwitch?.isOn.intValue)!)"
            case 1:
                push_VM.commentPush = "\((statusSwitch?.isOn.intValue)!)"
            case 2:
                push_VM.tagPostPush = "\((statusSwitch?.isOn.intValue)!)"
            case 3:
                push_VM.tagCommentPush = "\((statusSwitch?.isOn.intValue)!)"
            case 4:
                push_VM.receive_follow_request_push = "\((statusSwitch?.isOn.intValue)!)"
            case 5:
                push_VM.receiveFollowPush = "\((statusSwitch?.isOn.intValue)!)"
            case 6:
                push_VM.like_forum_push = "\((statusSwitch?.isOn.intValue)!)"
            case 7:
                push_VM.comment_forum_push = "\((statusSwitch?.isOn.intValue)!)"
            default:
                push_VM.mention_comment = "\((statusSwitch?.isOn.intValue)!)"
            }
            
            
            push_VM.responseRecieved = {
                
                // refresh content
                let setting = self.pushSettings[statusSwitch!.tag]
                setting.isEnabled = (setting.isEnabled == 0) ? 1 : 0
                cell.setSettings(setting: setting)
            }
            
            push_VM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if self.push_VM.isLoading {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            push_VM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = self?.push_VM.alertMessage {
                        self?.showAlert(self ?? UIViewController(), message: message)
                        let setting = self!.pushSettings[statusSwitch!.tag]
                        cell.setSettings(setting: setting)
                    }
                }
            }
            push_VM.callUpdateSettingService = true
            
        }else{
            statusSwitch?.setOn(false, animated: true)
            let message = "Setings not Updated from Account, PLease refresh"
            let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "Refresh", style: .default, handler: { action in
                self.fetchPushNotificationSetting()
            }))
            alertController.view.tintColor = UIColor.black
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    
    
}
