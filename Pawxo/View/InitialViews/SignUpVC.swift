//
//  SignUpVC.swift
//  Pawxo
//
//  Created by 42works on 27/12/19.
//  Copyright © 2019 42works. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {

  //  MARK:- PROPERTIES
    
    @IBOutlet weak var nameTF: RegularTextField!
    @IBOutlet weak var emailTF: RegularTextField!
    @IBOutlet weak var usernameTF: RegularTextField!
    @IBOutlet weak var passwordTF: RegularTextField!
    @IBOutlet weak var confirmPasswordTF: RegularTextField!
    @IBOutlet weak var emailOkImageView: UIImageView!
    @IBOutlet weak var nameImgVew: UIImageView!
    @IBOutlet weak var userName_ImgVew: UIImageView!
    @IBOutlet weak var passwordImgVew: UIImageView!
    @IBOutlet weak var confirmImgVew: UIImageView!
    
    var signUpVM : Register_VM?
    
  
    //  MARK:- LIFECYCLE

    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
  
    //  MARK:- UIBUTTON ACTIONS
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        
        signUpVM = Register_VM()
        signUpVM?.email = emailTF.text
        signUpVM?.name = nameTF.text
        signUpVM?.username = usernameTF.text
        signUpVM?.pwd = passwordTF.text
        signUpVM?.confirmPwd = confirmPasswordTF.text
        
        let validData = signUpVM?.isValidRegisterData()
        if validData?.0 == true{
            
//            let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
//            let vc_TABBAR = storyboardMain.instantiateViewController(withIdentifier: "MainTABViewController") as! MainTABViewController
//            self.navigationController?.pushViewController(vc_TABBAR, animated: true)
          self.initRegisterVM()
            
        }else{
            self.showAlert(self, message: validData?.1 ?? "")
        }
        
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}



//MARK:- intialize register view model
extension SignUpVC {

    
func initRegisterVM(){
    
    signUpVM?.updateLoadingStatus = { [weak self] () in
        DispatchQueue.main.async {
            if self?.signUpVM?.isLoading ?? false {
                //self?.showLoader(self)
                 LoaderView.shared.showLoader()
            } else {
               // self?.hideLoader(self)
                 LoaderView.shared.hideLoader()
            }
        }
    }
    
    signUpVM?.showAlertClosure = {[weak self] () in
        DispatchQueue.main.async {
            if let message = self?.signUpVM?.alertMessage {
                self?.showAlert(self ?? UIViewController(), message: message)
            }
        }
    }
    
    signUpVM?.responseRecieved = {
        
      
       //to be done as response will be changed
      //  self?.showAlert(self ?? UIViewController(), message: "Registered successfully")
       // self?.openTutorialsScreen() Open Home Screen or Tutorial Screen
        
        if let status = CommonFunctions.getuserFromDefaults().agreement_Status, status == 1{
            DispatchQueue.main.async {
                UserDefaults.setObject(true, forKey: UserDefault.isLoggedIn)
            
            if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
            }
        }else{
            DispatchQueue.main.async {
            
                if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "EulaVC") as? EulaVC{
                    vc.isFromSignUP = true
                self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        
        
//        UserDefaults.setObject(true, forKey: UserDefault.isLoggedIn)
//
//        if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
//            self?.navigationController?.pushViewController(vc, animated: true)
//        }
        
    }
    
    signUpVM?.email = emailTF?.text ?? ""
    //signUpVM?.accountType = AccountType.email
    signUpVM?.name = nameTF?.text ?? ""
    signUpVM?.username = usernameTF?.text ?? ""
    signUpVM?.password = passwordTF?.text ?? ""
    signUpVM?.confirmPwd = confirmPasswordTF?.text ?? ""
    signUpVM?.deviceToken = UserDefaults.objectForKey(UserDefault.deviceToken)
    
    //Call Api
    
    signUpVM?.callRegisterUserService = true
    
}

}


extension SignUpVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var updatedText = ""
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
           
        }
        print(updatedText)
       
        if textField == emailTF{
            self.emailOkImageView.isHidden = !updatedText.isValidEmail()
        }
        
        if textField == nameTF{
            if updatedText.length == 0{
                self.nameImgVew.isHidden = true
            }else{
                self.nameImgVew.isHidden = false
            }
        }
        if textField == usernameTF{
        if updatedText.length == 0{
            self.userName_ImgVew.isHidden = true
        }else{
            self.userName_ImgVew.isHidden = false
        }
        }
        if textField == passwordTF{
            if (updatedText.length >= 6 ){
                 self.passwordImgVew.isHidden = false
            }else{
                self.passwordImgVew.isHidden = true
            }
        }
      if textField == confirmPasswordTF{
            if passwordTF.text == updatedText{
                self.confirmImgVew.isHidden = false
            }else{
               self.confirmImgVew.isHidden = true
            }
        }
    
          return true
    }
    
}
