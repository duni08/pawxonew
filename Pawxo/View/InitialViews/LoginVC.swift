//
//  LoginVC.swift
//  Pawxo
//
//  Created by 42works on 26/12/19.
//  Copyright © 2019 42works. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import TwitterKit
import TwitterCore
import AuthenticationServices


class LoginVC: UIViewController {

    @IBOutlet weak var mainLoginView: UIView!
    @IBOutlet weak var PawLogoHeight: NSLayoutConstraint!
    @IBOutlet weak var pawlogoWidth: NSLayoutConstraint!
    @IBOutlet weak var topLogo: UIImageView!
    @IBOutlet weak var centerLogo: UIImageView!
    @IBOutlet weak var centerLogoYConstraint: NSLayoutConstraint!
    @IBOutlet weak var centerLogoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var centerLogoConstraint: NSLayoutConstraint!
    @IBOutlet weak var pswdImgVew: UIImageView!
    
    @IBOutlet weak var emailOkImage: UIImageView!
    var loginVM : LoginViewModel?
    var registerViewModel: Register_VM?
    
    //Facebook variables
    var strEmail: String?
    var strName: String?
    var strUserName: String?
    var strProfilePicUrl: String?
    var fbImageData : Data?
    var strCoverPicUrl: String?
    var strFacebookId: String?
    var strDateOfBirth: String?
    var count = 0
    
    @IBOutlet weak var emailTF: RegularTextField!
    
    
    @IBOutlet weak var outerAppleLoginView: UIView!
    
    @IBOutlet weak var outerSocialLoginConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var passwordTF: RegularTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.mainLoginView.alpha = 0.0
        
       // self.setImage(color: AppColor.colorBubbleGrey, imgView: self.topLogo)
        
        // Do any additional setup after loading the view.
        emailOkImage.isHidden = true
       // emailTF.text = "john@yopmail.com"
        //passwordTF.text = "123456"

        self.emailOkImage.isHidden = !emailTF.text!.isValidEmail()
        
        //self.setupSOAppleSignIn()
        
        
    }
    
    
    @IBAction func appleSignInPressed(_ sender: Any) {
        self.actionHandleAppleSignin()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupSOAppleSignIn()
    }
    
    func setupSOAppleSignIn() {
        if #available(iOS 13.0, *) {
//            let btnAuthorization = ASAuthorizationAppleIDButton()
//            btnAuthorization.frame = CGRect(x: 5, y: 5, width: 40 , height: 40)
//
//            btnAuthorization.addTarget(self, action: #selector(actionHandleAppleSignin), for: .touchUpInside)
//            self.outerAppleLoginView.addSubview(btnAuthorization)
            //btnAuthorization.center = self.outerAppleLoginView.center
        } else {
            self.outerSocialLoginConstraint.constant = 110
            // Fallback on earlier versions
        }
        
    }
    
   
    
    // Perform acton on click of Sign in with Apple button
    @objc func actionHandleAppleSignin() {
        if #available(iOS 13.0, *) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
        }
    }
    
    
    public func setImage(color: UIColor, imgView: UIImageView) {
        
        if #available(iOS 13.0, *) {
            imgView.image =  imgView.image?.withTintColor(color, renderingMode: .alwaysTemplate)
        } else {
            // Fallback on earlier versions
           imgView.image  = imgView.image?.withRenderingMode(.alwaysTemplate)
            imgView.tintColor = color
        }
//        self.setImage(imgView.image?.withRenderingMode(.alwaysTemplate) i )
    }
    
    override func viewDidLayoutSubviews() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            //self.animateLogo()
        }
    }
    
    func animateLogo(){
        self.centerLogoConstraint.constant = -100
        self.centerLogoYConstraint.constant = -20
        
        self.PawLogoHeight.constant = 350
        self.pawlogoWidth.constant  = 300
        self.centerLogoHeightConstraint.constant = 120
        
        UIView.animate(withDuration: 1.0, animations: {
            self.view.layoutIfNeeded()
            self.centerLogo.alpha = 0
            self.topLogo.alpha = 1.0
            self.mainLoginView.alpha = 1.0
        }) { (completed) in
            self.centerLogo.isHidden = true
            self.topLogo.alpha = 1
        }
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        // Add Validation checks
        loginVM = LoginViewModel()
        loginVM?.email = emailTF.text
        loginVM?.password = passwordTF.text
        let validLogin =  loginVM?.isValidLoginData()
        if validLogin?.0 == true{

           initLoginVM()
        }else{
            self.showAlert(self, message: validLogin?.1 ?? "")
        }
        
        //self.performSegue(withIdentifier: "", sender: self)
    }
    
    
    
    
    @IBAction func facebookButtonPressed(_ sender: Any) {
        self.loginWithFacebook()
    }
    
    //MARK:- helper methods
    func loginWithFacebook() {
        
        SocialIntegration.shared.getFacebookUserInfo(viewController: self, completion:{ (_ status,_ response, _ msg) -> () in
            
            if status == true {
                  
              if  let emailStr = response.object(forKey: "email") as? String {
                    self.strEmail = emailStr
                }
              
                if  let idStr = response.object(forKey: "id") as? String {
                    self.strFacebookId = idStr
                }
                
                if  let nameStr = response.object(forKey: "name") as? String {
                    self.strName = nameStr
                }
                
                if let profilePicData = response.object(forKey: "profilepicData") as? Data {
                    self.fbImageData = profilePicData
                }
                
                self.initFacebookLoginVM()
            
            }else{
                self.showAlert(self, message: msg)
            }
        })

    }
    
    
    func initAppleLoginVM() {
        registerViewModel = Register_VM()
        registerViewModel?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.registerViewModel?.isLoading ?? false {
                    //self?.showLoader(self)
                    LoaderView.shared.showLoader()
                } else {
                    //self?.hideLoader(self)
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        registerViewModel?.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.registerViewModel?.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        
        registerViewModel?.responseRecieved = {
            kAppDelegate.registerPush()
            if let status = CommonFunctions.getuserFromDefaults().agreement_Status, status == 1{
                
                UserDefaults.setObject(true, forKey: UserDefault.isLoggedIn)
                
                
                if let isSignup = CommonFunctions.getuserFromDefaults().is_signup, isSignup == 0{
                    DispatchQueue.main.async {
                    if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    }
                }
                else{
                    if breedArray.count >  0 {
                        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
                         let vc_questionare = storyboardPet.instantiateViewController(withIdentifier: "QuestionnaireViewController") as! QuestionnaireViewController
                        vc_questionare.fromSignUpLogin = true
                        self.navigationController?.pushViewController(vc_questionare, animated: true)
                        
                    }else{
                        self.callBreeds()
                    }
                }
                
            }else{
                DispatchQueue.main.async {
                
                    if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "EulaVC") as? EulaVC{
                        vc.isFromSignUP = true
                    self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
        
        registerViewModel?.email = strEmail ?? ""
        registerViewModel?.accountType = AccountType.facebook
        registerViewModel?.name = strName ?? ""
        registerViewModel?.username = strName!.replacingOccurrences(of: " ", with: "")
        registerViewModel?.facebookId = strFacebookId ?? ""
        registerViewModel?.coverPicUrl = strCoverPicUrl ?? ""
        registerViewModel?.profilePicUrl = strProfilePicUrl ?? ""
        registerViewModel?.dateOfBirth = strDateOfBirth ?? ""
        registerViewModel?.deviceToken = UserDefaults.objectForKey(UserDefault.deviceToken)
        registerViewModel?.deviceId = CommonFunctions.getDeviceId()
        //Call Api
        registerViewModel?.callSocialLoginService = true
    }
    
    
    
    func initFacebookLoginVM() {
        registerViewModel = Register_VM()
        registerViewModel?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.registerViewModel?.isLoading ?? false {
                    //self?.showLoader(self)
                    LoaderView.shared.showLoader()
                } else {
                    //self?.hideLoader(self)
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        registerViewModel?.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.registerViewModel?.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        
        registerViewModel?.responseRecieved = {
            kAppDelegate.registerPush()
            if let status = CommonFunctions.getuserFromDefaults().agreement_Status, status == 1{
                
                UserDefaults.setObject(true, forKey: UserDefault.isLoggedIn)
                
                
                if let isSignup = CommonFunctions.getuserFromDefaults().is_signup, isSignup == 0{
                    DispatchQueue.main.async {
                        
                    
                    if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    }
                }
                else{
                    if breedArray.count >  0 {
                        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
                         let vc_questionare = storyboardPet.instantiateViewController(withIdentifier: "QuestionnaireViewController") as! QuestionnaireViewController
                        vc_questionare.fromSignUpLogin = true
                        self.navigationController?.pushViewController(vc_questionare, animated: true)
                        
                    }else{
                        self.callBreeds()
                    }
                }
                
            }else{
                DispatchQueue.main.async {
                
                    if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "EulaVC") as? EulaVC{
                        vc.isFromSignUP = true
                    self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
        
        registerViewModel?.email = strEmail ?? ""
        registerViewModel?.accountType = AccountType.facebook
        registerViewModel?.name = strName ?? ""
        registerViewModel?.username = strName!.replacingOccurrences(of: " ", with: "")
        registerViewModel?.facebookId = strFacebookId ?? ""
        registerViewModel?.coverPicUrl = strCoverPicUrl ?? ""
        registerViewModel?.profilePicUrl = strProfilePicUrl ?? ""
        registerViewModel?.fbImageData = self.fbImageData
        registerViewModel?.dateOfBirth = strDateOfBirth ?? ""
        registerViewModel?.deviceToken = UserDefaults.objectForKey(UserDefault.deviceToken)
        registerViewModel?.deviceId = CommonFunctions.getDeviceId()
        //Call Api
        registerViewModel?.callSocialLoginService = true
    }
    
    
    func initTwitterLoginVM() {
        registerViewModel = Register_VM()
        registerViewModel?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.registerViewModel?.isLoading ?? false {
                    //self?.showLoader(self)
                    LoaderView.shared.showLoader()
                } else {
                    //self?.hideLoader(self)
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        registerViewModel?.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.registerViewModel?.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        
        registerViewModel?.responseRecieved = {
            kAppDelegate.registerPush()
           if let status = CommonFunctions.getuserFromDefaults().agreement_Status, status == 1{
                
                UserDefaults.setObject(true, forKey: UserDefault.isLoggedIn)
                
                
                if let isSignup = CommonFunctions.getuserFromDefaults().is_signup, isSignup == 0{
                    DispatchQueue.main.async {
                        
                    
                    if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    }
                }
                else{
                    if breedArray.count >  0 {
                        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
                         let vc_questionare = storyboardPet.instantiateViewController(withIdentifier: "QuestionnaireViewController") as! QuestionnaireViewController
                        vc_questionare.fromSignUpLogin = true
                        self.navigationController?.pushViewController(vc_questionare, animated: true)
                        
                    }else{
                        self.callBreeds()
                    }
                }
                
            }else{
                DispatchQueue.main.async {
                
                    if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "EulaVC") as? EulaVC{
                        vc.isFromSignUP = true
                    self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
        
        registerViewModel?.email = strEmail ?? ""
        registerViewModel?.accountType = AccountType.twitter
        registerViewModel?.name = strName ?? ""
        registerViewModel?.username = strUserName!.replacingOccurrences(of: " ", with: "")
        registerViewModel?.facebookId = strFacebookId ?? ""
        registerViewModel?.coverPicUrl = strCoverPicUrl ?? ""
        registerViewModel?.profilePicUrl = strProfilePicUrl ?? ""
        registerViewModel?.dateOfBirth = strDateOfBirth ?? ""
        registerViewModel?.deviceToken = UserDefaults.objectForKey(UserDefault.deviceToken)
        registerViewModel?.deviceId = CommonFunctions.getDeviceId()
        //Call Api
        registerViewModel?.callSocialLoginService = true
    }
    
    
    @IBAction func twitterButtonPressed(_ sender: Any) {
        
        SocialIntegration.shared.getTwitterUserInfo(viewController: self, completion: { (_ status,_ response, _ msg) -> () in
            
                if status == true {
                
                    if let emailStr = response.object(forKey: "email") as? String {
                        self.strEmail = emailStr
                    }
                    
                    if let idStr = response.object(forKey: "id") as? String {
                        self.strFacebookId = idStr
                    }
                    
                    if let userStr = response.object(forKey: "username") as? String {
                        self.strUserName = userStr
                    }
                   
                    
                    if let nameStr = response.object(forKey: "name") as? String {
                       self.strName = nameStr
                    }
                    
                    if let profilePicUrlStr = response.object(forKey: "profileImageURL") as? String {
                        self.strProfilePicUrl = profilePicUrlStr
                    }
                    
                    if let coverPicUrl = response.object(forKey: "") as? String {
                        self.strCoverPicUrl = coverPicUrl
                    }
                    self.initTwitterLoginVM()
                }else{
                    self.showAlert(self, message: msg)
                }
            
            })
    }
    
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "showSignUp", sender: self)
    }
    
    
    
    @IBAction func forgotPasswordButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "showForgotPassword", sender: self)
    }
    
}



@available(iOS 13.0, *)
@available(iOS 13.0, *)
extension LoginVC: ASAuthorizationControllerDelegate {
    
    // ASAuthorizationControllerDelegate function for authorization failed
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
        //self.showAlert(self, message: error.localizedDescription)
    }
    
    // ASAuthorizationControllerDelegate function for successful authorization
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Create an account as per your requirement
            let appleId = appleIDCredential.user
            let appleUserFirstName = appleIDCredential.fullName?.givenName
            let appleUserLastName = appleIDCredential.fullName?.familyName
            let appleUserEmail = appleIDCredential.email
            
            self.strEmail = ""
            
            
            if let email = appleUserEmail {
                
                // First time sign In
                
                
                if email.contains("privaterelay.appleid.com"){
                    
                }else{
                    self.strEmail = appleUserEmail
                }
                
                if let firstName = appleUserFirstName, let lastName = appleUserLastName{
                    self.strName = "\(firstName) \(lastName)"
                }
                
                
                
                let dict  : NSDictionary = ["username":self.strName!,"email":self.strEmail!,"userId":appleId]
                
                do {
                let dataExample: Data =  try NSKeyedArchiver.archivedData(withRootObject: dict, requiringSecureCoding: true)
                    
                    let status = KeyChain.save(key: appleId, data: dataExample)
                    print("status: ", status)
                    
                }catch{
                    
                }
                
                
                
               

                
                
                print(self.strEmail,self.strName)
                UserDefaults.standard.setValue(appleId, forKey: "appleId")
                self.initAppleLoginVM()
            }else{
                
                if let receivedData = KeyChain.load(key: appleId) {
                    
                    do {
                        let dictionary: NSDictionary? = try NSKeyedUnarchiver.unarchivedObject(ofClass: NSDictionary.self, from: receivedData)
                    
//                    let encoder = JSONEncoder()
//                    if let jsonData = try? encoder.encode(dic) {
//                        if let jsonString = String(data: jsonData, encoding: .utf8) {
//                            print(jsonString)
//                        }
//                    }
//                    let jsonString = String(data: receivedData, encoding: .utf8)!
                    //let result = receivedData.to(type: NSDictionary.self)
                    print("result: ", dictionary)
                        
                        self.strEmail = dictionary!["email"] as? String
                        self.strName = dictionary!["username"] as? String
                        self.initAppleLoginVM()
                        UserDefaults.standard.setValue(appleId, forKey: "appleId")
                        
                    }catch{
                        
                    }
                }
                
                //self.showAlert(self, message: "Sign in  ")
            }
            
            //Write your code
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            let appleUsername = passwordCredential.user
            let applePassword = passwordCredential.password
            //Write your code
        }
    }
    
    
    
    private func setupAppleIDCredentialObserver() {
      let authorizationAppleIDProvider = ASAuthorizationAppleIDProvider()

      authorizationAppleIDProvider.getCredentialState(forUserID: "currentUserIdentifier") { (credentialState: ASAuthorizationAppleIDProvider.CredentialState, error: Error?) in
        if let error = error {
          print(error)
          // Something went wrong check error state
          return
        }
        switch (credentialState) {
        case .authorized:
          //User is authorized to continue using your app
          break
        case .revoked:
          //User has revoked access to your app
          break
        case .notFound:
          //User is not found, meaning that the user never signed in through Apple ID
          break
        default: break
        }
      }
    }
    
}




extension LoginVC: ASAuthorizationControllerPresentationContextProviding {
    //For present window
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}


extension LoginVC{
    //MARK:- intialize login view model
    func initLoginVM(){
        //For loading indicator
        loginVM?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.loginVM?.isLoading ?? false {
                     LoaderView.shared.showLoader()
                   // self?.showLoader(self,message: "")
                } else {
                     LoaderView.shared.hideLoader()
                    // self?.hideLoader(self)
                }
            }
        }
        
        //For Alert message
        loginVM?.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.loginVM?.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        
        //Response recieved
        loginVM?.responseRecieved = {
            //to be done as response will be changed
            //self?.showAlert(self ?? UIViewController(), message: "Logged in successfully")
            //Save User model
            
            kAppDelegate.registerPush()
            if let status = CommonFunctions.getuserFromDefaults().agreement_Status, status == 1{
                
                UserDefaults.setObject(true, forKey: UserDefault.isLoggedIn)
                
                
                if  CommonFunctions.getuserFromDefaults().hasPets{
                    DispatchQueue.main.async {
                        
                    
                    if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    }
                }
                else{
                    if breedArray.count >  0 {
                        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
                         let vc_questionare = storyboardPet.instantiateViewController(withIdentifier: "QuestionnaireViewController") as! QuestionnaireViewController
                        vc_questionare.fromSignUpLogin = true
                        self.navigationController?.pushViewController(vc_questionare, animated: true)
                        
                    }else{
                        self.callBreeds()
                    }
                }
                
            }else{
                DispatchQueue.main.async {
                
                    if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "EulaVC") as? EulaVC{
                    self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            
//            DispatchQueue.main.async {
//                UserDefaults.setObject(true, forKey: UserDefault.isLoggedIn)
//
//            if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//            }
        }
        loginVM?.email = emailTF.text ?? ""
        loginVM?.password = passwordTF.text ?? ""
        loginVM?.callLoginService = true
        
        
    }
    
    
    
    func callBreeds(){
        let breedVM = Breed_VM()
        breedVM.responseRecievedBreed = { breed in
            breedArray = breed
            //self.tableViewObj.reloadData()
            print("self.breedArray== \(breedArray.count)")
            let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
            let vc_questionare = storyboardPet.instantiateViewController(withIdentifier: "QuestionnaireViewController") as! QuestionnaireViewController
            vc_questionare.fromSignUpLogin = true
            self.navigationController?.pushViewController(vc_questionare, animated: true)
            
        }
        
        breedVM.showAlertClosure = {
            self.getBreeeds(count:self.count)
        }
        breedVM.callGetBreedListService = true
    }
    
    
    func getBreeeds(count:Int){
        
        if count > 2 {
            if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            let alertController = UIAlertController(title: AppMessages.retryAlert, message: nil, preferredStyle: UIAlertController.Style.alert)
            
            
            let somethingAction = UIAlertAction(title: "Retry", style: .default, handler: {(alert: UIAlertAction!) in print("")
                self.count += 1
                self.callBreeds()
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")
                
                if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
            })
            
            alertController.addAction(somethingAction)
            alertController.addAction(cancelAction)
            alertController.view.tintColor = UIColor.black
            DispatchQueue.main.async {
                self.present(alertController, animated: true, completion:{})
            }
        }
        
    }
}


extension LoginVC:TWTRComposerViewControllerDelegate {
    
    
    func composerDidCancel(_ controller: TWTRComposerViewController) {
        print("composerDidCancel, composer cancelled tweet")
    }
    
    func composerDidSucceed(_ controller: TWTRComposerViewController, with tweet: TWTRTweet) {
        print("composerDidSucceed tweet published")
    }
    func composerDidFail(_ controller: TWTRComposerViewController, withError error: Error) {
        print("composerDidFail, tweet publish failed == \(error.localizedDescription)")
    }
    
}



extension LoginVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var updatedText = ""
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
           
        }
        
        print(updatedText)
        if textField == emailTF{
            self.emailOkImage.isHidden = !updatedText.isValidEmail()
        }
        
        if textField == passwordTF{
             if (updatedText.length >= 6 ){
                    self.pswdImgVew.isHidden =  false
            }else{
                self.pswdImgVew.isHidden =  true
            }
        }
     
        return true
    }
    
   
    
    
}
