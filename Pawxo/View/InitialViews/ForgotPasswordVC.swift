//
//  ForgotPasswordVC.swift
//  Pawxo
//
//  Created by 42works on 27/12/19.
//  Copyright © 2019 42works. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    
    @IBOutlet weak var emailTF: RegularTextField!
    
    var forgotVM : ForgetPasswordViewModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    


    @IBAction func resetPasswordPressed(_ sender: Any) {
        
        forgotVM = ForgetPasswordViewModel()
        forgotVM?.email = emailTF.text
        
        let validLogin =  forgotVM?.isFormValidForgotPassword()
        if validLogin?.0 == true{
            initForgotVM()
        }else{
            self.showAlert(self, message: validLogin?.1 ?? "")
        }
    }
    
    @IBAction func backToLoginPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}


extension ForgotPasswordVC{
    //MARK:- intialize login view model
    func initForgotVM(){
        //For loading indicator
        forgotVM?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.forgotVM?.isLoading ?? false {
                   LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        //For Alert message
        forgotVM?.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.forgotVM?.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        
        //Response recieved
        forgotVM?.responseRecieved = { 
            //to be done as response will be changed
            //self?.showAlert(self ?? UIViewController(), message: "Please check your email to reset new password.")
            //Save User model
            DispatchQueue.main.async {
            self.emailTF?.text = ""
            
            if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC{
                self.navigationController?.pushViewController(vc, animated: true)
            }
            }
            /*
            UserDefaults.setObject(true, forKey: UserDefault.isLoggedIn)
            
            var isShowCheck1 = false
            var isShowCheck2 = false
            if let check1 = UserDefaults.standard.value(forKey: UserDefault.isShowCheck1) as? Bool{
                isShowCheck1 = check1
            }
            if let check2 = UserDefaults.standard.value(forKey: UserDefault.isShowCheck2) as? Bool{
                isShowCheck2 = check2
            }
            if isShowCheck1 == true && isShowCheck2 == true{
                if let vc = UIStoryboard(name: StoryboardName.login, bundle: nil).instantiateViewController(withIdentifier: "Camera_VC") as? Camera_VC{
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            }else{
                if let vc = UIStoryboard(name: StoryboardName.login, bundle: nil).instantiateViewController(withIdentifier: "HowToUse_VC") as? HowToUse_VC{
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            } */
        }
        forgotVM?.email = emailTF?.text ?? ""
        forgotVM?.callForgetPasswordService = true
    }
}
