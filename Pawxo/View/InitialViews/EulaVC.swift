//
//  EulaVC.swift
//  Pawxo
//
//  Created by 42works on 11/03/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class EulaVC: UIViewController {

    
    @IBOutlet weak var acceptButton: SemiBoldButton!
    
    @IBOutlet weak var eTextView: UITextView!
    
    var count = 0
    
    var isFromSignUP = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        acceptButton.isEnabled = false
        self.acceptButton.alpha = 0.5
        // Do any additional setup after loading the view.
        let htmlString = """
            <html>
            <title>HTML</title>
            <head>
              <style>
                body{
                    font-family: arial;
                    color:#555;
                 }

                 h1{
                  margin-bottom:20px
                 }
                 p{
                   margin-bottom:20px;
                   font-size:15px;
                 }
                h3{
                  margin-bottom:20px;
                  font-size:17px;
                }
                 ul li{
                    margin-bottom:8px;
                    font-size:15px;
                 }
              </style>

            </head>
            <body style="">

                <h2>End-User License Agreement (EULA) of Cuddl App</h2>

                <p>This End-User License Agreement ("EULA") is a legal agreement between you and 42Works Info Solutions Pvt Ltd</p>

                <p>This EULA agreement governs your acquisition and use of our Cuddl Appsoftware ("Software") directly from 42Works Info Solutions Pvt Ltd or indirectly through a 42Works Info Solutions Pvt Ltd authorized reseller or distributor (a "Reseller").</p>

                <p>Please read this EULA agreement carefully before completing the installation process and using the Cuddl App software. It provides a license to use the Cuddl App software and contains warranty information, liability disclaimers and no tolerance for objectionable content or abusive users.</p>

                <p>If you register for a free trial of the Cuddl App software, this EULA agreement will also govern that trial. By clicking "accept" or installing and/or using the Cuddl App software, you are confirming your acceptance of the Software and agreeing to become bound by the terms of this EULA agreement.</p>


                <p>If you are entering into this EULA agreement on behalf of a company or other legal entity, you represent that you have the authority to bind such entity and its affiliates to these terms and conditions. If you do not have such authority or if you do not agree with the terms and conditions of this EULA agreement, do not install or use the Software, and you must not accept this EULA agreement.</p>

                <p>This EULA agreement shall apply only to the Software supplied by 42Works Info Solutions Pvt Ltd herewith regardless of whether other software is referred to or described herein. The terms also apply to any 42Works Info Solutions Pvt Ltd updates, supplements, Internet-based services, and support services for the Software, unless other terms accompany those items on delivery. If so, those terms apply. This EULA was created by EULA Template for Cuddl App.</p>

                <h3>License Grant</h3>

                <p>42Works Info Solutions Pvt Ltd hereby grants you a personal, non-transferable, non-exclusive licence to use the Cuddl App software on your devices in accordance with the terms of this EULA agreement.</p>

                <p>You are permitted to load the Cuddl App software (for example a PC, laptop, mobile or tablet) under your control. You are responsible for ensuring your device meets the minimum requirements of the Cuddl App software.</p>

                <p>You are not permitted to:</p>

                <ul>
                    <li>Edit, alter, modify, adapt, translate or otherwise change the whole or any part of the Software nor permit the whole or any part of the Software to be combined with or become incorporated in any other software, nor decompile, disassemble or reverse engineer the Software or attempt to do any such things</li>

                    <li>Reproduce, copy, distribute, resell or otherwise use the Software for any commercial purpose</li>

                    <li>Allow any third party to use the Software on behalf of or for the benefit of any third party
                        Use the Software in any way which breaches any applicable local, national or international law
                    </li>

                    <li>use the Software for any purpose that 42Works Info Solutions Pvt Ltd considers is a breach of this EULA agreement</li>

                    <li>Under no circumstances, an objectionable content will be tolerated. A user violating this will be barred from using the software thereon.</li>

                </ul>
                
                 <p>A user can:</p>

                <ul>

                <li> Ask Cuddl to remove another user’s post with the ‘Report abuse’ option, If a post receives ‘Report Abuse’ more than 3 times, Cuddl will remove the post from the platform, permanently.</li>
                <li>Block another user, if he/she is troubling you or you do not wish to continue the communication. However, you are allowed to unblock the same user by exercising the same option, at your discretion.</li>

                </ul>

                <h3>Intellectual Property and Ownership</h3>

                <p>42Works Info Solutions Pvt Ltd shall at all times retain ownership of the Software as originally downloaded by you and all subsequent downloads of the Software by you. The Software (and the copyright, and other intellectual property rights of whatever nature in the Software, including any modifications made thereto) are and shall remain the property of 42Works Info Solutions Pvt Ltd.</p>

                <p>42Works Info Solutions Pvt Ltd reserves the right to grant licences to use the Software to third parties.</p>

                <h3>Termination</h3>

                <p>This EULA agreement is effective from the date you first use the Software and shall continue until terminated. You may terminate it at any time upon written notice to 42Works Info Solutions Pvt Ltd.</p>

                <p>It will also terminate immediately if you fail to comply with any term of this EULA agreement. Upon such termination, the licenses granted by this EULA agreement will immediately terminate and you agree to stop all access and use of the Software. The provisions that by their nature continue and survive will survive any termination of this EULA agreement.</p>

                <h3>Governing Law</h3>

                <p>This EULA agreement, and any dispute arising out of or in connection with this EULA agreement, shall be governed by and construed in accordance with the laws of in.</p>

            </body>
            </html>
            """

        let htmlData = NSString(string: htmlString).data(using: String.Encoding.unicode.rawValue)

        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]

        let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)

        eTextView.attributedText = attributedString
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func acceptButtonPressed(_ sender: Any) {
        print("Accept Pressed")
        
        let loginVM = LoginViewModel()
        
        loginVM.updateLoadingStatus = { [weak self] () in
                    DispatchQueue.main.async {
                        if loginVM.isLoading ?? false {
                             LoaderView.shared.showLoader()
                           // self?.showLoader(self,message: "")
                        } else {
                             LoaderView.shared.hideLoader()
                            // self?.hideLoader(self)
                        }
                    }
                }
                
                //For Alert message
                loginVM.showAlertClosure = {[weak self] () in
                    DispatchQueue.main.async {
                        if let message = loginVM.alertMessage {
                            self?.showAlert(self ?? UIViewController(), message: message)
                        }
                    }
                }
                
                //Response recieved
                loginVM.responseRecieved = {
                    //to be done as response will be changed
                            DispatchQueue.main.async {
                            UserDefaults.setObject(true, forKey: UserDefault.isLoggedIn)
                           
                                
                                if self.isFromSignUP{
                                    if breedArray.count >  0 {
                                        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
                                         let vc_questionare = storyboardPet.instantiateViewController(withIdentifier: "QuestionnaireViewController") as! QuestionnaireViewController
                                        vc_questionare.fromSignUpLogin = true
                                        self.navigationController?.pushViewController(vc_questionare, animated: true)
                                        
                                    }else{
                                        self.callBreeds()
                                    }
                                }else{
                                    if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                                
                                
                               /* if CommonFunctions.getuserFromDefaults().hasPets{
                                    DispatchQueue.main.async {
                                    
                                    if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                    }
                                }
                                else{
                                    if breedArray.count >  0 {
                                        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
                                         let vc_questionare = storyboardPet.instantiateViewController(withIdentifier: "QuestionnaireViewController") as! QuestionnaireViewController
                                        vc_questionare.fromSignUpLogin = true
                                        self.navigationController?.pushViewController(vc_questionare, animated: true)
                                        
                                    }else{
                                        self.callBreeds()
                                    }
                                }*/
                           }
                }
            loginVM.userId = CommonFunctions.getuserFromDefaults().userId
                
            loginVM.callAcceptEulaService = true

       
    }
    
    
    func callBreeds(){
        let breedVM = Breed_VM()
        breedVM.responseRecievedBreed = { breed in
            breedArray = breed
            //self.tableViewObj.reloadData()
            print("self.breedArray== \(breedArray.count)")
            let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
            let vc_questionare = storyboardPet.instantiateViewController(withIdentifier: "QuestionnaireViewController") as! QuestionnaireViewController
            vc_questionare.fromSignUpLogin = true
            self.navigationController?.pushViewController(vc_questionare, animated: true)
            
        }
        
        breedVM.showAlertClosure = {
            self.getBreeeds(count:self.count)
        }
        breedVM.callGetBreedListService = true
    }
    
    
    func getBreeeds(count:Int){
        
        if count > 2 {
            if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            let alertController = UIAlertController(title: AppMessages.retryAlert, message: nil, preferredStyle: UIAlertController.Style.alert)
            
            
            let somethingAction = UIAlertAction(title: "Retry", style: .default, handler: {(alert: UIAlertAction!) in print("")
                self.count += 1
                self.callBreeds()
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")
                
                if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
            })
            
            alertController.addAction(somethingAction)
            alertController.addAction(cancelAction)
            alertController.view.tintColor = UIColor.black
            DispatchQueue.main.async {
                self.present(alertController, animated: true, completion:{})
            }
        }
        
    }
    
    
    
    @IBAction func rejectButtonPressed(_ sender: Any) {
        print("Reject pressed")
        kAppDelegate.logoutFromApp()
    }
    
    
    
    

    
}


extension EulaVC : UITextViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        bottomRefresh(scrollView)
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            bottomRefresh(scrollView)
        }
    }
    
    func bottomRefresh(_ scrollView : UIScrollView)
       {

           let scrollViewHeight = scrollView.frame.size.height;
           let scrollContentSizeHeight = scrollView.contentSize.height;
           let scrollOffset = scrollView.contentOffset.y;
           if (scrollOffset >  scrollContentSizeHeight - scrollViewHeight - 80)
           {
            self.acceptButton.isEnabled = true
            self.acceptButton.alpha = 1.0
           }
        
       }
}
