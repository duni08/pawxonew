//
//  SplashVC.swift
//  Pawxo
//
//  Created by 42works on 21/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    
    @IBOutlet weak var nextImgView: UIImageView!
    
    
    
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var centerImageViewOne: UIImageView!
    
    @IBOutlet weak var centerImageCenterConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var nextViewCenterConstraint: NSLayoutConstraint!
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        centerImageCenterConstraint.constant = self.view.frame.width
        nextViewCenterConstraint.constant = self.view.frame.width
        //centerImageViewOne.transform = CGAffineTransform(scaleX: 0, y: 0)
       // self.nextView.alpha = 0
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.centerImageCenterConstraint.constant = 0
            UIView.animate(withDuration: 0.6, animations: {
                self.view.layoutIfNeeded()
                //self.centerImageViewOne.transform = CGAffineTransform.identity
            }) { (success) in
                // change image
                self.animateImage(countNew: 1)
            }
        }
        // Do any additional setup after loading the view.
    }
    
    
    func animateImage(countNew:Int){
        self.count = countNew
        if self.count > 6 {
            // finished
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.nextAnimation()
            
            }
            return
        }
        
        UIView.transition(with: centerImageViewOne, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.centerImageViewOne.image = UIImage(named: "img\(self.count)")
            self.count += 1
        }) { (sucess) in
            self.animateImage(countNew: self.count)
        }
        
    }
    
    func nextAnimation(){
        // DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
        UIView.animate(withDuration: 0.4, animations: {
            self.centerImageViewOne.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }) { (success) in
            // change Nextimage
            
                self.nextViewCenterConstraint.constant = 0
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }) { (sucess) in
                // next
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {


//                    let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
//                    let vc_TABBAR = storyboardMain.instantiateViewController(withIdentifier: "MainTABViewController") as! MainTABViewController
//                    let navigationController = UINavigationController(rootViewController: vc_TABBAR)
//                    navigationController.isNavigationBarHidden = true
//                    kAppDelegate.window?.rootViewController = navigationController



//                    UIView.transition(with: vc_TABBAR.view, duration: 0.4, options: .transitionCrossDissolve, animations: {
//                        self.view.alpha = 0
//                    }, completion: nil)


                    self.openInitialViewController()

//                    UIView.transition(with: self.nextView, duration: 0.5, options: .transitionCrossDissolve, animations: {
//                        self.nextView.alpha = 0.1
//                    }) { (sucess) in
//                        // next
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//                            print("navigate to next screen")
//                            let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
//                            let vc_TABBAR = storyboardMain.instantiateViewController(withIdentifier: "MainTABViewController") as! MainTABViewController
//                            kAppDelegate.window?.rootViewController = vc_TABBAR
//
//
//
//                        }
//                    }

                }
            }
            
        }
        //}
        
    }
    
    public func setImage(color: UIColor, imgView: UIImageView) {
            
            if #available(iOS 13.0, *) {
                imgView.image =  imgView.image?.withTintColor(color, renderingMode: .alwaysTemplate)
            } else {
                // Fallback on earlier versions
               imgView.image  = imgView.image?.withRenderingMode(.alwaysTemplate)
                imgView.tintColor = color
            }
    //        self.setImage(imgView.image?.withRenderingMode(.alwaysTemplate) i )
        }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func openInitialViewController() {
        
        if UserDefaults.standard.bool(forKey: UserDefault.isLoggedIn) == true {
            let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
            let vc_TABBAR = storyboardMain.instantiateViewController(withIdentifier: "MainTABViewController") as! MainTABViewController
            let navigationController = UINavigationController(rootViewController: vc_TABBAR)
            navigationController.isNavigationBarHidden = true
            kAppDelegate.window?.rootViewController = navigationController
            
            UIView.transition(with: vc_TABBAR.view, duration: 0.1, options: .transitionCrossDissolve, animations: {
                self.view.alpha = 0
            }, completion: nil)
        }else{
            let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
            let vc_TABBAR = storyboardMain.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let navigationController = UINavigationController(rootViewController: vc_TABBAR)
            navigationController.isNavigationBarHidden = true
            kAppDelegate.window?.rootViewController = navigationController
            
            UIView.transition(with: vc_TABBAR.view, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.view.alpha = 0
            }, completion: nil)
        }
        

    }
    
    
}
