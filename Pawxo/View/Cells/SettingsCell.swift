//
//  SettingsCell.swift
//  Pawxo
//
//  Created by Apple on 16/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {

    @IBOutlet weak var title_Lbl: UILabel!
    @IBOutlet weak var description_Lbl: UILabel!
    
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var nameTopConstraints: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
