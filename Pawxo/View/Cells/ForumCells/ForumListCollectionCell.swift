//
//  ForumListCollectionCell.swift
//  Pawxo
//
//  Created by 42works on 08/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import ActiveLabel

protocol ForumListCollectionCellProtocol {
    func cellClicked(frame:CGRect,index:Int,forumList:ForumList,image:UIImage?,cell:ForumListCollectionCell)
    func profileClicked(userId:Int)
}


class ForumListCollectionCell: UICollectionViewCell {

  var staticCount = 0
    
    var cellType = 0
    
    @IBOutlet weak var noInternetView: UIView!
    @IBOutlet weak var noResultsView: UIView!
    var forumCellTypeOne = "ForumListTypeOneCell"
    
    @IBOutlet weak var noForumView: UIView!
    var forumCellTypeTwo = "ForumListTypeTwoCell"
    
    var delegate : ForumListCollectionCellProtocol?
    
    var tempLabel : ActiveLabel!
    var tempLabel2 : ActiveLabel!
    var descriptionText = "Jason Stathom buy four new dogs for their petclub #Feelinghappy #withdogs #Petclub #Petshow"
    var text  = "Lorium ipsum is simple dummy text!"
    @IBOutlet weak var forumTableView: UITableView!
    
    var showAlertClosure : ((String)->())?
    var hashTagPressed : ((String)->())?
    
    var forumList = [ForumList]()
    var shareForumPressed : ((ForumList)->())?
    var editForumPressed : ((ForumList)->())?
    var deleteForumPressed : ((ForumList,IndexPath,ForumListCollectionCell)->())?
    
    let gif = UIImage.gifImageWithName("gif")
    
    var refresh = UIRefreshControl()
    var loader = UIImageView()
    
    var request : ForumRequestModel?
    
    var isInternetConnected = true
    
    var listToRefresh : ((ForumRequestModel,ForumListCollectionCell)->())?
    var tryAgainPressed : ((ForumRequestModel,ForumListCollectionCell)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        forumTableView.register(UINib(nibName: forumCellTypeOne, bundle: nil), forCellReuseIdentifier: forumCellTypeOne)
        cellType = 0
        staticCount = 0
        forumTableView.register(UINib(nibName: forumCellTypeTwo, bundle: nil), forCellReuseIdentifier: forumCellTypeTwo)
        forumTableView.estimatedRowHeight = 467
        //forumTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right:0)
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = 0
        tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        tempLabel.font = UIFont.JosefinSansRegular(size: 16.0)
        // Initialization code
            
            refresh.tintColor = UIColor.clear
           refresh.backgroundColor = UIColor.clear
           refresh.attributedTitle = NSAttributedString(string: "Pull to refresh")
           refresh.addTarget(self, action:#selector(refreshTable), for: UIControl.Event.valueChanged)
           // Add Custom Loader
           
           
           loader.center = self.refresh.center
            loader = UIImageView(frame: CGRect(x: kAppDelegate.window!.frame.size.width/2 - 20, y: refresh.center.y, width: 40, height: 40))
            loader.image = gif
           refresh.insertSubview(loader, at: 0)
           
           self.forumTableView.addSubview(refresh)

           let emptyLabel = UILabel.init(frame: self.forumTableView.frame)
           var msg = ""
        
           msg = "No latest posts yet."
           emptyLabel.isHidden = true
           emptyLabel.text = msg
           emptyLabel.textAlignment = .center
           emptyLabel.numberOfLines = 0
           emptyLabel.textColor = UIColor.darkGray
           self.forumTableView.addSubview(emptyLabel)
        
    }
    
    
    @objc private func refreshTable() {
        self.request?.page = 1
//        if self.request?.page == 3 {
//            return
//        }
        
        self.listToRefresh!(request!, self)
    }
    
    
    func endRefreshing(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.refresh.endRefreshing()
        }
    }
    
    
    func bottomRefresh(_ scrollView : UIScrollView)
    {
        let scrollViewHeight = scrollView.frame.size.height;
        let scrollContentSizeHeight = scrollView.contentSize.height;
        let scrollOffset = scrollView.contentOffset.y;
        if (scrollOffset >  scrollContentSizeHeight - scrollViewHeight)
        {
            if request!.isLoaded {
                request?.page += 1
                if request!.hasMore {
                    self.listToRefresh!(request!,self)
                }
            }
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let cells = self.forumTableView.visibleCells as? [ForumListTypeOneCell]{
            for cell in cells{
                if cell.optionsOuterView.isHidden == false{
                    cell.optionsOuterView.isHidden = true
                    cell.setOptionsView()
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let _ = scrollView as? UITableView {
            bottomRefresh(scrollView)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            if let _ = scrollView as? UITableView {
                bottomRefresh(scrollView)
            }
        }
    }
    
    
    
    @IBAction func tryAgainPressed(_ sender: Any) {
        self.request?.page = 1
        //        if self.request?.page == 3 {
        //            return
        //        }
        self.tryAgainPressed?(request!, self)
    }
    
    func setStaticcount(count:Int){
        self.staticCount = count
        self.forumTableView.reloadData()
    }
    
    func setForumList(forumList:[ForumList]){
        
        if !self.request!.internetConnected{
            self.noResultsView.isHidden = true
            self.noInternetView.isHidden = false
            self.forumTableView.addSubview(noInternetView)
            self.forumList = forumList
            self.forumTableView.reloadData()
            return
        }
        
        self.noInternetView.isHidden = true
        loader.image = UIImage.gifImageWithName("gif")
        
        self.forumList = forumList
        if self.forumList.count == 0 {
            if self.request?.searchText != ""{
            self.noResultsView.isHidden = false
            self.forumTableView.addSubview(noResultsView)
            }else{
                self.noForumView.isHidden = false
                self.forumTableView.addSubview(noForumView)
            }
        }else{
            
            self.noResultsView.isHidden = true
            self.noForumView.isHidden = true
        }
        self.forumTableView.reloadData()
    }
    
    
    func deleteForumButtonPressed(forumlist:ForumList,indexPath:IndexPath){
        var forumVM = Forum_VM()
        forumVM.userId = CommonFunctions.getuserFromDefaults().userId
        forumVM.forumId = "\((forumlist.id)!)"
        
        
        forumVM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if forumVM.isLoading ?? false {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        forumVM.responseRecieved = {
            // refresh button like
            self.forumList.remove(at: indexPath.row)
            self.forumTableView.deleteRows(at: [indexPath], with: .automatic)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.setForumList(forumList: self.forumList)
            }
          //  self.postList.remove(at: indexPath!.row)
          //  self.homeTableView.deleteRows(at: [indexPath!], with: .automatic)
        }
        
        forumVM.showAlertClosure = {
            DispatchQueue.main.async {
                if let message = forumVM.alertMessage {
                   // self.showAlert(self , message: message)//
                }
            }
        }
        
        
        forumVM.callDeleteForumService = true
       
    }
    
    
}


extension ForumListCollectionCell : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.forumList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ForumListTypeOneCell =  tableView.dequeueReusableCell(withIdentifier: forumCellTypeOne, for: indexPath) as! ForumListTypeOneCell
        
        let forumList = self.forumList[indexPath.row]
        cell.delegate = self
        cell.setUpForumList(forumlist: forumList)
        cell.showAlertClosure = { message in
            self.showAlertClosure?(message)
        }
        
        cell.shareForumPressed = { forumList in
            self.shareForumPressed?(forumList)
        }
        
        cell.hashTagPressed = { hashTag in
            self.hashTagPressed?(hashTag)
        }
        
        cell.editForumPressed = { forumList in
            self.editForumPressed?(forumList)
        }
        
        cell.deleteForumPressed = { forumList in
            self.deleteForumPressed?(forumList,indexPath,self)
            //self.deleteForumButtonPressed(forumlist: forumList, indexPath: indexPath)
        }
        
        //cell.setTextDescription(text: text, descriptionText: descriptionText)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return UITableView.automaticDimension
        
        
        let forumList = self.forumList[indexPath.row]
        
        
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.font = UIFont.JosefinSansSemiBold(size: 16.0)
        tempLabel.numberOfLines = 0
        tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        tempLabel.text = forumList.title ?? ""
        tempLabel.sizeToFit()
        
       // let calculatedHeight = tempLabel.frame.height
        
        let height = tempLabel.frame.height
        
        tempLabel.text = ""
        tempLabel.text = "A"
        tempLabel.sizeToFit()
        
        let twoLineTextHeight = tempLabel.frame.height
        
        var calculatedHeight : CGFloat = 0
        if height > twoLineTextHeight {
            calculatedHeight = twoLineTextHeight
        }else{
            calculatedHeight = height
        }
        
        
        
        
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.font = UIFont.JosefinSansSemiBold(size: 16.0)
        tempLabel.numberOfLines = 0
        tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        tempLabel.text = forumList.content ?? ""
        tempLabel.sizeToFit()
        
        //let calculatedHeightDescription = tempLabel.frame.height
        
        let heightDescription = tempLabel.frame.height
        
        tempLabel.text = ""
        tempLabel.text = "A \n B"
        tempLabel.sizeToFit()
        
        let twoLineTextHeightDescription = tempLabel.frame.height
        
        var calculatedHeightDescription : CGFloat = 0
        if heightDescription > twoLineTextHeightDescription {
            calculatedHeightDescription = twoLineTextHeightDescription
        }else{
            calculatedHeightDescription = heightDescription
        }
        
        if tempLabel.text != "" {
            
            return calculatedHeight + calculatedHeightDescription + 420
        }else{
            return calculatedHeight + 420
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return UIView()
    }
}


extension ForumListCollectionCell : ForumListTypeOneCellProtocol {
    func cellClicked(frame: CGRect,index:Int,forumlist:ForumList,image:UIImage?) {
        delegate?.cellClicked(frame: frame, index: index, forumList: forumlist, image: image, cell: self)
    }
    
    func profileClicked(userId:Int){
       
        delegate?.profileClicked(userId: userId)
    }
    
}
