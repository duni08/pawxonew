//
//  ForumHeaderCell.swift
//  Pawxo
//
//  Created by 42works on 23/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import ActiveLabel



protocol ForumDetailsHeaderCellPosProtocol {
    func favouriteButtonPressed(cell:ForumDetailsHeaderCell)
    func shareButtonPressed(cell:ForumDetailsHeaderCell)
    //func doubleTAPLikePressed(cell:ForumHeaderCell)
    //func petPressed(pet:PetList,isMYPet:Bool)
    //func petListPressed(petlist :[PetList],isMYPet:Bool)
    func videoButtonPressed(cell:ForumDetailsHeaderCell)
}

class ForumDetailsHeaderCell: UITableViewCell {

    @IBOutlet weak var pageControl: UIPageControl!
    
    let cellIdentifier = "ForumMediaCell"
    var tempLabel : ActiveLabel!
    
    @IBOutlet weak var titleLabel: ActiveLabel!
    @IBOutlet weak var forumCollectionView: UICollectionView!
    
    @IBOutlet weak var favouriteButton: UIButton!
    
    @IBOutlet weak var titleLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionLabel: ActiveLabel!
    @IBOutlet weak var descriptionHeightConstraint: NSLayoutConstraint!
    
    var post : PostList?
    var forumList : ForumList?
    @IBOutlet weak var likeCountLabel: LightLabel!
    
    @IBOutlet weak var forumOptionsView: UIView!
    
    var selectedIndex = 0
    var mediaItems = [ForumMediaItem]()
    
    var delegate : HashTagUserMentionProtocol?
    var forumDelegate : ForumDetailsHeaderCellPosProtocol?
    
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var firstPetImgView: UIImageView!
    
    @IBOutlet weak var secondPetCountView: UIView!
    
    @IBOutlet weak var secondPetImgView: UIImageView!
    
    @IBOutlet weak var firstPetButton: UIButton!
    @IBOutlet weak var petCountLabel: UILabel!
    
    @IBOutlet weak var petsOuterView: UIView!
    
    @IBOutlet weak var secondPetButton: UIButton!
    
    var isFavourite = false
    
    var showAlertClosure : ((String)->())?
    
    var playVideoUrl :((String)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        forumCollectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = 0
        tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        tempLabel.font = UIFont.JosefinSansRegular(size: 16.0)
        descriptionLabel.hashtagColor = AppColor.hashTagColor
        descriptionLabel.mentionColor = AppColor.userMentionColor
        descriptionLabel.enabledTypes = [.mention, .hashtag, .url]
        descriptionLabel.font = UIFont.JosefinSansSemiBold(size: 16.0)
        descriptionLabel.textColor = AppColor.colorGreySubHeading
        // Initialization code
        
        
        titleLabel.font = UIFont.JosefinSansSemiBold(size: 16.0)
        titleLabel.textColor = AppColor.userNameColor
        
        
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(didPressPartButton))
        singleTapGesture.numberOfTapsRequired = 1
        //self.likeButton.addGestureRecognizer(singleTapGesture)

        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(didDoubleTap))
        doubleTapGesture.numberOfTapsRequired = 2
        //self.likeButton.addGestureRecognizer(doubleTapGesture)

        singleTapGesture.require(toFail: doubleTapGesture)
        
    }
    
    var staticCount = 0
    
    
    
    @objc func didPressPartButton(){
        print("single Tap")
      //  self.postDelegate?.likeButtonPressed(cell: self)
       // self.homeCellDelegate?.likeButtonPressed(post: self.post!)
    }
    
    @objc func didDoubleTap(){
        print("dobule tap")
       // self.postDelegate?.doubleTAPLikePressed(cell: self)
    }
    
    
    func setUpMediaItems(){
        
        self.mediaItems.removeAll()
        
        if let image_0 = self.forumList?.image0{
            let mediaItem = ForumMediaItem()
            mediaItem.imageVideoUrl = image_0
            
            if let image_0_type = self.forumList?.image0_Type{
                mediaItem.type = image_0_type
            }else{
                mediaItem.type = "image"
            }
            
            if let image_0_thumb = self.forumList?.image0_Thumb{
                mediaItem.imageVideoThumbUrl = image_0_thumb
            }
            
            self.mediaItems.append(mediaItem)
        }
        
        
        if let image_1 = self.forumList?.image1{
            let mediaItem = ForumMediaItem()
            mediaItem.imageVideoUrl = image_1
            
            if let image_1_type = self.forumList?.image1_Type{
                mediaItem.type = image_1_type
            }else{
                mediaItem.type = "image"
            }
            
            if let image_1_thumb = self.forumList?.image1_Thumb{
                mediaItem.imageVideoThumbUrl = image_1_thumb
            }
            
            self.mediaItems.append(mediaItem)
        }
        
        
        if let image_2 = self.forumList?.image2{
            let mediaItem = ForumMediaItem()
            mediaItem.imageVideoUrl = image_2
            
            if let image_2_type = self.forumList?.image2_Type{
                mediaItem.type = image_2_type
            }else{
                mediaItem.type = "image"
            }
            
            if let image_2_thumb = self.forumList?.image2_Thumb{
                mediaItem.imageVideoThumbUrl = image_2_thumb
            }
            
            self.mediaItems.append(mediaItem)
        }
        
        if let image_3 = self.forumList?.image3{
            let mediaItem = ForumMediaItem()
            mediaItem.imageVideoUrl = image_3
            
            if let image_3_type = self.forumList?.image3_Type{
                mediaItem.type = image_3_type
            }else{
                mediaItem.type = "image"
            }
            
            if let image_3_thumb = self.forumList?.image3_Thumb{
                mediaItem.imageVideoThumbUrl = image_3_thumb
            }
            
            self.mediaItems.append(mediaItem)
        }
        
    }
    
    
    func setUpForumList(forumlist:ForumList,selectedIndex:Int){
        self.forumList = forumlist
        self.setUpMediaItems()
        self.forumList?.mediaItems = self.mediaItems
        self.selectedIndex = selectedIndex
        self.setUpData(count: forumlist.mediaItems!.count, currentPage: selectedIndex)
        self.setTextDescription(text: forumlist.content ?? "", titleText: forumlist.title ?? "")
        //self.likeCountLabel.text = (forumlist.stats?.likes != nil) ? "\((forumlist.stats?.likes!)!)" : "0"
        
        if let isLike = forumlist.hasLiked {
            self.setLikeImage(like: isLike)
        }else{
            self.setLikeImage(like: false)
        }
        
    }
    
    
    func setLikeImage(like:Bool){
        isFavourite = like
        if like{
            self.favouriteButton.setImage(DefaultImage.favouriteImage, for: .normal)
        }else{
            self.favouriteButton.setImage(DefaultImage.unFavouriteImage, for: .normal)
        }
    }
    
    
    func setUpData(count:Int, currentPage:Int){
        
        self.forumCollectionView.reloadData()
        
        if count == 0 || count == 1 {
            self.pageControl.isHidden = true
            self.staticCount = 1
        }else{
            self.pageControl.isHidden = false
            self.pageControl.numberOfPages = count
            self.pageControl.currentPage = currentPage
            self.forumCollectionView.scrollToItem(at: IndexPath(item: currentPage, section: 0), at: .centeredHorizontally, animated: false)
            self.staticCount = count
        }
        
    }
    
    
    
    
    func setTextDescription(text:String,titleText:String){
        
        tempLabel.frame =  CGRect(x: 0, y: 0, width: self.frame.width - 40, height: CGFloat.greatestFiniteMagnitude)
        titleLabel.text = titleText
        tempLabel.font = UIFont.JosefinSansSemiBold(size: 16.0)
        tempLabel.text = titleText
        tempLabel.sizeToFit()
        let calculatedHeightTitle = tempLabel.frame.height
        self.titleLabelHeightConstraint.constant = calculatedHeightTitle
        
        
        
        
        var description = text
        var hashArr = [String]()
        let replaced = description.replacingOccurrences(of: " #", with: "#")
        let finalreplaced = replaced.replacingOccurrences(of: "#", with: " #")
        description = finalreplaced
        print(description)
        
        let descriptionNew = description.components(separatedBy: " ")
        for obj in descriptionNew ?? [String]() {
            if obj.first == "#" {
                var temp = obj
                temp.remove(at: temp.startIndex)
                hashArr.append(temp)
            }
        }
        
        var array = [ActiveType]()
        
        if hashArr.count != 0 {
            
            for hashTag in hashArr{
                let tag = "#\(hashTag)"
                let NewCustomType = ActiveType.custom(pattern: tag)
                array.append(NewCustomType)
                self.descriptionLabel.customColor[NewCustomType] = AppColor.hashTagColor
                self.descriptionLabel.customSelectedColor[NewCustomType] = AppColor.hashTagColor
                self.descriptionLabel.handleCustomTap(for: NewCustomType) { element in
                    print("Custom type tapped: \(element)")
                    self.delegate?.hashTagPressed(hashTag: element)
                    
                }
            }
            
        }
        array.append(.mention)
        array.append(.url)
        
            self.descriptionLabel.enabledTypes.removeAll()
            self.descriptionLabel.enabledTypes = array
        
            self.descriptionLabel.handleMentionTap({ (mention) in
            print("Success. You just tapped the \(mention) user")
        })

        self.descriptionLabel.text = text
        tempLabel.frame =  CGRect(x: 0, y: 0, width: self.frame.width - 40, height: CGFloat.greatestFiniteMagnitude)
        tempLabel.font = UIFont.JosefinSansSemiBold(size: 16.0)
        tempLabel.text = text
        tempLabel.sizeToFit()
        let calculatedHeight = tempLabel.frame.height
        print("forum header celll Height")
        print(calculatedHeight)
        self.descriptionHeightConstraint.constant = calculatedHeight
        
        
        
        
    }
    
    
    
    
    func userMention(name:String){
        var userSelected : Userlist?
        if let userlist =  post?.usertagList {
            for user in userlist{
                if user.username! == name{
                    userSelected = user
                    break
                }
            }
        }
        
        if let user =  userSelected {
            self.delegate?.profilePressed(userId: user.id!)
        }
    }
    
    func setCollectionView(show:Bool){
        self.forumCollectionView.isHidden = !show
    }
    
    
    func setUIForHome(){
        self.pageControl.isHidden = true
        self.staticCount = 1
        self.forumCollectionView.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    
    
    
    @IBAction func shareButtonPressedForum(_ sender: Any) {
        forumDelegate?.shareButtonPressed(cell: self)
    }
    
    
    @IBAction func favouriteButtonPressed(_ sender: Any) {
        let forum_VM = Forum_VM()
        forum_VM.userId = CommonFunctions.getuserFromDefaults().userId!
        forum_VM.forumId = "\((self.forumList?.id!)!)"
        if self.isFavourite{
        forum_VM.isFavourite = "0"
        }else{
        forum_VM.isFavourite = "1"
        }
        forum_VM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if forum_VM.isLoading {
                   // LoaderView.shared.showLoader()
                } else {
                   // LoaderView.shared.hideLoader()
                }
            }
        }
        
        forum_VM.responseRecieved = {
            // refresh content
            DispatchQueue.main.async {
            print("Final Response recieved")
                self.forumList?.hasLiked = !self.forumList!.hasLiked!
                self.setUpForumList(forumlist: self.forumList!, selectedIndex: self.selectedIndex)
            }
        }
        
        forum_VM.showAlertClosure = {
            DispatchQueue.main.async {
                if let message = forum_VM.alertMessage {
                    self.showAlertClosure!(message)
                }
            }
        }
        
        forum_VM.callFavouriteUnfavouriteService = true
        
        
        
    }
    
    
    
    
    
}


extension ForumDetailsHeaderCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.forumList?.mediaItems!.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let mediaCell : ForumMediaCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ForumMediaCell
        
        let mediaItem = self.forumList?.mediaItems![indexPath.item]
        if mediaItem!.type == "video"{
            mediaCell.centerImageView.setImage(fromUrl: mediaItem?.imageVideoThumbUrl ?? "" , defaultImage: DefaultImage.defaultPostImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: true)
            mediaCell.videoButton.isHidden = false
            mediaCell.mediaPlay = {
                let url = mediaItem?.imageVideoUrl
                self.playVideoUrl?(url!)
            }
        }else{
            mediaCell.centerImageView.setImage(fromUrl: mediaItem?.imageVideoThumbUrl ?? "" , defaultImage: DefaultImage.defaultPostImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
            mediaCell.videoButton.isHidden = true
        }
        
        
        return mediaCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //let height =  self.bounds.size.height - (self.view.safeAreaInsets.top + self.view.safeAreaInsets.bottom + 101)
        return CGSize(width: self.frame.size.width, height: 254)
    }
       // return CGSize(width: 100, height: 100)
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print(indexPath.item)
        let mediaItem = self.forumList?.mediaItems![indexPath.item]
        if mediaItem!.type == "video"{
            let url = mediaItem?.imageVideoUrl
            self.playVideoUrl?(url!)
        }
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            
            let visibleRect = CGRect(origin: forumCollectionView.contentOffset, size: forumCollectionView.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            let visibleIndexPath = forumCollectionView.indexPathForItem(at: visiblePoint)
            // print(visibleIndexPath?.item)
            
        self.pageControl.currentPage = visibleIndexPath!.item
            
    //        if(visibleIndexPath?.item ==  0){
    //           // self.setUpIndicatorView(index:0, animate: true, scroll: false, sender: lostLabel)
    //        }else if(visibleIndexPath?.item ==  1){
    //           // self.setUpIndicatorView(index:1, animate: true, scroll: false, sender: rescueLabel)
    //        }else{
    //            //self.setUpIndicatorView(index:2, animate: true, scroll: false, sender: healthLabel)
    //        }
            
        }
}
