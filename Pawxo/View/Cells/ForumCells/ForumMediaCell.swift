//
//  ForumMediaCell.swift
//  Pawxo
//
//  Created by 42works on 23/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class ForumMediaCell: UICollectionViewCell {
    
    
    @IBOutlet weak var centerImageView: UIImageView!
    
    @IBOutlet weak var videoButton: UIButton!
    
    var mediaPlay : (()-> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    @IBAction func playPressed(_ sender: Any) {
        self.mediaPlay?()
    }
    
    
    
}
