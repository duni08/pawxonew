//
//  ForumHeaderCell.swift
//  Pawxo
//
//  Created by 42works on 23/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import ActiveLabel



protocol ForumHeaderCellPostProtocol {
    func likeButtonPressed(cell:ForumHeaderCell)
    func shareButtonPressed(cell:ForumHeaderCell)
    func doubleTAPLikePressed(cell:ForumHeaderCell)
    func petPressed(pet:PetList,isMYPet:Bool)
    func petListPressed(petlist :[PetList],isMYPet:Bool)
    func videoButtonPressed()
}

class ForumHeaderCell: UITableViewCell {

    @IBOutlet weak var pageControl: UIPageControl!
    
    let cellIdentifier = "ForumMediaCell"
    var tempLabel : ActiveLabel!
    
    @IBOutlet weak var forumCollectionView: UICollectionView!
    
    @IBOutlet weak var descriptionLabel: ActiveLabel!
    @IBOutlet weak var descriptionHeightConstraint: NSLayoutConstraint!
    
    var post : PostList?
    var forumList : ForumList?
    @IBOutlet weak var likeCountLabel: LightLabel!
    
    @IBOutlet weak var forumOptionsView: UIView!
    
    var delegate : HashTagUserMentionProtocol?
    var postDelegate : ForumHeaderCellPostProtocol?
    
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var firstPetImgView: UIImageView!
    
    @IBOutlet weak var secondPetCountView: UIView!
    
    @IBOutlet weak var secondPetImgView: UIImageView!
    
    @IBOutlet weak var firstPetButton: UIButton!
    @IBOutlet weak var petCountLabel: UILabel!
    
    @IBOutlet weak var petsOuterView: UIView!
    
    @IBOutlet weak var secondPetButton: UIButton!
    
    var userNotFound : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        forumCollectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = 0
        tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        tempLabel.font = UIFont.JosefinSansRegular(size: 15.0)
        descriptionLabel.hashtagColor = AppColor.hashTagColor
        descriptionLabel.mentionColor = AppColor.userMentionColor
        //descriptionLabel.enabledTypes = [.mention, .hashtag, .url]
        descriptionLabel.font = UIFont.JosefinSansSemiBold(size: 15.0)
        descriptionLabel.textColor = AppColor.colorGreySubHeading
        // Initialization code
        
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(didDoubleTap))
        singleTapGesture.numberOfTapsRequired = 1
        self.likeButton.addGestureRecognizer(singleTapGesture)

//        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(didDoubleTap))
//        doubleTapGesture.numberOfTapsRequired = 2
//        self.likeButton.addGestureRecognizer(doubleTapGesture)

        //singleTapGesture.require(toFail: doubleTapGesture)
        
        
        let singleTapGestureLike = UITapGestureRecognizer(target: self, action: #selector(didPressPartButton))
        singleTapGestureLike.numberOfTapsRequired = 1
        self.likeCountLabel.isUserInteractionEnabled = true
        self.likeCountLabel.addGestureRecognizer(singleTapGestureLike)
    }
    
    var staticCount = 0
    
    
    
    @objc func didPressPartButton(){
        print("single Tap")
        self.postDelegate?.likeButtonPressed(cell: self)
       // self.homeCellDelegate?.likeButtonPressed(post: self.post!)
    }
    
    @objc func didDoubleTap(){
        print("dobule tap")
        self.postDelegate?.doubleTAPLikePressed(cell: self)
    }
    
    
    func setUpPost(post:PostList){
        self.post = post
        self.setUpData(count: 0, currentPage: 0)
        self.setTextDescription(text: post.postListDescription ?? "")
        //self.forumOptionsView.isHidden = true
        
        var likeStr = "0 likes"
        
        if let count = post.likeCount {
            if count > 1{
                likeStr = "\(count) likes"
            }else if count == 1{
                likeStr = "1 like"
            }
        }
        
        self.likeCountLabel.text = likeStr
        
        if let isLike = post.isLike {
            self.setLikeImage(like: isLike.boolValue)
        }
        
        
        if let pet = self.post?.petsList, pet.count > 0 {
            self.petsOuterView.isHidden = false
            if pet.count == 1 {
                let petFirst = pet[0]
                self.firstPetImgView.setImage(fromUrl: petFirst.petImages ?? "", defaultImage: DefaultImage.defaultPetImage)
                self.secondPetButton.isHidden = true
                self.secondPetImgView.isHidden = true
                self.petCountLabel.isHidden = true
                self.secondPetCountView.isHidden = true
            }else if pet.count  >= 2 {
                self.secondPetButton.isHidden = false
                self.secondPetImgView.isHidden = false
                self.secondPetCountView.isHidden = false
                let petFirst = pet[0]
                self.firstPetImgView.setImage(fromUrl: petFirst.petImages ?? "", defaultImage: DefaultImage.defaultPetImage)
                let petSecond = pet[1]
                self.secondPetImgView.setImage(fromUrl: petSecond.petImages ?? "", defaultImage: DefaultImage.defaultPetImage)
                if pet.count > 2 {
                    let count = pet.count - 2
                    self.petCountLabel.isHidden = false
                    self.secondPetCountView.isHidden = false
                    self.petCountLabel.text = "+\(count)"
                }else{
                    self.petCountLabel.isHidden = true
                    self.secondPetCountView.isHidden = true
                }
            }
        }else{
            self.petsOuterView.isHidden = true
        }
        
    }
    
    func setLikeImage(like:Bool){
        if like{
            self.likeButton.setImage(DefaultImage.likeImage, for: .normal)
        }else{
            self.likeButton.setImage(DefaultImage.unlikeImage, for: .normal)
        }
    }
    
    
    func setUpData(count:Int, currentPage:Int){
        
        if count == 0 {
            self.pageControl.isHidden = true
            self.staticCount = 1
        }else{
            self.pageControl.isHidden = false
            self.pageControl.numberOfPages = count
            self.pageControl.currentPage = currentPage
            self.staticCount = count
        }
        self.forumCollectionView.reloadData()
    }
    
    
    
    
    func setTextDescription(text:String){
        descriptionLabel.text = text
        var description = text
        var hashArr = [String]()
        let replaced = description.replacingOccurrences(of: " #", with: "#")
        let finalreplaced = replaced.replacingOccurrences(of: "#", with: " #")
        description = finalreplaced
        print(description)
        
        let descriptionNew = description.components(separatedBy: " ")
        for obj in descriptionNew ?? [String]() {
            if obj.first == "#" {
                var temp = obj
                temp.remove(at: temp.startIndex)
                hashArr.append(temp)
            }
        }
        
        var array = [ActiveType]()
        
        
        if hashArr.count != 0 {
            
            for hashTag in hashArr{
               let NewCustomType = ActiveType.custom(pattern: "#\(hashTag)")
                array.append(NewCustomType)
                descriptionLabel.customColor[NewCustomType] = AppColor.hashTagColor
                descriptionLabel.customSelectedColor[NewCustomType] = AppColor.hashTagColor
                descriptionLabel.handleCustomTap(for: NewCustomType) { element in
                    print("Custom type tapped: \(element)")
                    self.delegate?.hashTagPressed(hashTag: element)
                    
                }
            }
            
        }
        array.append(.mention)
        array.append(.url)
        
        descriptionLabel.enabledTypes = array
        
        
        descriptionLabel.handleMentionTap({ (mention) in
            print("Success. You just tapped the \(mention) user")
            self.userMention(name: mention)
        })
        
        tempLabel.frame =  CGRect(x: 0, y: 0, width: self.frame.width - 40, height: CGFloat.greatestFiniteMagnitude)
        tempLabel.text = text
        tempLabel.sizeToFit()
        let calculatedHeight = tempLabel.frame.height
        print("forum header celll Height")
        print(calculatedHeight)
        //self.descriptionHeightConstraint.constant = calculatedHeight
        
    }
    
    
    
    
    func userMention(name:String){
        var userSelected : Userlist?
        if let userlist =  post?.usertagList {
            for user in userlist{
                if user.username! == name{
                    userSelected = user
                    break
                }
            }
        }
        
        if let user =  userSelected {
            self.delegate?.profilePressed(userId: user.id!)
        }else{
            self.userNotFound?()
        }
    }
    
    func setCollectionView(show:Bool){
        self.forumCollectionView.isHidden = !show
    }
    
    
    func setUIForHome(){
        self.pageControl.isHidden = true
        self.staticCount = 1
        self.forumCollectionView.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    
    
    
    @IBAction func shareButtonPressedForum(_ sender: Any) {
        
    }
    
    
    @IBAction func favouriteButtonPressed(_ sender: Any) {
        
    }
    
    
    @IBAction func sharePostPressed(_ sender: Any) {
        postDelegate?.shareButtonPressed(cell: self)
    }
    
    
    @IBAction func firstButtonPressed(_ sender: UIButton) {
    //        self.homeCellDelegate?.petPressed
        var isMYPet = false
        let userLogin = CommonFunctions.getuserFromDefaults()
        if self.post?.userID == Int(userLogin.userId!) {
            isMYPet = true
        }
        self.postDelegate?.petPressed(pet: (self.post?.petsList?.first!)!, isMYPet: isMYPet)
        }
        
        
        @IBAction func secondButtonPressed(_ sender: UIButton) {
            
            var isMYPet = false
            let userLogin = CommonFunctions.getuserFromDefaults()
            if self.post?.userID == Int(userLogin.userId!) {
                isMYPet = true
            }
            
            if let pets = self.post?.petsList{
                if pets.count > 2 {
                    self.postDelegate?.petListPressed(petlist: pets, isMYPet: isMYPet)
                }else{
                    
                    if let pet = pets.last{
                        self.postDelegate?.petPressed(pet: pet, isMYPet: isMYPet)
                    }
                }
            }
        }
    
    
    
    
    
    
    
    
}


extension ForumHeaderCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.staticCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let mediaCell : ForumMediaCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ForumMediaCell
        
        
        if self.post?.type == "video"{
            mediaCell.centerImageView.setImage(fromUrl: self.post?.thumbnail?.first ?? "" , defaultImage: DefaultImage.defaultPostImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
            mediaCell.videoButton.isHidden = false
            mediaCell.mediaPlay = {
                self.postDelegate?.videoButtonPressed()
            }
        }else{
            mediaCell.centerImageView.setImage(fromUrl: self.post?.media?.first ?? "" , defaultImage: DefaultImage.defaultPostImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
            mediaCell.videoButton.isHidden = true
        }
        
        
       // print(indexPath.item/3)
//        mediaCell.imgView.layer.cornerRadius = 0
//        mediaCell.imgView.layer.masksToBounds = true
//        mediaCell.imgView.image = UIImage(named: "tempImage")
        
        
        return mediaCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //let height =  self.bounds.size.height - (self.view.safeAreaInsets.top + self.view.safeAreaInsets.bottom + 101)
        return CGSize(width: self.frame.size.width, height: 254)
    }
       // return CGSize(width: 100, height: 100)
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print(indexPath.item)
        if self.post?.type == "video"{
            postDelegate?.videoButtonPressed()
        }
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            
            let visibleRect = CGRect(origin: forumCollectionView.contentOffset, size: forumCollectionView.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            let visibleIndexPath = forumCollectionView.indexPathForItem(at: visiblePoint)
            // print(visibleIndexPath?.item)
            
        self.pageControl.currentPage = visibleIndexPath!.item
            
    //        if(visibleIndexPath?.item ==  0){
    //           // self.setUpIndicatorView(index:0, animate: true, scroll: false, sender: lostLabel)
    //        }else if(visibleIndexPath?.item ==  1){
    //           // self.setUpIndicatorView(index:1, animate: true, scroll: false, sender: rescueLabel)
    //        }else{
    //            //self.setUpIndicatorView(index:2, animate: true, scroll: false, sender: healthLabel)
    //        }
            
        }
}
