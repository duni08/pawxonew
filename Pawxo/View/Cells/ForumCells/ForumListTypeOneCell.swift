//
//  ForumListTypeOneCell.swift
//  Pawxo
//
//  Created by 42works on 08/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import ActiveLabel


protocol ForumListTypeOneCellProtocol {
    func cellClicked(frame:CGRect,index:Int,forumlist:ForumList,image:UIImage?)
    func profileClicked(userId:Int)
}


class ForumListTypeOneCell: UITableViewCell {

    
    @IBOutlet weak var optionsOuterView: UIView!
    
    @IBOutlet weak var optionsButton: UIButton!
    
    @IBOutlet weak var commentLabel: LightLabel!
    @IBOutlet weak var nameLabel: RegularLabel!
    
    
    @IBOutlet weak var timeLabel: LightLabel!
    
    @IBOutlet weak var profileImageView: UIImageView!
    var staticCount = 0
    var heightArray = [CGSize]()
    var heightDefault : CGFloat = 254.0
    let cellIdentifier = "MediaAddCell"
    var paddingInternal : CGFloat = 2
    
    var delegate : ForumListTypeOneCellProtocol?
    
    @IBOutlet weak var descriptionLabel: ActiveLabel!
    @IBOutlet weak var titleLabel: ActiveLabel!
    @IBOutlet weak var mediaCollectionView: UICollectionView!
    
    @IBOutlet weak var titleLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var descriptionLabelHeightConstraint: NSLayoutConstraint!
    var tempLabel : ActiveLabel!
    
    var forumList : ForumList?
    var mediaItems = [ForumMediaItem]()
    
    var isFavourite = false
    @IBOutlet weak var favouriteButton: UIButton!
    
    var showAlertClosure : ((String)->())?
    
    var hashTagPressed : ((String)->())?
    
    var shareForumPressed : ((ForumList)->())?
    
    var editForumPressed : ((ForumList)->())?
    
    var deleteForumPressed : ((ForumList)->())?
    
    var unFavouritePressed :((ForumListTypeOneCell)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        staticCount = 0
        mediaCollectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        if let layout = mediaCollectionView?.collectionViewLayout as? AddMediaLayout {
          layout.delegate = self
          layout.cellPadding = paddingInternal
        }
        
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = 0
        tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        tempLabel.font = UIFont.JosefinSansRegular(size: 16.0)

        descriptionLabel.hashtagColor = AppColor.hashTagColor
        descriptionLabel.mentionColor = AppColor.userMentionColor
        
        titleLabel.hashtagColor = AppColor.hashTagColor
        titleLabel.mentionColor = AppColor.userMentionColor
        titleLabel.enabledTypes = [.mention, .url]
        titleLabel.font = UIFont.JosefinSansSemiBold(size: 16.0)
        titleLabel.textColor = AppColor.userNameColor
        
        
       // descriptionLabel.hashtagColor = AppColor.hashTagColor
        //descriptionLabel.mentionColor = AppColor.userMentionColor
        //descriptionLabel.enabledTypes = [.mention, .hashtag, .url]
        descriptionLabel.font = UIFont.JosefinSansSemiBold(size: 16.0)
        descriptionLabel.textColor = AppColor.colorGreySubHeading
        
                
        
        
        self.optionsOuterView.isHidden = true
        // Initialization code
        
        
        if let forumList = self.forumList{
            self.setUpForumList(forumlist: forumList)
        }
    }
    
    
    @IBAction func commentButtonPressed(_ sender: Any) {
        let cell: MediaAddCell = self.mediaCollectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as! MediaAddCell
        
        
        let buttonAbsoluteFrame = cell.imgView.convert(cell.imgView.bounds, to: kAppDelegate.window)
        //CGPoint p = [cell.imgView.superview convertPoint:cell.im.center toView:self.view]
        let frameTest = buttonAbsoluteFrame
        print(frameTest)
        delegate?.cellClicked(frame: frameTest, index: 0, forumlist: self.forumList!, image: cell.imgView.image)
        
    }
    
    
    
    func setTextDescription(text:String,descriptionText:String){
        
        
        
        
        var description = descriptionText
        var hashArr = [String]()
        let replaced = description.replacingOccurrences(of: " #", with: "#")
        let finalreplaced = replaced.replacingOccurrences(of: "#", with: " #")
        description = finalreplaced
        print(description)
        
        let descriptionNew = description.components(separatedBy: " ")
        for obj in descriptionNew ?? [String]() {
            if obj.first == "#" {
                var temp = obj
                temp.remove(at: temp.startIndex)
                hashArr.append(temp)
            }
        }
        
        var array = [ActiveType]()
        
        if hashArr.count != 0 {
            
            for hashTag in hashArr{
                let tag = "#\(hashTag)"
                let NewCustomType = ActiveType.custom(pattern: tag)
                array.append(NewCustomType)
                self.descriptionLabel.customColor[NewCustomType] = AppColor.hashTagColor
                self.descriptionLabel.customSelectedColor[NewCustomType] = AppColor.hashTagColor
                self.descriptionLabel.handleCustomTap(for: NewCustomType) { element in
                    print("Custom type tapped: \(element)")
                    self.hashTagPressed!(element)
                    
                }
            }
            
        }
        array.append(.mention)
        array.append(.url)
        
            self.descriptionLabel.enabledTypes.removeAll()
            self.descriptionLabel.enabledTypes = array
        
            self.descriptionLabel.handleMentionTap({ (mention) in
            print("Success. You just tapped the \(mention) user")
        })

            self.descriptionLabel.text = descriptionText
            self.tempLabel.frame =  CGRect(x: 0, y: 0, width: self.frame.width - 40, height: CGFloat.greatestFiniteMagnitude)
            self.tempLabel.font = UIFont.JosefinSansSemiBold(size: 16.0)
            self.tempLabel.text = descriptionText
            self.tempLabel.sizeToFit()
        
            let height = self.tempLabel.frame.height
        
            self.tempLabel.text = ""
            self.tempLabel.text = "A \n B"
            self.tempLabel.sizeToFit()
        
            let twoLineTextHeight = self.tempLabel.frame.height
        
        var calculatedHeight : CGFloat = 0
        if height > twoLineTextHeight {
            calculatedHeight = twoLineTextHeight
        }else{
            calculatedHeight = height
        }
        
        
      //  self.descriptionLabelHeightConstraint.constant = calculatedHeight
        
        
        
        
            self.tempLabel.frame =  CGRect(x: 0, y: 0, width: self.frame.width - 40, height: CGFloat.greatestFiniteMagnitude)
            self.titleLabel.text = text
            self.tempLabel.font = UIFont.JosefinSansSemiBold(size: 16.0)
            self.tempLabel.text = text
            self.tempLabel.sizeToFit()
       // let calculatedHeightTitle = tempLabel.frame.height
        
            let heightDescriptionTitle = self.tempLabel.frame.height
        
            self.tempLabel.text = ""
            self.tempLabel.text = "A"
            self.tempLabel.sizeToFit()
        
            let twoLineTextHeightTitle = self.tempLabel.frame.height
        
        var calculatedHeightTitle : CGFloat = 0
        if heightDescriptionTitle > twoLineTextHeightTitle {
            calculatedHeightTitle = twoLineTextHeightTitle
        }else{
            calculatedHeightTitle = heightDescriptionTitle
        }
        
        
      //  self.titleLabelHeightConstraint.constant = calculatedHeightTitle
        
    }
    
    
    
    
    func setUpForumList(forumlist:ForumList){
        self.forumList = forumlist
        self.setUpMediaItems()
        self.forumList?.mediaItems = self.mediaItems
        self.setUpUI()
        self.setUpLayout()
    }
    
    
    func setUpMediaItems(){
        
        self.mediaItems.removeAll()
        
        if let image_0 = self.forumList?.image0{
            let mediaItem = ForumMediaItem()
            mediaItem.imageVideoUrl = image_0
            
            if let image_0_type = self.forumList?.image0_Type{
                mediaItem.type = image_0_type
            }else{
                mediaItem.type = "image"
            }
            
            if let image_0_thumb = self.forumList?.image0_Thumb{
                mediaItem.imageVideoThumbUrl = image_0_thumb
            }
            
            self.mediaItems.append(mediaItem)
        }
        
        
        if let image_1 = self.forumList?.image1{
            let mediaItem = ForumMediaItem()
            mediaItem.imageVideoUrl = image_1
            
            if let image_1_type = self.forumList?.image1_Type{
                mediaItem.type = image_1_type
            }else{
                mediaItem.type = "image"
            }
            
            if let image_1_thumb = self.forumList?.image1_Thumb{
                mediaItem.imageVideoThumbUrl = image_1_thumb
            }
            
            self.mediaItems.append(mediaItem)
        }
        
        
        if let image_2 = self.forumList?.image2{
            let mediaItem = ForumMediaItem()
            mediaItem.imageVideoUrl = image_2
            
            if let image_2_type = self.forumList?.image2_Type{
                mediaItem.type = image_2_type
            }else{
                mediaItem.type = "image"
            }
            
            if let image_2_thumb = self.forumList?.image2_Thumb{
                mediaItem.imageVideoThumbUrl = image_2_thumb
            }
            
            self.mediaItems.append(mediaItem)
        }
        
        if let image_3 = self.forumList?.image3{
            let mediaItem = ForumMediaItem()
            mediaItem.imageVideoUrl = image_3
            
            if let image_3_type = self.forumList?.image3_Type{
                mediaItem.type = image_3_type
            }else{
                mediaItem.type = "image"
            }
            
            if let image_3_thumb = self.forumList?.image3_Thumb{
                mediaItem.imageVideoThumbUrl = image_3_thumb
            }
            
            self.mediaItems.append(mediaItem)
        }
        
    }
    
    
    
    func setOptionsView(){
        if self.forumList?.user?.id == Int(CommonFunctions.getuserFromDefaults().userId!){
            self.optionsButton.isHidden = false
        }else{
            self.optionsButton.isHidden = true
        }
    }
    
    func setUpUI(){
        
        self.setOptionsView()
        self.nameLabel.text = self.forumList?.user?.name ?? ""
        self.timeLabel.text = self.forumList?.createdAt?.UTCToLocal().timeAgoSinceDate(numericDates: true)
        self.titleLabel.text = self.forumList?.title ?? ""
        self.descriptionLabel.text = self.forumList?.content ?? ""
        self.setTextDescription(text: self.titleLabel.text!, descriptionText: self.descriptionLabel.text!)
        self.profileImageView.setImage(fromUrl: self.forumList?.user?.image ?? "", defaultImage: DefaultImage.defaultProfileImage)
        if let liked = self.forumList?.hasLiked ,liked == true{
            self.favouriteButton.setImage(DefaultImage.favouriteImage, for: .normal)
            isFavourite = true
        }else{
            self.favouriteButton.setImage(DefaultImage.unFavouriteImage, for: .normal)
            isFavourite = false
        }
        
        
        var commentStr = "0 replies"

        if let count = self.forumList?.stats?.threads {
            if count > 1 {
            commentStr = "\(count) replies"
            }else if count == 1 {
                commentStr = "1 reply"
            }
        }
        
        self.commentLabel.text =  commentStr
        
    }
    
    
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        self.shareForumPressed!(self.forumList!)
        
    }
    
    
    
    
    @IBAction func optionsButtonPressed(_ sender: Any) {
        self.optionsOuterView.isHidden = false
        self.optionsButton.isHidden = true
        self.bringSubviewToFront(self.optionsOuterView)
        
    }
    
    
    @IBAction func hideOptionsView(_ sender: Any) {
        self.optionsOuterView.isHidden = true
        self.setOptionsView()
        // hide options view
    }
    
    
    @IBAction func editforumPressed(_ sender: Any) {
        self.optionsOuterView.isHidden = true
        // Edit call
        self.setOptionsView()
        self.editForumPressed?(self.forumList!)
        
    }
    
    
    @IBAction func deleteForumPressed(_ sender: Any) {
        
        self.optionsOuterView.isHidden = true
        self.setOptionsView()
        //delete API call
        self.deleteForumPressed?(self.forumList!)
    }
    
    
    
    @IBAction func favouriteButtonPressed(_ sender: Any) {
        
       
            let forum_VM = Forum_VM()
            forum_VM.userId = CommonFunctions.getuserFromDefaults().userId!
            forum_VM.forumId = "\((self.forumList?.id!)!)"
            if self.isFavourite{
            forum_VM.isFavourite = "0"
            }else{
            forum_VM.isFavourite = "1"
            }
            forum_VM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if forum_VM.isLoading {
                      //  LoaderView.shared.showLoader()
                    } else {
                      //  LoaderView.shared.hideLoader()
                    }
                }
            }
            
            forum_VM.responseRecieved = {
                // refresh content
                DispatchQueue.main.async {
                print("Final Response recieved")
                    
                    self.forumList?.hasLiked = !self.forumList!.hasLiked!
                    self.setUpForumList(forumlist: self.forumList!)
                    if forum_VM.isFavourite == "0"{
                        self.unFavouritePressed?(self)
                    }
                }
            }
            
            forum_VM.showAlertClosure = {
                DispatchQueue.main.async {
                    if let message = forum_VM.alertMessage {
                        self.showAlertClosure!(message)
                    }
                }
            }
            
            forum_VM.callFavouriteUnfavouriteService = true
       
    }
    
    
    
    @IBAction func profileButtonPressed(_ sender: Any) {
        delegate?.profileClicked(userId: (self.forumList?.user?.id!)!)
    }
    
    func setUpLayout(){
        if mediaItems.count == 1{
            heightArray.removeAll()
           // let newWidth =
            let size = CGSize(width: self.frame.size.width - 40, height: heightDefault)
            heightArray.append(size)
        }else if mediaItems.count == 2 {
            heightArray.removeAll()
            let size = CGSize(width: (self.frame.size.width - 40 - paddingInternal)/2, height: heightDefault)
            heightArray.append(size)
            heightArray.append(size)
        }else if mediaItems.count == 3 {
            heightArray.removeAll()
            let size = CGSize(width: (self.frame.size.width - 40 - paddingInternal)/2, height: heightDefault)
            heightArray.append(size)
            let sizeTwo = CGSize(width: (self.frame.size.width - 40 - paddingInternal)/2, height: (heightDefault-paddingInternal)/2)
            heightArray.append(sizeTwo)
            heightArray.append(sizeTwo)
        }else if mediaItems.count == 4 {
            heightArray.removeAll()
            let size = CGSize(width: (self.frame.size.width - 40 - paddingInternal)/2, height: (heightDefault-paddingInternal)/2)
            heightArray.append(size)
            heightArray.append(size)
            heightArray.append(size)
            heightArray.append(size)
            
        }
        
        mediaCollectionView.collectionViewLayout.invalidateLayout()
        if let layout = mediaCollectionView?.collectionViewLayout as? AddMediaLayout {
            layout.delegate = self
            layout.cellPadding = paddingInternal
        }
        
        self.mediaCollectionView.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}


extension ForumListTypeOneCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mediaItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let mediaCell : MediaAddCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! MediaAddCell
        
        let mediaItem = self.mediaItems[indexPath.item]
        
        mediaCell.setMediaItem(item: mediaItem)
        
        return mediaCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //let height =  self.bounds.size.height - (self.view.safeAreaInsets.top + self.view.safeAreaInsets.bottom + 101)
        return self.heightArray[indexPath.item]
    }
       // return CGSize(width: 100, height: 100)
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print(indexPath.item)
        
        let cell: MediaAddCell = self.mediaCollectionView.cellForItem(at: indexPath) as! MediaAddCell
        
        
        let buttonAbsoluteFrame = cell.imgView.convert(cell.imgView.bounds, to: kAppDelegate.window)
        //CGPoint p = [cell.imgView.superview convertPoint:cell.im.center toView:self.view]
        let frameTest = buttonAbsoluteFrame
        print(frameTest)
        delegate?.cellClicked(frame: frameTest, index: indexPath.item, forumlist: self.forumList!, image: cell.imgView.image)
        
    }
    
}


extension ForumListTypeOneCell: AddMediaLayoutDelegate {
  func collectionView(
    _ collectionView: UICollectionView,
    heightForMediaItemAtIndexPath indexPath:IndexPath) -> CGSize {
    return self.heightArray[indexPath.item]
  }
    
    func setHeightArray() -> [CGSize]{
        return self.heightArray
    }
}
