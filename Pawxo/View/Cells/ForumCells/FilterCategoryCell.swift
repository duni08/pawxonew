//
//  FilterCategoryCell.swift
//  Pawxo
//
//  Created by 42works on 21/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class FilterCategoryCell: UITableViewCell {

    @IBOutlet weak var checkBoxImageV: UIImageView!
    
    @IBOutlet weak var categoryCountLabel: RegularLabel!
    
    @IBOutlet weak var categoryName: RegularLabel!
    
    var selectedCheckBox = UIImage(named: "checkBoxSelected")
    var unSelectedCheckBox = UIImage(named: "checkBoxUnselected")
    
    var isChecked = false
    
    var category : Category?
    
    var categorySelected : ((Bool)->())?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        isChecked = false
        self.checkBoxImageV.image = unSelectedCheckBox
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
        func setCategory(category:Category){
        self.category = category
        self.categoryName.text = category.name!
        self.categoryCountLabel.text = "(\((category.count)!))"
            if let isSelectedCategory = self.category!.isSelectedCategory, isSelectedCategory == true{
                self.isChecked = true
              self.checkBoxImageV.image = selectedCheckBox
            }else{
                self.isChecked = false
                self.checkBoxImageV.image = unSelectedCheckBox
            }

    }
    
    
    @IBAction func selectCategory(_ sender: Any) {
        
        self.isChecked = !self.isChecked
        
        self.categorySelected!(self.isChecked)
        
    }
    
}
