//
//  AddForumCell.swift
//  Pawxo
//
//  Created by 42works on 09/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import Foundation

protocol AddForumCellProtocol {
    func mediaAddPressed()
    func categoryPressed(id:Int)
    func shownHashTagTable(cell:AddForumCell,show:Bool)
}

typealias locationResponseCallBack = (Bool?, String?) -> Void


class AddForumCell: UITableViewCell {
    
    
    @IBOutlet weak var tickIcon: UIImageView!
    @IBOutlet weak var hashTagTableView: UITableView!
    @IBOutlet weak var titleTF: RegularTextField!
    
    @IBOutlet weak var dropDownArrow: UIImageView!
    
    @IBOutlet weak var locationTF: RegularTextField!
    
    @IBOutlet weak var descriptionPlaceholderLabel: LightLabel!
    
    @IBOutlet weak var categoryTF: RegularTextField!
    
    @IBOutlet weak var descriptionTV: UITextView!
    
    var delegate : AddForumCellProtocol?
    
    var categories : [Category]?
    
    var pickerView: UIPickerView!
    
    
    var tapGesture : UITapGestureRecognizer?
    var globalRange = NSRange()
    var mutString = NSMutableString()
    var isupdateTable = false
    var globalInt : Int = 1
    var addName = NSMutableArray()
    var idToSendApi = NSMutableArray()
    var currentWord : String = ""
    var updatedWord : String = ""
    var rangeToAppend = NSRange()
    var removeCount : Int = 0
    
    var userList = [Userlist]()
    
    var hashTagList = [HashTag]()
    var isUserTable = false
    var createEditVM : CreateEditPost_VM?
    var tagCellIdentifier = "TagCell"
    var taggedUserList = [Userlist]()
    
    var showLocation :((AddForumCell)->())?
    
    var forumList : ForumList?
    
    
    @IBOutlet weak var userTableHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        
       
        self.hashTagTableView.isHidden = true
        hashTagTableView.register(UINib(nibName: tagCellIdentifier, bundle: nil), forCellReuseIdentifier: tagCellIdentifier)
        // Initialization code
        setUpPlaceHolderLabelView()
        self.tickIcon.isHidden = true
        
        let degreesToRadians: (CGFloat) -> CGFloat = { (degrees: CGFloat) in
            return degrees / 180.0 * CGFloat.pi
        }
        self.dropDownArrow.transform = CGAffineTransform(rotationAngle:degreesToRadians(-90))
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpPlaceHolderLabelView(){
        if self.descriptionTV.text.count > 0 {
            self.descriptionPlaceholderLabel.alpha = 0
        }else{
            self.descriptionPlaceholderLabel.alpha = 1
        }
    }
    
    func setEditView(forumlist:ForumList){
        self.forumList = forumlist
        self.titleTF.text = forumlist.title ?? ""
        self.tickIcon.isHidden = false
        self.descriptionTV.text = forumlist.content ?? ""
        self.categoryTF.text = forumlist.category?.name ?? ""
        self.locationTF.text = forumlist.location_name ?? ""
        setUpPlaceHolderLabelView()
        var index = 0
        if let forumlist = self.forumList{
            index = (self.categories?.lastIndex{
                $0.name == forumlist.category?.name
                })!
        }
        delegate?.categoryPressed(id:categories![index].id!)
    }
    
    
    
    @IBAction func locationButtonPressed(_ sender: Any) {
        
        self.showLocation?(self)
        
    }
    
    
    @IBAction func categoryButtonPressed(_ sender: Any) {
        self.ShowPickerView(textField: categoryTF)
        var index = 0
        if let forumlist = self.forumList{
            index = (self.categories?.lastIndex{
                $0.name == forumlist.category?.name
                })!
        }
        self.pickerView.selectRow(index, inComponent: 0, animated: true)
        self.pickerView(self.pickerView, didSelectRow: index, inComponent: 0)
        
    }
    
    
    @objc func doneClick(sender : UIButton) {
        
        categoryTF.resignFirstResponder()
        
        //        catSelected?(self.categoryTextField.text ?? "")
    }
    
    func ShowPickerView(textField:UITextField){
        self.pickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 216))
                self.pickerView.backgroundColor = UIColor.white
                textField.inputView = self.pickerView
                
        //        let pickerCategory = CustomPickerView()
        //        pickerCategory.array = arrCategory
                pickerView.delegate = self
                pickerView.dataSource = self
                
                // ToolBar
                let toolBar = UIToolbar()
                toolBar.barStyle = .default
                toolBar.isTranslucent = true
                toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
                toolBar.sizeToFit()
                
                // Adding Button ToolBarz
            
            let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneClick))
            doneButton.tag = textField.tag
        
                doneButton.setTitleTextAttributes([
                    NSAttributedString.Key.font: UIFont.JosefinSansRegular(size: 18.0),
                    NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
                
                let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
                toolBar.setItems([spaceButton, doneButton], animated: false)
                toolBar.isUserInteractionEnabled = true
            textField.inputAccessoryView = toolBar
            textField.becomeFirstResponder()
        if let forumList = self.forumList {
            
        }else{
            self.categoryTF.text = self.categories![0].name!
        }
        
    }
    
    @IBAction func shareButtonPressed(_ sender: Any) {
    }
    
    
    @IBAction func addMediaButtonPressed(_ sender: Any) {
        delegate?.mediaAddPressed()
    }
    
    
}



extension AddForumCell : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
                    if newText.count > 0 {
                        descriptionPlaceholderLabel.alpha = 0
                    }else{
                        descriptionPlaceholderLabel.alpha = 1
                    }
            
            globalRange = range
            if text == "\n" {
                textView.resignFirstResponder()
                mutString = NSMutableString()
            } else {
                if text == "" {
                    var str:String = mutString as String
                    if str != "" {
                        str.remove(at: str.index(before: str.endIndex))
                        mutString = NSMutableString()
                        mutString.append(str)
                    }
                }
                if descriptionTV.text.length == 0
                {
                    isupdateTable = false
                }
                if text == " " {
                    mutString = ""
                    isupdateTable = false
                } else {
                    mutString.append(text)
                }
                let isBackSpace = strcmp(text, "\\b")
                if (isBackSpace == -92) {
                    globalInt = 0
                    
                    //removeCount = removeCount-1
                }else
                {
                    globalInt = 1
                    //removeCount = removeCount+1
                }
                
                if isupdateTable == true {
                    showTaggedUserSuggestion(mutString)
                }
                
                if text == "@" {
                    mutString = NSMutableString()
                    isupdateTable = true
                }
                
                var isExist = false
                var index = 0
                for i in 0 ..< addName.count {
                    index = i
                    isExist = false
                    if descriptionTV.text!.range(of: addName[i] as! String) != nil {
                        isExist = true
                    }
                    
                    if isExist == false {
                        if idToSendApi.count <= index+1 {
                            idToSendApi.removeObject(at: index)
                            addName.removeObject(at: index)
                        }
                    }
                }
            }
            
            return true
        }
        
        func showTaggedUserSuggestion(_ search : NSMutableString){
            print("search", search)
            if search.length <= 1
            {
                return
            }
            else{
               // setPopOver(textToSeach: search as String)
                print(" query for users")
    //            objUserView.search(search: mutString as String, idArray:idToSendApi, oncompletion: { (height) -> Void in
    //                if height ?? 0 >= 9 {
    //                    self.objUserView.frame = CGRect(x: 50, y: self.descriptionTextView.frame.maxY + 60, width: self.view.frame.width - 80, height: 260)
    //                }else {
    //                    self.objUserView.frame = CGRect(x: 50, y: self.descriptionTextView.frame.maxY + 60, width: self.view.frame.width - 80, height: CGFloat(height! * 45))
    //                }
    //            })

            }
        }
        
        
        func textViewDidChange(_ textView: UITextView) {
            let text = textView.text as NSString
            if text == ""
            {
                return
            }
            else{
            let substring = text as String
            currentWord = substring.components(separatedBy: " ").last!
            var wordWithSpace = substring.components(separatedBy: " ").last!
            wordWithSpace = " "+wordWithSpace
            let lastChar = String(currentWord.suffix(1))
            
            
           
            if (wordWithSpace.contains(" #")) && !(lastChar==" ")
            {
                var textWidth = textView.frame.inset(by: textView.textContainerInset).width
                textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
                let boundingRect =  substring.rectForText(withConstrainedWidth: textWidth, font: textView.font!)   //sizeOfString(string: substring, constrainedToWidth: Double(textWidth), font: textView.font!)
                let numberOfLines = boundingRect.height / textView.font!.lineHeight;
                
                updatedWord =  currentWord
                print("==== HashTag",updatedWord)
                
                self.setPopOverForHashTag(textToSeach: updatedWord)
                //self.hashSearchApi(currentWord: updatedWord, numberOfLines: Int(numberOfLines))
                
                if let range = textView.selectedTextRange {
                    if range.start == range.end {
                        let beginning = textView.beginningOfDocument
                        let selectedRange = textView.selectedTextRange
                        let selectionStart = selectedRange?.start
                        let selectionEnd = selectedRange?.end
                        let location = textView.offset(from: beginning, to: selectionStart!)
                        let length = textView.offset(from: selectionStart!, to: selectionEnd!)
                        print(length)
                        if removeCount<0
                        {
                            removeCount = 0
                        }
                        //print("currentWord======",currentWord)
                        rangeToAppend =  NSMakeRange((location-(currentWord.count)), (currentWord.count))
                        print("rangeToAppend location ==",rangeToAppend.location)
                        print("rangeToAppend length ==",rangeToAppend.length)
                        if rangeToAppend.length == 1{
                            self.showUserTableView(show: false)
                        }
                        
                        
                    }
                }
            } else if (wordWithSpace.contains(" @")) && !(lastChar==" ")
            {
                updatedWord =  currentWord.replacingOccurrences(of: "@", with: "")
             
                if let range = textView.selectedTextRange {
                    if range.start == range.end {
                        let beginning = textView.beginningOfDocument
                        let selectedRange = textView.selectedTextRange
                        let selectionStart = selectedRange?.start
                        let selectionEnd = selectedRange?.end
                        let location = textView.offset(from: beginning, to: selectionStart!)
                        let length = textView.offset(from: selectionStart!, to: selectionEnd!)
                        print(length)
                        if removeCount<0
                        {
                            removeCount = 0
                        }
                        print("currentWord======",currentWord)
                        rangeToAppend =  NSMakeRange((location-(currentWord.count)), (currentWord.count))
                        print("rangeToAppend location ==",rangeToAppend.location)
                        print("rangeToAppend length ==",rangeToAppend.length)
                        if rangeToAppend.length == 1{
                            self.showUserTableView(show: false)
                        }
                    }
                }
              }
            }
        }
        
        func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        // change    transparentBGView.isHidden = false
        // change    postButton.isSelected = true
            return true
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
           // change transparentBGView.isHidden = true
           // change postButton.isSelected = false
        }
        
    
        
        
        func showUserTableView(show:Bool){
            self.hashTagTableView.isHidden = !show
            
            if show {
                self.delegate?.shownHashTagTable(cell: self, show: show)
            }else{
                self.delegate?.shownHashTagTable(cell: self, show: show)
                if self.userList.count > 0 {
                    self.userList.removeAll()
                }
                if self.hashTagList.count > 0 {
                    self.hashTagList.removeAll()
                }
            }
            
            self.hashTagTableView.reloadData()
            
            if isUserTable {
                
                if self.userList.count > 5 {
                    self.userTableHeight.constant = 200
                }else{
                    if self.userList.count < 4 {
                        self.userTableHeight.constant = 150.0
                    }else{
                        self.userTableHeight.constant = CGFloat(self.userList.count * 50)
                    }
                    
                }
            }else{
                if self.hashTagList.count > 5 {
                    self.userTableHeight.constant = 200
                }else{
                    if self.hashTagList.count < 4 {
                        self.userTableHeight.constant = 150.0
                    }else{
                        self.userTableHeight.constant = CGFloat(self.hashTagList.count * 50)
                    }
                    
                }
            }
        }
        
        
        @objc func handle(sender: UITapGestureRecognizer) {
            if sender.state == .ended {
                showUserTableView(show: false)
            }
            sender.cancelsTouchesInView = false
        }
        
        
        
        func setIdAndNameUser(id:String, name:String) {
                
                isupdateTable = false
                
                var str : NSMutableString = NSMutableString(string:descriptionTV.text)
                let newString = "@" + name + " "
                str.replaceCharacters(in: rangeToAppend, with: newString)
                //str = str.replacingCharacters(in: rangeToAppend, with: newString) as! NSMutableString
                descriptionTV.text = str.copy() as? String ?? ""
                
        //        let commentTemp = self.descriptionTextView.text.replacingOccurrences(of: "@", with: "@")
               // let comment =  self.descriptionTextView.text.replacingOccurrences(of: mutString as String, with:"")
                
                let name = name.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                print(name)
                print(id)
                
                if self.addName.contains(name) {
                    
                } else {
                    self.addName.add(name)
                    self.idToSendApi.add(id)
                }
                
        //        self.descriptionTextView.text = ""
        //        //self.commentTextView.textColor = UIColor.black
        //        self.descriptionTextView.text = "\(comment)\(name)"
                //setRedColor(text: "\(comment)\(name)")
                self.showUserTableView(show: false)
            }
        
        //MARK: POPover
        func setPopOver(textToSeach :String){
            let userLogin = CommonFunctions.getuserFromDefaults()
                    createEditVM = CreateEditPost_VM()
                    
                    createEditVM?.userId = userLogin.userId
                    createEditVM?.perPage = "10"
                    createEditVM?.page = "1"
                    createEditVM?.nameStr = textToSeach
                    createEditVM?.callGetUserSearchListService = true
                    createEditVM?.responseRecieved = {
                        self.userList = self.createEditVM!.userList
                        // refresh content
                        self.isUserTable = true
                        if self.userList.count == 0 {
                            self.showUserTableView(show: false)
                        }else{
                            self.showUserTableView(show: true)
                        }
                        //self.setPetViewHeight()
                    }
                    
                    createEditVM?.showAlertClosure = {[weak self] () in
                        DispatchQueue.main.async {
                            if let message = self?.createEditVM?.alertMessage {
                                //self?.showAlert(self ?? UIViewController(), message: message)
                                self?.userList.removeAll()
                                self?.showUserTableView(show: false)
                            }
                        }
                    }
        }
        
        //MARK: POPover
        func setPopOverForHashTag(textToSeach :String){
            
            if currentWord.length < 2 {
                return
            }
            var searchText = currentWord
            searchText.remove(at: searchText.startIndex)
            
            
            let userLogin = CommonFunctions.getuserFromDefaults()
                    createEditVM = CreateEditPost_VM()
                    
                    createEditVM?.userId = userLogin.userId
                    createEditVM?.nameStr = searchText
                    
                    createEditVM?.isForumHahTag = true
                    createEditVM?.responseRecieved = {
                        self.hashTagList = self.createEditVM!.hashTagList
                        // refresh content
                        self.isUserTable = false
                        
                        
                        if self.hashTagList.count == 0 {
                            self.showUserTableView(show: false)
                        }else{
                            self.showUserTableView(show: true)
                        }
                        //self.setPetViewHeight()
                    }
                    
                    createEditVM?.showAlertClosure = {[weak self] () in
                        DispatchQueue.main.async {
                            if let message = self?.createEditVM?.alertMessage {
                                //self?.showAlert(self ?? UIViewController(), message: message)
                                self?.userList.removeAll()
                                self?.showUserTableView(show: false)
                            }
                        }
                    }
            
                createEditVM?.callGetHashTAGListService = true
        }
    
}


//MARK:- PickerView Delegate and DataSource
extension AddForumCell: UIPickerViewDataSource, UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories!.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories![row].name!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.categoryTF.text = self.categories![row].name!
        delegate?.categoryPressed(id:categories![row].id!)
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view as? UILabel { label = v }
        label.font = UIFont.JosefinSansRegular(size: 18.0)
        
        label.text = categories![row].name!
       
        label.textAlignment = .center
        return label
    }
    
}

extension AddForumCell : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text as? NSString)!.replacingCharacters(in: range, with: string)
        
        if newText.count > 0{
            self.tickIcon.isHidden = false
        }else{
            self.tickIcon.isHidden = true
        }
        
        return true
    }
}

extension  AddForumCell : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isUserTable{
        return self.userList.count
        }else{
            return self.hashTagList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isUserTable{
            let cell : UserSearchCell =  tableView.dequeueReusableCell(withIdentifier: "UserSearchCell", for: indexPath) as! UserSearchCell
            //        cell.homeCellDelegate = self
            // cell.indexNumber = indexPath
            let user = self.userList[indexPath.row]
            //        let post = postList[indexPath.row]
            //
            //        cell.setPost(post: post)
            cell.setUI(user: user)
            return cell
        }else{
            let cell : TagCell =  tableView.dequeueReusableCell(withIdentifier: tagCellIdentifier, for: indexPath) as! TagCell
            //cell.homeCellDelegate = self
            let hashTag = self.hashTagList[indexPath.row]
            cell.setHashTag(hashTag:hashTag)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isUserTable{
            let user = self.userList[indexPath.row]
            self.taggedUserList.append(user)
            //self.userList.removeAll()
            self.setIdAndNameUser(id: "\(user.id!)", name: "\(user.username!)")
        }else{
            // for hashTag
            
            var str : NSMutableString = NSMutableString(string:descriptionTV.text)
            let newString = "#" + hashTagList[indexPath.row].name! + " "
            str = str.replacingCharacters(in: rangeToAppend, with: newString) as! NSMutableString
            descriptionTV.text = str.copy() as! String
            self.showUserTableView(show: false)
            descriptionTV.becomeFirstResponder()
            
        }
    }
}
