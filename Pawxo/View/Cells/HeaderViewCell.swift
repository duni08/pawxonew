//
//  HeaderViewCell.swift
//  Pawxo
//
//  Created by 42works on 07/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class HeaderViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var centerImageView: UIImageView!
    
    @IBOutlet weak var petImageView: UIImageView!
    
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
//            self.centerImageView.isHidden = false
//        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setCenterImage(show:Bool){
        self.centerImageView.isHidden = !show
        self.petImageView.isHidden = !show
    }
    
    
}
