//
//  GoogleAutocompleteCell.swift
//  Pawxo
//
//  Created by Apple on 22/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class GoogleAutocompleteCell: UITableViewCell {

    @IBOutlet weak var locationTitle_lbl: UILabel!
    @IBOutlet weak var locationSubTitle_lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
