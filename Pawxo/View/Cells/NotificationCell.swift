//
//  NotificationCell.swift
//  Pawxo
//
//  Created by 42works on 08/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
protocol notificationListProtocol {
    func openProfile(id: String)
    func getNotificationList()
    func openPost(postId: String)
    func openforum(postId: String)
}
class NotificationCell: UITableViewCell {
   
    @IBOutlet weak var outerView: UIView!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileImageButton: UIButton!

    @IBOutlet weak var headingLabel: LightLabel!
    var delegate: notificationListProtocol?
    @IBOutlet weak var notificationImageV: UIImageView!
    @IBOutlet weak var notificationButton: RegularButton!
    @IBOutlet weak var notificationImageVButton: UIButton!
    @IBOutlet weak var notificationRejectButton: RegularButton!
    @IBOutlet weak var textTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var textBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var textTopConstraint: NSLayoutConstraint!

    var notification: NotificationList?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //
       // self.setAttributtedText()
    }
    
    
    func setAttributtedText(name:String,text:String){
        
        let str =  NSMutableAttributedString(string:"\(name) ", attributes: [NSAttributedString.Key.foregroundColor: AppColor.darktextColor,NSAttributedString.Key.font : UIFont.init(name: "JosefinSans-Regular", size: self.headingLabel.font.pointSize)!])
        
        let str2 =  NSMutableAttributedString(string:text, attributes: [NSAttributedString.Key.foregroundColor: AppColor.colorGreyTime,NSAttributedString.Key.font : UIFont.init(name: "JosefinSans-Regular", size: self.headingLabel.font.pointSize)!])
        let finalStr  = NSMutableAttributedString()
        finalStr.append(str)
        finalStr.append(str2)
        self.headingLabel.attributedText = finalStr
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setNotification(notification:NotificationList)
    {
        self.notification = notification
        print(self.notification!)
        let name = self.notification?.name
        self.setAttributtedText(name: name!, text: self.notification!.notification!)
        self.profileImage.setImage(fromUrl: self.notification!.image ?? "" , defaultImage: DefaultImage.defaultProfileImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: true)
       
       
        if(self.notification!.notificationType == "follow_request")
                {
                    if(self.notification!.isAccepted == 0)
                    {
                    self.notificationButton.isHidden = false
                    self.notificationRejectButton.isHidden = false
                    self.notificationImageV.isHidden = true
                    self.textTrailingConstraint.constant = 75
                    self.textBottomConstraint.constant = 37.5
                    self.textTopConstraint.constant = 10
                    self.headingLabel.translatesAutoresizingMaskIntoConstraints = false
                    }
                     else
                    {
                        self.notificationButton.isHidden = true
                        self.notificationRejectButton.isHidden = true
                        self.notificationImageV.isHidden = false
                        self.textTrailingConstraint.constant = 75
                        self.textBottomConstraint.constant = 20
                        self.textTopConstraint.constant = 20
                        self.headingLabel.translatesAutoresizingMaskIntoConstraints = false
                    }
//                    if(self.notification!.isAccepted == 0 && self.notification?.isRead == 1)
//                    {
//                        self.notificationButton.isHidden = true
//                        self.notificationRejectButton.isHidden = true
//                        self.notificationImageV.isHidden = false
//                        self.textTrailingConstraint.constant = 75
//                        self.textBottomConstraint.constant = 20
//                        self.textTopConstraint.constant = 20
//                        self.headingLabel.translatesAutoresizingMaskIntoConstraints = false
//                    }
                    //self.headingLabel.centerXAnchor.constraint(equalTo: profileImage.centerXAnchor).isActive = false
                    
                   // cell.setNotificationType(type: notifications[indexPath.row].isAccepted!)
                }
            else if(self.notification!.notificationType == "follow")
        {
            self.notificationButton.isHidden = true
            self.notificationRejectButton.isHidden = true
            self.notificationImageV.isHidden = true
            self.textTrailingConstraint.constant = 75
           self.textBottomConstraint.constant = 20
            self.textTopConstraint.constant = 20
            self.headingLabel.translatesAutoresizingMaskIntoConstraints = false
             //self.headingLabel.centerXAnchor.constraint(equalTo: profileImage.centerXAnchor).isActive = true
            
        }
                else
                {
                    self.notificationButton.isHidden = true
                    self.notificationRejectButton.isHidden = true
                     self.notificationImageV.isHidden = false
                      self.textTrailingConstraint.constant = 75
                self.textBottomConstraint.constant = 20
                    self.textTopConstraint.constant = 20
                    self.headingLabel.translatesAutoresizingMaskIntoConstraints = false
                 //   self.headingLabel.centerXAnchor.constraint(equalTo: profileImage.centerXAnchor).isActive = true
                   
                }
        if(self.notification!.notificationType == "forum")
        {
            self.notificationButton.isHidden = true
            self.notificationRejectButton.isHidden = true
            self.notificationImageV.isHidden = true
            self.textTrailingConstraint.constant = 75
           self.textBottomConstraint.constant = 20
            self.textTopConstraint.constant = 20
            self.headingLabel.translatesAutoresizingMaskIntoConstraints = false
             //self.headingLabel.centerXAnchor.constraint(equalTo: profileImage.centerXAnchor).isActive = true
            
        }
//                else
//                {
//                    self.notificationButton.isHidden = true
//                    self.notificationRejectButton.isHidden = true
//                     self.notificationImageV.isHidden = false
//                      self.textTrailingConstraint.constant = 75
//                self.textBottomConstraint.constant = 20
//                    self.textTopConstraint.constant = 20
//                    self.headingLabel.translatesAutoresizingMaskIntoConstraints = false
//                 //   self.headingLabel.centerXAnchor.constraint(equalTo: profileImage.centerXAnchor).isActive = true
//
//                }
        if(self.notification!.postMedia != "")
            {
                self.notificationImageV.isHidden = false
                self.notificationImageV.setImage(fromUrl: self.notification!.postMedia ?? "" , defaultImage: DefaultImage.defaultProfileImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: true)
            }
        else
            {
                self.notificationImageV.isHidden = true
            }
        if(self.notification!.notificationType == "admin")
        {
            self.profileImageButton.isEnabled = false
        }
        else
        {
            self.profileImageButton.isEnabled = true
        }
               // self.profileImageButton.addTarget(self, action: #selector(moveToProfile(sender:)), for: .touchUpInside)
                //self.profileImageButton.tag = indexPath.row
    }
    
    func setNotificationType(type:Int){
        if type == 0{
            self.notificationButton.isHidden = false
            self.notificationImageV.isHidden = true
            self.textTrailingConstraint.constant = 80
        }else if type == 1{
            self.notificationButton.isHidden = true
            self.notificationImageV.isHidden = false
            self.textTrailingConstraint.constant = 75
        }else{
            self.notificationButton.isHidden = true
            self.notificationImageV.isHidden = true
            self.textTrailingConstraint.constant = 8
        }
    }
    @IBAction func ProfileButtonPressed(_ sender: Any) {
        DispatchQueue.main.async {
        let id = self.notification?.sender_id
            self.delegate?.openProfile(id: "\(id ?? 0)")
        }
    }
    @IBAction func ForumButtonPressed(_ sender: Any)
    {   DispatchQueue.main.async {
        if self.notification?.notificationType == "post"{
            let id = self.notification?.id
            self.delegate?.openPost(postId: "\(id ?? 0)")
        }else if self.notification?.notificationType == "forum"{
            let id = self.notification?.id
            self.delegate?.openforum(postId: "\(id ?? 0)")
        }
        }
    }
   
     @IBAction func AcceptButtonPressed(_ sender: Any)
     {
        let id = self.notification?.id
      //  print(id)
        let status = "1"
        callAcceptRejectRequest(id: "\(id ?? 0)", status: status)
    }
     @IBAction func RejectButtonPressed(_ sender: Any)
     {
        let id = self.notification?.id
     //   print(id)
               let status = "2"
       callAcceptRejectRequest(id: "\(id ?? 0)", status: status)
    }
    func callAcceptRejectRequest(id: String, status: String)
    {
        let notificationVM = Notification_VM()
                  let userLogin = CommonFunctions.getuserFromDefaults()
               notificationVM.userId = userLogin.userId
               notificationVM.request_id = id
        notificationVM.requests_status = status
        notificationVM.updateLoadingStatus = { [weak self] () in
                      DispatchQueue.main.async {
                       if notificationVM.isLoading ?? false {
                              LoaderView.shared.showLoader()
                          } else {
                              LoaderView.shared.hideLoader()
                          }
                      }
                  }
                  
               notificationVM.responseRecieved = {
                self.delegate?.getNotificationList()
                   //self.notifications.append(notificationVM.notification_list)
                      // refresh content
                      
                  }
                  
               notificationVM.showAlertClosure = {[weak self] () in
                      DispatchQueue.main.async {
                       if let message = notificationVM.alertMessage {
                            
                          }
                      }
                  }
                  
               notificationVM.callAcceptNotificationRequestService = true
            
    }
   
}




