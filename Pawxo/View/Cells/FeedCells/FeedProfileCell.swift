//
//  FeedProfileCell.swift
//  Pawxo
//
//  Created by 42works on 15/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

protocol FeedProfileCellDelegate {
    func editPetButtonPressed()
    func editPetImagePressed(cell:FeedProfileCell)
}


class FeedProfileCell: UITableViewCell {

    @IBOutlet weak var outerStackView: UIStackView!
    @IBOutlet weak var descriptionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionTopConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var categoryWidthConstraint: NSLayoutConstraint!
    
    var delegate : FeedProfileCellDelegate?
    
    @IBOutlet weak var editPetImageButton: UIButton!
    @IBOutlet weak var editpetButton: RegularButton!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var nameLabel: RegularLabel!
    
    @IBOutlet weak var descriptionLabel: RegularLabel!
    
    @IBOutlet weak var locationLabel: RegularLabel!
    
    @IBOutlet weak var categoryLabel: RegularLabel!
    
    @IBOutlet weak var genderLabel: RegularLabel!
    
    
    var pet : PetList?
    
    @IBOutlet weak var dividerView: DividerView!
    
    
    @IBOutlet weak var yearLabel: RegularLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImageProfile(image:UIImage){
        self.profileImage.image = image
    }
    
    
    func setPet(pet:PetList){
        self.pet = pet
        self.categoryLabel.text = pet.petBreed ?? pet.petType ?? ""
        
       
        
        //self.genderLabel.text = pet.petGender ?? ""
        self.locationLabel.text = pet.location ?? ""
        self.nameLabel.text = pet.petName ?? ""
        
        if let dob = pet.petDob , dob != "" ,dob != "0000-00-00" {
            self.yearLabel.text = dob.yearsOld(numericDates: true)
        }else{
            self.yearLabel.text = "N/A"
        }
        
        if let gender  = pet.petGender {
            if gender == "Boy"{
                self.genderLabel.text = "Male"
            }else if gender == "Girl"{
                self.genderLabel.text = "Female"
            }else{
                self.genderLabel.text = gender
            }
        }
        
        
        self.profileImage.setImage(fromUrl: pet.petImages ?? "", defaultImage: DefaultImage.defaultPetImage)
        
        let labelText = pet.petBio ?? ""
        self.descriptionLabel.text = labelText
        let textHeight = labelText.height(withConstrainedWidth: self.descriptionLabel.frame.width , font: self.descriptionLabel.font)
        //self.descriptionHeightConstraint.constant = textHeight
    }
    
    
    func setMyPet(isMyPet:Bool){
        self.editpetButton.isHidden = !isMyPet
        self.editPetImageButton.isHidden = !isMyPet
        if isMyPet {
            if let bio = self.pet?.petBio, bio != "" {
                self.descriptionTopConstraints.constant = 60
            }else{
                self.descriptionTopConstraints.constant = 30
            }
        }else{
            if let bio = self.pet?.petBio, bio != "" {
                self.descriptionTopConstraints.constant = 32
            }else{
                self.descriptionTopConstraints.constant = 0
            }
            
            
        }
    }
    
    func showDividerView(show:Bool){
        self.dividerView.isHidden = !show
    }
    
    func calculateWidthForCateogry(){
        let str = self.categoryLabel.text
        var calculatedWidth = str!.width(withConstrainedHeight: CGFloat (21.0), font: self.categoryLabel.font)
        
        if calculatedWidth < 80 {
            calculatedWidth = 80
        }else if calculatedWidth > (self.outerStackView.frame.width/2 - 25){
            //self.categoryWidthConstraint.constant = (self.outerStackView.frame.width/2 - 25)
        }else{
           // self.categoryWidthConstraint.constant = 25 + calculatedWidth
        }
        
    }
    
    
    
    @IBAction func editProfilePressed(_ sender: Any) {
        
        delegate?.editPetImagePressed(cell: self)
    }
    
    @IBAction func editPetPressed(_ sender: Any) {
        delegate?.editPetButtonPressed()
        
    }
    
}
