//
//  FeedPostCell.swift
//  Pawxo
//
//  Created by 42works on 15/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit


protocol FeedPostCellDelegate {
   func setheight(height:CGFloat)
    func gotoPostDetailScreen(post:PostList)
}

class FeedPostCell: UITableViewCell {

    
    
    @IBOutlet weak var feedPostCollectionView: UICollectionView!
    
    let cellIdentifier = "MediaAddCell"
    var staticCount = 30
    
    var posts =  [PostList]()
    var delegate : FeedPostCellDelegate?
    
    var getHeight = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        feedPostCollectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        // Initialization code
        setLayout()
        
      }
    
    
    override func layoutSubviews() {
        if getHeight{
            self.perform(#selector(changeHeight), with: nil, afterDelay: 0.5)
            //self.feedPostCollectionView.isScrollEnabled = true
        }else{
            //self.feedPostCollectionView.isScrollEnabled = false
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setLayout(){
        let layout = TestLayout()
        layout.padding = 2
        feedPostCollectionView.setCollectionViewLayout(layout, animated: false)
    }
    
    func setPosts(posts:[PostList]){
        self.posts = posts
        setLayout()
        self.feedPostCollectionView.reloadData()
//        if getHeight{
//            self.perform(#selector(changeHeight), with: nil, afterDelay: 0.5)
//        }
    }
    
    @objc func changeHeight(){
        delegate?.setheight(height: self.feedPostCollectionView!.contentSize.height)
    }
    
}


extension FeedPostCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let mediaCell : MediaAddCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! MediaAddCell
        
       // print(indexPath.item/3)
        
        let post = self.posts[indexPath.item]
        
        
        if post.type == "image"{
            mediaCell.videoButton.isHidden = true
            
            mediaCell.imgView.setImage(fromUrl: post.media?.first ?? "", defaultImage: DefaultImage.defaultPostImage)
        }else{
            mediaCell.videoButton.isHidden = false
           
            mediaCell.imgView.setImage(fromUrl: post.thumbnail?.first ?? "", defaultImage: DefaultImage.defaultPostImage)
        }
        
        
        return mediaCell
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        //let height =  self.bounds.size.height - (self.view.safeAreaInsets.top + self.view.safeAreaInsets.bottom + 101)
//        return self.heightArray[indexPath.item]
//    }
       // return CGSize(width: 100, height: 100)
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print(indexPath.item)
         let post = self.posts[indexPath.item]
        self.delegate?.gotoPostDetailScreen(post: post)
        
    }
    
}
