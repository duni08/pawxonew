//
//  PetListCell.swift
//  Pawxo
//
//  Created by 42works on 15/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

protocol PetListCellDelegate {
    func choosedPet(cell:PetListCell)
}

class PetListCell: UITableViewCell {

    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var petNameLabel: LightLabel!
    @IBOutlet weak var profileImageView: UIImageView!
    var delegate : PetListCellDelegate?
    var pet : PetList?
    var isSelectedPet = false{
        didSet {
            if isSelectedPet {
                selectedImageView.isHidden = false
            }else{
                selectedImageView.isHidden = true
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setImage(color: .white, imgView: self.selectedImageView)
        
    }
    
    func setPet(pet:PetList){
        self.pet = pet
//        isSelectedPet = false //pet.isSelectedPet
        self.petNameLabel.text = pet.petName
        
        self.profileImageView.setImage(fromUrl: pet.petImages ?? "", defaultImage: DefaultImage.defaultPetImage)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    @IBAction func selectPetPressed(_ sender: UIButton) {
        isSelectedPet = true
       // pet?.isSelectedPet = true
        delegate?.choosedPet(cell: self)
    }
    
    
    public func setImage(color: UIColor, imgView: UIImageView) {
            
            if #available(iOS 13.0, *) {
                imgView.image =  imgView.image?.withTintColor(color, renderingMode: .alwaysTemplate)
            } else {
                // Fallback on earlier versions
               imgView.image  = imgView.image?.withRenderingMode(.alwaysTemplate)
                imgView.tintColor = color
            }
    //        self.setImage(imgView.image?.withRenderingMode(.alwaysTemplate) i )
        }
}
