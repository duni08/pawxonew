//
//  PushNotificationCell.swift
//  Pawxo
//
//  Created by Apple on 15/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit


protocol PushNotificationCellProtocol {
    func switchChanged(cell:PushNotificationCell)
}


class PushNotificationCell: UITableViewCell {

    @IBOutlet weak var title_Lbl: UILabel!
    @IBOutlet weak var swicth_Btn: UISwitch!
    
    var index = 0 
    var delegate : PushNotificationCellProtocol?
    var setting : PushSetting?
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    func setSettings(setting:PushSetting){
        self.setting = setting
        self.title_Lbl.text = setting.title
        self.swicth_Btn.isOn = setting.isEnabled.boolValue
        self.swicth_Btn.tag = index
    }
    
    
    @IBAction func buttonChanged(_ sender: UISwitch) {
        
        self.delegate?.switchChanged(cell: self)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}


extension Int {
    var boolValue: Bool {
        return self != 0
    }
}


extension Bool {
    var intValue: Int {
        return self ? 1 : 0
    }
}
