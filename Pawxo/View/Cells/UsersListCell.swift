//
//  LikesCell.swift
//  Pawxo
//
//  Created by 42works on 07/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

enum UserProfileStatus {
    case IsPrivate
    case IsPublic
    
}

enum FollwowStatus {
    case isFollwed
    case isNotFollowed
}

enum RequestStatus {
    case isRequested
    case isNotRequested
}


enum ButtonStatus {
    case Follow
    case Requested
    case Following
}



protocol UsersListCellProtocol {
    func profilePressed(cell:UsersListCell)
    func followbuttonPressed(cell:UsersListCell)
}




struct UserAlerts {
    static let A = ""
    static let B = ""
    static let C = ""
    static let D = ""
    static let E = ""
    static let F = ""
    
}


class UsersListCell: UITableViewCell {
    
    
    @IBOutlet weak var userProfileImageView: UIImageView!
    
    @IBOutlet weak var followButton: RegularButton!
    
    var delegate : UsersListCellProtocol?
    var user : Userlist?
    
    
    var buttonStatus = ButtonStatus.Follow
    
    var alertString  = ""
    
    var profileStatus = profileFollowStatus.requested
    
    @IBOutlet weak var subheadingLabel: LightLabel!
    
    @IBOutlet weak var headingLabel: RegularLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setUpButtonStatus(profileStatus:profileFollowStatus){
        switch profileStatus {
        case .requested:
            print("requested")
            self.followButton.setTitle("Requested", for: .normal)
            self.setFollowButtonFilled(fill: false)
        case .follow:
            print("follow")
            self.followButton.setTitle("Follow", for: .normal)
            self.setFollowButtonFilled(fill: true)
        case .following:
            print("following")
            self.followButton.setTitle("Following", for: .normal)
            self.setFollowButtonFilled(fill: false)
        default:
            print("Edit")
        }
    }
    
    
    func setFollowButtonFilled(fill:Bool){
        if !fill{
            self.followButton.backgroundColor = AppColor.baseColor
            self.followButton.setTitleColor(.white, for: .normal)
            self.followButton.borderWidth = 0
        }else{
            self.followButton.backgroundColor = .white
            self.followButton.setTitleColor(AppColor.baseColor, for: .normal)
            self.followButton.borderWidth = 1.5
            self.followButton.borderColor = AppColor.baseColor
        }
    }
    
    
    func setUpFollowButton(user:Userlist){
        
        let userLogin = CommonFunctions.getuserFromDefaults()
        
        if self.user?.id == Int(userLogin.userId!) {
            self.followButton.isHidden = true
        }else{
            self.followButton.isHidden = false
            //self.setCurrentStatus(user: self.user!)
        }
        
        
        
        if let isPrivate = user.isProfilePrivate,isPrivate.boolValue == true {
            if user.isFollowed!.boolValue{
                profileStatus = .following
            }else if !(Int(user.isRequested!)!.boolValue) {
                profileStatus = .follow
            }else{
                profileStatus = .requested
            }
        }else{
            // public check for follow unfoolow
            if user.isFollowed!.boolValue{
                profileStatus = .following
            }else{
                profileStatus = .follow
            }
        }
        
        self.setUpButtonStatus(profileStatus: profileStatus)
    }
    
    
    func setUpUser(user:Userlist){
        self.user = user
        self.headingLabel.text = user.username!
        self.subheadingLabel.text = user.name!
        self.userProfileImageView.setImage(fromUrl: user.userImage ?? "", defaultImage: DefaultImage.defaultProfileImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
        self.setUpFollowButton(user: user)
        
    }
    
    
    @IBAction func followButtonPressed(_ sender: Any) {
//        self.isFollowed = !self.isFollowed
//        self.setUpFollowButton()
        // call follow API
        self.delegate?.followbuttonPressed(cell: self)
    }
    
    
    @IBAction func profileButtonPressed(_ sender: Any) {
        delegate?.profilePressed(cell: self)
    }
    
    
}
