//
//  AddMediaForumCell.swift
//  Pawxo
//
//  Created by 42works on 09/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit



protocol AddMediaForumCellProtocol {
    func setheight(height:CGFloat)
}


class AddMediaForumCell: UITableViewCell {
    
    
    var heightArray = [CGSize]()
    var heightDefault : CGFloat = 300.0
    var staticCount = 0
    
    @IBOutlet weak var mediaCollectionView: UICollectionView!
    
    let cellIdentifier = "MediaAddCell"
    
    var delegate : AddMediaForumCellProtocol?
    var mediaArray = [ForumMediaItem]()
    
    var paddingInternal : CGFloat = 2
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mediaCollectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        // Initialization code
        self.setUpLayout()
        if let layout = mediaCollectionView?.collectionViewLayout as? AddMediaLayout {
            layout.delegate = self
            layout.cellPadding = paddingInternal
        }
        self.perform(#selector(changeHeight), with: nil, afterDelay: 1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUPMediaObject(mediaArray:[ForumMediaItem]){
        self.mediaArray = mediaArray
        self.setUpLayout()
    }
    
    
    func setUpLayout(){
        
        if mediaArray.count == 1{
            heightArray.removeAll()
            
           // let newWidth =
            let size = CGSize(width: self.frame.size.width - 40, height: heightDefault)
            heightArray.append(size)
        }else if mediaArray.count == 2 {
            heightArray.removeAll()
            let size = CGSize(width: (self.frame.size.width - 40 - paddingInternal)/2, height: heightDefault)
            heightArray.append(size)
            heightArray.append(size)
        }else if mediaArray.count == 3 {
            heightArray.removeAll()
            let size = CGSize(width: (self.frame.size.width - 40 - paddingInternal)/2, height: heightDefault)
            heightArray.append(size)
            let sizeTwo = CGSize(width: (self.frame.size.width - 40 - paddingInternal)/2, height: (heightDefault-paddingInternal)/2)
            heightArray.append(sizeTwo)
            heightArray.append(sizeTwo)
        }else if mediaArray.count == 4 {
            heightArray.removeAll()
            let size = CGSize(width: (self.frame.size.width - 40 - paddingInternal)/2, height: (heightDefault-paddingInternal)/2)
            heightArray.append(size)
            heightArray.append(size)
            heightArray.append(size)
            heightArray.append(size)
            
        }
        
        mediaCollectionView.collectionViewLayout.invalidateLayout()
        if let layout = mediaCollectionView?.collectionViewLayout as? AddMediaLayout {
            layout.delegate = self
            layout.cellPadding = paddingInternal
        }
        self.mediaCollectionView.reloadData()
    }
  
    
   @objc func changeHeight(){
        delegate?.setheight(height: self.mediaCollectionView!.contentSize.height)
    }

}




extension AddMediaForumCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mediaArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let mediaCell : MediaAddCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! MediaAddCell
        
       // print(indexPath.item/3)
        let mediaItem = mediaArray[indexPath.item]
        mediaCell.setMediaItem(item: mediaItem)
       // mediaCell.imgView.image = mediaImage
        
        
        return mediaCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //let height =  self.bounds.size.height - (self.view.safeAreaInsets.top + self.view.safeAreaInsets.bottom + 101)
        return self.heightArray[indexPath.item]
    }
       // return CGSize(width: 100, height: 100)
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print(indexPath.item)
    }
    
}




extension AddMediaForumCell: AddMediaLayoutDelegate {
  func collectionView(
    _ collectionView: UICollectionView,
    heightForMediaItemAtIndexPath indexPath:IndexPath) -> CGSize {
    return self.heightArray[indexPath.item]
  }
    
    func setHeightArray() -> [CGSize]{
        return self.heightArray
    }
}
