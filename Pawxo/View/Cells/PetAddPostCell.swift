//
//  PetAddPostCell.swift
//  Pawxo
//
//  Created by 42works on 10/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit


protocol PetAddPostCellProtocol {
    func petAdded(pet:PetList)
    func petRemoved(pet:PetList)
}


class PetAddPostCell: UICollectionViewCell {
    
    var isSelectedPet = false
    
    var refreshSelectedPet:(()->())?
    
    var pet : PetList?
    
    var petDelegate : PetAddPostCellProtocol?
    
    @IBOutlet weak var selectedView: UIView!
    
    @IBOutlet weak var petNameLabel: HomeScreenTitleLabel!
    @IBOutlet weak var selectImageV: UIImageView!
    @IBOutlet weak var petImage: UIImageView!
    
    
    
    
    
    
    @IBOutlet weak var petSwitch: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.setImage(color: .white, imgView: self.selectImageV)
        //self.selectedView.isHidden = true
        // Initialization code
    }
    
    
    @IBAction func petSwitchPressed(_ sender: UISwitch) {
        
        if sender.isOn{
            self.petDelegate?.petAdded(pet: self.pet!)
        }else{
            self.petDelegate?.petRemoved(pet: self.pet!)
        }
    }
    
    @IBAction func petButtonPressed(_ sender: UIButton) {
        
        self.setImage(color: .white, imgView: self.selectImageV)
        
        self.pet!.isSelectedPet = !self.pet!.isSelectedPet
        if self.pet!.isSelectedPet {
            selectedView.isHidden = false
            self.petDelegate?.petAdded(pet: self.pet!)
        }else{
            selectedView.isHidden = true
            self.petDelegate?.petRemoved(pet: self.pet!)
        }
        
    }
    
    func setPet(pet:PetList){
        self.pet = pet
        if let petImageUrl = pet.petImages {
            self.petImage.setImage(fromUrl: petImageUrl, defaultImage: UIImage(named: "TempProfile"), isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: true)
        }else{
            self.petImage.image = UIImage(named: "TempProfile")
        }
        
        self.petNameLabel.text = self.pet?.petName
        
        if self.pet!.isSelectedPet {
            self.petSwitch.setOn(true, animated: false)
        }else{
            self.petSwitch.setOn(false, animated: true)
        }
    }
    
    
    
    public func setImage(color: UIColor, imgView: UIImageView) {
            
            if #available(iOS 13.0, *) {
                imgView.image =  imgView.image?.withTintColor(color, renderingMode: .alwaysTemplate)
            } else {
                // Fallback on earlier versions
               imgView.image  = imgView.image?.withRenderingMode(.alwaysTemplate)
                imgView.tintColor = color
            }
    //        self.setImage(imgView.image?.withRenderingMode(.alwaysTemplate) i )
        }
    
    
}
