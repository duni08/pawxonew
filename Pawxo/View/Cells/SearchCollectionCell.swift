//
//  SearchCollectionCell.swift
//  Pawxo
//
//  Created by 42works on 08/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit


protocol SearchCollectionCellProtocol {
    func profileButtonPressed(user:Userlist)
    func alert(message:String)
    func tagPressed(hashTag:String)
    func alertForUnFollow(cell:SearchCollectionCell,user:Userlist)
}


class SearchCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var searchTableView: UITableView!
    
    var staticCount = 10
    var staticTagCount = 2
    var usersListCellIdentifier = "UsersListCell"
    var tagCellIdentifier = "TagCell"
    var tagCellDefaultIdentifier = "TagHeaderSuggestCell"
    
    var isTagView = false
    
    var userList = [Userlist]()
    var hashTagList = [HashTag]()
    
    var delegate : SearchCollectionCellProtocol?
    
    
    var unFollowRequest: (()->())?
    
    
    var isSearchResultHashTag = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        searchTableView.register(UINib(nibName: usersListCellIdentifier, bundle: nil), forCellReuseIdentifier: usersListCellIdentifier)
        searchTableView.register(UINib(nibName: tagCellIdentifier, bundle: nil), forCellReuseIdentifier: tagCellIdentifier)
        searchTableView.register(UINib(nibName: tagCellDefaultIdentifier, bundle: nil), forCellReuseIdentifier: tagCellDefaultIdentifier)
        // Initialization code
    }
    
    func setListing(){
        if isTagView{
            if hashTagList.count > 0 {
                self.searchTableView.isHidden = false
                self.searchTableView.reloadData()
            }else{
                self.searchTableView.isHidden = true
                self.searchTableView.reloadData()
            }
        }else{
            if userList.count > 0 {
                self.searchTableView.isHidden = false
                self.searchTableView.reloadData()
            }else{
                self.searchTableView.isHidden = true
                self.searchTableView.reloadData()
            }
        }
    }
    
}



extension SearchCollectionCell : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isTagView {
            return self.hashTagList.count
        }else{
            return self.userList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isTagView {
            let cell : TagCell =  tableView.dequeueReusableCell(withIdentifier: tagCellIdentifier, for: indexPath) as! TagCell
            //cell.homeCellDelegate = self
            
            let hashTag = self.hashTagList[indexPath.row]
            cell.setHashTag(hashTag: hashTag)
            return cell
        }else{
            print(userList)
            let cell : UsersListCell =  tableView.dequeueReusableCell(withIdentifier: usersListCellIdentifier, for: indexPath) as! UsersListCell
            //cell.homeCellDelegate = self
            let user = self.userList[indexPath.row]
            cell.setUpUser(user: user)
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isTagView {
            return 60
        }else{
            return 65.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if isTagView && !isSearchResultHashTag {
//            return 40
//        }else{
            return 0.1
       // }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell : TagHeaderSuggestCell =  tableView.dequeueReusableCell(withIdentifier: tagCellDefaultIdentifier) as! TagHeaderSuggestCell
        //cell.homeCellDelegate = self
        return cell.contentView
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isTagView{
            let tag = self.hashTagList[indexPath.row]
            delegate?.tagPressed(hashTag: tag.name!)
        }
    }
}


extension SearchCollectionCell : UsersListCellProtocol{
    func profilePressed(cell:UsersListCell) {
        
        self.delegate?.profileButtonPressed(user: cell.user!)
        //self.goToProfileView(userId:cell.user!.id!)
    }
    
    
    
    func followbuttonPressed(cell:UsersListCell){
        let follow_VM = Follow_VM()
        follow_VM.userId = CommonFunctions.getuserFromDefaults().userId
        follow_VM.followerId = "\((cell.user?.id)!)"
        
        let status = cell.profileStatus
        
        if status == .following {
            //Show Alert
            delegate?.alertForUnFollow(cell: self, user: cell.user!)
            
            self.unFollowRequest = {
                
                
                print("UnFollow Accepted")
                
                follow_VM.responseRecieved = {
                    let user = cell.user!
                    user.isFollowed = 0
                    cell.setUpUser(user: user)
                }
                follow_VM.showAlertClosure = {[weak self] () in
                    DispatchQueue.main.async {
                        if let message = follow_VM.alertMessage {
                            self?.delegate?.alert(message: message)
                        }
                    }
                }
                
                follow_VM.updateLoadingStatus = {
                    DispatchQueue.main.async {
                        if follow_VM.isLoading {
                            LoaderView.shared.showLoader()
                        } else {
                            LoaderView.shared.hideLoader()
                        }
                    }
                }
                
                follow_VM.callFollowerService = true
                
                
            }
            
            
            
        }else if status == .requested{
            
            let user = cell.user!
            
            follow_VM.responseRecieved = {
                
                 user.isRequested = "0"
                cell.setUpUser(user: user)
            }
            
            follow_VM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if follow_VM.isLoading {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            follow_VM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = follow_VM.alertMessage {
                        self?.delegate?.alert(message: message)
                    }
                }
            }
            
            if let isPrivate = user.isProfilePrivate,isPrivate.boolValue == true {
                follow_VM.callRequestUserFollowService = true
            }else{
                // public check for follow unfoolow
                follow_VM.callFollowerService = true
            }
            
        }
        else
        {
            
            
           let user = cell.user!
            
            
            follow_VM.responseRecieved = {
                
                
                if let isPrivate = user.isProfilePrivate, isPrivate.boolValue == true {
                    user.isRequested = "1"
                }else{
                    // public check for follow unfoolow
                    user.isFollowed = 1
                }
                cell.setUpUser(user: user)
            }
            
            follow_VM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if follow_VM.isLoading {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            follow_VM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = follow_VM.alertMessage {
                        self?.delegate?.alert(message: message)
                    }
                }
            }
            
            if let isPrivate = user.isProfilePrivate, isPrivate.boolValue == true {
                follow_VM.callRequestUserFollowService = true
            }else{
                // public check for follow unfoolow
                follow_VM.callFollowerService = true
            }
            
            
        }
    }
}
