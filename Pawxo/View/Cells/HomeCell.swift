//
//  HomeCell.swift
//  Pawxo
//
//  Created by 42works on 06/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import ActiveLabel

protocol HomeCellDelegate {
    func commentButtonPressed(index:IndexPath)
    func likeButtonPressed(post:PostList)
    func shareButtonPressed(cell:HomeCell)
    func optionsButtonPressed()
    func deleteButtonPressed(cell:HomeCell)
    func editButtonPressed(cell:HomeCell)
    func reportButtonPressed(cell:HomeCell)
    func gotoProfilePressed(cell:HomeCell)
    func doubleTAPLikePressed(cell:HomeCell)
    func hashTagPressed(hashTag:String)
    func userMentionTap(user:Userlist)
    func petPressed(pet:PetList,isMYPet:Bool)
    func petListPressed(petlist :[PetList],isMYPet:Bool)
}

class HomeCell: UITableViewCell {

    @IBOutlet weak var centerImageView: UIImageView!
    
    
    @IBOutlet weak var ownView: UIView!
    
    @IBOutlet weak var reportView: UIView!
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: RegularLabel!
    
    @IBOutlet weak var timeLabel: LightLabel!
    
    @IBOutlet weak var optionsButton : UIButton!
    var homeCellDelegate : HomeCellDelegate?
    
    var indexNumber : IndexPath?
    
    @IBOutlet weak var likeCountLabel: LightLabel!
    
    @IBOutlet weak var editView: UIView!
    
    @IBOutlet weak var commentCountLabel: LightLabel!
    
    @IBOutlet weak var hashTagLabel: LightLabel!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var descriptionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionLabel: ActiveLabel!
     var tempLabel : ActiveLabel!
    var customType  : ActiveType!
    
    
    @IBOutlet weak var firstPetImgView: UIImageView!
    
    @IBOutlet weak var secondPetCountView: UIView!
    @IBOutlet weak var secondPetImgView: UIImageView!
    var post : PostList?
    
    @IBOutlet weak var firstPetButton: UIButton!
    @IBOutlet weak var petCountLabel: UILabel!
    
    @IBOutlet weak var petsOuterView: UIView!
    
    
    var userNotFound : (()->())?
    
    @IBOutlet weak var secondPetButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        editView.isHidden = true
        // Initialization code
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = 0
        tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        tempLabel.font = UIFont.JosefinSansBold(size: 15.0)
        descriptionLabel.hashtagColor = AppColor.hashTagColor
        descriptionLabel.mentionColor = AppColor.userMentionColor
        descriptionLabel.textColor = AppColor.colorGreySubHeading
        //self.likeButton.addTarget(self, action: #selector(multipleTap(_:event:)), for: UIControl.Event.touchDownRepeat)
        
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(didPressPartButton))
        singleTapGesture.numberOfTapsRequired = 1
        self.likeButton.addGestureRecognizer(singleTapGesture)

        let singleTapGestureLike = UITapGestureRecognizer(target: self, action: #selector(didDoubleTap))
        singleTapGestureLike.numberOfTapsRequired = 1
        self.likeCountLabel.isUserInteractionEnabled = true
        self.likeCountLabel.addGestureRecognizer(singleTapGestureLike)

        //singleTapGesture.require(toFail: doubleTapGesture)
    }
    
    
    
    @objc func didPressPartButton(){
        print("single Tap")
        self.homeCellDelegate?.doubleTAPLikePressed(cell: self)
        //self.homeCellDelegate?.likeButtonPressed(post: self.post!)
    }
    
    @objc func didDoubleTap(){
        print("dobule tap")
        //self.homeCellDelegate?.doubleTAPLikePressed(cell: self)
        self.homeCellDelegate?.likeButtonPressed(post: self.post!)
    }
    
    
    
    
    func userMention(name:String){
        print(name)
        
        var userSelected : Userlist?
        if let userlist =  post?.usertagList {
            for user in userlist{
                if user.username! == name{
                    userSelected = user
                    break
                }
            }
        }
        
        if userSelected != nil {
            homeCellDelegate?.userMentionTap(user: userSelected!)
        }else{
            self.userNotFound?()
        }
    }
    
    
    func setPost(post:PostList){
        self.post = post
        //Regex that looks for "with"
        descriptionLabel.text =  "\(post.postListDescription ?? "")"
        customType = ActiveType.custom(pattern: post.userName!)
        
        
        var description = post.postListDescription ?? ""
        var hashArr = [String]()
        let replaced = description.replacingOccurrences(of: " #", with: "#")
        let finalreplaced = replaced.replacingOccurrences(of: "#", with: " #")
        description = finalreplaced
        print(description)
        
        let descriptionNew = description.components(separatedBy: " ")
        for obj in descriptionNew ?? [String]() {
            if obj.first == "#" {
                var temp = obj
                temp.remove(at: temp.startIndex)
                hashArr.append(temp)
            }
        }
        
        var array = [ActiveType]()
        
        
        if hashArr.count != 0 {
            
            for hashTag in hashArr{
               let NewCustomType = ActiveType.custom(pattern: "#\(hashTag)")
                array.append(NewCustomType)
                descriptionLabel.customColor[NewCustomType] = AppColor.hashTagColor
                descriptionLabel.customSelectedColor[NewCustomType] = AppColor.hashTagColor
                descriptionLabel.handleCustomTap(for: NewCustomType) { element in
                    print("Custom type tapped: \(element)")
                    self.homeCellDelegate?.hashTagPressed(hashTag: element)
                    
                }
            }
            
        }
        array.append(.mention)
        array.append(.url)
        
        descriptionLabel.enabledTypes = array
        
        descriptionLabel.customColor[customType] = AppColor.userNameColor
        descriptionLabel.customSelectedColor[customType] = AppColor.userNameColor
        descriptionLabel.customize { (activeLabel) in
             print("Success. You just tapped the \(activeLabel) activeLabel")
        }
        
        descriptionLabel.handleCustomTap(for: customType) { element in
            print("Custom type tapped: \(element)")
            self.homeCellDelegate?.hashTagPressed(hashTag: element)
            
        }
        
        descriptionLabel.handleHashtagTap { hashtag in
            print("Success. You just tapped the \(hashtag) hashtag")
            self.homeCellDelegate?.hashTagPressed(hashTag: hashtag)
        }
        
        descriptionLabel.handleMentionTap({ (mention) in
            print("Success. You just tapped the \(mention) user")
            self.userMention(name: mention)
            
        })
        
        tempLabel.frame =  CGRect(x: 0, y: 0, width: self.frame.width - 40, height: CGFloat.greatestFiniteMagnitude)
        tempLabel.text = "\(post.postListDescription ?? "")"
        tempLabel.sizeToFit()
        
        
        let height = tempLabel.frame.height
        tempLabel.text = ""
        tempLabel.text = "A \n B"
        tempLabel.sizeToFit()
        
        let twoLineTextHeight = tempLabel.frame.height
        
        var calculatedHeight : CGFloat = 0
        if height > twoLineTextHeight {
            calculatedHeight = twoLineTextHeight
        }else{
            calculatedHeight = height
        }
        
       // self.descriptionHeightConstraint.constant = calculatedHeight
        
        //self.profileImageView.setImage(fromUrl: post.userImage ?? "", defaultImage: DefaultImage.defaultProfileImage)
        
        self.profileImageView.setImage(fromUrl: post.userImage ?? "" , defaultImage: DefaultImage.defaultProfileImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
        
        if post.type == "video"{
            self.centerImageView.setImage(fromUrl: post.thumbnail?.first ?? "", defaultImage: DefaultImage.defaultPostImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
        }else{
        self.centerImageView.setImage(fromUrl: post.media?.first ?? "", defaultImage: DefaultImage.defaultPostImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
        }
        
        
        var likeStr = "0 likes"
        
        if let count = post.likeCount {
            if count > 1{
                likeStr = "\(count) likes"
            }else if count == 1{
                likeStr = "1 like"
            }
        }
        
        
        self.commentCountLabel.text = likeStr
        self.likeCountLabel.text = likeStr
        
        
        self.nameLabel.text = post.userName
        self.timeLabel.text = post.createdDate?.UTCToLocal().timeAgoSinceDate(numericDates: true)
        
        self.setLikeButton(isLike: post.isLike!)
        
       
        
        
        if let pet = self.post?.petsList, pet.count > 0 {
            self.petsOuterView.isHidden = false
            if pet.count == 1 {
                let petFirst = pet[0]
                self.firstPetImgView.setImage(fromUrl: petFirst.petImages ?? "", defaultImage: DefaultImage.defaultPetImage)
                self.secondPetButton.isHidden = true
                self.secondPetImgView.isHidden = true
                self.petCountLabel.isHidden = true
                self.secondPetCountView.isHidden = true
            }else if pet.count  >= 2 {
                self.secondPetButton.isHidden = false
                self.secondPetImgView.isHidden = false
                self.secondPetCountView.isHidden = false
                let petFirst = pet[0]
                self.firstPetImgView.setImage(fromUrl: petFirst.petImages ?? "", defaultImage: DefaultImage.defaultPetImage)
                let petSecond = pet[1]
                self.secondPetImgView.setImage(fromUrl: petSecond.petImages ?? "", defaultImage: DefaultImage.defaultPetImage)
                if pet.count > 2 {
                    let count = pet.count - 2
                    self.petCountLabel.isHidden = false
                    self.secondPetCountView.isHidden = false
                    self.petCountLabel.text = "+\(count)"
                }else{
                    self.petCountLabel.isHidden = true
                    self.secondPetCountView.isHidden = true
                }
            }
        }else{
            self.petsOuterView.isHidden = true
        }
    }
    
    
    
    
    
    func setLikeButton(isLike:Int){
        if isLike.boolValue{
            self.likeButton.setImage(DefaultImage.likeImage, for: .normal)
        }else{
            self.likeButton.setImage(DefaultImage.unlikeImage, for: .normal)
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        homeCellDelegate?.shareButtonPressed(cell:self)
    }
    
    @IBAction func likeButtonPressed(_ sender: Any) {
      //  homeCellDelegate?.likeButtonPressed()
    }
    
    @IBAction func hidePressed(_ sender: Any) {
        self.editView.isHidden = true
    }
    
    
    @IBAction func commentButtonPressed(_ sender: Any) {
        homeCellDelegate?.commentButtonPressed(index: indexNumber!)
    }
    
    
    @IBAction func reportButtonPressed(_ sender: Any) {
        editView.isHidden = true
        homeCellDelegate?.reportButtonPressed(cell: self)
    }
    
    
    @IBAction func optionButtonPressed(_ sender: Any) {
        
        editView.isHidden = false
        let userLogin = CommonFunctions.getuserFromDefaults()
        
        if self.post?.userID == Int(userLogin.userId!) {
            self.ownView.isHidden = false
            self.reportView.isHidden = true
        }else{
            self.ownView.isHidden = true
            self.reportView.isHidden = false
        }
    }
    
    @IBAction func editButtonPressed(_ sender: Any) {
        editView.isHidden = true
        homeCellDelegate?.editButtonPressed(cell: self)
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        editView.isHidden = true
        homeCellDelegate?.deleteButtonPressed(cell: self)
    }
    
    
    @IBAction func profileButtonPressed(_ sender: Any) {
        homeCellDelegate?.gotoProfilePressed(cell: self)
    }
    
    
    
    @IBAction func firstButtonPressed(_ sender: UIButton) {
//        self.homeCellDelegate?.petPressed
        var isMYPet = false
        let userLogin = CommonFunctions.getuserFromDefaults()
        if self.post?.userID == Int(userLogin.userId!) {
            isMYPet = true
        }
        self.homeCellDelegate?.petPressed(pet: (self.post?.petsList?.first!)!, isMYPet: isMYPet)
    }
    
    
    @IBAction func secondButtonPressed(_ sender: UIButton) {
        
        var isMYPet = false
        let userLogin = CommonFunctions.getuserFromDefaults()
        if self.post?.userID == Int(userLogin.userId!) {
            isMYPet = true
        }
        if let pets = self.post?.petsList{
            if pets.count > 2 {
                self.homeCellDelegate?.petListPressed(petlist: pets, isMYPet: isMYPet)
            }else{
                
                if let pet = pets.last{
                    self.homeCellDelegate?.petPressed(pet: pet, isMYPet: isMYPet)
                }
            }
        }
    }
    
}
