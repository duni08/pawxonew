//
//  AddressCell.swift
//  Pawxo Demo
//
//  Created by Apple on 15/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

@objc protocol EditAddressCellDelegate {
    //@objc optional func sharingClickedEvent(index: Int, cell:EventTableViewCell)
    @objc optional func editAddressEvent(sender:UIButton)
}

class AddressCell: UITableViewCell {

    @IBOutlet weak var title_Lbl: UILabel!
    @IBOutlet weak var addres_LbL: UILabel!
    
    @IBOutlet weak var editBtn: UIButton!
    
    weak var delegate: EditAddressCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func editTappedButton (_ sender:UIButton) {
        
        delegate?.editAddressEvent?(sender:sender)
    }
    
}
