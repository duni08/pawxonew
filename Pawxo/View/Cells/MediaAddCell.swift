//
//  MediaAddCell.swift
//  Pawxo
//
//  Created by 42works on 09/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class MediaAddCell: UICollectionViewCell {
    
    var item : ForumMediaItem?
    
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    
    @IBAction func playButtonPressed(_ sender: Any) {
        
    }
    
    
    func setMediaItem(item:ForumMediaItem){
        self.item = item
        
        if item.type == "image"{
            self.videoButton.isHidden = true
            if let thumbImage = item.thumbImage{
                self.imgView.image = thumbImage
            }else{
                self.imgView.setImage(fromUrl: item.imageVideoUrl ?? "", defaultImage: DefaultImage.defaultPostImage)
            }
        }else{
            self.videoButton.isHidden = false
            if let thumbImage = item.thumbImage{
                self.imgView.image = thumbImage
            }else{
                self.imgView.setImage(fromUrl: item.imageVideoThumbUrl ?? "", defaultImage: DefaultImage.defaultPostImage)
            }
        }
    }

}
