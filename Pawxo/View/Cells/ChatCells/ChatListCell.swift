//
//  ChatListCell.swift
//  Pawxo
//
//  Created by 42works on 20/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class ChatListCell: UITableViewCell {
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userName: RegularLabel!
    @IBOutlet weak var userMsg: RegularLabel!
    @IBOutlet weak var timeLbl: RegularLabel!
    @IBOutlet weak var inlineOffline: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
