//
//  NewChatCell.swift
//  Pawxo
//
//  Created by 42works on 20/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class NewChatCell: UITableViewCell {
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userName: RegularLabel!
    @IBOutlet weak var userDesc: LightLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
