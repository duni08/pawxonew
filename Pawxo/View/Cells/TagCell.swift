//
//  TagCell.swift
//  Pawxo
//
//  Created by 42works on 08/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class TagCell: UITableViewCell {

    @IBOutlet weak var headingLabel: RegularLabel!
    
    
    @IBOutlet weak var subheadingLabel: LightLabel!
    
    var hashTag : HashTag?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUPHashTag(hashTag:HashTag){
        self.hashTag = hashTag
        
        self.headingLabel.text = "#\(hashTag.name!)"
        self.subheadingLabel.text = "\(hashTag.postCount ?? 0) posts"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setHashTag(hashTag:HashTag){
        self.headingLabel.text = "#\(hashTag.name!)"
        self.subheadingLabel.text =  (hashTag.postCount != nil) ?  "\(hashTag.postCount!) posts" : ""
    }
    
}
