//
//  commentCell.swift
//  Pawxo
//
//  Created by 42works on 07/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import ActiveLabel

protocol HashTagUserMentionProtocol {
    func profilePressed(userId:Int)
    func hashTagPressed(hashTag:String)
}

class CommentCell: UITableViewCell {
    
    var delegate : HashTagUserMentionProtocol?
    var comment : CommentList?
    var forumComment : ForumCommentList?
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet var commentLabel : ActiveLabel!
    @IBOutlet weak var commentHeightConstraint: NSLayoutConstraint!
    var tempLabel : ActiveLabel!
    
    
    var longPressedForDelete : (()->())?
    
    var userNotFound : (()->())?
    
    @IBOutlet weak var userNameLabel: RegularLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.frame.width - 76, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = 0
        tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        tempLabel.font = UIFont.JosefinSansRegular(size: 14.0)
        commentLabel.hashtagColor = AppColor.hashTagColor
        commentLabel.mentionColor = AppColor.userMentionColor
        commentLabel.enabledTypes = [.mention, .hashtag, .url]
        commentLabel.font = UIFont.JosefinSansRegular(size: 14.0)
        commentLabel.textColor = AppColor.colorGreyTime
        // Initialization code
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action:#selector(self.longPressed(sender:)) )
        self.addGestureRecognizer(longPressRecognizer)
    }
    
    
    @IBAction func longPressed(sender: UILongPressGestureRecognizer)
    {
        self.longPressedForDelete?()
        //Different code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func profilePressed(_ sender: Any) {
        
        if let uid = self.comment?.userID{
            delegate?.profilePressed(userId: uid)
        }
        
        if let uid = self.forumComment?.user?.id{
            delegate?.profilePressed(userId: uid)
        }
    }
    
    
    func userMention(name:String){
        var userSelected : Userlist?
        if let userlist =  comment?.taggedUser {
            for user in userlist{
                if user.username! == name{
                    userSelected = user
                    break
                }
            }
        }
        
        if let userlist =  forumComment?.taggedUsers {
            for user in userlist{
                if user.username! == name{
                    userSelected = user
                    break
                }
            }
        }
        
        if let user =  userSelected {
            if let block = user.block, block.boolValue == true{
                self.userNotFound?()
            }else{
                delegate?.profilePressed(userId: user.id!)
            }
        }else{
            self.userNotFound?()
        }
    }
    
    
    
    func setComment(comment:CommentList){
        self.comment = comment
        commentLabel.text = comment.name!
        userNameLabel.text = comment.userName!
        self.userProfileImage.setImage(fromUrl: comment.userImage ?? "" , defaultImage: DefaultImage.defaultProfileImage)
        
            commentLabel.handleHashtagTap { hashtag in
                print("Success. You just tapped the \(hashtag) hashtag")
                self.delegate?.hashTagPressed(hashTag: hashtag)
            }
            
            commentLabel.handleMentionTap({ (mention) in
                print("Success. You just tapped the \(mention) user")
                self.userMention(name: mention)
            })
            
            tempLabel.frame =  CGRect(x: 0, y: 0, width: self.frame.width - 76, height: CGFloat.greatestFiniteMagnitude)
            tempLabel.text = comment.name!
            tempLabel.sizeToFit()
            let calculatedHeight = tempLabel.frame.height
            self.commentHeightConstraint.constant = calculatedHeight
            
        }
    
    
    func setComment(forumComment:ForumCommentList){
        self.forumComment = forumComment
        commentLabel.text = forumComment.content ?? ""
        userNameLabel.text = forumComment.user?.name ?? ""
        self.userProfileImage.setImage(fromUrl: forumComment.user?.image ?? "" , defaultImage: DefaultImage.defaultProfileImage)
        
        commentLabel.handleHashtagTap { hashtag in
            print("Success. You just tapped the \(hashtag) hashtag")
            self.delegate?.hashTagPressed(hashTag: hashtag)
        }
        
        commentLabel.handleMentionTap({ (mention) in
            print("Success. You just tapped the \(mention) user")
            self.userMention(name: mention)
        })
        
        tempLabel.frame =  CGRect(x: 0, y: 0, width: self.frame.width - 76, height: CGFloat.greatestFiniteMagnitude)
        tempLabel.text = forumComment.content ?? ""
        tempLabel.sizeToFit()
        let calculatedHeight = tempLabel.frame.height
        self.commentHeightConstraint.constant = calculatedHeight
        
        
        
        if self.forumComment!.approved! {
            self.userProfileImage.alpha = 1.0
            self.commentLabel.alpha = 1.0
            self.userNameLabel.alpha = 1.0
        }else{
            self.userProfileImage.alpha = 0.5
            self.commentLabel.alpha = 0.5
            self.userNameLabel.alpha = 0.5
        }
        
    }
    
}
