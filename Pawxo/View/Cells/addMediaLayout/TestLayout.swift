//
//  TestLayout.swift
//  Pawxo
//
//  Created by 42works on 10/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class TestLayout: UICollectionViewLayout {
    
    
    private var cache: [UICollectionViewLayoutAttributes] = []
    
    
    private var contentHeight: CGFloat = 0
    
    private var numberOfColumns = 3
    
    var padding : CGFloat = 5
    
    private var contentWidth: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.width
    }
    
    // 5
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
        
        // Loop through the cache and look for items in the rect
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }
    
    
    override func prepare() {
        // 1
        guard
            cache.isEmpty == true,
            let collectionView = collectionView
            else {
                return
        }
        // 2
        
        
        
        let height = (collectionView.bounds.width - (padding * (CGFloat(numberOfColumns) + CGFloat(1))))/CGFloat(numberOfColumns)
        let width = collectionView.bounds.width - (padding * CGFloat(numberOfColumns)) - height
        
        
        
        
        let maxHeight :CGFloat = width
        let minHeight :CGFloat = height
        
        let maxWidth : CGFloat = width
        let minWidth : CGFloat = height
        
        let initialVal :CGFloat = 2
        var change = false
        var updatedVal = initialVal
        
        var lastX :CGFloat = 0
        var lastY :CGFloat = 0
        var initialY : CGFloat = 3
        let firstX = padding + 0
        let SecondX = firstX + minWidth + padding
        let thirdX = SecondX + padding + minWidth
        
        var ySeries = getYValue()
        var XSeries = getXValue(firstX: firstX,secondX: SecondX,ThirdX: thirdX)
        
        
        for item in 0..<collectionView.numberOfItems(inSection: 0) {
            let indexPath = IndexPath(item: item, section: 0)
            
            var toAdd = false
            if CGFloat(item + 1) == updatedVal {
                // print(i)
                toAdd = true
                change = !change
                if change {
                    updatedVal += 8
                }else{
                    updatedVal += 10
                }
            }
            
            if (CGFloat(item + 1) == initialY){
                lastY += minHeight + padding
                initialY += ySeries.first!
                ySeries.removeFirst()
                if ySeries.isEmpty {
                    ySeries = self.getYValue()
                }
            }
            
            
            lastX = XSeries.first!
            XSeries.removeFirst()
            if XSeries.isEmpty {
                XSeries = self.getXValue(firstX: firstX, secondX: SecondX, ThirdX: thirdX)
            }
            let rectNew = CGRect(x: lastX, y: lastY + padding, width: toAdd ? maxWidth : minWidth, height: toAdd ? maxHeight : minHeight)
            print(rectNew)
            // 5
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = rectNew
            cache.append(attributes)
            
            contentHeight = max(contentHeight, rectNew.maxY)
        }
    }
    
    
    func getYValue() -> [CGFloat]{
        return [1,3,3,2]
    }
    
    func getXValue(firstX:CGFloat,secondX:CGFloat,ThirdX:CGFloat) -> [CGFloat]{
        return [firstX,secondX,firstX,firstX,secondX,ThirdX,firstX,secondX,ThirdX,firstX,ThirdX,ThirdX,firstX,secondX,ThirdX,firstX,secondX,ThirdX]
    }
    
}
