
import UIKit


protocol AddMediaLayoutDelegate: AnyObject {
  func collectionView(_ collectionView: UICollectionView, heightForMediaItemAtIndexPath indexPath: IndexPath) -> CGSize
    func setHeightArray() -> [CGSize]
}

class AddMediaLayout: UICollectionViewLayout {
  // 1
  weak var delegate: AddMediaLayoutDelegate?

  // 2
  private let numberOfColumns = 2
    
  var cellPadding: CGFloat = 2

  // 3
  private var cache: [UICollectionViewLayoutAttributes] = []

  // 4
  private var contentHeight: CGFloat = 0

  private var contentWidth: CGFloat {
    guard let collectionView = collectionView else {
      return 0
    }
    let insets = collectionView.contentInset
    return (collectionView.bounds.width - (cellPadding * CGFloat(numberOfColumns + 1)))/CGFloat(numberOfColumns)
  }

  // 5
  override var collectionViewContentSize: CGSize {
    return CGSize(width: collectionView!.bounds.width, height: (collectionView?.bounds.height)!)
  }
  
  override func prepare() {
    // 1
    guard        
      let collectionView = collectionView
      else {
        return
    }
    cache = []
    // 2
    let columnWidth = contentWidth
    var xOffset: [CGFloat] = []
    for column in 0..<numberOfColumns {
      xOffset.append(CGFloat(column) * columnWidth + (cellPadding * (CGFloat(column) + CGFloat(1))))
    }
    var column = 0
    var yOffset: [CGFloat] = .init(repeating: 0, count: numberOfColumns)
      
    // 3
    
    
    
    let heightArray = delegate?.setHeightArray()
    
    if heightArray?.count == 1 {
        let frame = CGRect(x: 0, y: 0, width: (heightArray?[0].width)!, height: (heightArray?[0].height)!)
        let attributes = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: 0, section: 0))
        attributes.frame = frame
        cache.append(attributes)
    }else if heightArray?.count == 2 {
        let frame = CGRect(x: 0, y: 0, width: (heightArray?[0].width)!, height: (heightArray?[0].height)!)
        let attributes = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: 0, section: 0))
        
        attributes.frame = frame
        cache.append(attributes)
        
        let frameOne = CGRect(x: (heightArray?[0].width)! + cellPadding, y: 0, width: (heightArray?[1].width)!, height: (heightArray?[1].height)!)
        let attributesOne = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: 1, section: 0))
        
        attributesOne.frame = frameOne
        cache.append(attributesOne)
    }else if heightArray?.count == 3 {
        let frame = CGRect(x: 0, y: 0, width: (heightArray?[0].width)!, height: (heightArray?[0].height)!)
        let attributes = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: 0, section: 0))
        
        attributes.frame = frame
        cache.append(attributes)
        
        let frameOne = CGRect(x: (heightArray?[0].width)! + cellPadding, y: 0, width: (heightArray?[1].width)!, height: (heightArray?[1].height)!)
        let attributesOne = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: 1, section: 0))
        
        attributesOne.frame = frameOne
        cache.append(attributesOne)
        
        let frameTwoY : CGFloat =  10
        let frameTwo = CGRect(x: (heightArray?[0].width)! + cellPadding, y: frameOne.origin.y + cellPadding + frameOne.size.height, width: (heightArray?[2].width)!, height: (heightArray?[2].height)!)
        let attributesTwo = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: 2, section: 0))
        
        attributesTwo.frame = frameTwo
        cache.append(attributesTwo)
    }else if heightArray?.count == 4{
        let frame = CGRect(x: 0, y: 0, width: (heightArray?[0].width)!, height: (heightArray?[0].height)!)
        let attributes = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: 0, section: 0))
        
        attributes.frame = frame
        cache.append(attributes)
        
        let frameOne = CGRect(x: (heightArray?[0].width)! + cellPadding, y: 0, width: (heightArray?[1].width)!, height: (heightArray?[1].height)!)
        let attributesOne = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: 1, section: 0))
        
        attributesOne.frame = frameOne
        cache.append(attributesOne)
        
        let frameTwoY : CGFloat =  10
        let frameTwo = CGRect(x: 0, y: frameOne.origin.y + cellPadding + frameOne.size.height, width: (heightArray?[2].width)!, height: (heightArray?[2].height)!)
        let attributesTwo = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: 2, section: 0))
        
        attributesTwo.frame = frameTwo
        cache.append(attributesTwo)
        
        
        
        let frameThree = CGRect(x: (heightArray?[0].width)! + cellPadding, y:frameTwo.origin.y, width: (heightArray?[3].width)!, height: (heightArray?[3].height)!)
        let attributesThree = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: 3, section: 0))
        
        attributesThree.frame = frameThree
        cache.append(attributesThree)
    }
    
    
   /* for item in 0..<collectionView.numberOfItems(inSection: 0) {
      let indexPath = IndexPath(item: item, section: 0)
        
      // 4
      let photoHeight = delegate?.collectionView(
        collectionView,
        heightForMediaItemAtIndexPath: indexPath)
      
        if item == 0{
            if item == collectionView.numberOfItems(inSection: 0){
                let frame = CGRect(x: xOffset[column],
                y: yOffset[column],
                width: photoHeight!.width,
                height: photoHeight!.height)
            }else{
                
            }
        }
        
      let height = cellPadding * 2 + photoHeight
      let frame = CGRect(x: xOffset[column],
                         y: yOffset[column],
                         width: columnWidth,
                         height: height)
      let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
        
      // 5
      let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
      attributes.frame = insetFrame
      cache.append(attributes)
        
      // 6
      contentHeight = max(contentHeight, frame.maxY)
      yOffset[column] = yOffset[column] + height
        
      column = column < (numberOfColumns - 1) ? (column + 1) : 0
    }*/
  }
  
  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
    
    // Loop through the cache and look for items in the rect
    for attributes in cache {
      if attributes.frame.intersects(rect) {
        visibleLayoutAttributes.append(attributes)
      }
    }
    return visibleLayoutAttributes
  }
  
//  override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
//    return cache[indexPath.item]
//  }
}
