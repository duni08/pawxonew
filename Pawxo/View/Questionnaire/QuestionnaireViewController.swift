//
//  QuestionnaireViewController.swift
//  PetConnet
//
//  Created by 42Works-Worksys2 on 02/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit


class QuestionnaireViewController: UIViewController {
    
    var submitPet : SubmitPet_VM?
    
    
    
    
    @IBOutlet weak var skipOptionsButton: SemiBoldButton!
    
    
    //MARK:- Variables
    var newPet:((Pet)->())?
    lazy var isFromEditProfile: Bool = false
    var photoManager:PhotoManager!
    var questionaireModel: QuestionaireModel!

//    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
//    var progressBlock: AWSS3TransferUtilityProgressBlock?
//    let transferUtility = AWSS3TransferUtility.default()
    
    @IBOutlet weak var nextButton: RegularButton!
    
    var fromSignUpLogin = false
    @IBOutlet weak var skipButton: SemiBoldButton!
    
    //MARK:- IBOutlet
    @IBOutlet var tableViewObj: UITableView!
    @IBOutlet var paginationLbl: RegularLabel!
    @IBOutlet var paginationImagesCollection: [UIImageView]!
    @IBOutlet var leadingOfPaginationLbl: NSLayoutConstraint!
    
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.isStatusBarHidden = true
//        self.setNeedsStatusBarAppearanceUpdate
        
        self.skipButton.isHidden = !fromSignUpLogin
        
        self.skipOptionsButton.isHidden = true
        
        UIApplication.shared.isStatusBarHidden = true
        
        
        print("idValue-- \(breedArray.count)")
        
        self.questionaireModel = QuestionaireModel()
       // self.questionaireModel.category = breedArray[0].name!//self.breedArray[0].name!//"Dog"
        
        nextButton.setTitleColor(UIColor.orange, for: .normal)
        nextButton.setTitleColor(UIColor.gray, for: .disabled)
        self.nextButton.isEnabled = false
//        self.progressBlock = {(task, progress) in
//            DispatchQueue.main.async(execute: {
//                print("\(progress.fractionCompleted)")
//            })
//        }
//
//        self.completionHandler = { (task, error) -> Void in
//            DispatchQueue.main.async(execute: {
//                if let error = error {
//                    print("Failed with error: \(error)")
//                }
//
//                else{
//                    print("Success")
//                }
//            })
//        }
        
       self.collectionMove(0, arr: [.category, .name , .breed ,.gender ,.age,.location,.photo])
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       UIApplication.shared.isStatusBarHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.isStatusBarHidden = true
    }
    
    @IBAction func skipButtonPressed(_ sender: Any) {
        self.skip()
    }
    
    
    func calculateDate() -> String{
        let day = self.questionaireModel.ageDate
        let month = self.questionaireModel.ageMonth
        let years = self.questionaireModel.ageYear
        
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        
        
        let finalYear = year - Int(years)!
        
        let index = CalendarP.monthArr.firstIndex(of: month)
        let finalIndex = index! + 1
        
        let finalDate = CommonFunctions.makeDate(year: finalYear, month: finalIndex , day: Int(day)!, hr: 0, min: 0, sec: 0)
        
        print(finalDate.getDate())
        return finalDate.getDate()
        
        
    }
    
    @IBAction func NextTap(){
        self.nextTapChange()
        
        if nextButton.titleLabel?.text == "Submit" {
           
            submitPet = SubmitPet_VM()
            submitPet?.petName = questionaireModel.name
            submitPet?.petType = questionaireModel.category
            submitPet?.perImage = questionaireModel.profileImage
            submitPet?.breed = questionaireModel.breed
            submitPet?.gender = questionaireModel.gender == 1 ? "Male" : "Female"
            submitPet?.dateOfBirth = self.questionaireModel.ageDate != "" ?  self.calculateDate() : ""
            submitPet?.location = questionaireModel.location
            submitPet?.latitude = questionaireModel.lat
            submitPet?.longitude = questionaireModel.long
           
            let userInfo = CommonFunctions.getuserFromDefaults()
            submitPet?.userIdFinal = userInfo.userId
            
            self.submitPetInformationApi()
            
        }
        
    }
    
    func submitPetInformationApi() {
        
        submitPet?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.submitPet?.isLoading ?? false {
                    //self?.showLoader(self)
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                   // self?.hideLoader(self)
                }
            }
        }
        
        submitPet?.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
//                if let message = self?.signUpVM?.alertMessage {
//                    self?.showAlert(self ?? UIViewController(), message: message)
//                }
            }
        }
        
        submitPet?.responseRecieved = {[weak self] in
           //to be done as response will be changed
          //  self?.showAlert(self ?? UIViewController(), message: "Registered successfully")
           // self?.openTutorialsScreen() Open Home Screen or Tutorial Screen
            if self!.fromSignUpLogin {
                self?.skip()
            }else{
                self?.navigationController?.popViewController(animated: false)
            }
        }
        
        //Call Api

        submitPet?.callRegisterNewPetService = true
    }
    
    
    @IBAction func backTapped(){
        self.tapChange()
    }
    
    //MARK:- File Private Method
    fileprivate func handleNextButton(_ questionCase: Questions) {
        if let headerCell = tableViewObj.cellForRow(at: IndexPath(row: 0, section: 0)) as? QuestionnaireHeader {
            let button = self.nextButton!//headerCell.nextButton ?? UIButton()
            self.skipOptionsButton.isHidden = true
            button.setTitle("Next", for: .normal)
            switch(questionCase) {
            case .category:
                button.isEnabled = questionaireModel.category != "" ? true : false
                break
                
            case .name:
                button.isEnabled = questionaireModel.name != "" ? true : false
                break
                
            case .breed:
                button.isEnabled = questionaireModel.breed != "" ? true : false
                break
                
            case .gender:
                button.isEnabled = questionaireModel.gender != 0 ? true : false
                break
                
            case .age:
                
                button.isEnabled = true//questionaireModel.ageMonth != "" && questionaireModel.ageDate != "" ? true : false
                //self.skipOptionsButton.isHidden = button.isEnabled
                break
                
            case .photo:
                //self.skipOptionsButton.isHidden = false
                button.setTitle("Submit", for: .normal)
                button.isEnabled = true//(questionaireModel.profileImage != nil) ? true : false
                //questionaireModel.profileImage != #imageLiteral(resourceName: "location") ? true : false
                break
                
            case .location:
                button.isEnabled = questionaireModel.location !=  "" ? true : false
                break
            }
        }
    }
    
    //MARK:- Location Open
    func backFromSearchViewStaticLoc(latStr:Double ,longituteStr:Double ,string:String) {
        if let quesCell = tableViewObj.cellForRow(at: IndexPath(row: 1, section: 0)) as? QuestionnaireCell {
            
            if let index = quesCell.arrQuestions.index(of: .location), index < quesCell.arrQuestions.count {
                if let locationCell = quesCell.collectionViewObj.cellForItem(at: IndexPath(row: index, section: 0)) as? LocationCell {
                    self.questionaireModel.lat = Float(latStr)
                    self.questionaireModel.long = Float(longituteStr)
                    self.questionaireModel.location = string
                    locationCell.setLocation(lat: Float(latStr), long: Float(longituteStr), city: string)
                    self.handleNextButton(.location)
                }
            }
        }
    }
}

//MARK:- Delegates
extension QuestionnaireViewController : QuestionnaireScrollDelegate,QuestionnaireDelegate
{
    //MARK:- Private Method
    private func paging(_ index: Int, reset: Bool = false , count:Int) {
        var index = index
//        if index > 2 && (questionaireModel.category != "Dog" && questionaireModel.category != "Cat" && questionaireModel.category != "Rodent" && questionaireModel.category != "Reptile" && questionaireModel.category != "Bird" && questionaireModel.category != "Invertebrate") {
//            index += 1
//        }
        self.leadingOfPaginationLbl.constant = CGFloat(35*(index))
        self.paginationLbl.text = "\(index+1) of \(count)"
        
        if count == 6 {
            paginationImagesCollection.last?.isHidden = true
        }else{
            paginationImagesCollection.last?.isHidden = false
        }
        
        for imageViewObj in paginationImagesCollection {
            if reset {
                imageViewObj.image = #imageLiteral(resourceName: "Grey-Tab")
            }
            imageViewObj.tag = 1
        }
        
//        if index == 2 && (questionaireModel.category != "Dog" && questionaireModel.category != "Cat" && questionaireModel.category != "Rodent" && questionaireModel.category != "Reptile" && questionaireModel.category != "Bird" && questionaireModel.category != "Invertebrate"){
//            paginationImagesCollection[index].image = #imageLiteral(resourceName: "top-tab-2x")
//            paginationImagesCollection[index].tag = 10
//            index = index + 1
//
//            self.leadingOfPaginationLbl.constant = CGFloat(35*(index))
//            self.paginationLbl.text = "\(index+1)of7"
//        }
        
        paginationImagesCollection[index].image = #imageLiteral(resourceName: "top-tab-2x")
        paginationImagesCollection[index].tag = 10
    }
    
    func onResetValue(_ question: Questions, arr: [Questions]) {
        if question == .category {
            //questionaireModel.resetQuestionaire()
            self.paging(arr.index(of: question) ?? 0, reset: true , count: arr.count)
        }
        self.handleNextButton(question)
    }
    
    func collectionMove(_ index: Int, arr: [Questions]) {
        if fromSignUpLogin{
        if index == 0 {
            self.skipButton.isHidden = false
        }else{
            self.skipButton.isHidden = true
            }
        }
        self.paging(index, count: arr.count)
        self.handleNextButton(arr[index])
    }
    
    func tapChange() {
        if let cell = tableViewObj.cellForRow(at: IndexPath(row: 1, section: 0)) as? QuestionnaireCell {
            var counter = 0
            for imageViewObj in paginationImagesCollection {
                if imageViewObj.tag == 10 {
                    cell.screenChange(counter)
                    break;
                }
                counter = counter+1
            }
        }
    }
    
    func nextTapChange() {
        if let cell = tableViewObj.cellForRow(at: IndexPath(row: 1, section: 0)) as? QuestionnaireCell {
            var counter = 0
            for imageViewObj in paginationImagesCollection {
                if  imageViewObj.tag == 10 {
                    cell.screenChangeNext(counter)
                    break
                }
                counter = counter+1
            }
        }
    }
    
    
    func skip(){
        if let vc = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: "MainTABViewController") as? MainTABViewController{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    func back() {
        
        if self.fromSignUpLogin{
            
        }else{
         _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func next() {
        //Image Uploading
        
  //           addPetService("")
    }
    
    func onLocaionTap() {
//        let location = self.storyboard!.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
//        location.delegate = self
//        self.navigationController?.pushViewController(location, animated: true)
        
        if let vc = UIStoryboard(name: StoryboardName.Settings, bundle: nil).instantiateViewController(withIdentifier: "EditLocationVC") as? EditLocationVC{
        self.navigationController?.pushViewController(vc, animated: true)
              vc.delegate = self
          }
        
    }
    
    func onSelectValue(_ selection: Bool) {
        if selection {
            if let headerCell = tableViewObj.cellForRow(at: IndexPath(row: 0, section: 0)) as? QuestionnaireHeader {
                self.nextButton.isEnabled = true
            }
        }
    }
    
    func imageSelection() {
        photoManager = PhotoManager(navigationController: self.navigationController!, allowEditing: true, videoType: false, callback: { (pickedImage,urlVideo)  in
            
            
            if let pickedImage = pickedImage {
            
            self.questionaireModel.profileImage = pickedImage ?? #imageLiteral(resourceName: "TempProfile")
            
            if let quesCell = self.tableViewObj.cellForRow(at: IndexPath(row: 1, section: 0)) as? QuestionnaireCell {
                if let index = quesCell.arrQuestions.index(of: .photo), index < quesCell.arrQuestions.count {
                    self.handleNextButton(.photo)
                    
                    if let pictureCell = quesCell.collectionViewObj.cellForItem(at: IndexPath(row: index, section: 0)) as? PictureCell {
                        pictureCell.selectPhotButton.isHidden = true
                        pictureCell.profileImageView.isHidden = false
                        //pictureCell.photoButton.setImage(pickedImage, for: .normal)
                        pictureCell.profileImageView.image = pickedImage
                        pictureCell.photoButton.isHidden = false
                        quesCell.addLocation()
                    }
                }
            }
            }
            
            
           /* else
            {
                if let quesCell = self.tableViewObj.cellForRow(at: IndexPath(row: 1, section: 0)) as? QuestionnaireCell {
                    if let index = quesCell.arrQuestions.index(of: .photo), index < quesCell.arrQuestions.count {
                        
                        if let pictureCell = quesCell.collectionViewObj.cellForItem(at: IndexPath(row: index, section: 0)) as? PictureCell {
                            pictureCell.photoButton.setImage(DefaultImage.defaultPetImage, for: .normal)
                            pictureCell.photoButton.isHidden = false
                            self.questionaireModel.profileImage = nil
                            self.handleNextButton(.photo)
                        }
                    }
                }
            }*/
        })
    }
    
    
    
    func ChooseLocation(){
        if let vc = UIStoryboard(name: StoryboardName.Settings, bundle: nil).instantiateViewController(withIdentifier: "EditLocationVC") as? EditLocationVC{
         self.navigationController?.pushViewController(vc, animated: true)
               vc.delegate = self
           }
    }
    
    
    
    
    
    //MARK:- Service Methods
    func uploadImage(with data: Data) {

    }
    
    private func addPetService(_ url: String) {
        if let headerCell = tableViewObj.cellForRow(at: IndexPath(row: 0, section: 0)) as? QuestionnaireHeader {
            headerCell.nextButton.isUserInteractionEnabled = false
            headerCell.nextButton.isEnabled = false
        }
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        let yearToShow = year - (Int(questionaireModel.ageYear) ?? 0)
        var newMonth = ""
        switch(questionaireModel.ageMonth) {
                case "January":
                    newMonth = "01"
                    break
                case "February":
                    newMonth = "02"
                    break
                case "March":
                    newMonth = "03"
                    break
                case "April":
                     newMonth = "04"
                    break
                case "May":
                     newMonth = "05"
                    break
                case "June":
                     newMonth = "06"
                    break
                case "July":
                     newMonth = "07"
                break
                case "August":
                     newMonth = "08"
                break
                case "September":
                     newMonth = "09"
                break
                case "October":
                     newMonth = "10"
                break
                case "November":
                     newMonth = "11"
                break
                case "December":
                     newMonth = "12"
                break
                default:
                    break
    }
        var keyValue : [String: Any] = [
            "petType": questionaireModel.category,
            "petName": questionaireModel.name,
            "gender": questionaireModel.gender == 1 ? "Boy" : "Girl",
            "dob": "\(yearToShow)-\(newMonth)-\(questionaireModel.ageDate)",
            "location": questionaireModel.location,
            "latitude": "\(questionaireModel.lat)",
            "longitude": "\(questionaireModel.long)",
            "user_id": UserDefaults.standard.value(forKey: UserDefault.userId) ?? 0
        ]
     
        if questionaireModel.breed != "" {
            keyValue["breed"] = questionaireModel.breed
        }
        
        if questionaireModel.profileImage != nil {
            let data = questionaireModel.profileImage?.jpegData(compressionQuality: 0.5) ?? Data()
            keyValue["data"] = data
        }
//        let service =  WebServices.init()
//        service.addPet(methodName: ServiceEndPoint.addPet, params: keyValue as NSDictionary) { (check, message, response) in
//            self.hideLoader()
//        if check == true{
//            let pet = Pet()
//            pet.isSelected = false
//            pet.name = response?["pet_name"] as? String ?? ""
//            pet.id = response?["id"] as? String ?? ""
//            pet.petType = response?["pet_type"] as? String ?? ""
//            pet.breed = response?["pet_breed"] as? String ?? ""
//            pet.gender = response?["pet_gender"] as? String ?? ""
//            pet.isForAdd = false
//
//            if self.isFromEditProfile
//            {
//            let alertController = UIAlertController(title: "", message: "Pet added successfully", preferredStyle: .alert)
//
//            // Create the actions
//
//                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                UIAlertAction in
//
//                    self.newPet?(pet)
//                    _ = self.navigationController?.popViewController(animated: true)
//            }
//            alertController.addAction(okAction)
//            self.present(alertController, animated: true, completion: nil)
//            }
//            else{
//                Utility.obj.savePet()
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EndingViewController") as! EndingViewController
//                self.navigationController?.pushViewController(vc, animated: true)
//                }
//        }
//        else{
//                let alertController = UIAlertController(title: "", message: "Pet cannot be added , sorry for the inconvenience. Please try again.", preferredStyle: .alert)
//
//            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                    UIAlertAction in
//                   _ = self.navigationController?.popViewController(animated: true)
//                }
//                alertController.addAction(okAction)
//                self.present(alertController, animated: true, completion: nil)
//            }
//
//            }
        }
    
    }


//MARK:- TableView DataSource
extension QuestionnaireViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellIdentifier = ""
        if indexPath.row == 0 {
            cellIdentifier = "QuestionnaireHeader"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! QuestionnaireHeader
            cell.delegate = self
            return cell
        } else {
            cellIdentifier = "QuestionnaireCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! QuestionnaireCell
            cell.delegate = self
            cell.questionaireModel = self.questionaireModel
            return cell
        }
    }
}

//MARK:- TableView Delegates
extension QuestionnaireViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200
        } else {
            return 500
        }
    }
}


extension QuestionnaireViewController : EditLocationVCProtocol {
func locationDetails(lat: Double, long: Double, address: String) {
    self.questionaireModel.location = address
    self.questionaireModel.lat = Float(lat)
    self.questionaireModel.long = Float(long)
        
    
    
    if let quesCell = self.tableViewObj.cellForRow(at: IndexPath(row: 1, section: 0)) as? QuestionnaireCell {
        if let index = quesCell.arrQuestions.index(of: .location), index < quesCell.arrQuestions.count {
            self.handleNextButton(.location)
            
            if let locationCell = quesCell.collectionViewObj.cellForItem(at: IndexPath(row: index, section: 0)) as? LocationCell {
                locationCell.locationTextField.text = address
            }
        }
    }
    }
}
