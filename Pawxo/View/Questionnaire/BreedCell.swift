//
//  BreedCell.swift
//  PetConnet
//
//  Created by 42works on 04/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit

class BreedCell: UICollectionViewCell {

    //MARK:- Variable
    lazy var animalCategory = String()
    var pickerView: UIPickerView!
    lazy var arrSubCategory = [String]()
    var subCatSelected: ((String)-> ())?
    
    //MARK:- IBOutlet
    @IBOutlet weak var subCategoryTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK:- Initial Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK:- Void
    func config(_ questionaire: QuestionaireModel) {
        subCategoryTextField.text = questionaire.breed
        subCategoryTextField.font = UIFont.JosefinSansRegular(size: 18.0)
        titleLabel.text = "What breed is \(questionaire.name.capitalizingFirstLetter())?"
    }
    
    //MARK:- Inflate Array
    func fillSubCategoryArray(_ catName: String) {
        arrSubCategory = AnimalCategory.categoryDict[catName] ?? [String]()
        self.pickUp(subCategoryTextField)
    }
    
    //MAKR:- PickerView
    //MARK:- Private Method
    
    @objc func doneClick() {
        subCategoryTextField.resignFirstResponder()
//        subCatSelected?(self.subCategoryTextField.text ?? "")
    }
    
    private func pickUp(_ textField : UITextField) {
        // UIPickerView
        self.pickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 216))
        self.pickerView.backgroundColor = UIColor.white
        textField.inputView = self.pickerView        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        doneButton.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont.JosefinSansRegular(size: 18.0),
            NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
}

extension BreedCell: UIPickerViewDataSource, UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return petBreedArray.count//arrSubCategory.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return petBreedArray[row].name//arrSubCategory[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // pickerDelegate?.didSelect(arrCategory[row].trim())
        self.subCategoryTextField.text = petBreedArray[row].name?.trim()//arrSubCategory[row].trim()
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view as? UILabel { label = v }
        label.font = UIFont.JosefinSansRegular(size: 18.0)
        label.text =  petBreedArray[row].name
        label.textAlignment = .center
        return label
    }
}

extension BreedCell: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.text?.length == 0 {
            textField.text = petBreedArray[0].name?.trim()//arrSubCategory[0].trim()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.length != 0 {
            subCategoryTextField.resignFirstResponder()
            subCatSelected?(self.subCategoryTextField.text ?? "")
        }
    }
}
