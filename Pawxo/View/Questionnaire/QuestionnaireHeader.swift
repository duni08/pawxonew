//
//  LoginCell.swift
//  PetConnet
//
//  Created by 42Works-Worksys2 on 02/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit

@objc protocol QuestionnaireDelegate {
    func tapChange()
    func nextTapChange()
}

class QuestionnaireHeader: UITableViewCell {
    
    //MARK:- Variables
    var delegate: QuestionnaireDelegate?
    
    //MARK:- IBOutlet
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    //MARK:- Initial views
    override func awakeFromNib() {
        super.awakeFromNib()
        nextButton.setTitleColor(UIColor.black, for: .normal)
        nextButton.setTitleColor(UIColor.lightGray, for: .disabled)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- IBAction
    @IBAction func backTap() {
      delegate?.tapChange()
    }
    
    @IBAction func nextTap(_ sender: Any) {
        delegate?.nextTapChange()
    }
}
