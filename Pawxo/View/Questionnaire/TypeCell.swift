//
//  TypeCell.swift
//  PetConnet
//
//  Created by 42works on 04/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit

class TypeCell: UICollectionViewCell {

    //MARK:- Variable
    var arrCategory: [String]!
    var pickerView: UIPickerView!
    var catSelected: ((String)-> ())?
    
    
    //MARK:- IBOutlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryTextField: UITextField!
    
    //MARK:- Initial Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        arrCategory = AnimalCategory.animalName.components(separatedBy: ",")
        categoryTextField.font = UIFont.JosefinSansRegular(size: 18.0)
        self.pickUp(categoryTextField)
    }
    
    func config(_ questionaire: QuestionaireModel) {
        categoryTextField.text = questionaire.category
    }
    
    //MAKR:- PickerView
    //MARK:- Private Method
    
    @objc func doneClick() {
        categoryTextField.resignFirstResponder()
        catSelected?(self.categoryTextField.text ?? "")
    }
    
    private func pickUp(_ textField : UITextField) {
        // UIPickerView
        self.pickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 216))
        self.pickerView.backgroundColor = UIColor.white
        textField.inputView = self.pickerView
        
//        let pickerCategory = CustomPickerView()
//        pickerCategory.array = arrCategory
        pickerView.delegate = self
        pickerView.dataSource = self
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBarz
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneClick))
        doneButton.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont.JosefinSansRegular(size: 18.0),
            NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
}

extension TypeCell: UIPickerViewDataSource, UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return breedArray.count//arrCategory.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return breedArray[row].name//arrCategory[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       // pickerDelegate?.didSelect(arrCategory[row].trim())
        self.categoryTextField.text = breedArray[row].name!.trim()//arrCategory[row].trim()
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view as? UILabel { label = v }
        label.font = UIFont.JosefinSansRegular(size: 18.0)
        label.text =  breedArray[row].name //arrCategory[row]
        label.textAlignment = .center
        return label
    }
    
}

extension TypeCell: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.text?.length == 0 {
            textField.text = breedArray[0].name?.trim()//arrCategory[0].trim()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text != "" {
            
            let index = breedArray.firstIndex(where: { $0.name == textField.text })
            
            print("index-- \(String(describing: index))")
            
            if let petBreedArr = breedArray[index!].petBreedArray {
                petBreedArray.removeAll()
                for petDic in petBreedArr {
                    let pet = PetBreed(petInfo: petDic as! NSDictionary)
                    petBreedArray.append(pet)
                    
                }
                print("petBreedArray-- \(petBreedArray.count)")
            }
            
            categoryTextField.resignFirstResponder()
            catSelected?(self.categoryTextField.text ?? "")
        }
    }
}
