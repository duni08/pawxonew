//
//  QuestionnaireCell.swift
//  PetConnet
//
//  Created by 42Works-Worksys2 on 03/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit
import IQKeyboardManager

enum Questions {
    case category
    case name
    case breed
    case location
    case age
    case photo
    case gender
}

protocol QuestionnaireScrollDelegate {
    func onResetValue(_ question: Questions, arr: [Questions])
    func collectionMove(_ index: Int, arr: [Questions])
    func back()
    func next()
    func onLocaionTap()
    func imageSelection()
    func onSelectValue(_ selection: Bool)
}

class QuestionnaireCell: UITableViewCell {
    
    //MARK:- Variables
    var arrQuestions = [Questions]()
    lazy var animalCategory = String()
    var questionaireModel: QuestionaireModel!
    var delegate: QuestionnaireScrollDelegate?
    
    var emptyBreedArray :[Questions] = [.category, .name ,.gender ,.age,.location,.photo]
    var withBreedArray : [Questions] = [.category, .name , .breed ,.gender ,.age,.location,.photo]
    //MARK:- IBOutlet
    @IBOutlet weak var collectionViewObj : UICollectionView!
    
    //MARK:- Initial methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Register CollectionView Cells
        collectionViewObj.register(UINib(nibName: "TypeCell", bundle: Bundle.main), forCellWithReuseIdentifier: "TypeCell")
        collectionViewObj.register(UINib(nibName: "NameCell", bundle: Bundle.main), forCellWithReuseIdentifier: "NameCell")
        collectionViewObj.register(UINib(nibName: "BreedCell", bundle: Bundle.main), forCellWithReuseIdentifier: "BreedCell")
        collectionViewObj.register(UINib(nibName: "GenderCell", bundle: Bundle.main), forCellWithReuseIdentifier: "GenderCell")
        collectionViewObj.register(UINib(nibName: "LocationCell", bundle: Bundle.main), forCellWithReuseIdentifier: "LocationCell")
        collectionViewObj.register(UINib(nibName: "AgeCell", bundle: Bundle.main), forCellWithReuseIdentifier: "AgeCell")
        collectionViewObj.register(UINib(nibName: "PictureCell", bundle: Bundle.main), forCellWithReuseIdentifier: "PictureCell")
        
        collectionViewObj.delegate = self
        collectionViewObj.dataSource = self
        
        arrQuestions = emptyBreedArray
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        
        print("breedArray-- \(String(describing: breedArray.count))")
        
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Void Method
    func addLocation() {
        if !arrQuestions.contains(.location) {
            arrQuestions.append(.location)
            collectionViewObj.reloadData()
        }
    }
    
    func screenChange(_ index: Int) {
        var index = index
        self.endEditing(true)
        if index == 0
        {
           delegate?.back()
            return
        }
        
//        if !(questionaireModel.category == "Dog" || questionaireModel.category == "Cat" || questionaireModel.category == "Rodent" || questionaireModel.category == "Reptile" || questionaireModel.category == "Bird" || questionaireModel.category == "Invertebrate"){
//            if index > 2 {
//                index -= 1
//            }
//        }
        
        collectionViewObj.scrollToItem(at: IndexPath(row: index-1, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    func screenChangeNext(_ index: Int) {
        var index = index
        var totCount = 6
        
        if petBreedArray.count > 0{
            totCount =   7
        }
        self.endEditing(true)
//         if (questionaireModel.category == "Dog" || questionaireModel.category == "Cat" || questionaireModel.category == "Rodent" || questionaireModel.category == "Reptile" || questionaireModel.category == "Bird" || questionaireModel.category == "Invertebrate"){
//            totCount = 7
//        } else {
//            totCount = 6
//            if index > 2 {
//                index -= 1
//            }
//        }
        
        if index == totCount - 1 {
            delegate?.next()
            return
        }
        
        let newIndex = index + 1
        print(newIndex)
        
        collectionViewObj.scrollToItem(at: IndexPath(row: newIndex, section: 0), at: .centeredHorizontally, animated: true)
    }
}


//MARK:- ScrollView Delegate
extension QuestionnaireCell: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.moveCollection(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            self.moveCollection(scrollView)
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        self.moveCollection(scrollView)
    }
    
    private func moveCollection(_ scrollView: UIScrollView) {
       print(scrollView.contentOffset.x)
        let index = scrollView.contentOffset.x/self.frame.size.width
        print(index)
        delegate?.collectionMove(Int(index), arr: arrQuestions)
    }
}

//MARK:- CollectionView DataSource
extension QuestionnaireCell : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrQuestions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch arrQuestions[indexPath.row] {
        case .category:
            let cellIdentifier = "TypeCell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! TypeCell
            
            cell.config(self.questionaireModel)
            cell.catSelected = { catName in
                
//                if self.questionaireModel.category != "" && self.questionaireModel.category != catName {
//                    self.arrQuestions.removeAll()
//                    self.arrQuestions.append(.category)
//
//                    self.delegate?.onResetValue(.category, arr: self.arrQuestions)
//                }
                
                self.questionaireModel.category = catName.trim()
                self.animalCategory = catName
                if petBreedArray.count == 0 {
                    self.arrQuestions = self.emptyBreedArray
                }else{
                    self.arrQuestions = self.withBreedArray
                }
               
//                if !self.arrQuestions.contains(.name) {
//                    self.arrQuestions.append(.name)
//                }
                self.delegate?.onResetValue(.category, arr: self.arrQuestions)
                
                self.delegate?.onSelectValue(true)
                collectionView.reloadData()
            }
            return cell
            
        case .name:
            let cellIdentifier = "NameCell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! NameCell
            cell.config(self.questionaireModel)
            cell.petNameSelected = { petName in
                self.questionaireModel.name = petName.trim()
                print(petName)
//                if petBreedArray.count == 0 {
//                    if self.arrQuestions.contains(.breed) {
//                        self.arrQuestions.remove(at: self.arrQuestions.lastIndex(of: .breed)!)
//                    }
//                    self.arrQuestions.append(.gender)
//                }else{
//                    self.arrQuestions.append(.breed)
//                }
                self.delegate?.onResetValue(.name, arr: self.arrQuestions)
                collectionView.reloadData()
            }
            return cell
            
        case .breed:
            let cellIdentifier = "BreedCell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! BreedCell
            cell.fillSubCategoryArray(questionaireModel.category)
            cell.config(self.questionaireModel)
            cell.subCatSelected = { subCat in
                self.questionaireModel.breed = subCat.trim()
                print(subCat)
//                if !self.arrQuestions.contains(.gender) {
//                    self.arrQuestions.append(.gender)
//                }
                self.delegate?.onResetValue(.breed, arr: self.arrQuestions)
                collectionView.reloadData()
            }
            return cell
            
        case .gender:
            let cellIdentifier = "GenderCell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! GenderCell
            cell.config(self.questionaireModel)
            cell.genderSelected = { gender in
                self.questionaireModel.gender = gender
                print(gender)
//                if !self.arrQuestions.contains(.age) {
//                    self.arrQuestions.append(.age)
//                }
                self.delegate?.onResetValue(.gender, arr: self.arrQuestions)
                collectionView.reloadData()
            }
            return cell
            
        case .age:
            let cellIdentifier = "AgeCell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! AgeCell
            cell.config(self.questionaireModel)
//            if !self.arrQuestions.contains(.location){
//                self.arrQuestions.append(.location)
//                self.arrQuestions.append(.photo)
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//                    collectionView.reloadData()
//                }
//            }
            //self.arrQuestions.append(.location)
            cell.ageSelected = { (year, month, day) in

                self.questionaireModel.ageYear = year.trim()
                self.questionaireModel.ageMonth = month.trim()
                self.questionaireModel.ageDate = day.trim()
                //  print(image)
                self.delegate?.onResetValue(.age, arr: self.arrQuestions)
//                if !self.arrQuestions.contains(.photo) {
//                    self.arrQuestions.append(.location)
//                }
//
//                if !self.arrQuestions.contains(.photo){
//                    self.arrQuestions.append(.photo)
//                }
                collectionView.reloadData()
            }
            
            
            
            return cell
            
        case .photo:
            let cellIdentifier = "PictureCell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! PictureCell
            cell.config(self.questionaireModel)
            cell.imageClick = {
                //self.delegate?.next()
                self.delegate?.imageSelection()
                self.delegate?.onResetValue(.photo, arr: self.arrQuestions)
            }
            return cell
            
        case .location:
            let cellIdentifier = "LocationCell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! LocationCell
            cell.config(self.questionaireModel)
            cell.fillSubCategoryArray(questionaireModel.category)
            cell.locationSelected = { location in
                print(location)
                self.questionaireModel.location = location
                self.delegate?.onResetValue(.location, arr: self.arrQuestions)
                collectionView.reloadData()
            }
            cell.locationTap = {
                self.delegate?.onLocaionTap()
            }
            return cell
        }
    }
}

//MARK:- CollectionView Delegates
extension QuestionnaireCell:  UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            return CGSize(width: collectionView.frame.size.width, height: 500)
        } else {
            return CGSize(width: collectionView.frame.size.width, height: 485)
        }
    }
}
