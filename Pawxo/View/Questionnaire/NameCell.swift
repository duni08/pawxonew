//
//  NameCell.swift
//  PetConnet
//
//  Created by 42works on 04/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit

class NameCell: UICollectionViewCell {

    //MARK:- Variable
    var petNameSelected: ((String)-> ())?
    
    //MARK:- IBOutlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK:- Void
    func config(_ questionaire: QuestionaireModel) {
        nameTextField.text = questionaire.name
        nameTextField.font = UIFont.JosefinSansRegular(size: 18.0)
        titleLabel.text = "What's your \(questionaire.category.lowercased()) name?"
    }
}

extension NameCell: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.trim().length != 0 {
            petNameSelected?(textField.text ?? "")
        }
    }
}
