//
//  AgeCell.swift
//  PetConnet
//
//  Created by 42works on 04/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit

class AgeCell: UICollectionViewCell {
    
    //MARK:- Variable
    var selectedTextField = 0
    var year = ""
    lazy var monthArr = [String]()
    lazy var daysArr = [String]()
    var pickerView: UIPickerView!
    var ageSelected: ((
    _ year: String, _ month: String, _ day: String)-> ())?

    //MARK:- IBOutlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var monthTextField: UITextField!
    @IBOutlet weak var dayTextField: UITextField!
    
    @IBOutlet var topLabel: LightLabel!
    @IBOutlet weak var ageSlider: UISlider!
    @IBOutlet weak var labelYears: UILabel!
    @IBOutlet weak var dotLeadingConstring: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dotLeadingConstring.constant = ageSlider.thumbCenterX
        labelYears.text = "10"
        monthArr = CalendarP.monthArr
        self.dayArrFilter()
        self.pickUp()
    }
    
    func dayArrFilter() {
        for tempIndex in 1...31 {
            daysArr.append("\(tempIndex)")
        }
    }
    
    //MARK:- Void
    func config(_ questionaire: QuestionaireModel) {
        monthTextField.text = questionaire.ageMonth
        dayTextField.text = questionaire.ageDate
        monthTextField.font = UIFont.JosefinSansRegular(size: 18.0)
        dayTextField.font = UIFont.JosefinSansRegular(size: 18.0)
        titleLabel.text = "How old is \(questionaire.name.capitalizingFirstLetter())?"
        if questionaire.gender == 2
        {
            topLabel.text = "When is her birthday?"
        }
        else{
            topLabel.text = "When is his birthday?"
        }
    }
    

    
    //MAKR:- PickerView
    //MARK:- Private Method
    @objc func doneClick() {
        monthTextField.resignFirstResponder()
        dayTextField.resignFirstResponder()
    }
    
    private func pickUp() {
        // UIPickerView
        self.pickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 216))
        self.pickerView.backgroundColor = UIColor.white
        monthTextField.inputView = self.pickerView
        dayTextField.inputView = self.pickerView
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        doneButton.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont.JosefinSansRegular(size: 18.0),
            NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        monthTextField.inputAccessoryView = toolBar
        dayTextField.inputAccessoryView = toolBar
    }
    
    //MARK:- IBAction
    @IBAction func changeSlider(_ sender: UISlider) {
        dotLeadingConstring.constant = sender.thumbCenterX - 7
        labelYears.text = "\(Int(sender.value.rounded()))"
        if monthTextField.text?.trim().length != 0 && dayTextField.text?.trim().length != 0 {
            year = labelYears.text ?? ""
            ageSelected?(year, monthTextField.text ?? "", dayTextField.text ?? "")
        }
    }
    
    @IBAction func endSlide(_ sender: UISlider) {
        let value = sender.value
        sender.setValue(value.rounded(), animated: true)
        print(sender.value)
        dotLeadingConstring.constant = sender.thumbCenterX - 7
        labelYears.text = "\(Int(value.rounded()))"
        print(dotLeadingConstring.constant)
        
        if monthTextField.text?.trim().length != 0 && dayTextField.text?.trim().length != 0 {
            year = labelYears.text ?? ""
            ageSelected?(year, monthTextField.text ?? "", dayTextField.text ?? "")
        }
    }
}

//MARK:- TextField Delegates
extension AgeCell: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if monthTextField.text?.trim().length != 0 && dayTextField.text?.trim().length != 0 {
            year = labelYears.text ?? ""
            ageSelected?(year, monthTextField.text ?? "", dayTextField.text ?? "")
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) { 
        if textField == monthTextField {
            selectedTextField = 1
        } else {
            selectedTextField = 2
        }
        pickerView.reloadAllComponents()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == dayTextField {
            if monthTextField.text?.trim().length == 0 {
                return false
            } else {
                if let index = daysArr.index(of: dayTextField.text ?? ""), index < 31 {
                    pickerView.selectRow(index, inComponent: 0, animated: true)
                } else {
                    pickerView.selectRow(0, inComponent: 0, animated: true)
                }
                
                if textField.text?.length == 0 {
                    textField.text = daysArr[0]
                }
                return true
            }
        }
        
        if textField.text?.length == 0 {
            textField.text = monthArr[0]
        }
        if let index = monthArr.index(of: monthTextField.text ?? ""), index < 12 {
            pickerView.selectRow(index, inComponent: 0, animated: true)
        } else {
            pickerView.selectRow(0, inComponent: 0, animated: true)
        }
        return true
    }
}

//MARK:- PickerView Delegate and DataSource
extension AgeCell: UIPickerViewDataSource, UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if selectedTextField == 1 {
            return monthArr.count
        } else {
            return daysArr.count
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if selectedTextField == 1 {
            return monthArr[row]
        } else {
            return daysArr[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if selectedTextField == 1 {
            monthTextField.text = monthArr[row].trim()
            self.dayArrFilter()
        } else {
            dayTextField.text = daysArr[row].trim()
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view as? UILabel { label = v }
        label.font = UIFont.JosefinSansRegular(size: 18.0)
        if selectedTextField == 1 {
           label.text = monthArr[row]
        } else {
            label.text = daysArr[row]
        }
        label.textAlignment = .center
        return label
    }
    
    
}

//MARK:- UISlider Extension
extension UISlider {
    var thumbCenterX: CGFloat {
        let trackRect = self.trackRect(forBounds: frame)
        let thumbRect = self.thumbRect(forBounds: bounds, trackRect: trackRect, value: value)
        print(".....\(thumbRect.midX)")
        return thumbRect.midX
    }
}
