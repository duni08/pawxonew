//
//  LocationCell.swift
//  PetConnet
//
//  Created by 42works on 04/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit

class LocationCell: UICollectionViewCell {

    //MARK:- Variable
    
    //MARK:- Variable
    lazy var animalCategory = String()
    var pickerView: UIPickerView!
    lazy var arrSubCategory = [String]()
    var subCatSelected: ((String)-> ())?
    
    var locationTap: (()->())?
    var locationSelected: ((String)-> ())?
    
    //MARK:- IBOutlet
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK:- Void
    func config(_ questionaire: QuestionaireModel) {
        locationTextField.text = questionaire.location
        titleLabel.text = "Where does \(questionaire.name.capitalizingFirstLetter()) live?"
    }
    
    //MARK:- Inflate Array
    func fillSubCategoryArray(_ catName: String) {
        arrSubCategory = AnimalCategory.categoryDict[catName] ?? [String]()
        self.pickUp(locationTextField)
    }
    
    
    @objc func doneClick() {
            locationTextField.resignFirstResponder()
    //        subCatSelected?(self.subCategoryTextField.text ?? "")
        }
    
    private func pickUp(_ textField : UITextField) {
        // UIPickerView
        self.pickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 216))
        self.pickerView.backgroundColor = UIColor.white
        textField.inputView = self.pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        doneButton.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 18.0)!,
            NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Void
    func setLocation(lat: Float, long: Float, city: String) {
        locationTextField.text = city
    }
    
    //MARK:- IBAction
    @IBAction func locationButtonTap() {
        locationTap?()
    }
}

extension LocationCell: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.trim().length != 0 {
            locationSelected?(textField.text ?? "")
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.text?.length == 0 {
            textField.text = arrSubCategory[0].trim()
        }
        return true
    }
}


extension LocationCell: UIPickerViewDataSource, UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrSubCategory.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrSubCategory[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // pickerDelegate?.didSelect(arrCategory[row].trim())
        self.locationTextField.text = arrSubCategory[row].trim()
    }
}
