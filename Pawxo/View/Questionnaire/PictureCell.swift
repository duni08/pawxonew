//
//  PictureCell.swift
//  PetConnet
//
//  Created by 42works on 04/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit

class PictureCell: UICollectionViewCell {
    
    //MARK:- Variables
    var imageClick: (()-> Void)?
    var nextClick: (()-> Void)?
    
    //MARK:- IBOutlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectPhotButton: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK:- Void
    func config(_ questionaire: QuestionaireModel) {
        profileImageView.image = questionaire.profileImage
        if profileImageView.image != nil{
            profileImageView.isHidden = false
        }else{
            profileImageView.isHidden = true
        }
        titleLabel.text = "Upload a photo of \(questionaire.name.capitalizingFirstLetter())?"
    }
    
    func setImage(_ image: UIImage) {
        profileImageView.image = image
    }
    
    //MARK:- IBAction
    @IBAction func photoClick(_ sender: UIButton) {
        if sender.tag == 1 {
            
        } else {
            
        }
        imageClick?()
    }
}
