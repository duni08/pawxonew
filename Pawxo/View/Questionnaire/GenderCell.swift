//
//  GenderCell.swift
//  PetConnet
//
//  Created by 42works on 04/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit

class GenderCell: UICollectionViewCell {

    //MARK:- Variable
    var genderSelected: ((Int)-> ())?
    
    //MARK:- IBOutlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
   
    //MARK:- Void
    func config(_ questionaire: QuestionaireModel) {
        titleLabel.text = "Is \(questionaire.name.capitalizingFirstLetter()) Boy or a Girl?"
        if questionaire.gender == 1{
            femaleButton.backgroundColor = UIColor.gray
            maleButton.backgroundColor = Colour.primary
        } else if questionaire.gender == 2 {
            maleButton.backgroundColor = UIColor.gray
            femaleButton.backgroundColor = Colour.primary
        } else {
            femaleButton.backgroundColor = UIColor.gray
            maleButton.backgroundColor = UIColor.gray
        }
        
    }

    //MARK:- IBAction
    @IBAction func genderTap(_ sender: UIButton) {
        genderSelected?(sender.tag)
        sender.backgroundColor = Colour.primary
        if sender.tag == 1 {
            femaleButton.backgroundColor = UIColor.lightGray
        } else {
            maleButton.backgroundColor = UIColor.lightGray
        }
    }
}
