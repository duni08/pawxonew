////
//  ViewController.swift
//  Pawxo Demo
//
//  Created by Apple on 15/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import FSCalendar

class AppointmentVC: UIViewController {

    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var showDate_Lbl: UILabel!
    @IBOutlet weak var addres_TBL: UITableView!
    @IBOutlet weak var countinue_Btn: UIButton!
    @IBOutlet weak var hours_Lbl: UILabel!
    @IBOutlet weak var minuts_Lbl: UILabel!
    @IBOutlet weak var timeZoneAM_Lbl: UIButton!
    @IBOutlet weak var timeZonePM_Lbl: UIButton!
    @IBOutlet weak var month_View: UIView!
    @IBOutlet weak var year_View: UIView!
    
    @IBOutlet weak var month_Btn: UIButton!
    @IBOutlet weak var year_Btn: UIButton!
    
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var monthAndYearPicker: UIPickerView!
    
    @IBOutlet weak var editView: UIView!
    
    var hours:Int = 0
    var minutes:Int = 0
    var indexNumber = Int()
    
    var isYear = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setView()
    }
    
    func setView()  {
        
        pickerView.layer.masksToBounds = false
        pickerView.layer.shadowRadius = 14
        pickerView.layer.shadowOpacity = 5
        pickerView.layer.shadowColor = UIColor.gray.cgColor
        pickerView.layer.shadowOffset = CGSize(width: 0 , height:2)
        
        month_View.layer.borderColor = UIColor.lightGray.cgColor
        month_View.layer.borderWidth = 1
        month_View.layer.cornerRadius = month_View.frame.height / 2
        month_View.layer.masksToBounds = true
        
        year_View.layer.borderColor = UIColor.lightGray.cgColor
        year_View.layer.borderWidth = 1
        year_View.layer.cornerRadius = year_View.frame.height / 2
        year_View.layer.masksToBounds = true
        
        showDate_Lbl.layer.cornerRadius = showDate_Lbl.frame.height / 2
        showDate_Lbl.layer.masksToBounds = true
        
        countinue_Btn.layer.cornerRadius = countinue_Btn.frame.height / 2
        countinue_Btn.layer.masksToBounds = true
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let result = formatter.string(from: date)
        showDate_Lbl.text = result
        
        formatter.dateFormat = "a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        let dateString = formatter.string(from: Date())
        
        if dateString == "PM" {
            
            timeZonePM_Lbl.setTitleColor(UIColor.init(named: "BaseColor"), for: .normal)
            timeZoneAM_Lbl.setTitleColor(UIColor.lightGray, for: .normal)
        
        }else{
        
            timeZonePM_Lbl.setTitleColor(UIColor.lightGray, for: .normal)
            timeZoneAM_Lbl.setTitleColor(UIColor.init(named: "BaseColor"), for: .normal)
        
        }
    }
    
    
    @IBAction func hoursIncrementDecrement(_ sender:UIButton) {
        
        if sender.tag == 0 {
            
            if hours != 12 {
                
                hours = hours + 1
                
                if hours.description.count == 1 {
                    hours_Lbl.text = "0\(hours) h"
                }else{
                    hours_Lbl.text = "\(hours) h"
                }
                
            }else{
                
                hours = 0
                
                if hours.description.count == 1 {
                    hours_Lbl.text = "0\(hours) h"
                }else{
                    hours_Lbl.text = "\(hours) h"
                }
            }
        }else if sender.tag == 1 {
            
            if hours != 0 {
                
                hours = hours - 1
                
                if hours.description.count == 1 {
                
                    hours_Lbl.text = "0\(hours) h"
                
                }else{
                
                    hours_Lbl.text = "\(hours) h"
                
                }
                
            }else{
                
                hours = 12
                
                if hours.description.count == 1 {
                    hours_Lbl.text = "0\(hours) h"
                }else{
                    hours_Lbl.text = "\(hours) h"
                }
                
            }
        }
        
    }
    
    @IBAction func minutesIncrementDecrement(_ sender:UIButton) {
        
        if sender.tag == 0 {
            
            if minutes != 59 {
                
                minutes = minutes + 1
                
                if minutes.description.count == 1 {
                    minuts_Lbl.text = "0\(minutes) m"
                }else{
                    minuts_Lbl.text = "\(minutes) m"
                }
                
            }else{
                minutes = 0
                if minutes.description.count == 1 {
                    minuts_Lbl.text = "0\(minutes) m"
                }else{
                    minuts_Lbl.text = "\(minutes) m"
                }
            }
            
            
        }else if sender.tag == 1 {
            
            if minutes != 0 {
                
                minutes = minutes - 1
                
                if minutes.description.count == 1 {
                
                    minuts_Lbl.text = "0\(minutes) m"
                
                }else{
                
                    minuts_Lbl.text = "\(minutes) m"
                
                }
                
            }else{
                minutes = 59
                if minutes.description.count == 1 {
                
                    minuts_Lbl.text = "0\(minutes) m"
                
                }else{
                
                    minuts_Lbl.text = "\(minutes) m"
                
                }
            }
        }
    }
    
    @IBAction func timeZoneIncrementDecrement(_ sender:UIButton) {
        
        if sender.tag == 0 {
            timeZonePM_Lbl.setTitleColor(UIColor.lightGray, for: .normal)
            timeZoneAM_Lbl.setTitleColor(UIColor.init(named: "BaseColor"), for: .normal)
        }else if sender.tag == 1 {
            timeZonePM_Lbl.setTitleColor(UIColor.init(named: "BaseColor"), for: .normal)
            timeZoneAM_Lbl.setTitleColor(UIColor.lightGray, for: .normal)
        }
    }
    
    @IBAction func selectButtonTapped(_ sender:UIButton) {
        pickerView.isHidden = true
        
        
        if isYear == true {
            year_Btn.setTitle(CalendarP.yearArr[indexNumber], for: .normal)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            let someDateTime = formatter.date(from:"\(CalendarP.yearArr[indexNumber])/03/01")
            self.calendar.select(someDateTime)
        }else{
            
            month_Btn.setTitle(CalendarP.monthShortArr[indexNumber], for: .normal)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            let someDateTime = formatter.date(from:"2020/\(CalendarP.monthShortArr[indexNumber])/01")
            self.calendar.select(someDateTime)
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender:UIButton) {
        pickerView.isHidden = true
    }
    
    @IBAction func yearButtonTapped(_ sender:UIButton) {
        pickerView.isHidden = false
        monthAndYearPicker.delegate = self
        monthAndYearPicker.reloadAllComponents()
        isYear = true
    }
    
    @IBAction func monthNameButtonTapped(_ sender:UIButton) {
        pickerView.isHidden = false
        monthAndYearPicker.delegate = self
        monthAndYearPicker.reloadAllComponents()
        isYear = false
    }
    
    @IBAction func hideEditViewTapped(_ sender:UIButton) {
        editView.isHidden = true
    }
    
    
}

extension AppointmentVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:AddressCell = (tableView.dequeueReusableCell(withIdentifier: "Cell") as? AddressCell)!
        cell.editBtn.tag = indexPath.row
        cell.delegate = self as EditAddressCellDelegate
        return cell
    }
     
}


extension AppointmentVC : EditAddressCellDelegate {
    
    func editAddressEvent(sender : UIButton) {
       editView.isHidden = false
//        let frame = sender.superview!.convert(sender.frame, to: self.view)
//        print("frame-- \(frame)")
//
//        var xPosition = Int(addres_TBL.frame.origin.x)
//        xPosition = Int(frame.origin.x) - Int(self.editView.frame.size.width - 10)
//
//        var yPosition = addres_TBL.frame.origin.y
//        yPosition = CGFloat(Int(yPosition) + Int(sender.tag * 60) + Int(frame.size.height))
//
//        UIView.animate(withDuration: 0.4, animations: {
//            self.editView.isHidden = false
//            self.editView.frame = CGRect(x: CGFloat(xPosition), y: yPosition, width: self.editView.frame.size.width, height: self.editView.frame.size.height)
//        })
        
    }
    
}

extension AppointmentVC :UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
    
        return 1
    
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if isYear == true {
          
            return CalendarP.yearArr.count
            
        }else{
            
            return CalendarP.monthShortArr.count
            
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
       // indexNumber = row
        
        if isYear == true {
            
            return "\(CalendarP.yearArr[row])"
            
        }else{
          return "\(CalendarP.monthShortArr[row])"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            indexNumber = row
    }
}


extension AppointmentVC:FSCalendarDataSource, FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let result = formatter.string(from: date)
        showDate_Lbl.text = result
        print("Select date-- \(date.description) -- position-- \(monthPosition)")
        
//        print("calendar did select date \(self.formatter.string(from: date))")
//        if monthPosition == .previous || monthPosition == .next {
//            calendar.setCurrentPage(date, animated: true)
//        }
    }
}

//class NewDesign : UIView {
//
//    @IBInspectable var cornerRadius: Double {
//         get {
//           return Double(self.layer.cornerRadius)
//         }set {
//           self.layer.cornerRadius = CGFloat(newValue)
//         }
//    }
//    @IBInspectable var borderWidth: Double {
//          get {
//            return Double(self.layer.borderWidth)
//          }
//          set {
//           self.layer.borderWidth = CGFloat(newValue)
//          }
//    }
//    @IBInspectable var borderColor: UIColor? {
//         get {
//            return UIColor(cgColor: self.layer.borderColor!)
//         }
//         set {
//            self.layer.borderColor = newValue?.cgColor
//         }
//    }
//    @IBInspectable var shadowColor: UIColor? {
//        get {
//           return UIColor(cgColor: self.layer.shadowColor!)
//        }
//        set {
//           self.layer.shadowColor = newValue?.cgColor
//        }
//    }
//    @IBInspectable var shadowOpacity: Float {
//        get {
//           return self.layer.shadowOpacity
//        }
//        set {
//           self.layer.shadowOpacity = newValue
//       }
//    }
//}
