//
//  FollowVC.swift
//  Pawxo
//
//  Created by 42works on 22/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class FollowVC: UIViewController {

    
    @IBOutlet weak var noResultsView: UIView!
    @IBOutlet weak var noInternetView: UIView!
    
    @IBOutlet weak var followTableView: UITableView!
    var userlist = [Userlist]()
    var staticCount = 10
    var usersListCellIdentifier = "UsersListCell"
    
    @IBOutlet weak var titleLabel: SemiBoldLabel!
    var followersId : String?
    var userId : String?
    
    var follow_VM = Follow_VM()
    
    var isFollowing = false
    
    
    var unFollowRequest: (()->())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        followTableView.register(UINib(nibName: usersListCellIdentifier, bundle: nil), forCellReuseIdentifier: usersListCellIdentifier)
        // Do any additional setup after loading the view.
        self.getFollowersList()
        
        self.titleLabel.text = isFollowing ? "Following" : "Followers"
    }
    

    @IBAction func tryAgainPressed(_ sender: Any) {
        self.getFollowersList()
    }
    
    
    func getFollowersList(){
        
        
        if !(WebServices().isInternetWorking()){
           // self.refresh.endRefreshing()
            self.noInternetView.isHidden = false
            self.userlist.removeAll()
            self.followTableView.isHidden = false
            self.followTableView.addSubview(self.noInternetView)
            self.followTableView.reloadData()
            return
        }else{
            self.noInternetView.isHidden = true
        }
        
        
        let userLogin = CommonFunctions.getuserFromDefaults()
        follow_VM.userId = userId
        follow_VM.perPage = "50"
        follow_VM.page = "1"
        follow_VM.followerId = followersId
        follow_VM.isFollowing = self.isFollowing
        
        follow_VM.responseRecieved = {
            self.userlist = self.follow_VM.userList
            // refresh content
            self.refreshUI()
        }
        
        follow_VM.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.follow_VM.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
         follow_VM.callFollowerUsersService = true
    }
    
    func refreshUI(){
        if self.userlist.count > 0 {
            self.followTableView.isHidden = false
            self.followTableView.reloadData()
        }else{
            self.noInternetView.isHidden = true
            self.noResultsView.isHidden = false
            self.followTableView.isHidden = true
        }
        
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func goToProfileView(userId:Int){
        let storyboardProfile = UIStoryboard(name: "Profile", bundle: nil)
        let vc_Profile = storyboardProfile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc_Profile.isFromOtherScreen = true
        vc_Profile.userId = "\(userId)"
        self.navigationController?.pushViewController(vc_Profile, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension FollowVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UsersListCell =  tableView.dequeueReusableCell(withIdentifier: usersListCellIdentifier, for: indexPath) as! UsersListCell
        //cell.homeCellDelegate = self
        let user = userlist[indexPath.row]
        cell.delegate = self
        cell.setUpUser(user: user)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_PostDetail = storyboard.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
        //self.navigationController?.pushViewController(vc_PostDetail, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
}


extension FollowVC : UsersListCellProtocol {
    func profilePressed(cell:UsersListCell) {
        self.goToProfileView(userId:cell.user!.id!)
    }
    
    
    
    func followbuttonPressed(cell:UsersListCell){
        let follow_VM = Follow_VM()
        follow_VM.userId = CommonFunctions.getuserFromDefaults().userId
        follow_VM.followerId = "\((cell.user?.id)!)"
        
        let status = cell.profileStatus
        
        if status == .following {
            //Show Alert
           let user = cell.user!
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let unfollow = storyboard.instantiateViewController(withIdentifier: "UnFollowAlertVC") as! UnFollowAlertVC
            unfollow.nameStr = user.name ?? ""
            unfollow.imageStr = user.userImage ?? ""
            unfollow.followDelegate = self
            unfollow.modalPresentationStyle = .overCurrentContext
            unfollow.modalTransitionStyle = .crossDissolve
            self.present(unfollow, animated: true, completion: nil)
            
            
            self.unFollowRequest = {
                
                print("UnFollow Accepted")
                
                follow_VM.responseRecieved = {
                    
                    
                    if let indexPath = self.followTableView.indexPath(for: cell){
                        self.userlist.remove(at: indexPath.row)
                        self.followTableView.deleteRows(at: [indexPath], with: .automatic)
                        self.refreshUI()
                    }
//                    let user = cell.user!
//                    user.isFollowed = 0
//                    cell.setUpUser(user: user)
                }
                follow_VM.showAlertClosure = {
                    DispatchQueue.main.async {
                        if let message = follow_VM.alertMessage {
                            self.showAlert(self, message: message)
                        }
                    }
                }
                
                follow_VM.updateLoadingStatus = {
                    DispatchQueue.main.async {
                        if follow_VM.isLoading {
                            LoaderView.shared.showLoader()
                        } else {
                            LoaderView.shared.hideLoader()
                        }
                    }
                }
                
                follow_VM.callFollowerService = true
                
                
            }
            
            
            
        }else if status == .requested{
            
            let user = cell.user!
            
            follow_VM.responseRecieved = {
                
                 user.isRequested = "0"
                cell.setUpUser(user: user)
            }
            
            follow_VM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if follow_VM.isLoading {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            follow_VM.showAlertClosure = {
                DispatchQueue.main.async {
                    if let message = follow_VM.alertMessage {
                        self.showAlert(self, message: message)
                    }
                }
            }
            
            if let isPrivate = user.isProfilePrivate, isPrivate.boolValue == true {
                follow_VM.callRequestUserFollowService = true
            }else{
                // public check for follow unfoolow
                follow_VM.callFollowerService = true
            }
            
        }
        else
        {
            
            
           let user = cell.user!
            
            
            follow_VM.responseRecieved = {
                
                
                if let isPrivate = user.isProfilePrivate, isPrivate.boolValue == true {
                    user.isRequested = "1"
                }else{
                    // public check for follow unfoolow
                    user.isFollowed = 1
                }
                cell.setUpUser(user: user)
            }
            
            follow_VM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if follow_VM.isLoading {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            follow_VM.showAlertClosure = {
                DispatchQueue.main.async {
                    if let message = follow_VM.alertMessage {
                        self.showAlert(self, message: message)
                    }
                }
            }
            
            if let isPrivate = user.isProfilePrivate, isPrivate.boolValue == true {
                follow_VM.callRequestUserFollowService = true
            }else{
                // public check for follow unfoolow
                follow_VM.callFollowerService = true
            }
            
            
        }
    }
}
