//
//  FeedVC.swift
//  Pawxo
//
//  Created by 42works on 14/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
var photoManager:PhotoManager!


class PetTemp : NSObject {
    var name : String?
    var isSelectedPet : Bool = false
    
     init(name:String,isSelected:Bool) {
        self.name = name
        self.isSelectedPet = isSelected
    }
}

class FeedVC: UIViewController {
    
    @IBOutlet weak var petNameLabel: SemiBoldLabel!
    @IBOutlet weak var petWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var petListTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var addPetButton: RegularButton!
    
    var isFromOtherScreen = false
    
    @IBOutlet weak var petChooseArrow: UIImageView!
    var staticCount = 2
    var feedProfileCellIdentifier = "FeedProfileCell"
    var feedListCellIdentifier = "FeedPostCell"
    var noPostCellIdentifier = "NoPostCell"
    
    var cellHeight : CGFloat = 500
    
    var getHeight = true
    //var petArray = [PetTemp(name: "TestPet", isSelected: true),PetTemp(name: "Joey", isSelected: false),PetTemp(name: "NewJoey", isSelected: false)]
    
    var petListArray = [PetList]()
    
    var petObject:PetList?
    
    var posts = [PostList]()
    
    var dataArrayMain = [String:Any]()
    
    @IBOutlet weak var feedTableView: UITableView!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var petChooseButton: UIButton!
    var isNavigated = false
    
    @IBOutlet weak var cuddlLogo: UIImageView!
    
    
    var petVM : Pet_VM?
    
    var petId : Int?
    
    var isMYPet = true
    
    var descriptionText = ""
    
    var padding  : CGFloat = 20
    
    var profileCellHeight : CGFloat = 0
    
    var page = 1
    var refresh = UIRefreshControl()
    var total_page = 0
    var IsloadedPosts = false
    
    var isForPullToRefresh = false
    
    @IBOutlet weak var noInternetView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        feedTableView.register(UINib(nibName: feedProfileCellIdentifier, bundle: nil), forCellReuseIdentifier: feedProfileCellIdentifier)
        feedTableView.register(UINib(nibName: feedListCellIdentifier, bundle: nil), forCellReuseIdentifier: feedListCellIdentifier)
        feedTableView.register(UINib(nibName: noPostCellIdentifier, bundle: nil), forCellReuseIdentifier: noPostCellIdentifier)
        
        self.backButton.isHidden = !isFromOtherScreen
        self.cuddlLogo.isHidden = isFromOtherScreen
        self.petChooseButton.isHidden = isFromOtherScreen
        self.petChooseArrow.isHidden = isFromOtherScreen
        self.addPetButton.isHidden = isFromOtherScreen
        
        
        
        
        if isFromOtherScreen {
            trailingConstraint.constant = 20
        }else{
            self.petChooseButton.isHidden = true
            self.petChooseArrow.isHidden = true
            self.petNameLabel.isHidden = true
            
           // self.calculateWidhtforTitle()
        }
        
        refresh.tintColor = UIColor.clear
           refresh.backgroundColor = UIColor.clear
           refresh.attributedTitle = NSAttributedString(string: "Pull to refresh")
           refresh.addTarget(self, action:#selector(refreshTable), for: UIControl.Event.valueChanged)
           // Add Custom Loader
           var loader = UIImageView()
           let gif = UIImage.gifImageWithName("gif")
           loader.center = self.view.center
           loader = UIImageView(frame: CGRect(x: self.view.frame.size.width/2 - 20, y: refresh.center.y, width: 40, height: 40))
           loader.image = gif
           refresh.insertSubview(loader, at: 0)
           
           
           
           self.feedTableView.addSubview(refresh)

           let emptyLabel = UILabel.init(frame: self.feedTableView.frame)
           var msg = ""
        
           msg = "No latest posts yet."
           
           emptyLabel.isHidden = true
           emptyLabel.text = msg
           emptyLabel.textAlignment = .center
           emptyLabel.numberOfLines = 0
           emptyLabel.textColor = UIColor.darkGray
           self.feedTableView.addSubview(emptyLabel)
        
            self.getDataForFeedView()
        
    }
    
    
    @objc private func refreshTable() {
        page = 1
        self.isForPullToRefresh = true
        getDataForFeedView()
    }
    
    
    @IBAction func tryAgainPressed(_ sender: Any) {
        self.refreshTable()
    }
    
    
    func getDataForFeedView(){
        if !(WebServices().isInternetWorking()){
            self.refresh.endRefreshing()
            self.noInternetView.isHidden = false
            self.dataArrayMain.removeAll()
            self.feedTableView.isHidden = false
            self.feedTableView.addSubview(self.noInternetView)
            self.feedTableView.reloadData()
            self.isForPullToRefresh = false
            return
        }else{
            self.noInternetView.isHidden = true
        }
        
        if isFromOtherScreen {
            self.setHeaderProfileWithPet(pet: petObject!)
        }else{
            self.getPetListApi()
           // self.calculateWidhtforTitle()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.getDataForFeedView()
    }
    
    
    
    
    func getPetListApi() {
        
        let userLogin = CommonFunctions.getuserFromDefaults()
        
        let petVM = CreateEditPost_VM()
        petVM.userId = userLogin.userId
        petVM.perPage = "50"
        petVM.page = "1"
        
        
        petVM.responseRecieved = {
            
            self.petListArray = petVM.petList
            self.setHeader()
            // refresh content
        }
        
        petVM.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = petVM.alertMessage {
                   // self?.showAlert(self ?? UIViewController(), message: message)
                    self!.feedTableView.isHidden = true
                    self?.petListArray.removeAll()
                    self!.setHeader()
                }
            }
        }
        
        petVM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if petVM.isLoading {
                    if self.isForPullToRefresh == false{
                        LoaderView.shared.showLoader()
                    }
                } else {
                    if self.isForPullToRefresh == false{
                        LoaderView.shared.hideLoader()
                    }else{
                        self.isForPullToRefresh = false
                    }
                }
            }
        }
        
        petVM.callGetPetListService = true
        
    }
    
    func setHeightForProfileView(pet:PetList){
        // calculateHeight for descriptionView
        
        self.descriptionText = pet.petBio ?? ""
         
        var textHeight = descriptionText.height(withConstrainedWidth: self.view.frame.width - (padding * 2), font: UIFont.JosefinSansSemiBold(size: 16.0))
        
        if isMYPet {
            if self.descriptionText != ""{
                textHeight += 170
            }else{
                textHeight = 170
            }
        }else{
            if self.descriptionText != ""{
                textHeight += 148
            }else{
                textHeight = 148
            }
        }
        profileCellHeight = textHeight
    }
    
    
    func setHeader(){
        if self.petListArray.count > 0 {
            
            self.feedTableView.isHidden = false
            self.addPetButton.isHidden = true
            
            if let val = UserDefaults.standard.value(forKey: "petSelected") as? Int{
                
                if let index = self.petListArray.lastIndex(where: {$0.petID == val}) {
                    let obj = self.petListArray[index]
                    self.setHeaderProfileWithPet(pet: obj)
                }else{
                    if let pet = self.petListArray.first{
                        self.setHeaderProfileWithPet(pet: pet)
                    }
                }
            }else{
                if let pet = self.petListArray.first{
                    self.setHeaderProfileWithPet(pet: pet)
                    
                }
            }
            
            
            if petListArray.count == 3 {
                self.petChooseButton.isHidden = false
                self.petChooseArrow.isHidden = false
            }
            
            
        }else{
            // Do for empty View
            self.feedTableView.isHidden = true
            self.petNameLabel.text = ""
            self.addPetButton.isHidden = false
            self.calculateWidhtforTitle()
        }
    }
    
    
    func setHeaderProfileWithPet(pet:PetList){
        UserDefaults.standard.setValue(pet.petID!, forKey: "petSelected")
        self.petNameLabel.text = pet.petName ?? ""
        self.calculateWidhtforTitle()
        // self.getPosts
        self.petObject = pet
        self.petNameLabel.isHidden = false
        self.dataArrayMain["profile"] = self.petObject
        setHeightForProfileView(pet: pet)
        if let obj = self.dataArrayMain["posts"]{
            self.dataArrayMain.removeValue(forKey: "posts")
        }
        self.posts.removeAll()
        self.feedTableView.reloadData()
        self.getPostsForPet(petId: pet.petID!)
    }
    
    
    func bottomRefresh(_ scrollView : UIScrollView)
    {
        let scrollViewHeight = scrollView.frame.size.height;
        let scrollContentSizeHeight = scrollView.contentSize.height;
        let scrollOffset = scrollView.contentOffset.y;
        if (scrollOffset >  scrollContentSizeHeight - scrollViewHeight)
        {
            if IsloadedPosts {
                page = page + 1
                
                if page <= self.total_page{
                    self.getPostsForPet(petId: (self.petObject?.petID!)!)
                }
            }
        }
    }
    
    
    func getPostsForPet(petId:Int){
        
        print("Get post API call")
        self.getHeight = true
        IsloadedPosts = false
        let userLogin = CommonFunctions.getuserFromDefaults()
          let petVM = Pet_VM()
          petVM.userId = userLogin.userId
          petVM.perPage = "50"
          petVM.page = "\(page)"
          petVM.pets_post = petId
          
          
          petVM.responseRecieved = {
            self.refresh.endRefreshing()
            self.posts = petVM.postList
            self.setPosts()
            self.IsloadedPosts = true
              // refresh content
          }
          
          petVM.showAlertClosure = {
              DispatchQueue.main.async {
                self.setPosts()
                self.refresh.endRefreshing()
                  if let message = petVM.alertMessage {
                     // self.showAlert(self , message: message)
                  }
              }
          }
          
          petVM.updateLoadingStatus = {
              DispatchQueue.main.async {
                  if petVM.isLoading {
                    if !self.refresh.isRefreshing{
                      LoaderView.shared.showLoader()
                    }
                  } else {
                      LoaderView.shared.hideLoader()
                  }
              }
          }
       
        
          petVM.callGetPetPostListService = true
        //
    }
    
    func setPosts(){
        if posts.count > 0 {
            self.dataArrayMain["posts"] = posts
            self.dataArrayMain.removeValue(forKey: "Nodata")
        }else{
            self.dataArrayMain["Nodata"] = "No data"
        }
        self.feedTableView.reloadData()
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func petChooseButtonPressed(_ sender: UIButton) {
        
        if !(WebServices().isInternetWorking()){
            self.noInternetView.isHidden = false
            self.dataArrayMain.removeAll()
            self.feedTableView.isHidden = false
            self.feedTableView.addSubview(self.noInternetView)
            self.feedTableView.reloadData()
            return
        }else{
            self.noInternetView.isHidden = true
        }
        
        
        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
        let vc_ChoosePet = storyboardPet.instantiateViewController(withIdentifier: "ChoosePetVC") as! ChoosePetVC
        vc_ChoosePet.petListArray = petListArray
        vc_ChoosePet.modalPresentationStyle = .overCurrentContext
        vc_ChoosePet.modalTransitionStyle = .crossDissolve
        vc_ChoosePet.selectedPet = self.petObject
        vc_ChoosePet.delegate = self
        self.present(vc_ChoosePet, animated: true, completion: nil)
    }
    
    @IBAction func addPet(_ sender: Any) {
        
        if breedArray.count > 0 {
            goToAdd()
        }else{
        
        breedVM = Breed_VM()
        
        breedVM?.responseRecievedBreed = { breed in
            breedArray = breed
            //self.tableViewObj.reloadData()
            print("self.breedArray== \(breedArray.count)")
            self.goToAdd()
            
        }
        breedVM?.showAlertClosure = {
            
        }
        
        breedVM?.callGetBreedListService = true
        }
    }
    
    func goToAdd(){
        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
         let vc_questionare = storyboardPet.instantiateViewController(withIdentifier: "QuestionnaireViewController") as! QuestionnaireViewController
        self.navigationController?.pushViewController(vc_questionare, animated: true)
    }
    
    
    
   @IBAction func gotoEditPet(){
        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
        let vc_editPet = storyboardPet.instantiateViewController(withIdentifier: "EditPetVC") as! EditPetVC
        vc_editPet.petProfileObject = petObject
        vc_editPet.isEditProfile = true
        vc_editPet.petUpdated = {
            self.getDataForFeedView()
        }
        self.navigationController?.pushViewController(vc_editPet, animated: true)
    }
    
    
    func calculateWidhtforTitle(){
        let str = self.petNameLabel.text!
        let widthCalculated = str.width(withConstrainedHeight: 21.0, font: self.petNameLabel.font)
        self.petWidthConstraint.constant = widthCalculated + 28
        if str != "" {
            if !isFromOtherScreen{
            self.petChooseButton.isHidden = false
            self.petChooseArrow.isHidden = false
            }else{
                trailingConstraint.constant = 20
            }
        }else{
            self.petChooseButton.isHidden = true
            self.petChooseArrow.isHidden = true
        }
    }
    
}

extension FeedVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return dataArrayMain.keys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let pet = dataArrayMain["profile"] as? PetList
            
            let cell : FeedProfileCell =  tableView.dequeueReusableCell(withIdentifier: feedProfileCellIdentifier, for: indexPath) as! FeedProfileCell
            cell.setPet(pet: pet!)
            cell.delegate = self
            cell.setMyPet(isMyPet:self.isMYPet)
            if self.posts.count > 0 {
                cell.showDividerView(show:false)
            }else{
                cell.showDividerView(show:true)
            }
            cell.calculateWidthForCateogry()
            return cell
        }else{
            
            if self.posts.count > 0 {
                let cell : FeedPostCell =  tableView.dequeueReusableCell(withIdentifier: feedListCellIdentifier, for: indexPath) as! FeedPostCell
                cell.delegate = self
                cell.getHeight = self.getHeight
                cell.setPosts(posts:self.posts)
                return cell
                
            }else{
                let cell : NoPostCell =  tableView.dequeueReusableCell(withIdentifier: noPostCellIdentifier, for: indexPath) as! NoPostCell
                cell.titleLabel.text = "Spread the love!"
                cell.descriptionLabel.text = "Share everything from your pet's adorable photos,\n fun-videos and more."
                return cell
            }
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_PostDetail = storyboard.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
        //self.navigationController?.pushViewController(vc_PostDetail, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            if indexPath.row == 0 {
                return UITableView.automaticDimension
            }else{
                
                let NewHeight = self.view.frame.height  - self.view.safeAreaInsets.top - 44 - profileCellHeight
                
                if (cellHeight > NewHeight){
                    return cellHeight
                }else{
                    return NewHeight
                }
            }
    }
}


extension FeedVC : FeedPostCellDelegate {
    
    func setheight(height: CGFloat) {
        if height == self.cellHeight{
            self.getHeight = false
        }
        self.cellHeight = height
        self.feedTableView.reloadData()
    }
    
    func gotoPostDetailScreen(post:PostList){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_PostDetail = storyboard.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
        vc_PostDetail.post = post
        vc_PostDetail.isFromProfileOrPet = true
        self.navigationController?.pushViewController(vc_PostDetail, animated: true)
    }
}

extension FeedVC : FeedProfileCellDelegate {
    func editPetButtonPressed() {
        self.gotoEditPet()
    }
    
    func editPetImagePressed(cell:FeedProfileCell) {
        
        photoManager = PhotoManager(navigationController: self.navigationController!, allowEditing: true, videoType: false, callback: { (pickedImage,urlVideo)  in
            //self.questionaireModel.profileImage = pickedImage ?? #imageLiteral(resourceName: "TempProfile")
            if let image = pickedImage {
                
                let submitPet = SubmitPet_VM()
                
                submitPet.imageData = image.pngData()
                
                let userInfo = CommonFunctions.getuserFromDefaults()
                submitPet.userIdFinal = userInfo.userId
                submitPet.petId = "\((cell.pet?.petID)!)"
                submitPet.updateLoadingStatus = {
                    DispatchQueue.main.async {
                        if submitPet.isLoading {
                            LoaderView.shared.showLoader()
                        } else {
                            LoaderView.shared.hideLoader()
                        }
                    }
                }
                
                submitPet.showAlertClosure = {
                    DispatchQueue.main.async {
                        if let message = submitPet.alertMessage {
                            self.showAlert(self , message: message)
                        }
                    }
                }
                
                submitPet.responseRecieved = {
                    DispatchQueue.main.async {
                        self.showAlert(self , message: "Pet Updated Successfully.")
                    }
                    cell.profileImage.image = image
                }
                
                //Call Api
                
                submitPet.callEditPetService = true
                
                
                
                cell.setImageProfile(image: image)
            }
            
        })
    }
}

extension FeedVC : ChoosePetVCDelegate {
    
    func addPetPressed() {
        self.goToAdd()
    }
    
    func petChoosed(pet: PetList) {
        cellHeight = 500
        setHeaderProfileWithPet(pet:pet)
    }
    
    
}
