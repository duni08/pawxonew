//
//  EditPetVC.swift
//  Pawxo
//
//  Created by 42works on 16/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class EditPetVC: UIViewController {

    @IBOutlet weak var nameTF: RegularTextField!
    @IBOutlet weak var typeTF: RegularTextField!
    @IBOutlet weak var breedTF: RegularTextField!
    @IBOutlet weak var dobTF: RegularTextField!
    @IBOutlet weak var locationTF: RegularTextField!
    @IBOutlet weak var bioTV: RegularTextView!
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var maleRadioImg: UIImageView!
    @IBOutlet weak var femaleRadioImg: UIImageView!
    var pickerView: UIPickerView!
    var arrCategory: [String]! = ["A","B","C"]
    var currentTextField : UITextField?
    var photoManager:PhotoManager!
    var petProfileObject:PetList?
    var isEditProfile = false
    
    var currentLat : Double?
    var currentLong : Double?
    var currentAddress : String = ""
    
    var isUpdatedImage = false
    
    var petUpdated:(()->())?
    
    @IBOutlet weak var placeHolderLabel: RegularLabel!
    
    
    // For DOB
        var selectedTextField = 0
        var year = ""
        lazy var monthArr = [String]()
        lazy var daysArr = [String]()
        var ageSelected: ((
        _ year: String, _ month: String, _ day: String)-> ())?

        //MARK:- IBOutlet
        @IBOutlet weak var titleLabel: UILabel!
        @IBOutlet weak var monthTextField: UITextField!
        @IBOutlet weak var dayTextField: UITextField!
            
        var pickerViewNew: UIPickerView!
        @IBOutlet var topLabel: LightLabel!
        @IBOutlet weak var ageSlider: UISlider!
        @IBOutlet weak var labelYears: UILabel!
        @IBOutlet weak var dotLeadingConstring: NSLayoutConstraint!
    //
    
    @IBOutlet weak var dobView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    
    var petBreedsArray = [PetBreed]()
    
    var selectedTypeIndex = -1
    var selectedBreedIndex = -1
    
    @IBOutlet weak var dropDownArrow: UIImageView!
    var isBreedFound = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let degreesToRadians: (CGFloat) -> CGFloat = { (degrees: CGFloat) in
            return degrees / 180.0 * CGFloat.pi
        }
        self.dropDownArrow.transform = CGAffineTransform(rotationAngle:degreesToRadians(-90))
        self.nameTF.tag  = 1
        self.typeTF.tag  = 2
        self.breedTF.tag  = 3
        self.locationTF.tag  = 4
        // Do any additional setup after loading the view.
        
        if isEditProfile == true {
            
            self.nameTF.text = petProfileObject?.petName
            self.typeTF.text = petProfileObject?.petType
            self.breedTF.text = petProfileObject?.petBreed
            self.locationTF.text = petProfileObject?.location ?? "Select Location"
            self.dobTF.text = petProfileObject?.petDob
            self.bioTV.text = petProfileObject?.petBio
            
            let profileImage = petProfileObject?.petImages
            
            if profileImage!.isEmpty {
                self.profileImage.image =  #imageLiteral(resourceName: "TempProfile")
            }else{
                let url = URL(string: profileImage!)
                self.profileImage.kf.setImage(with: url)
            }
            
            
            if petProfileObject!.petGender == "Boy" || petProfileObject!.petGender == "Male" {
               maleBtn.isSelected = true
               femaleBtn.isSelected = false
               maleRadioImg.image = UIImage.init(named: "radioSelected")
               femaleRadioImg.image = UIImage.init(named: "radioUnselected")
            }else{
                maleBtn.isSelected = false
                femaleBtn.isSelected = true
                femaleRadioImg.image = UIImage.init(named: "radioSelected")
                maleRadioImg.image = UIImage.init(named: "radioUnselected")
            }
            
            if let location = self.petProfileObject?.location {
                self.currentAddress = location
            }
            
//            if let lat = self.petProfileObject?.latitude {
//                self.currentLat = Double(lat)
//            }
//
//            if let long = self.petProfileObject?.longitude {
//                self.currentLong = Double(long)
//            }
            if bioTV.text.count > 0 {
                placeHolderLabel.alpha = 0
            }else{
                placeHolderLabel.alpha = 1
            }
            dobView.isHidden = true
            
        }
        
        dotLeadingConstring.constant = ageSlider.thumbCenterX
        labelYears.text = "10"
        monthArr = CalendarP.monthArr
        self.dayArrFilter()
        self.pickUp()
        titleLabel.text = "How old is \((petProfileObject?.petName)!)"
        if petProfileObject?.petGender == "0"
        {
            topLabel.text = "When is his birthday?"
        }
        else{
            topLabel.text = "When is her birthday?"
        }
        
        
        if breedArray.count >  0 {
        
        
            if let index = breedArray.firstIndex(where: {$0.name! == typeTF.text!}){
                self.selectedTypeIndex = index
                self.setBreedArray(arrayBreed: breedArray[index].petBreedArray!)
                self.breedTF.text = petProfileObject?.petBreed
            }
        
        
        
            if let indexSecond = petBreedsArray.firstIndex(where: {$0.name! == breedTF.text!}){
                self.selectedBreedIndex = indexSecond
            }
        }else{
                
               var breedVM = Breed_VM()
                breedVM.responseRecievedBreed = { breed in
                    breedArray = breed
                    //self.tableViewObj.reloadData()
                    print("self.breedArray== \(breedArray.count)")
                    self.isBreedFound = true
                }
            
            breedVM.showAlertClosure = {
                self.isBreedFound = false
            }
                breedVM.callGetBreedListService = true
        
        }
        
    }
    
    
    @objc func doneClickNew() {
        
       
        
        if dayTextField.isFirstResponder{
            if self.labelYears.text == "0"{
                self.showAlert(self, message: "Please select year")
            }else{
                self.dobView.isHidden = true
                self.calculateDate()
            }
        }
        monthTextField.resignFirstResponder()
        dayTextField.resignFirstResponder()
    }
    
    private func pickUp() {
        // UIPickerView
        self.pickerViewNew = UIPickerView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 216))
        self.pickerViewNew.backgroundColor = UIColor.white
        monthTextField.inputView = self.pickerViewNew
        dayTextField.inputView = self.pickerViewNew
        
        pickerViewNew.delegate = self
        pickerViewNew.dataSource = self
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickNew))
        doneButton.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont.JosefinSansRegular(size: 18.0),
            NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        monthTextField.inputAccessoryView = toolBar
        dayTextField.inputAccessoryView = toolBar
    }
    
    //MARK:- IBAction
    @IBAction func changeSlider(_ sender: UISlider) {
        dotLeadingConstring.constant = sender.thumbCenterX - 7
        labelYears.text = "\(Int(sender.value.rounded()))"
        if monthTextField.text?.trim().length != 0 && dayTextField.text?.trim().length != 0 {
            year = labelYears.text ?? ""
            ageSelected?(year, monthTextField.text ?? "", dayTextField.text ?? "")
        }
    }
    
    @IBAction func endSlide(_ sender: UISlider) {
        let value = sender.value
        sender.setValue(value.rounded(), animated: true)
        print(sender.value)
        dotLeadingConstring.constant = sender.thumbCenterX - 7
        labelYears.text = "\(Int(value.rounded()))"
        print(dotLeadingConstring.constant)
        
        if monthTextField.text?.trim().length != 0 && dayTextField.text?.trim().length != 0 {
            year = labelYears.text ?? ""
            ageSelected?(year, monthTextField.text ?? "", dayTextField.text ?? "")
        }
    }
    
    func dayArrFilter() {
        for tempIndex in 1...31 {
            daysArr.append("\(tempIndex)")
        }
    }
    
    
    func calculateDate(){
        let day = self.dayTextField.text!
        let month = self.monthTextField.text!
        let years = self.labelYears.text!
        
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        
        
        let finalYear = year - Int(years)!
        
        let index = monthArr.firstIndex(of: month)
        let finalIndex = index! + 1
        
        let finalDate = CommonFunctions.makeDate(year: finalYear, month: finalIndex , day: Int(day)!, hr: 0, min: 0, sec: 0)
        
        print(finalDate.getDate())
        self.dobTF.text = finalDate.getDate()
        
        
        
    }
    
    
    @IBAction func editPetPressed(_ sender: Any) {
        
        
        if self.dobView.isHidden == false{
            
            self.monthTextField.resignFirstResponder()
            self.dayTextField.resignFirstResponder()
            if self.monthTextField.text == ""{
                self.showAlert(self, message: "Please select Month")
                return
            }
            
            if self.dayTextField.text == ""{
                self.showAlert(self, message: "Please select day")
                return
            }
            
            if self.labelYears.text ==  "0"{
                self.showAlert(self, message: "Please select Year")
                return
            }
            
            self.dobView.isHidden = true
            
            self.calculateDate()
            return
        }
        
        
        if nameTF.text == ""{
            self.showAlert(self, message: "Please enter Pet name")
            return
        }
        
        if typeTF.text == "" {
            self.showAlert(self, message: "Please select Pet Type")
            return
        }
        
        if breedTF.text == "" && self.petBreedsArray.count > 0 {
            self.showAlert(self, message: "Please select Pet Breed")
            return
        }
        
        if dobTF.text == "" {
            self.showAlert(self, message: "Please select Pet DOB")
            return
        }
        
        if locationTF.text == "" {
            self.showAlert(self, message: "Please choose Pet Location")
            return
        }
        
        if bioTV.text == "" {
            self.showAlert(self, message: "Please enter Pet bio")
            return
        }

        
        let submitPet = SubmitPet_VM()
        
        if self.isUpdatedImage{
            submitPet.imageData = self.profileImage.image!.pngData()
        }
        
        let userInfo = CommonFunctions.getuserFromDefaults()
        submitPet.userIdFinal = userInfo.userId
        submitPet.petId = "\((self.petProfileObject?.petID)!)"
        
        submitPet.petName = self.nameTF.text!
        submitPet.location = self.currentAddress
        submitPet.breed = self.breedTF.text!
        submitPet.petType = self.typeTF.text!
        submitPet.dateOfBirth = self.dobTF.text
        submitPet.gender = maleBtn.isSelected ? "Male" : "Female"
        submitPet.petBio = self.bioTV.text!
        submitPet.updateLoadingStatus = {
            DispatchQueue.main.async {
                if submitPet.isLoading {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        submitPet.showAlertClosure = {
            DispatchQueue.main.async {
                if let message = submitPet.alertMessage {
                    self.showAlert(self , message: message)
                }
            }
        }
        
        submitPet.responseRecieved = {
            DispatchQueue.main.async {
                self.showAlert(self , message: "Pet Updated Successfully.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.petUpdated?()
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
        //Call Api
        
        submitPet.callEditPetService = true
        
        
    }
    
    
    
    
    @IBAction func radioButton(sender: UIButton) {
        if sender == maleBtn {
          //  strInterested = "yes"
            maleBtn.isSelected = true
            femaleBtn.isSelected = false
            maleRadioImg.image = UIImage.init(named: "radioSelected")
            femaleRadioImg.image = UIImage.init(named: "radioUnselected")

        } else if sender == femaleBtn{
           // strInterested = "no"
            maleBtn.isSelected = false
            femaleBtn.isSelected = true
            femaleRadioImg.image = UIImage.init(named: "radioSelected")
            maleRadioImg.image = UIImage.init(named: "radioUnselected")

        }
    }
    
    @IBAction func back(sender: UIButton)
    {
        if !self.dobView.isHidden{
            self.dobView.isHidden = true
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    @objc func doneClick(sender : UIButton) {
        
        if sender.tag == 1{
            nameTF.resignFirstResponder()
        }else if sender.tag == 2{
            typeTF.resignFirstResponder()
        }else if sender.tag == 3{
            breedTF.resignFirstResponder()
        }else if sender.tag == 4{
            locationTF.resignFirstResponder()
        }else{
            nameTF.resignFirstResponder()
        }
            
    //        catSelected?(self.categoryTextField.text ?? "")
        }
    
    
    func ShowPickerView(textField:UITextField){
        self.pickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 216))
                self.pickerView.backgroundColor = UIColor.white
                textField.inputView = self.pickerView
                
        //        let pickerCategory = CustomPickerView()
        //        pickerCategory.array = arrCategory
                pickerView.delegate = self
                pickerView.dataSource = self
                
                // ToolBar
                let toolBar = UIToolbar()
                toolBar.barStyle = .default
                toolBar.isTranslucent = true
                toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
                toolBar.sizeToFit()
                
                // Adding Button ToolBarz
            
            let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneClick))
            doneButton.tag = textField.tag
            
        
                doneButton.setTitleTextAttributes([
                    NSAttributedString.Key.font: UIFont.JosefinSansRegular(size: 18.0),
                    NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
                
                let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
                toolBar.setItems([spaceButton, doneButton], animated: false)
                toolBar.isUserInteractionEnabled = true
            textField.inputAccessoryView = toolBar
            textField.becomeFirstResponder()
            currentTextField = textField
        
    }
    
    
    @IBAction func typeButtonPressed(_ sender: Any) {
        
        if isBreedFound{
            self.ShowPickerView(textField: typeTF)
            if self.selectedTypeIndex != -1{
                self.pickerView.selectRow(self.selectedTypeIndex, inComponent: 0, animated: false)
                self.pickerView(self.self.pickerView, didSelectRow: self.selectedTypeIndex, inComponent: 0)
            }
        }
    }
    
    
    @IBAction func breedButtonPressed(_ sender: Any) {
        if isBreedFound{
            if self.petBreedsArray.count  > 0 {
            self.ShowPickerView(textField: breedTF)
            if self.selectedBreedIndex != -1{
                self.pickerView.selectRow(self.selectedBreedIndex, inComponent: 0, animated: false)
                self.pickerView(self.self.pickerView, didSelectRow: self.selectedBreedIndex, inComponent: 0)
            }
            }
        }
    }
    
    
    
    func calculateDateForDateChooser(){
        
        
        if dobTF.text! != "" {
            let date = dobTF.text!.convertToDate()
            let calendar = Calendar.current
            let year = calendar.component(.year, from: date)
            let month = calendar.component(.month, from: date)
            let day = calendar.component(.day, from: date)
            
            let dateCurrent = Date()
            
            let currentYear = calendar.component(.year, from: dateCurrent)
            
            let val = currentYear - year
            
            if val >= 0 {
                self.labelYears.text = "\(val)"
                
                let slideVal = Float(val)
                self.ageSlider.setValue(slideVal, animated: false)
                self.changeSlider(self.ageSlider)
                
            }else{
                self.labelYears.text = "1"
                let slideVal = Float((self.ageSlider.frame.width / 20) * CGFloat(1))
                self.ageSlider.setValue(slideVal, animated: false)
            }
            
            self.monthTextField.text = self.monthArr[month - 1]
            self.dayTextField.text = "\(day)"
            
            
            
        }
        
        
//
//        let day = self.dayTextField.text!
//        let month = self.monthTextField.text!
//        let years = self.labelYears.text!
//
//        let date = Date()
//        let calendar = Calendar.current
//        let year = calendar.component(.year, from: date)
//
//
//        let finalYear = year - Int(years)!
//
//        let index = monthArr.firstIndex(of: month)
//        let finalIndex = index! + 1
//
//        let finalDate = CommonFunctions.makeDate(year: finalYear, month: finalIndex , day: Int(day)!, hr: 0, min: 0, sec: 0)
//
//        print(finalDate.getDate())
//        self.dobTF.text = finalDate.getDate()
        
        
        
    }
    
    @IBAction func dobButtonPressed(_ sender: Any) {
        dobView.isHidden = false
        calculateDateForDateChooser()
        
    }
    
    
    @IBAction func locationButtonPressed(_ sender: Any) {
         if let vc = UIStoryboard(name: StoryboardName.Settings, bundle: nil).instantiateViewController(withIdentifier: "EditLocationVC") as? EditLocationVC{
            if self.isEditProfile == true{
                vc.isEdit = true
            }
                   self.navigationController?.pushViewController(vc, animated: true)
                   vc.delegate = self
               }
    }
    
    
    @IBAction func editImagePressed(_ sender: UIButton){
        photoManager = PhotoManager(navigationController: self.navigationController!, allowEditing: true, videoType: false, callback: { (pickedImage,urlVideo)  in
            //self.questionaireModel.profileImage = pickedImage ?? #imageLiteral(resourceName: "TempProfile")
            if let image = pickedImage{
                self.profileImage.image = image
                self.isUpdatedImage = true
            }
        })
    }
   

}



extension EditPetVC : EditLocationVCProtocol,UITextViewDelegate {
    func locationDetails(lat: Double, long: Double, address: String) {
        self.currentLat = lat
        self.currentLong = long
        self.currentAddress = address
        self.locationTF.text = address
    }
    
    
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            if newText.count > 0 {
                placeHolderLabel.alpha = 0
            }else{
                placeHolderLabel.alpha = 1
            }
            return newText.count < 121
        }
}

 /*extension EditPetVC: UIPickerViewDataSource, UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCategory.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrCategory[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       // pickerDelegate?.didSelect(arrCategory[row].trim())
        self.currentTextField?.text = arrCategory[row].trim()
    }
}*/



//MARK:- TextField Delegates
extension EditPetVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if monthTextField.text?.trim().length != 0 && dayTextField.text?.trim().length != 0 {
            year = labelYears.text ?? ""
            ageSelected?(year, monthTextField.text ?? "", dayTextField.text ?? "")
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == monthTextField {
            selectedTextField = 1
            pickerViewNew.reloadAllComponents()
        } else if textField == dayTextField{
            selectedTextField = 2
            pickerViewNew.reloadAllComponents()
        }else if textField == typeTF{
            selectedTextField = 3
            pickerView.reloadAllComponents()
        }else{
            selectedTextField =  4
            pickerView.reloadAllComponents()
        }
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == dayTextField {
            if monthTextField.text?.trim().length == 0 {
                return false
            } else {
                if let index = daysArr.index(of: dayTextField.text ?? ""), index < 31 {
                    pickerViewNew.selectRow(index, inComponent: 0, animated: true)
                } else {
                    pickerViewNew.selectRow(0, inComponent: 0, animated: true)
                }
                
                if textField.text?.length == 0 {
                    textField.text = daysArr[0]
                }
                return true
            }
        }
        
        if textField == monthTextField {
        if textField.text?.length == 0 {
            textField.text = monthArr[0]
        }
        if let index = monthArr.index(of: monthTextField.text ?? ""), index < 12 {
            pickerViewNew.selectRow(index, inComponent: 0, animated: true)
        } else {
            pickerViewNew.selectRow(0, inComponent: 0, animated: true)
        }
        }
        return true
    }
}

//MARK:- PickerView Delegate and DataSource
extension EditPetVC: UIPickerViewDataSource, UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == self.pickerViewNew {
        if selectedTextField == 1 {
            return monthArr.count
        } else {
            return daysArr.count
        }
        }else{
            if selectedTextField == 3 {
                return breedArray.count
            } else {
                return petBreedsArray.count
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.pickerViewNew {
        if selectedTextField == 1 {
            return monthArr[row]
        } else {
            return daysArr[row]
            }
            
        }else{
            
            if selectedTextField == 3{
                return breedArray[row].name!
            }else if selectedTextField == 4{
                
                let breed = petBreedsArray[row]
                return breed.name
            }
            
            return arrCategory[row]
        }
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view as? UILabel { label = v }
        label.font = UIFont.JosefinSansRegular(size: 18.0)
        
        if pickerView == self.pickerViewNew {
            if selectedTextField == 1 {
                label.text = monthArr[row]
            } else {
                label.text = daysArr[row]
            }
        }else{
            if selectedTextField == 3 {
                label.text =  breedArray[row].name
            } else {
                label.text = petBreedsArray[row].name
            }
        }
        
        label.textAlignment = .center
        return label
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.pickerViewNew {
        if selectedTextField == 1 {
            monthTextField.text = monthArr[row].trim()
            self.dayArrFilter()
        } else {
            dayTextField.text = daysArr[row].trim()
        }
        }else{
            if selectedTextField == 3 {
                self.currentTextField?.text = breedArray[row].name
                self.setBreedArray(arrayBreed: breedArray[row].petBreedArray!)
                
            } else {
                self.currentTextField?.text = petBreedsArray[row].name
            }
        }
    }
    
    
    
    
    
    
    
    func setBreedArray(arrayBreed:NSArray){
        self.petBreedsArray.removeAll()
        for arr in arrayBreed{
            let dict = arr as! NSDictionary
            
            let breed = PetBreed(petInfo: dict)
            self.petBreedsArray.append(breed)
        }
        
        self.breedTF.text = ""
        
    }
    
    
//    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
//        if pickerView == self.pickerViewNew {
//        var label = UILabel()
//        if let v = view as? UILabel { label = v }
//        label.font = UIFont.JosefinSansRegular(size: 18.0)
//        if selectedTextField == 1 {
//           label.text = monthArr[row]
//        } else {
//            label.text = daysArr[row]
//        }
//        label.textAlignment = .center
//        return label
//        }else{
//            return
//        }
//    }
    
    
}
