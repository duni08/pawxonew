//
//  ChoosePetVC.swift
//  Pawxo
//
//  Created by 42works on 15/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
protocol ChoosePetVCDelegate {
    func addPetPressed()
    func petChoosed(pet:PetList)
}

class ChoosePetVC: UIViewController {
    
    @IBOutlet weak var petListTableView: UITableView!
    
    var petArray = [PetTemp(name: "TestPet", isSelected: true),PetTemp(name: "Joey", isSelected: false),PetTemp(name: "NewJoey", isSelected: false)]
    
    var petListArray = [PetList]()
    
    var delegate : ChoosePetVCDelegate?
    
    var selectedPet : PetList?
    
    var petListCellIdentifier = "PetListCell"
    
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        petListTableView.register(UINib(nibName: petListCellIdentifier, bundle: nil), forCellReuseIdentifier: petListCellIdentifier)
        // Do any additional setup after loading the view.
        if self.petListArray.count < 5 {
            self.tableHeightConstraint.constant  = CGFloat((self.petListArray.count * 60) + 80)
        }else{
            self.tableHeightConstraint.constant  = CGFloat((5 * 60) + 80)
        }
        
    }
    

    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func addPetPressed(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.delegate?.addPetPressed()
        }
        
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ChoosePetVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return petListArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell : PetListCell =  tableView.dequeueReusableCell(withIdentifier: petListCellIdentifier, for: indexPath) as! PetListCell
            let pet: PetList = petListArray[indexPath.row]//petArray[indexPath.row]
            
        
        if pet.petID! ==  self.selectedPet?.petID! {
            cell.isSelectedPet = true
        }else{
            cell.isSelectedPet = false
        }
            
            cell.setPet(pet: pet)

            cell.delegate = self
            return cell
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_PostDetail = storyboard.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
        //self.navigationController?.pushViewController(vc_PostDetail, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return 60.0
        
    }
}


extension ChoosePetVC : PetListCellDelegate {
    func choosedPet(cell: PetListCell) {
        let indexPath = petListTableView.indexPath(for: cell)
        for i in 0...petArray.count - 1{
            if (indexPath?.row != i){
                let pet = petArray[i]
                pet.isSelectedPet = false
            }else{
                
            }
        }
        delegate?.petChoosed(pet: cell.pet!)
        self.dismiss(animated: true, completion: nil)
//        self.petListTableView.reloadData()
    }
}
