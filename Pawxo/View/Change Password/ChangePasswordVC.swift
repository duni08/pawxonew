//
//  ChangePasswordVC.swift
//  Pawxo Demo
//
//  Created by Apple on 15/01/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var currentPassword_tf: UITextField!
    @IBOutlet weak var newPassword_tf: UITextField!
    @IBOutlet weak var verifyPassword_tf: UITextField!
    @IBOutlet weak var reset_btn: UIButton!
    
    var changePassword_VM = ChangePassword_VM()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        reset_btn.layer.cornerRadius = reset_btn.frame.height / 2
        reset_btn.layer.masksToBounds = true
        
    }

    @IBAction func backButtonTapped(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resetPasswordTapped(_ sender:UIButton) {
        
        let userLogin = CommonFunctions.getuserFromDefaults()
        changePassword_VM.userId = userLogin.userId
        
        changePassword_VM.password = newPassword_tf.text!
        changePassword_VM.email = userLogin.email!
        changePassword_VM.oldpassword = currentPassword_tf.text!
        changePassword_VM.password_confirmation = verifyPassword_tf.text!
        
        
        
        changePassword_VM.responseRecieved = {
            // refresh content
            DispatchQueue.main.async {
                if let message = self.changePassword_VM.alertMessage {
                    self.showAlert(self, message: message)
                    self.currentPassword_tf.text = ""
                    self.newPassword_tf.text = ""
                    self.verifyPassword_tf.text = ""
                }
            }
            
        }
        
        changePassword_VM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if self.changePassword_VM.isLoading {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        changePassword_VM.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.changePassword_VM.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        
        changePassword_VM.callChangePasswordService = true
        
    }
    
    
    
}

extension ChangePasswordVC:UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

@IBDesignable
class EdgeInsetLabel: UILabel {
    var textInsets = UIEdgeInsets.zero {
        didSet { invalidateIntrinsicContentSize() }
    }

    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insetRect = bounds.inset(by: textInsets)
        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
        let invertedInsets = UIEdgeInsets(top: -textInsets.top,
                left: -textInsets.left,
                bottom: -textInsets.bottom,
                right: -textInsets.right)
        return textRect.inset(by: invertedInsets)
    }

    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: textInsets))
    }
}

extension EdgeInsetLabel {
    @IBInspectable
    var leftTextInset: CGFloat {
        set { textInsets.left = newValue }
        get { return textInsets.left }
    }

    @IBInspectable
    var rightTextInset: CGFloat {
        set { textInsets.right = newValue }
        get { return textInsets.right }
    }

    @IBInspectable
    var topTextInset: CGFloat {
        set { textInsets.top = newValue }
        get { return textInsets.top }
    }

    @IBInspectable
    var bottomTextInset: CGFloat {
        set { textInsets.bottom = newValue }
        get { return textInsets.bottom }
    }
}
