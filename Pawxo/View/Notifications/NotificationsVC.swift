//
//  NotificationsVC.swift
//  Pawxo
//
//  Created by 42works on 08/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
//protocol callNotificationListProtocol {
//    func getNotificationList()
//}
class NotificationsVC: UIViewController {
 var notificationVM : Notification_VM?
   //  var delegate: callNotificationListProtocol?
    @IBOutlet weak var notificationTableView: UITableView!
    var notificationIdentifier = "NotificationCell"
    var staticCount = 4
    var notificationArray = [NotificationList]()
    var notifications = [NotificationList]()
    var pageCount = 1
    var comment_VM : Comment_VM?
    var forum :Forum_VM?
    var post : PostList?
    var total_page = 0
     var refresh = UIRefreshControl()
     var IsloadedPosts = false
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationTableView.register(UINib(nibName: notificationIdentifier, bundle: nil), forCellReuseIdentifier: notificationIdentifier)
        refresh.tintColor = UIColor.clear
           refresh.backgroundColor = UIColor.clear
           refresh.attributedTitle = NSAttributedString(string: "Pull to refresh")
           refresh.addTarget(self, action:#selector(refreshTable), for: UIControl.Event.valueChanged)
           // Add Custom Loader
           var loader = UIImageView()
           let gif = UIImage.gifImageWithName("gif")
           loader.center = self.view.center
           loader = UIImageView(frame: CGRect(x: self.view.frame.size.width/2 - 20, y: refresh.center.y, width: 40, height: 40))
           loader.image = gif
           refresh.insertSubview(loader, at: 0)
           ////fddsfdsfdsfsdfdsfdsf
           //gdfgfdgdfgdfgsgtdsgsg
           //fdsfdsfdsf
           self.notificationTableView.addSubview(refresh)

           let emptyLabel = UILabel.init(frame: self.notificationTableView.frame)
           var msg = ""
        
           msg = "No latest posts yet."
           
           emptyLabel.isHidden = true
           emptyLabel.text = msg
           emptyLabel.textAlignment = .center
           emptyLabel.numberOfLines = 0
           emptyLabel.textColor = UIColor.darkGray
           self.notificationTableView.addSubview(emptyLabel)
        getNotificationList()
        // Do any additional setup after loading the view.
    }
    @objc private func refreshTable() {
           pageCount = 1
           getNotificationList()
       }
     override func viewWillAppear(_ animated: Bool)
     {
        
    }
    @IBAction func backButtonPressed(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    func getNotificationList(){
          let notificationVM = Notification_VM()
           if(pageCount == 1)
           {
          
                      let userLogin = CommonFunctions.getuserFromDefaults()
                   notificationVM.userId = userLogin.userId
                   notificationVM.perPage = "10"
                   notificationVM.page = "\(pageCount)"
                      
                      
                      
                   notificationVM.updateLoadingStatus = { [weak self] () in
                          DispatchQueue.main.async {
                           if notificationVM.isLoading ?? false {
                                  LoaderView.shared.showLoader()
                              } else {
                                  LoaderView.shared.hideLoader()
                              }
                          }
                      }
                      
                   notificationVM.responseRecieved = {
                     self.refresh.endRefreshing()
                    self.notificationTableView.isHidden = false
                     self.notificationArray = notificationVM.notification_list
                      // self.notifications.append(contentsOf: self.notificationArray)
                     self.notifications = notificationVM.notification_list
                    self.total_page = notificationVM.total_pages
                       //self.notifications.append(notificationVM.notification_list)
                          // refresh content
                          self.notificationTableView.reloadData()
                      }
                      
                   notificationVM.showAlertClosure = {[weak self] () in
                    self!.refresh.endRefreshing()
                          DispatchQueue.main.async {
                           
                           if let message = notificationVM.alertMessage {
                            if(message == "no notification found.")
                            {
                                self?.notificationTableView.isHidden = true
                              }
                            else
                            {
                                self?.showAlert(self ?? UIViewController(), message: message)
                            }
                            }
                          }
                      }
        }
        else
           {
           let userLogin = CommonFunctions.getuserFromDefaults()
        notificationVM.userId = userLogin.userId
        notificationVM.perPage = "10"
        notificationVM.page = "\(pageCount)"
           
           
           
        notificationVM.updateLoadingStatus = { [weak self] () in
               DispatchQueue.main.async {
                if notificationVM.isLoading ?? false {
                       LoaderView.shared.showLoader()
                   } else {
                       LoaderView.shared.hideLoader()
                   }
               }
           }
           
        notificationVM.responseRecieved = {
            self.notificationArray = notificationVM.notification_list
            self.notifications.append(contentsOf: self.notificationArray)
        //  self.notifications = notificationVM.notification_list
            //self.notifications.append(notificationVM.notification_list)
               // refresh content
               self.notificationTableView.reloadData()
           }
           
        notificationVM.showAlertClosure = {[weak self] () in
               DispatchQueue.main.async {
                if let message = notificationVM.alertMessage {
                       self?.showAlert(self ?? UIViewController(), message: message)
                   }
               }
           }
        }
        notificationVM.callGetNotificationListService = true
       }
    func bottomRefresh(_ scrollView : UIScrollView)
    {
        let scrollViewHeight = scrollView.frame.size.height;
        let scrollContentSizeHeight = scrollView.contentSize.height;
        let scrollOffset = scrollView.contentOffset.y;
        if (scrollOffset >  scrollContentSizeHeight - scrollViewHeight)
        {
            if IsloadedPosts {
                pageCount = pageCount + 1
                
                if pageCount <= self.total_page{
                    self.getNotificationList()
                }
            }
        }
    }
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//         //  handleScrollStop()
//           if let _ = scrollView as? UITableView {
//               bottomRefresh(scrollView)
//           }
//       }
//
//       func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//           if !decelerate {
//            //   handleScrollStop()
//               if let _ = scrollView as? UITableView {
//                   bottomRefresh(scrollView)
//               }
//           }
//       }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

             //Bottom Refresh

             if scrollView == notificationTableView{

                 if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
                 {
                         if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0)
                         {
                             print("up")
                            self.pageCount = 1
                         }
                         else
                         {
                             print("down")
                            //if IsloadedPosts {
                                           pageCount = pageCount + 1
                                           
                                           if pageCount <= self.total_page{
                                               self.getNotificationList()
                                           }
                                    //   }
//                             pageCount =  pageCount + 1
//                           //  createLoadMorefotter()
//                             //                        spinner.startAnimating()
//                             getNotificationList()
                             }

                 }

             }

         }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NotificationsVC : UITableViewDelegate , UITableViewDataSource ,notificationListProtocol{
    func openPost(postId: String) {
        comment_VM = Comment_VM()
                                     let userLogin = CommonFunctions.getuserFromDefaults()
                                     comment_VM?.userId = userLogin.userId
                                     comment_VM?.postId = "\(postId)"
                                     
                
                                     comment_VM?.responseRecieved = {
                                      
                                         self.post = self.comment_VM!.postObj
                                         print("post==\(self.post!)")
                                         self.getPostDetail(postId: postId,post: self.comment_VM!.postObj)
        //                                self.post = self.postList[0]
                                        // self.postDetailTableView.reloadData()
                                         
                                     }
                                     
                                     comment_VM?.showAlertClosure = {[weak self] () in
                                         DispatchQueue.main.async {
                                             if let message = self?.comment_VM?.alertMessage {
                                                 self?.showAlert(self ?? UIViewController(), message: message)
                                             }
                                         }
                                     }
                                     comment_VM?.callFetchPostDetailService = true
                print("jnk")
                
    }
    

    func openforum(postId: String) {
         forum = Forum_VM()
                                            let userLogin = CommonFunctions.getuserFromDefaults()
                                            forum?.userId = userLogin.userId
                                            forum?.forumId = "\(postId)"
                                            
                       
                                            forum?.responseRecieved = {
//                                                self.getformDetail(postId: postId,post: self.forum?.self.forumLists?[0])
                                                let storyboard = UIStoryboard(name: "Forum", bundle: nil)
                                                let vc_ForumDetail = storyboard.instantiateViewController(withIdentifier: "ForumDetailVC") as! ForumDetailVC
                                                vc_ForumDetail.isFromProfileOrPet = true
                                                vc_ForumDetail.forumList = self.forum?.forumLists?[0]
//                                                vc_ForumDetail.for
                                                self.navigationController?.pushViewController(vc_ForumDetail, animated: true)
                                            }
                                            
                                            forum?.showAlertClosure = {[weak self] () in
                                                DispatchQueue.main.async {
                                                    if let message = self?.forum?.alertMessage {
                                                        self?.showAlert(self ?? UIViewController(), message: message)
                                                    }
                                                }
                                            }
                                            forum?.callshowforumList = true
                       print("jnk")
    }
    
    func getPostDetail(postId: String,post: PostList?)
     {
      //  print("list==\(post!)")
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let vc_PostDetail = storyboard.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
         vc_PostDetail.fromView = "Notification"
         vc_PostDetail.postId = postId
        vc_PostDetail.post = post
        // getPostDetail(postId: postId)
         vc_PostDetail.isFromProfileOrPet = true
         self.navigationController?.pushViewController(vc_PostDetail, animated: true)
     }
    
    func getformDetail(postId: String,post: ForumList?)
    {
     //  print("list==\(post!)")
              let storyboard = UIStoryboard(name: "Forum", bundle: nil)
              let vc_ForumDetail = storyboard.instantiateViewController(withIdentifier: "ForumDetailVC") as! ForumDetailVC
              vc_ForumDetail.forumList = forum?.forumLists?[0]
              self.navigationController?.pushViewController(vc_ForumDetail, animated: true)
    }
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : NotificationCell =  tableView.dequeueReusableCell(withIdentifier: notificationIdentifier, for: indexPath) as! NotificationCell
        cell.setNotification(notification: notifications[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func openProfile(id: String) {
         let storyboardProfile = UIStoryboard(name: "Profile", bundle: nil)
                      let vc_Profile = storyboardProfile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                      vc_Profile.isFromOtherScreen = true
               vc_Profile.userId = id
                      self.navigationController?.pushViewController(vc_Profile, animated: true)
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = cell as? NotificationCell
       // cell?.outerView.layer.masksToBounds = true
    }
}
