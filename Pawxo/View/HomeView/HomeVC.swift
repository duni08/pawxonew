//
//  HomeVC.swift
//  Pawxo
//
//  Created by 42works on 06/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import YPImagePicker
import AVFoundation
import ActiveLabel
import MMPlayerView
import Firebase
import Kingfisher

import AuthenticationServices


class HomeVC: UIViewController {
    
    var homeListVM = HomeList_VM()
    
    var offsetObservation: NSKeyValueObservation?
    
    lazy var mmPlayerLayer: MMPlayerLayer = {
        let l = MMPlayerLayer()
        l.cacheType = .memory(count: 5)
        l.coverFitType = .fitToPlayerView
        l.videoGravity = AVLayerVideoGravity.resizeAspectFill
        l.replace(cover: CoverA.instantiateFromNib())
        l.repeatWhenEnd = false
        l.autoHideCoverType = .disable
        l.fullScreenWhenLandscape = false
        return l
    }()
    
    
    var isForPullToRefresh = false
    
    
    @IBOutlet weak var homeTableView: UITableView!
    let simpleOver = SimpleOver()
    var frameTest : CGRect?
    var staticCount = 10
    var homeCellIdentifier = "HomeCell"
    
    var tempLabel : ActiveLabel!
    var text = ""
    var createEditVM : CreateEditPost_VM?
    
    var page = 1
    var refresh = UIRefreshControl()
    var total_page = 0
    var postList = [PostList]()
    
    var IsloadedPosts = false
    
    
    @IBOutlet weak var noInternetView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeTableView.register(UINib(nibName: homeCellIdentifier, bundle: nil), forCellReuseIdentifier: homeCellIdentifier)
        
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = 0
        tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        tempLabel.font = UIFont.JosefinSansRegular(size: 16.0)
        
        
        
        
        //self.navigationController?.delegate = self
        //        print("etst")
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        let vc_Home = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        // Do any additional setup after loading the view.
        
        
        offsetObservation = homeTableView.observe(\.contentOffset, options: [.new]) { [weak self] (_, value) in
            guard let self = self, self.presentedViewController == nil else {return}
            NSObject.cancelPreviousPerformRequests(withTarget: self)
            self.perform(#selector(self.startLoading), with: nil, afterDelay: 0.2)
        }
        homeTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right:0)
        
        homeTableView.estimatedRowHeight = 415
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
            self?.updateByContentOffset()
            self?.startLoading()
        }
        
        mmPlayerLayer.mmDelegate = self
        mmPlayerLayer.player?.isMuted = true
        
        mmPlayerLayer.getStatusBlock { [weak self] (status) in
            switch status {
            case .failed(let err):
                let alert = UIAlertController(title: "err", message: err.description, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self?.present(alert, animated: true, completion: nil)
            case .ready:
                print("Ready to Play")
            case .playing:
                print("Playing")
            case .pause:
                print("Pause")
            case .end:
                print("End")
            default: break
            }
        }
        mmPlayerLayer.getOrientationChange { (status) in
            print("Player OrientationChange \(status)")
        }
        
        getHomeList()
        
        
        refresh.tintColor = UIColor.clear
        refresh.backgroundColor = UIColor.clear
        refresh.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refresh.addTarget(self, action:#selector(refreshTable), for: UIControl.Event.valueChanged)
        // Add Custom Loader
        var loader = UIImageView()
        let gif = UIImage.gifImageWithName("gif")
        loader.center = self.view.center
        loader = UIImageView(frame: CGRect(x: self.view.frame.size.width/2 - 20, y: refresh.center.y, width: 40, height: 40))
        loader.image = gif
        refresh.insertSubview(loader, at: 0)
        
        
        
        self.homeTableView.addSubview(refresh)

        let emptyLabel = UILabel.init(frame: self.homeTableView.frame)
        var msg = ""
     
        msg = "No latest posts yet."
        
        
        emptyLabel.isHidden = true
        emptyLabel.text = msg
        emptyLabel.textAlignment = .center
        emptyLabel.numberOfLines = 0
        emptyLabel.textColor = UIColor.darkGray
        self.homeTableView.addSubview(emptyLabel)
        NotificationCenter.default.addObserver(self,
        selector: #selector(self.refreshTable),
        name: Notification.Name("RefreshHomeList"),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.openScreenForNotification(_:)),
        name: Notification.Name("PushNotificationRecieved"),
        object: nil)
        
        
        if let dict = notificationDictionary{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.performActionOnRecievingNotification(dict: dict)
            }
        }
        
        
        
        
//        if #available(iOS 13.0, *) {
//            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0)
//            {
//                if let appleID = UserDefaults.standard.value(forKey: "appleId") as? String {
//                    self.setupAppleIDCredentialObserver(appleId: appleID)
//                }
//            }
//        }
        
        
    }
    
    
    
    @available(iOS 13.0, *)
     func setupAppleIDCredentialObserver(appleId:String) {
      let authorizationAppleIDProvider = ASAuthorizationAppleIDProvider()

      authorizationAppleIDProvider.getCredentialState(forUserID: appleId) { (credentialState: ASAuthorizationAppleIDProvider.CredentialState, error: Error?) in
        if let error = error {
          print(error)
          // Something went wrong check error state
            kAppDelegate.logoutFromApp()
            
          return
        }
        switch (credentialState) {
        case .authorized:
          //User is authorized to continue using your app
          break
        case .revoked:
            kAppDelegate.logoutFromApp()
          //User has revoked access to your app
          break
        case .notFound:
            kAppDelegate.logoutFromApp()
          //User is not found, meaning that the user never signed in through Apple ID
          break
        default: break
        }
      }
    }
    
    
    
    
    
    func openforum(postId: String) {
        let forum = Forum_VM()
        let userLogin = CommonFunctions.getuserFromDefaults()
        forum.userId = userLogin.userId
        forum.forumId = "\(postId)"
        
        
        forum.updateLoadingStatus = {
            DispatchQueue.main.async {
                if forum.isLoading {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        forum.responseRecieved = {
            
            self.getformDetail(postId: postId, forum:forum.forumLists?[0])
            
        }
        
        forum.showAlertClosure = {
            DispatchQueue.main.async {
                if let message = forum.alertMessage {
                    self.showAlert(self , message: message)
                }
            }
        }
        forum.callshowforumList = true
    }
    
    
    func openPost(postId: String) {
        let comment_VM = Comment_VM()
        let userLogin = CommonFunctions.getuserFromDefaults()
        comment_VM.userId = userLogin.userId
        comment_VM.postId = "\(postId)"
        
        
        comment_VM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if comment_VM.isLoading {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        comment_VM.responseRecieved = {
            self.getPostDetail(postId: postId,post: comment_VM.postObj)
        }
        
        comment_VM.showAlertClosure = {
            DispatchQueue.main.async {
                if let message = comment_VM.alertMessage {
                    self.showAlert(self, message: message)
                }
            }
        }
        comment_VM.callFetchPostDetailService = true
    }
    
    
    func getPostDetail(postId: String,post: PostList?)
    {
     //  print("list==\(post!)")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_PostDetail = storyboard.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
        vc_PostDetail.fromView = "Notification"
        vc_PostDetail.postId = postId
        vc_PostDetail.post = post
       // getPostDetail(postId: postId)
        vc_PostDetail.isFromProfileOrPet = true
        if let mainTab = tabBar {
            mainTab.navigationController?.pushViewController(vc_PostDetail, animated: true)
        }
    }
    
    func getformDetail(postId: String,forum: ForumList?)
    {
        
    let storyboard = UIStoryboard(name: "Forum", bundle: nil)
     let vc_ForumDetail = storyboard.instantiateViewController(withIdentifier: "ForumDetailVC") as! ForumDetailVC
     vc_ForumDetail.isFromProfileOrPet = true
     vc_ForumDetail.forumList = forum
        if let mainTab = tabBar {
            mainTab.navigationController?.pushViewController(vc_ForumDetail, animated: true)
        }
    }
    
    
    func gotToChatDetails(dict:NSDictionary){
        
        let userLogin = CommonFunctions.getuserFromDefaults()
        
        let storyboardChat = UIStoryboard(name: "Chat", bundle: nil)
         let vc_ChatView = storyboardChat.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        vc_ChatView.imgUserStr = dict["user_image"] as? String ?? ""
        vc_ChatView.nameUserStr = dict["user_name"] as? String ?? ""
        vc_ChatView.descUserStr = (dict["last_message"] as? String)!
        vc_ChatView.idFrnd = dict["user_id"] as? String ?? ""
        vc_ChatView.timenew = (dict["timestamp"] as? Int)! ?? 0
        
        
        
        
        let ref : DatabaseReference!
        ref = Database.database().reference()
        ref.child("users").child(userLogin.userId ?? "").child((dict["chat_id"] as? String)!).child("allread").setValue(true)
        
        if let mainTab = tabBar {
            mainTab.navigationController?.pushViewController(vc_ChatView, animated: true)
        }
        
        
    }
    
    func performActionOnRecievingNotification(dict:NSDictionary){
        let dictNew = dict
        notificationDictionary = nil
        print(dictNew)
        //self.showAlert(self, message: "Notification Recieved")
        
        
        if let strMessage = dict["message"] as? String{
            if let dictNew = self.convertToDictionary(text: strMessage){
                if dictNew["type"] as! String == "chat"{
                    
                   if let mainTab = tabBar {
                        mainTab.setSelectedTABForNotification(index:2)
                        mainTab.navigationController?.popToRootViewController(animated: false)
                    }
                    
                    if let chatId = dictNew["id"] as? String{
                        // open post screen and getting details
                        
                        let userLogin = CommonFunctions.getuserFromDefaults()
                       
                        
                        let ref : DatabaseReference!
                        ref = Database.database().reference()
                        ref.child("users").child(userLogin.userId ?? "").child(chatId).observeSingleEvent(of: .value) { (snapshot) in
                            if let dict = snapshot.value as? NSDictionary {
                                print(dict)
                                
                                self.gotToChatDetails(dict: dict)
                                
                            }
                        }
                        
                    }
                    
                    
                    
                }else if dictNew["type"] as! String == "post"{
                    
                    if let mainTab = tabBar {
                        //mainTab.setSelectedTABForNotification(index:0)
                        mainTab.navigationController?.popToRootViewController(animated: false)
                    }
                    
                    if let postId = dictNew["id"] as? String{
                        // open post screen and getting details
                        self.openPost(postId: postId)
                    }
                    
                    
                }else if dictNew["type"] as! String == "forum"{
                    if let mainTab = tabBar {
                        //mainTab.setSelectedTABForNotification(index:0)
                        mainTab.navigationController?.popToRootViewController(animated: false)
                    }
                    
                    if let postId = dictNew["id"] as? String{
                        // open post screen and getting details
                        self.openforum(postId:postId)
                    }
                    
                }else if dictNew["type"] as! String == "user" || dictNew["type"] as! String == "follow_request"{
                    
                    if let mainTab = tabBar {
                        //mainTab.setSelectedTABForNotification(index:0)
                        mainTab.navigationController?.popToRootViewController(animated: false)
                        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
                        let vc_notifications = storyboardMain.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
                        mainTab.navigationController?.pushViewController(vc_notifications, animated: true)
                    }
                    
                    
                    
                }
            }
            
        }
        
        
    }
    
    func convertToDictionary(text: String) -> NSDictionary? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
            } catch {
                print(error.localizedDescription)
                return nil
            }
        }
        return nil
    }

    
    
    @objc func openScreenForNotification(_ notification: NSNotification) {
        let dict = notification.userInfo! as NSDictionary
        self.performActionOnRecievingNotification(dict: dict)
    }
    
    // Handle Push notification
    
    
    
    
    @objc private func refreshTable() {
        page = 1
        self.isForPullToRefresh = true
        getHomeList()
    }
    
    
    
    @IBAction func tryAgainPressed(_ sender: Any) {
        self.refreshTable()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.delegate = nil
        self.homeTableView.reloadData()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.mmPlayerLayer.player?.pause()
        mmPlayerLayer.player?.isMuted = true
    }
    
    deinit {
        offsetObservation?.invalidate()
        offsetObservation = nil
        print("ViewController deinit")
    }
    
    
    func getHomeList(){
        
        let cache = ImageCache.default
        cache.clearMemoryCache()
        cache.clearDiskCache { print("Done") }

        if !(WebServices().isInternetWorking()){
            self.noInternetView.isHidden = false
            self.refresh.endRefreshing()
            self.postList.removeAll()
            self.homeTableView.addSubview(self.noInternetView)
            self.homeTableView.reloadData()
            self.isForPullToRefresh = false
            return
        }else{
            self.noInternetView.isHidden = true
        }
        
        
        let userLogin = CommonFunctions.getuserFromDefaults()
        homeListVM.userId = userLogin.userId
        homeListVM.perPage = "50"
        homeListVM.page = "\(page)"
        
        
        
        homeListVM.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.homeListVM.isLoading ?? false {
                    if self?.isForPullToRefresh == false{
                    LoaderView.shared.showLoader()
                    }
                } else {
                    if self?.isForPullToRefresh == false{
                    LoaderView.shared.hideLoader()
                    }else{
                        self?.isForPullToRefresh = false
                    }
                }
            }
        }
        
        homeListVM.responseRecieved = {
            self.refresh.endRefreshing()
            self.IsloadedPosts = true
            if self.page != 1 {
                self.postList.append(contentsOf: self.homeListVM.postList)
            }else{
                self.postList = self.homeListVM.postList
            }
            self.total_page = self.homeListVM.total_pages
            // refresh content
            self.homeTableView.reloadData()
        }
        
        homeListVM.showAlertClosure = {
            self.refresh.endRefreshing()
            DispatchQueue.main.async {
                if let message = self.homeListVM.alertMessage {
                    self.showAlert(self , message: message)
                }
            }
        }
        
        IsloadedPosts = false
        homeListVM.callGetHomeService = true
    }
    
   
    func bottomRefresh(_ scrollView : UIScrollView)
    {
        let scrollViewHeight = scrollView.frame.size.height;
        let scrollContentSizeHeight = scrollView.contentSize.height;
        let scrollOffset = scrollView.contentOffset.y;
        if (scrollOffset >  scrollContentSizeHeight - scrollViewHeight)
        {
            if IsloadedPosts {
                page = page + 1
                
                if page <= self.total_page{
                    self.getHomeList()
                }
            }
        }
    }
    
    
    @IBAction func searchButtonPressed(_ sender : Any){
        self.mmPlayerLayer.player?.pause()
        mmPlayerLayer.player?.isMuted = true
        let storyboardSearch = UIStoryboard(name: "Search", bundle: nil)
        let vc_notifications = storyboardSearch.instantiateViewController(withIdentifier: "SearchMainVC") as! SearchMainVC
        self.navigationController?.pushViewController(vc_notifications, animated: true)
    }
    
    @IBAction func notificationPressed(_ sender:Any){
        self.mmPlayerLayer.player?.pause()
        mmPlayerLayer.player?.isMuted = true
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let vc_notifications = storyboardMain.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
        self.navigationController?.pushViewController(vc_notifications, animated: true)
    }
    
    @IBAction func addPostPressed(_ sender: Any) {
        
        self.mmPlayerLayer.player?.pause()
        mmPlayerLayer.player?.isMuted = true
      
        var config = YPImagePickerConfiguration()
        config.library.onlySquare = false
        config.bottomMenuItemSelectedTextColour = AppColor.baseColor!
        config.bottomMenuItemUnSelectedTextColour = AppColor.colorGreySubHeading
        config.colors.tintColor = AppColor.baseColor!
        config.isScrollToChangeModesEnabled = true
        config.onlySquareImagesFromCamera = true
        config.targetImageSize = .original
        config.library.mediaType = .photoAndVideo
        config.usesFrontCamera = false
        config.showsPhotoFilters = true
        config.shouldSaveNewPicturesToAlbum = true
        config.video.compression = AVAssetExportPresetMediumQuality
        config.albumName = "Pawxo"
        config.screens = [.library, .photo, .video]
        config.startOnScreen = .library
        config.video.recordingTimeLimit = 60
        config.video.libraryTimeLimit = 60
        config.video.fileType = .mp4
        config.library.maxNumberOfItems = 1
        // Build a picker with your configuration
        let picker = YPImagePicker(configuration: config)
        //  let picker = YPImagePicker()
        picker.modalPresentationStyle = .overFullScreen
        picker.didFinishPicking { [unowned picker] items, cancelled in
            
            if cancelled == true {
                picker.dismiss(animated: true, completion: nil)
            }
            
            if let photo = items.singlePhoto {
                print(photo.fromCamera) // Image source (camera or library)
                print(photo.image) // Final image selected by the user
                print(photo.originalImage) // original image selected by the user, unfiltered
                print(photo.modifiedImage) // Transformed image, can be nil
                print(photo.exifMeta) // Print exif meta data of original image.
                
                // image picked
                let data = photo.image.jpegData(compressionQuality: 0.5)
                self.createPost(img: photo.image, isImage: true, data: data, url: nil, picker: picker)
                //picker.dismiss(animated: true, completion: nil)
            }
            
            if let video = items.singleVideo {
                print(video.fromCamera)
                print(video.thumbnail)
                print(video.url)
                
                do {
                    let videoData = try Data(contentsOf: video.url)
                    self.createPost(img: video.thumbnail, isImage: false, data: videoData, url: video.url, picker: picker)
                } catch {
                    print("Unable to load data: \(error)")
                }
            }
            
        }
        
        //self.navigationController?.pushViewController(picker.viewControllers.first!, animated: true)
        self.present(picker, animated: true, completion: nil)
        
    }
    
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    
    
    
    
    private func createPost(img: UIImage, isImage: Bool, data: Data?,url:URL?, picker: YPImagePicker?) {
        // self.mmPlayerLayer.player?.pause()
        delay(0.1) { () -> () in
            let postStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = postStoryboard.instantiateViewController(withIdentifier: "CreatePostVC") as! CreatePostVC
            
            vc.isImage = isImage
            vc.mediaData = data
            vc.thumbnailImage = img
            vc.mediaUrl = url
            vc.delegate = self
            //picker?.navigationBar.isHidden = true
            picker?.pushViewController(vc, animated: true)
        }
        // self.presentedViewController?.present(vc, animated: false, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func goToProfileView(userId:Int){
        self.mmPlayerLayer.player?.pause()
        mmPlayerLayer.player?.isMuted = true
        let storyboardProfile = UIStoryboard(name: "Profile", bundle: nil)
        let vc_Profile = storyboardProfile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc_Profile.isFromOtherScreen = true
        vc_Profile.userId = "\(userId)"
        self.navigationController?.pushViewController(vc_Profile, animated: true)
    }
    
    
    
}


extension HomeVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : HomeCell =  tableView.dequeueReusableCell(withIdentifier: homeCellIdentifier, for: indexPath) as! HomeCell
        cell.homeCellDelegate = self
        cell.indexNumber = indexPath
        
        let post = postList[indexPath.row]
        cell.setPost(post: post)
        cell.userNotFound = {
            self.showAlert(self, message: "User not found.")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell : HomeCell =  tableView.dequeueReusableCell(withIdentifier: homeCellIdentifier, for: indexPath) as! HomeCell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        return UITableView.automaticDimension
        
        let post = postList[indexPath.row]
        
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = 0
        tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        tempLabel.font = UIFont.JosefinSansSemiBold(size: 15.0)
        tempLabel.text = ""
        tempLabel.text = "\(post.postListDescription ?? "")"
        tempLabel.sizeToFit()
        
        
        let height = tempLabel.frame.height
        
        tempLabel.text = ""
        tempLabel.text = "A \n B"
        tempLabel.sizeToFit()
        
        let twoLineTextHeight = tempLabel.frame.height
        
        var calculatedHeight : CGFloat = 0
        if height > twoLineTextHeight {
            calculatedHeight = twoLineTextHeight + 410
        }else{
            calculatedHeight = height + 410
        }
        
        post.height = calculatedHeight
        
        return post.height
        
//        tempLabel.text = ""
//        tempLabel.text = post.postListDescription!
//        tempLabel.sizeToFit()
//
//        let calculatedHeight = tempLabel.frame.height
//
//        print("Height for row \(indexPath.row) = \(calculatedHeight) \(tempLabel.text)")
//
//        if (indexPath.row == 0){
//            tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
//            tempLabel.text = ""
//            tempLabel.text = post.postListDescription!
//            tempLabel.sizeToFit()
//             let calculatedHeight111 = tempLabel.frame.height
//            print(calculatedHeight111)
//        }
//
//
//        return calculatedHeight + 383
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.mmPlayerLayer.player?.pause()
        mmPlayerLayer.player?.isMuted = true
        
        let cell: HomeCell = self.homeTableView.cellForRow(at: indexPath) as! HomeCell
        self.frameTest = self.homeTableView.rectForRow(at: indexPath)
        
        let p = cell.centerImageView.superview?.convert(cell.centerImageView.center, to: self.view)
        
        let buttonAbsoluteFrame = cell.centerImageView.convert(cell.centerImageView.bounds, to: self.view)
        //CGPoint p = [cell.imgView.superview convertPoint:cell.im.center toView:self.view]
        self.frameTest = buttonAbsoluteFrame
        //        self.performSegue(withIdentifier: "test", sender: self)
        
        
        print(self.frameTest!)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_PostDetail = storyboard.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
        vc_PostDetail.imageCenter = cell.centerImageView.image
        vc_PostDetail.imageFrame = self.frameTest
        vc_PostDetail.delegate = self
        let post  = self.postList[indexPath.row]
        vc_PostDetail.post = post
        vc_PostDetail.deletedPost = {
            self.postList.remove(at: indexPath.row)
            self.homeTableView.reloadData()
        }
        vc_PostDetail.setPost = { post in
            self.postList[indexPath.row] = post
            cell.setPost(post: post)
        }
        
        self.navigationController?.delegate = self
        self.navigationController?.pushViewController(vc_PostDetail, animated: true)
    }
    
}


extension HomeVC: PostDetailVCProtocol {
     func setNavigationDelegate() {
        self.navigationController?.delegate = self
    }
}


extension HomeVC : HomeCellDelegate {
    
    func hashTagPressed(hashTag:String){
        self.mmPlayerLayer.player?.pause()
        mmPlayerLayer.player?.isMuted = true
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_PostHashTag = storyboard.instantiateViewController(withIdentifier: "HashTagPostsVC") as! HashTagPostsVC
        vc_PostHashTag.hashTag = hashTag
        self.navigationController?.pushViewController(vc_PostHashTag, animated: true)
    }
    
    func userMentionTap(user:Userlist){
        self.goToProfileView(userId: user.id!)
    }
    
    func doubleTAPLikePressed(cell: HomeCell) {
        // add like aPI To Do
        let post = cell.post!
        
        if post.isLike! == 0{
            // like api
            let userLogin = CommonFunctions.getuserFromDefaults()
            createEditVM = CreateEditPost_VM()
            
            
            createEditVM?.userId = userLogin.userId
            createEditVM?.postId = "\(post.postID!)"
            
            createEditVM?.responseRecieved = {
                // refresh cell button
                post.isLike = 1
                post.likeCount! += 1
                cell.setPost(post: post)
            }
            
            createEditVM?.updateLoadingStatus = { [weak self] () in
                DispatchQueue.main.async {
                    if self?.createEditVM?.isLoading ?? false {
                        //LoaderView.shared.showLoader()
                    } else {
                        //LoaderView.shared.hideLoader()
                    }
                }
            }
            
            createEditVM?.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = self?.createEditVM?.alertMessage {
                        self?.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            
            createEditVM?.callLikePostService = true
        }else{
            //unlike api
            let userLogin = CommonFunctions.getuserFromDefaults()
            createEditVM = CreateEditPost_VM()
            
            
            createEditVM?.userId = userLogin.userId
            createEditVM?.postId = "\(post.postID!)"
            
            createEditVM?.responseRecieved = {
                // refresh button like
                post.isLike = 0
                post.likeCount! -= 1
                cell.setPost(post: post)
            }
            
            createEditVM?.updateLoadingStatus = { [weak self] () in
                DispatchQueue.main.async {
                    if self?.createEditVM?.isLoading ?? false {
                       // LoaderView.shared.showLoader()
                    } else {
                       // LoaderView.shared.hideLoader()
                    }
                }
            }
            
            createEditVM?.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = self?.createEditVM?.alertMessage {
                        self?.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            
            createEditVM?.callUnLikePostService = true
        }
    }
    
   
    
    func gotoProfilePressed(cell:HomeCell) {
        self.goToProfileView(userId: cell.post!.userID!)
    }
    
    func commentButtonPressed(index: IndexPath) {
        //go to details screen
        
        self.mmPlayerLayer.player?.pause()
        mmPlayerLayer.player?.isMuted = true
        
        
        let cell: HomeCell = self.homeTableView.cellForRow(at: index) as! HomeCell
        self.frameTest = self.homeTableView.rectForRow(at: index)
        
        let p = cell.centerImageView.superview?.convert(cell.centerImageView.center, to: self.view)
        
        let buttonAbsoluteFrame = cell.centerImageView.convert(cell.centerImageView.bounds, to: self.view)
        //CGPoint p = [cell.imgView.superview convertPoint:cell.im.center toView:self.view]
        self.frameTest = buttonAbsoluteFrame
        //        self.performSegue(withIdentifier: "test", sender: self)
        
        
        print(self.frameTest!)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_PostDetail = storyboard.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
        vc_PostDetail.imageCenter = cell.centerImageView.image
        vc_PostDetail.imageFrame = self.frameTest
        self.navigationController?.delegate = self
        vc_PostDetail.delegate = self
        let post  = self.postList[index.row]
        vc_PostDetail.deletedPost = {
            self.postList.remove(at: index.row)
            self.homeTableView.reloadData()
        }
        vc_PostDetail.setPost = { post in
            cell.setPost(post: post)
        }
        vc_PostDetail.post = post
        self.navigationController?.pushViewController(vc_PostDetail, animated: true)
    }
    
    func likeButtonPressed(post:PostList) {
        self.mmPlayerLayer.player?.pause()
        mmPlayerLayer.player?.isMuted = true
        
        // Go to Like views
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_LikesList = storyboard.instantiateViewController(withIdentifier: "PostLikesListVC") as! PostLikesListVC
        vc_LikesList.post = post
        self.navigationController?.pushViewController(vc_LikesList, animated: true)
    }
    
    func shareButtonPressed(cell:HomeCell) {
        // open share options
            
            
            var description = ""
             description = cell.descriptionLabel.text ?? ""
            let mediaLink =  cell.post?.media?.first ?? ""
        
            let text = "Checkout this post!! \n \(description)\n \(mediaLink)\n\n Let’s make this world a happier and better place for innocent beings, who do the same for us! \n Download https://play.google.com/ http://itunes.apple.com/"
            //let shareimage = UIImage(named: "Pet")
          
            let imageToShare = [text] as [Any]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.setValue("Cuddle | Check out", forKey: "subject")
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            //        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
            
    }
    
    func optionsButtonPressed() {
        // open options view
    }
    
    func editButtonPressed(cell: HomeCell) {
        
        self.mmPlayerLayer.player?.pause()
        mmPlayerLayer.player?.isMuted = true
        
        let postStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = postStoryboard.instantiateViewController(withIdentifier: "CreatePostVC") as! CreatePostVC
        
        //vc.isImage = isImage
        //vc.data = data
        //vc.image = img
        vc.isEdit = true
        vc.post = cell.post
        vc.thumbnailImage = cell.centerImageView.image
        vc.editPostFinished = {
            self.refreshTable()
        }
        
        self.navigationController?.delegate = nil
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func reportButtonPressed(cell: HomeCell) {
        
        
        let userLogin = CommonFunctions.getuserFromDefaults()
        
        var homeListVM = self.homeListVM
        let pid = cell.post?.postID!
        
        let key = "\(pid!)&\((userLogin.userId)!)"
        if let val = UserDefaults.standard.value(forKey: key){
            
            self.showAlert(self, message: AppMessages.reportAbuseSecond)
            
            return
        }
        
        
        let alertController = UIAlertController(title: AppMessages.reportAbuse, message: nil, preferredStyle: UIAlertController.Style.alert)

        

        let somethingAction = UIAlertAction(title: "Report", style: .default, handler: {(alert: UIAlertAction!) in print("Report API call")
            
            
            homeListVM = HomeList_VM()
            homeListVM.userId = userLogin.userId
            let pid = cell.post?.postID!
            homeListVM.postId = "\(pid!)"
            
            
            homeListVM.updateLoadingStatus = { [weak self] () in
                DispatchQueue.main.async {
                    if self?.homeListVM.isLoading ?? false {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            homeListVM.responseRecieved = {
                // refresh button like
                self.showAlert(self, message: AppMessages.reportAbuseFirst)
                UserDefaults.standard.setValue(true, forKey: key)
            }
            
            homeListVM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = homeListVM.alertMessage {
                        self?.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            
            
            homeListVM.reportPostService = true
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})

        alertController.addAction(somethingAction)
        alertController.addAction(cancelAction)
        alertController.view.tintColor = UIColor.black
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
    }
    
    
    
    func deleteButtonPressed(cell : HomeCell) {
        
        let alertController = UIAlertController(title: AppMessages.deletePost, message: nil, preferredStyle: UIAlertController.Style.alert)
        
        
        
        let somethingAction = UIAlertAction(title: "OK", style: .default, handler: {(alert: UIAlertAction!) in print("Report API call")
            
            let indexPath = self.homeTableView.indexPath(for: cell)
            let userLogin = CommonFunctions.getuserFromDefaults()
            
            self.homeListVM = HomeList_VM()
            self.createEditVM?.userId = userLogin.userId
            let pid = cell.post?.postID!
            self.homeListVM.postId = "\(pid!)"
            
            
            self.homeListVM.userId = userLogin.userId
            
            self.homeListVM.updateLoadingStatus = { [weak self] () in
                DispatchQueue.main.async {
                    if self?.homeListVM.isLoading ?? false {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            self.homeListVM.responseRecieved = {
                // refresh button like
                self.postList.remove(at: indexPath!.row)
                self.homeTableView.deleteRows(at: [indexPath!], with: .automatic)
            }
            
            self.createEditVM?.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = self?.homeListVM.alertMessage {
                        self?.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            
            
            self.homeListVM.deletePostService = true
            
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
        
        alertController.addAction(somethingAction)
        alertController.addAction(cancelAction)
        alertController.view.tintColor = UIColor.black
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
        // let indexpath = index //IndexPath(row: index, section: 0)
        
    }
    
    
    func petPressed(pet:PetList,isMYPet:Bool) {
        
        self.mmPlayerLayer.player?.pause()
        mmPlayerLayer.player?.isMuted = true
        
        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
        let vc_feed = storyboardPet.instantiateViewController(withIdentifier: "FeedVC") as! FeedVC
        vc_feed.isFromOtherScreen = true
        vc_feed.petObject = pet
        vc_feed.isMYPet = isMYPet
        self.navigationController?.delegate = nil
        self.navigationController?.pushViewController(vc_feed, animated: true)
    }
    
    
    
    func petListPressed(petlist :[PetList],isMYPet:Bool){
        
        self.mmPlayerLayer.player?.pause()
        mmPlayerLayer.player?.isMuted = true
        
        let storyboardPet = UIStoryboard(name: "Main", bundle: nil)
        let vc_PetList = storyboardPet.instantiateViewController(withIdentifier: "PetListVC") as! PetListVC
        vc_PetList.petlist = petlist
        vc_PetList.isMYPet = isMYPet
        self.navigationController?.delegate = nil
        self.navigationController?.pushViewController(vc_PetList, animated: true)
    }
    
    
}



class SimpleOver: NSObject, UIViewControllerAnimatedTransitioning {

    var popStyle: Bool = false
    
    var originFrame: CGRect?
    
//    init(originFrame: CGRect) {
//      self.originFrame = originFrame
//    }
    
    func transitionDuration(
        using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        if popStyle {

            animatePop(using: transitionContext)
            return
        }

        let fz = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let tz = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        let tzN = tz as! PostDetailVC
        
        let f = transitionContext.finalFrame(for: tz)

        let fOff = f.offsetBy(dx: f.width, dy: f.height)
       // tz.view.frame = fOff
//        tzN.heightConstraint.isActive = false
//        tzN.widthConstraint.isActive = false
//        tzN.topConstraint.isActive = false
//        tzN.leftConstraint.isActive = false
//        tzN.rightConstraint.isActive = false
//        tzN.topConstraint.isActive = false
//        tzN.heightConstraint.isActive = false
        
       // tzN.imgView.frame = originFrame!
        
        tzN.view.bringSubviewToFront(tzN.boxView)
        let imageView = tzN.centerImageView
        imageView?.frame = self.originFrame!
        //tzN.topView.alpha = 0
        tzN.postDetailTableView.alpha = 0
        tzN.backButton.alpha = 0
        let rect = tzN.cuddlLogo.frame
        //tzN.boxView.alpha = 0
        imageView?.layer.cornerRadius = 12
        transitionContext.containerView.insertSubview(tz.view, aboveSubview: fz.view)
        tzN.aView.isHidden = true
        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            animations: {
                imageView?.frame = CGRect(x: 0, y: (tzN.view.safeAreaInsets.top + tzN.topView.frame.size.height), width: tzN.view.frame.width , height: 254)
                tzN.cuddlLogo.frame = CGRect(x: (tzN.view.frame.width/2) - 56, y: rect.origin.y, width: rect.width, height:rect.height)
                tzN.topView.alpha = 1.0
                tzN.boxView.alpha = 1.0
                //tzN.cuddlLeadingConstraint.constant = (tzN.view.frame.width/2) - 56
                imageView?.layer.cornerRadius = 0
                tzN.postDetailTableView.alpha = 1.0
                tzN.backButton.alpha = 1.0
              //  tzN.imgView.layer.cornerRadius = 0
        }, completion: {_ in
//            tzN.leftConstraint.isActive = true
//            tzN.rightConstraint.isActive = true
//            tzN.topConstraint.isActive = true
//            tzN.heightConstraint.isActive = true
          //  tzN.setUpConstraintForImageView()
            transitionContext.completeTransition(true)
        })
    }
    
    
    func animatePop(using transitionContext: UIViewControllerContextTransitioning) {

        let fz = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let tz = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!

        let f = transitionContext.initialFrame(for: fz)
        let fOffPop = f.offsetBy(dx: f.width, dy: 55)
        
        
        let tzN = fz as! PostDetailVC
        let imageView = tzN.centerImageView
        imageView?.isHidden = false
        imageView?.removeAllConstraints()
        tzN.aView.isHidden = true
        let rect = tzN.cuddlLogo.frame
        transitionContext.containerView.insertSubview(tz.view, belowSubview: fz.view)
        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            animations: {
                
                imageView?.frame = self.originFrame!
                tzN.cuddlLogo.frame = CGRect(x: 10, y: rect.origin.y, width: rect.width, height:rect.height)
                tzN.backButton.alpha = 0
                tzN.postDetailTableView.alpha = 0
                //tzN.boxView.alpha = 0
                imageView?.layer.cornerRadius = 12
        }, completion: {_ in
                transitionContext.completeTransition(true)
        })
    }
}


extension UIView {

    public func removeAllConstraints() {
        var _superview = self.superview

        while let superview = _superview {
            for constraint in superview.constraints {

                if let first = constraint.firstItem as? UIView, first == self {
                    superview.removeConstraint(constraint)
                }

                if let second = constraint.secondItem as? UIView, second == self {
                    superview.removeConstraint(constraint)
                }
            }

            _superview = superview.superview
        }

        self.removeConstraints(self.constraints)
        self.translatesAutoresizingMaskIntoConstraints = true
    }
}


extension HomeVC: UIViewControllerTransitioningDelegate,UINavigationControllerDelegate {

//func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//    return FlipPresentAnimationController(originFrame: frameTest!)
//}
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//            return FlipPresentAnimationController(originFrame: frameTest!)
        simpleOver.popStyle = (operation == .pop)
        if !simpleOver.popStyle {
            simpleOver.originFrame = self.frameTest
        }else{
            
        }
        return simpleOver
    }
    
}


extension HomeVC : CreatePostVCProtocol {
    func createPostFinished(){
        self.postList.removeAll()
        self.homeTableView.reloadData()
        
        self.dismiss(animated: true, completion: nil)
        self.refreshTable()
        homeTableView.setContentOffset(.zero, animated:true)
    }
}



extension HomeVC {
    
   
}

//MARK:- MMVideoPlayer
extension HomeVC{
    
    @objc fileprivate func startLoading() {
        self.updateByContentOffset()
        if self.presentedViewController != nil {
            return
        }
        // start loading video
        mmPlayerLayer.resume()
    }
    
    fileprivate func updateCell(at indexPath: IndexPath) {
        
        
        let post = self.postList[indexPath.row]
        
        if post.type == "video"{
        
            if let cell = homeTableView.cellForRow(at: indexPath) as? HomeCell, let playURL = URL(string: post.media!.first!) { //cell.data?.play_Url
            // this thumb use when transition start and your video dosent start
            mmPlayerLayer.thumbImageView.image = cell.centerImageView.image
            // set video where to play
            mmPlayerLayer.playView = cell.centerImageView
            mmPlayerLayer.set(url: playURL)
        }
        }else{
            mmPlayerLayer.thumbImageView.image = nil
            mmPlayerLayer.playView = nil
        }
    }
    
    
    fileprivate func updateByContentOffset() {
        if mmPlayerLayer.isShrink {
            return
        }
        
        if let path = findCurrentPath(),
            self.presentedViewController == nil {
            self.updateCell(at: path)
            //Demo SubTitle
                /*
            if path.row == 0, self.mmPlayerLayer.subtitleSetting.subtitleType == nil {
                let subtitleStr = Bundle.main.path(forResource: "srtDemo", ofType: "srt")!
                if let str = try? String.init(contentsOfFile: subtitleStr) {
                    self.mmPlayerLayer.subtitleSetting.subtitleType = .srt(info: str)
                    self.mmPlayerLayer.subtitleSetting.defaultTextColor = .red
                    self.mmPlayerLayer.subtitleSetting.defaultFont = UIFont.boldSystemFont(ofSize: 20)
                }
            }*/
        }
    }

//    fileprivate func updateDetail(at indexPath: IndexPath) {
//        let value = DemoSource.shared.demoData[indexPath.row]
//        if let detail = self.presentedViewController as? DetailViewController {
//            detail.data = value
//        }
//
//        self.mmPlayerLayer.thumbImageView.image = value.image
//        self.mmPlayerLayer.set(url: DemoSource.shared.demoData[indexPath.row].play_Url)
//        self.mmPlayerLayer.resume()
//
//    }
    
    
    private func findCurrentPath() -> IndexPath? {
        let p = CGPoint(x: homeTableView.frame.width/2, y: homeTableView.contentOffset.y + homeTableView.frame.height/2)
        return homeTableView.indexPathForRow(at: p)
    }
    
    private func findCurrentCell(path: IndexPath) -> UITableViewCell {
        return homeTableView.cellForRow(at: path)!
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        mmPlayerLayer.player?.pause()
        mmPlayerLayer.player?.isMuted = true
        if let cells = self.homeTableView.visibleCells as? [HomeCell]{
            for cell in cells{
                if cell.editView.isHidden == false{
                    cell.editView.isHidden = true
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        handleScrollStop()
        if let _ = scrollView as? UITableView {
            bottomRefresh(scrollView)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            handleScrollStop()
            if let _ = scrollView as? UITableView {
                bottomRefresh(scrollView)
            }
        }
    }
    
    
    
    func handleScrollStop() {
         let point = CGPoint(x: homeTableView.frame.width/2, y: homeTableView.contentOffset.y + homeTableView.frame.height/2)
         if let path = homeTableView.indexPathForRow(at: point){
             if let cell = homeTableView.cellForRow(at: path) as? HomeCell {
                if cell.post?.type == "video"{
                     if mmPlayerLayer.playUrl != nil{
                         mmPlayerLayer.player?.play()
                     }
                     else{
                         updateCell(at: path)
                     }
                 }
             }
         }
    
     }
    
}

extension HomeVC : MMPlayerLayerProtocol{
    func touchInVideoRect(contain: Bool){
//
//        var currentCell : HomeCell?
//        let point = CGPoint(x: listTableView[selectedTap].frame.width/2, y: listTableView[selectedTap].contentOffset.y + listTableView[selectedTap].frame.width/2)
//        if let path = listTableView[selectedTap].indexPathForRow(at: point){
//            if let cell = listTableView[selectedTap].cellForRow(at: path) as? HomeCell {
//                currentCell = cell
//            }
//        }
        if mmPlayerLayer.player?.isMuted == true{
            mmPlayerLayer.player?.isMuted = false
        }
        else{
            mmPlayerLayer.player?.isMuted = true
        }
    }

}




