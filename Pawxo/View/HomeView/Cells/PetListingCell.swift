//
//  PetListCell.swift
//  Pawxo
//
//  Created by 42works on 05/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class PetListingCell: UITableViewCell {
    
    
    
    @IBOutlet weak var petImageView: UIImageView!
    
    @IBOutlet weak var petNameLabel: RegularLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setPet(pet:PetList){
        self.petImageView.setImage(fromUrl: pet.petImages ?? "", defaultImage: DefaultImage.defaultPetImage)
        self.petNameLabel.text = pet.petName ?? ""
    }

}
