//
//  UserSearchCell.swift
//  Pawxo
//
//  Created by 42works on 29/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class UserSearchCell: UITableViewCell {
    
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    
    
    @IBOutlet weak var userNameLabel: RegularLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUI(user:Userlist){
        self.userNameLabel.text = user.username!
        self.profileImageView.setImage(fromUrl: user.userImage ?? "", defaultImage: DefaultImage.defaultProfileImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
    }
}
