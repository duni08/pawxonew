//
//  CreatePostVC.swift
//  Pawxo
//
//  Created by 42works on 10/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import Foundation
import MobileCoreServices
import AVKit
import AVFoundation
import FBSDKCoreKit
import FBSDKLoginKit
import TwitterKit
import TwitterCore
import SafariServices

protocol CreatePostVCProtocol {
  func createPostFinished()
}


class CreatePostVC: UIViewController,UIGestureRecognizerDelegate,TWTRComposerViewControllerDelegate {

    @IBOutlet weak var captionTV: UITextView!
    @IBOutlet weak var postMedia: UIImageView!
    
    @IBOutlet weak var placeHolderLabel: LightLabel!
    
    
    @IBOutlet weak var titleLabel: SemiBoldLabel!
    var isImage = true
    
    var tapGesture : UITapGestureRecognizer?
    
    @IBOutlet weak var twitterSwitch: UISwitch!
    
    @IBOutlet weak var locationImage: UIImageView!
    var showTick = false
    
    
    @IBOutlet weak var petOuterView: UIView!
    
    
    @IBOutlet weak var shareButton: RegularButton!
    var taggedUserList = [Userlist]()
    
    var globalRange = NSRange()
    var mutString = NSMutableString()
    var isupdateTable = false
    var globalInt : Int = 1
    var addName = NSMutableArray()
    var idToSendApi = NSMutableArray()
    var currentWord : String = ""
    var updatedWord : String = ""
    var rangeToAppend = NSRange()
    var removeCount : Int = 0
    
    var userList = [Userlist]()
    
    var hashTagList = [HashTag]()
    var isUserTable = false
    @IBOutlet weak var dividerConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var videoButton: UIButton!
    var delegate : CreatePostVCProtocol?
    var mediaData : Data?
    var thumbnailData :Data?
    var thumbnailImage : UIImage?
    var mediaUrl : URL?
    @IBOutlet weak var petCollectionView: UICollectionView!
    let cellIdentifier = "PetAddPostCell"
    var staticCount = 10
    
    var petList = [PetList]()
    
    var SelectedPetList = [PetList]()
    
    var facebookToken : String?
    var currentLat : Double?
    var currentLong : Double?
    var currentAddress : String = ""
    var twitterToken : String?
    var twitterSecretAccessToken : String?
    
    @IBOutlet weak var locationLabel: RegularLabel!
    
    
    @IBOutlet weak var userTableView: UITableView!
    @IBOutlet weak var dividerViewHeightConstraint: NSLayoutConstraint!
    var isEdit = false
    
    @IBOutlet weak var userTableHeight: NSLayoutConstraint!
    @IBOutlet weak var petHeightConstraint: NSLayoutConstraint!
    
    var createEditVM : CreateEditPost_VM?
    
    var tagCellIdentifier = "TagCell"
    
    var post: PostList?
    
    var editPostFinished : (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPetViewHeight()
        
        let degreesToRadians: (CGFloat) -> CGFloat = { (degrees: CGFloat) in
            return degrees / 180.0 * CGFloat.pi
        }
        self.locationImage.transform = CGAffineTransform(rotationAngle:degreesToRadians(-90))
        
        if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
            if let session = TWTRTwitter.sharedInstance().sessionStore.session() {
                let client = TWTRAPIClient()
                client.loadUser(withID: session.userID) { (user, error) -> Void in
                    if user != nil
                    {
                        if let user = user {
                            // self.twitterUserName.text = user.screenName
                        }
                    }
                    else{
                        print("error: \(error?.localizedDescription)");
                    }
                }
                if let authToken = session.authToken as? String
                {
                    showTick = true
                    self.twitterToken = authToken
                    // self.obj.twitterAccessToken = authToken
                    // self.obj.isShare = "1"
                }
                if let secretAuthToken = session.authTokenSecret as? String
                {
                    showTick = true
                    self.twitterSecretAccessToken = secretAuthToken
                }
                
                
            }
            
            
            
            
            
            
            
        }
        
        self.showTickOnScreen(show: showTick)
        
        
        self.navigationController?.navigationBar.isHidden = true
        self.userTableView.isHidden = true
        self.videoButton.isHidden = isImage
        self.postMedia.image = thumbnailImage ?? UIImage() // PlaceHolder Image
        // GetPetListAPI
        
        
        if !isImage{
            self.thumbnailData = self.thumbnailImage!.pngData()
        }
        userTableView.register(UINib(nibName: tagCellIdentifier, bundle: nil), forCellReuseIdentifier: tagCellIdentifier)
        
        if let post = self.post{
            self.titleLabel.text = "Edit Post"
            
            if post.type == "image"{
                self.isImage = true
                self.videoButton.isHidden = true
            self.postMedia.setImage(fromUrl: (post.media?.first!)!, defaultImage: DefaultImage.defaultPostImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
            }else{
                self.isImage = false
                self.videoButton.isHidden = false
                self.postMedia.setImage(fromUrl: (post.thumbnail?.first!)!, defaultImage: DefaultImage.defaultPostImage, isCircled: false, borderColor: nil, borderWidth: nil, isCache: true, showIndicator: false)
            }
            
            self.captionTV.text = self.post?.postListDescription ?? ""
            self.locationLabel.text =  self.post?.location ?? "Select Location"
            if let location = self.post?.location {
                self.currentAddress = location
            }
            
            if let lat = self.post?.latitude {
                self.currentLat = Double(lat)
            }
            
            if let long = self.post?.longitude {
                self.currentLong = Double(long)
            }
            
            shareButton.setTitle("Submit", for: .normal)
            
            if let selectedUsers = self.post?.usertagList{
                for user in selectedUsers{
                    addName.add(user.name ?? "")
                    idToSendApi.add("\((user.id)!)")
                }
            }
            
        }
        
        getPetList()
        
        if captionTV.text.count > 0 {
            placeHolderLabel.alpha = 0
        }else{
            placeHolderLabel.alpha = 1
        }
        self.captionTV.textColor = AppColor.colorGreySubHeading
        self.captionTV.font = UIFont.JosefinSansSemiBold(size: 18.0)
        // Do any additional setup after loading the view.
    }
    
    
    func showTickOnScreen(show:Bool){
        //self.twitterSwitch.setOn(show, animated: false)
    }
    
    
    
    func getPetList(){
        let userLogin = CommonFunctions.getuserFromDefaults()
        createEditVM = CreateEditPost_VM()
        createEditVM?.userId = userLogin.userId
        createEditVM?.perPage = "10"
        createEditVM?.page = "1"
        
        
        createEditVM?.responseRecieved = {
            self.petList = self.createEditVM!.petList
            // refresh content
            self.setPreSelectedPet()
            self.setPetViewHeight()
        }
        
        createEditVM?.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.createEditVM?.alertMessage {
                   // self?.showAlert(self ?? UIViewController(), message: message)
                    self?.setPreSelectedPet()
                }
            }
        }
        createEditVM?.callGetPetListService = true
        
    }
    
    
    func setPreSelectedPet(){
        if let post = self.post{
            if self.petList.count == 0 {
                
                if let newPetList = post.petsList{
                    for petInner in newPetList{
                        petInner.isSelectedPet = true
                        self.petList.append(petInner)
                    }
                }
                
            }else{
                
                for pet in self.petList{
                    
                    if let petList =  self.post?.petsList {
                        for petInner in petList{
                            if petInner.petID == pet.petID {
                                pet.isSelectedPet = true
                                self.SelectedPetList.append(pet)
                            }
                        }
                    }
                    
                }
            }
            
        }
        
    }
    
    func setPetViewHeight(){
        if petList.isEmpty{
            //petHeightConstraint.constant = 0
            //dividerViewHeightConstraint.constant = 0
            //dividerConstraint.constant = 0
            self.petOuterView.isHidden = true
        }else{
//            petHeightConstraint.constant = 101
//            dividerViewHeightConstraint.constant = 1
//            dividerConstraint.constant = 20
            self.petOuterView.isHidden = false
        }
        self.petCollectionView.reloadData()
    }
    
    
    @IBAction func selectLocationPressed(_ sender: Any) {
        
//        EditLocationVC
        if let vc = UIStoryboard(name: StoryboardName.Settings, bundle: nil).instantiateViewController(withIdentifier: "EditLocationVC") as? EditLocationVC{
            
            if let post = self.post{
                vc.isEdit = true
            }
            self.navigationController?.pushViewController(vc, animated: true)
            vc.delegate = self
            
        }
    }
    
    
    @IBAction func facebookPressed(_ sender: UISwitch) {
        if sender.isOn
        {
            guard Reachability.isConnectedToNetwork() else {
                alertMethod(message: AppMessages.noInternetConnection)
                return
            }
            facebookSignUp(sender)
        }else{
//            let login = LoginManager()
//            login.logOut()
        }
    }
    
    
    
    
    //MARK:- TWTRComposerViewControllerDelegate
    
    func composerDidCancel(_ controller: TWTRComposerViewController) {
        print("composerDidCancel, composer cancelled tweet")
    }
    
    
    
    @IBAction func otherSocialPressed(_ sender: UISwitch) {
        if sender.isOn{
             if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                if let session = TWTRTwitter.sharedInstance().sessionStore.session() {
                    let client = TWTRAPIClient()
                    client.loadUser(withID: session.userID) { (user, error) -> Void in
                        if user != nil
                        {
                        if let user = user {
                           // self.twitterUserName.text = user.screenName
                        }
                        }
                        else{
                              print("error: \(error?.localizedDescription)");
                        }
                    }
                    if let authToken = session.authToken as? String
                    {
                        self.twitterToken = authToken
                       // self.obj.twitterAccessToken = authToken
                       // self.obj.isShare = "1"
                    }
                    if let secretAuthToken = session.authTokenSecret as? String
                    {
                        self.twitterSecretAccessToken = secretAuthToken
                      //  self.obj.twitterAccessTokenSecret = secretAuthToken
                    }
                }
             }
            else{

                TWTRTwitter.sharedInstance().logIn(with: self, completion: { (session, error) in
                    if (session != nil) {
                       // self.twitterUserName.text = session?.userName
                        if let authToken = session?.authToken
                        {
                            self.twitterToken = authToken
                        //    self.obj.twitterAccessToken = authToken
                        //    self.obj.isShare = "1"
                        }
                        if let secretAuthToken = session?.authTokenSecret
                        {
                            self.twitterSecretAccessToken = secretAuthToken
                        //    self.obj.twitterAccessTokenSecret = secretAuthToken
                        }
                    } else {
                      //  self.obj.isShare = "0"
                       // self.twitterUserName.text = ""
                        self.twitterSecretAccessToken = nil
                        self.twitterToken = nil
                        sender.setOn(false, animated: false)
                        print("error: \(error?.localizedDescription)");
                    }
                })

            }
        }
        else{
            self.twitterSecretAccessToken = nil
            self.twitterToken = nil
           // self.obj.isShare = "0"
           // self.twitterUserName.text = ""
        }
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        self.navigationController?.navigationBar.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    
    private func mimeType(for path: String) -> String {
        let url = URL(fileURLWithPath: path)
        let pathExtension = url.pathExtension

        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    
    
    
    
    func editPost(){
        let userLogin = CommonFunctions.getuserFromDefaults()
        createEditVM = CreateEditPost_VM()
        createEditVM?.userId = userLogin.userId
                createEditVM?.perPage = "10"
                createEditVM?.page = "1"
                createEditVM?.description = self.captionTV.text!
                createEditVM?.type = isImage ? "image" : "video"
                createEditVM?.postId = "\((self.post?.postID)!)"
                
                
                if let facebookToken = self.facebookToken {
                    createEditVM?.facebook_access_token = facebookToken
                }
                
                
                if let twitterToken = self.twitterToken {
                    createEditVM?.twitter_access_token = twitterToken
                }
                
                if let twitterSecretAccessToken = self.twitterSecretAccessToken {
                    createEditVM?.twitter_access_token_secret = twitterSecretAccessToken
                }
        //        createEditVM?.mimeType = isImage
                
        
        if self.currentAddress != "" {
            createEditVM?.location = self.currentAddress
        }
        
        if let latitude = self.currentLong {
            createEditVM?.longitude = String(format:"%f", self.currentLong!)
        }
        
        if let longitude = self.currentLat{
            createEditVM?.latitude = String(format:"%f", self.currentLat!)
        }
                
                var taggedPet = [String]()
                for pet in self.SelectedPetList{
                    taggedPet.append("\((pet.petID)!)")
                }
                var tagPetStr = ""
                if taggedPet.count > 0 {
                    // Pending Work
                    tagPetStr = taggedPet.joined(separator: ",")
                    createEditVM?.tagged_pets = tagPetStr
                }
                
                 
                
                var arr = [String]()
                        if idToSendApi.count != 0{
                            for obj in idToSendApi {
                                arr.append(obj as? String ?? "")
                            }
                        }
                        let taggedUserIdList = (arr.map{String($0)}).joined(separator: ",")
                createEditVM?.tagged_users = taggedUserIdList
                        
                //
                        //Hash tag get
                        
                        var description = self.captionTV.text!
                        var hashArr = [String]()
                        let replaced = description.replacingOccurrences(of: " #", with: "#")
                        let finalreplaced = replaced.replacingOccurrences(of: "#", with: " #")
                        description = finalreplaced
                        print(description)
                
                        let descriptionNew = description.components(separatedBy: " ")
                        for obj in descriptionNew ?? [String]() {
                            if obj.first == "#" {
                                var temp = obj
                                temp.remove(at: temp.startIndex)
                                hashArr.append(temp)
                            }
                        }
                        
                        if hashArr.count != 0 {
                            let hashArrList = (hashArr.map{String($0)}).joined(separator: ",")
                            createEditVM?.hashTags = hashArrList
                        }
                        
                createEditVM?.updateLoadingStatus = { [weak self] () in
                    DispatchQueue.main.async {
                        if self?.createEditVM?.isLoading ?? false {
                            LoaderView.shared.showLoader()
                        } else {
                            LoaderView.shared.hideLoader()
                        }
                    }
                }
                
                
                createEditVM?.responseRecieved = {
                    DispatchQueue.main.async {
                   // self.petList = self.createEditVM!.petList
                    // refresh content
                    //self.setPetViewHeight()
                    self.editPostFinished?()
                    self.navigationController?.popViewController(animated: true)
                    self.showAlert(self, message: "Post updated successfully")
                    }
                }
                
                createEditVM?.showAlertClosure = {[weak self] () in
                    DispatchQueue.main.async {
                        if let message = self?.createEditVM?.alertMessage {
                            self?.showAlert(self ?? UIViewController(), message: message)
                        }
                    }
                }
                createEditVM?.callEditPostService = true
    }
    
    
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        
       // self.view.endEditing(true)
        if self.isEdit{
            self.editPost()
            return
        }
        
        
        let userLogin = CommonFunctions.getuserFromDefaults()
        createEditVM = CreateEditPost_VM()
 
        
        createEditVM?.userId = userLogin.userId
        createEditVM?.perPage = "10"
        createEditVM?.page = "1"
        createEditVM?.description = self.captionTV.text!
        createEditVM?.type = isImage ? "image" : "video"
        
        if let url = self.mediaUrl {
            let filename = url.lastPathComponent
            let mimetype = mimeType(for: url.absoluteString)
            createEditVM?.mimeType = "video/mp4"
            createEditVM?.fileName = "file.mp4"
        }else{
            createEditVM?.mimeType = "image/png"
            createEditVM?.fileName = "image.png"
            createEditVM?.thumbnailData = self.postMedia.image?.jpegData(compressionQuality: 0.5)
        }
        
        
        if let thumbnailData = self.thumbnailData {
            createEditVM?.thumbnailData = thumbnailData
        }
        
        if let facebookToken = self.facebookToken {
            createEditVM?.facebook_access_token = facebookToken
        }
        
        
        if let twitterToken = self.twitterToken {
            createEditVM?.twitter_access_token = twitterToken
        }
        
        if let twitterSecretAccessToken = self.twitterSecretAccessToken {
            createEditVM?.twitter_access_token_secret = twitterSecretAccessToken
        }
//        createEditVM?.mimeType = isImage
        
        createEditVM?.location = self.currentAddress
        
        if isImage{
            createEditVM?.mediaData = self.postMedia.image?.jpegData(compressionQuality: 0.5)
        }else{
            createEditVM?.mediaData = self.mediaData!
        }
        
        
        if let latitude = self.currentLong{
            createEditVM?.longitude = String(format:"%f", self.currentLong!)
        }
        if let longitude = self.currentLong{
            createEditVM?.latitude = String(format:"%f", self.currentLat!)
        }
        
        var taggedPet = [String]()
        for pet in self.SelectedPetList{
            taggedPet.append("\((pet.petID)!)")
        }
        var tagPetStr = ""
        if taggedPet.count > 0 {
            // Pending Work
            tagPetStr = taggedPet.joined(separator: ",")
            createEditVM?.tagged_pets = tagPetStr
        }
        
        
        var arr = [String]()
                if idToSendApi.count != 0{
                    for obj in idToSendApi {
                        arr.append(obj as? String ?? "")
                    }
                }
                let taggedUserIdList = (arr.map{String($0)}).joined(separator: ",")
        createEditVM?.tagged_users = taggedUserIdList
                
        //
                //Hash tag get
                
                var description = self.captionTV.text!
                var hashArr = [String]()
                let replaced = description.replacingOccurrences(of: " #", with: "#")
                let finalreplaced = replaced.replacingOccurrences(of: "#", with: " #")
                description = finalreplaced
                print(description)
        
                let descriptionNew = description.components(separatedBy: " ")
                for obj in descriptionNew ?? [String]() {
                    if obj.first == "#" {
                        var temp = obj
                        temp.remove(at: temp.startIndex)
                        hashArr.append(temp)
                    }
                }
                
                if hashArr.count != 0 {
                    let hashArrList = (hashArr.map{String($0)}).joined(separator: ",")
                    createEditVM?.hashTags = hashArrList
                }
                
        createEditVM?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.createEditVM?.isLoading ?? false {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        
        
        
        createEditVM?.responseRecieved = {
            DispatchQueue.main.async {
           // self.petList = self.createEditVM!.petList
            // refresh content
            //self.setPetViewHeight()
                self.delegate?.createPostFinished()
            }
        }
        
        createEditVM?.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.createEditVM?.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        createEditVM?.callAddPostService = true
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension CreatePostVC : UITextViewDelegate {
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        if newText.count > 0 {
//            placeHolderLabel.alpha = 0
//        }else{
//            placeHolderLabel.alpha = 1
//        }
//        return true
//    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
                if newText.count > 0 {
                    placeHolderLabel.alpha = 0
                }else{
                    placeHolderLabel.alpha = 1
                }
        
        globalRange = range
        if text == "\n" {
            textView.resignFirstResponder()
            mutString = NSMutableString()
        } else {
            if text == "" {
                var str:String = mutString as String
                if str != "" {
                    str.remove(at: str.index(before: str.endIndex))
                    mutString = NSMutableString()
                    mutString.append(str)
                }
            }
            if captionTV.text.length == 0
            {
                isupdateTable = false
            }
            if text == " " {
                mutString = ""
                isupdateTable = false
            } else {
                mutString.append(text)
            }
            let isBackSpace = strcmp(text, "\\b")
            if (isBackSpace == -92) {
                globalInt = 0
                
                //removeCount = removeCount-1
            }else
            {
                globalInt = 1
                //removeCount = removeCount+1
            }
            
            if isupdateTable == true {
                showTaggedUserSuggestion(mutString)
            }
            
            if text == "@" {
                mutString = NSMutableString()
                isupdateTable = true
            }
            
            var isExist = false
            var index = 0
            for i in 0 ..< addName.count {
                index = i
                isExist = false
                if captionTV.text!.range(of: addName[i] as! String) != nil {
                    isExist = true
                }
                
                if isExist == false {
                    if idToSendApi.count <= index+1 {
                        idToSendApi.removeObject(at: index)
                        addName.removeObject(at: index)
                    }
                }
            }
        }
        
        return true
    }
    
    func showTaggedUserSuggestion(_ search : NSMutableString){
        print("search", search)
        if search.length <= 1
        {
            return
        }
        else{
            setPopOver(textToSeach: search as String)
            print(" query for users")
//            objUserView.search(search: mutString as String, idArray:idToSendApi, oncompletion: { (height) -> Void in
//                if height ?? 0 >= 9 {
//                    self.objUserView.frame = CGRect(x: 50, y: self.descriptionTextView.frame.maxY + 60, width: self.view.frame.width - 80, height: 260)
//                }else {
//                    self.objUserView.frame = CGRect(x: 50, y: self.descriptionTextView.frame.maxY + 60, width: self.view.frame.width - 80, height: CGFloat(height! * 45))
//                }
//            })

        }
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        let text = textView.text as NSString
        if text == ""
        {
            return
        }
        else{
        let substring = text as String
        currentWord = substring.components(separatedBy: " ").last!
        var wordWithSpace = substring.components(separatedBy: " ").last!
        wordWithSpace = " "+wordWithSpace
        let lastChar = String(currentWord.suffix(1))
        
        
       
        if (wordWithSpace.contains(" #")) && !(lastChar==" ")
        {
            var textWidth = textView.frame.inset(by: textView.textContainerInset).width
            textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
            let boundingRect =  substring.rectForText(withConstrainedWidth: textWidth, font: textView.font!)   //sizeOfString(string: substring, constrainedToWidth: Double(textWidth), font: textView.font!)
            let numberOfLines = boundingRect.height / textView.font!.lineHeight;
            
            updatedWord =  currentWord
            print("==== HashTag",updatedWord)
            
            self.setPopOverForHashTag(textToSeach: updatedWord)
            //self.hashSearchApi(currentWord: updatedWord, numberOfLines: Int(numberOfLines))
            
            if let range = textView.selectedTextRange {
                if range.start == range.end {
                    let beginning = textView.beginningOfDocument
                    let selectedRange = textView.selectedTextRange
                    let selectionStart = selectedRange?.start
                    let selectionEnd = selectedRange?.end
                    let location = textView.offset(from: beginning, to: selectionStart!)
                    let length = textView.offset(from: selectionStart!, to: selectionEnd!)
                    print(length)
                    if removeCount<0
                    {
                        removeCount = 0
                    }
                    //print("currentWord======",currentWord)
                    rangeToAppend =  NSMakeRange((location-(currentWord.count)), (currentWord.count))
                    print("rangeToAppend location ==",rangeToAppend.location)
                    print("rangeToAppend length ==",rangeToAppend.length)
                    if rangeToAppend.length == 1{
                        self.showUserTableView(show: false)
                    }
                    
                    
                }
            }
        } else if (wordWithSpace.contains(" @")) && !(lastChar==" ")
        {
            updatedWord =  currentWord.replacingOccurrences(of: "@", with: "")
         
            if let range = textView.selectedTextRange {
                if range.start == range.end {
                    let beginning = textView.beginningOfDocument
                    let selectedRange = textView.selectedTextRange
                    let selectionStart = selectedRange?.start
                    let selectionEnd = selectedRange?.end
                    let location = textView.offset(from: beginning, to: selectionStart!)
                    let length = textView.offset(from: selectionStart!, to: selectionEnd!)
                    print(length)
                    if removeCount<0
                    {
                        removeCount = 0
                    }
                    print("currentWord======",currentWord)
                    rangeToAppend =  NSMakeRange((location-(currentWord.count)), (currentWord.count))
                    print("rangeToAppend location ==",rangeToAppend.location)
                    print("rangeToAppend length ==",rangeToAppend.length)
                    if rangeToAppend.length == 1{
                        self.showUserTableView(show: false)
                    }
                }
            }
          }
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
    // change    transparentBGView.isHidden = false
    // change    postButton.isSelected = true
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
       // change transparentBGView.isHidden = true
       // change postButton.isSelected = false
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.userTableView) == true {
            return false
        }
        return true
    }
    
    
    func showUserTableView(show:Bool){
        self.userTableView.isHidden = !show
        
        if show {
            tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handle(sender:)))
            tapGesture!.delegate = self
            self.view.addGestureRecognizer(tapGesture!)
        }else{
            if tapGesture != nil {
                self.view.removeGestureRecognizer(tapGesture!)
            }
            if self.userList.count > 0 {
                self.userList.removeAll()
            }
            if self.hashTagList.count > 0 {
                self.hashTagList.removeAll()
            }
        }
        
        self.userTableView.reloadData()
        
        if isUserTable {
            
            if self.userList.count > 5 {
                self.userTableHeight.constant = 250
            }else{
                if self.userList.count < 4 {
                    self.userTableHeight.constant = 150.0
                }else{
                    self.userTableHeight.constant = CGFloat(self.userList.count * 50)
                }
                
            }
        }else{
            if self.hashTagList.count > 5 {
                self.userTableHeight.constant = 250
            }else{
                if self.hashTagList.count < 4 {
                    self.userTableHeight.constant = 150.0
                }else{
                    self.userTableHeight.constant = CGFloat(self.hashTagList.count * 50)
                }
                
            }
        }
    }
    
    
    @objc func handle(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            showUserTableView(show: false)
        }
        sender.cancelsTouchesInView = false
    }
    
    
    
    func setIdAndNameUser(id:String, name:String) {
            
            isupdateTable = false
            
            var str : NSMutableString = NSMutableString(string:captionTV.text)
            let newString = "@" + name + " "
            str.replaceCharacters(in: rangeToAppend, with: newString)
            //str = str.replacingCharacters(in: rangeToAppend, with: newString) as! NSMutableString
            captionTV.text = str.copy() as? String ?? ""
            
    //        let commentTemp = self.descriptionTextView.text.replacingOccurrences(of: "@", with: "@")
           // let comment =  self.descriptionTextView.text.replacingOccurrences(of: mutString as String, with:"")
            
            let name = name.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            print(name)
            print(id)
            
            if self.addName.contains(name) {
                
            } else {
                self.addName.add(name)
                self.idToSendApi.add(id)
            }
            
    //        self.descriptionTextView.text = ""
    //        //self.commentTextView.textColor = UIColor.black
    //        self.descriptionTextView.text = "\(comment)\(name)"
            //setRedColor(text: "\(comment)\(name)")
            self.showUserTableView(show: false)
        }
    
    //MARK: POPover
    func setPopOver(textToSeach :String){
        let userLogin = CommonFunctions.getuserFromDefaults()
                createEditVM = CreateEditPost_VM()
                
                createEditVM?.userId = userLogin.userId
                createEditVM?.perPage = "10"
                createEditVM?.page = "1"
                createEditVM?.nameStr = textToSeach
                
                createEditVM?.responseRecieved = {
                    self.userList = self.createEditVM!.userList
                    // refresh content
                    self.isUserTable = true
                    if self.userList.count == 0 {
                        self.showUserTableView(show: false)
                    }else{
                        self.showUserTableView(show: true)
                    }
                    //self.setPetViewHeight()
                }
                
                
                createEditVM?.showAlertClosure = {[weak self] () in
                    DispatchQueue.main.async {
                        if let message = self?.createEditVM?.alertMessage {
                           // self?.showAlert(self ?? UIViewController(), message: message)
                            self?.userList.removeAll()
                            self?.showUserTableView(show: false)
                        }
                    }
                }
        
                createEditVM?.callGetUserSearchListService = true
    }
    
    //MARK: POPover
    func setPopOverForHashTag(textToSeach :String){
        
        if currentWord.length < 1 {
            return
        }
        var searchText = currentWord
        searchText.remove(at: searchText.startIndex)
        
        
        let userLogin = CommonFunctions.getuserFromDefaults()
                createEditVM = CreateEditPost_VM()
                
                createEditVM?.userId = userLogin.userId
                createEditVM?.nameStr = searchText
                createEditVM?.callGetHashTAGListService = true
                createEditVM?.responseRecieved = {
                    self.hashTagList = self.createEditVM!.hashTagList
                    // refresh content
                    self.isUserTable = false
                    
                    
                    if self.hashTagList.count == 0 {
                        self.showUserTableView(show: false)
                    }else{
                        self.showUserTableView(show: true)
                    }
                    //self.setPetViewHeight()
                }
                
                createEditVM?.showAlertClosure = {[weak self] () in
                    DispatchQueue.main.async {
                        if let message = self?.createEditVM?.alertMessage {
                            self?.showAlert(self ?? UIViewController(), message: message)
                            self?.userList.removeAll()
                            self?.showUserTableView(show: false)
                        }
                    }
                }
    }
    
    /*func setPopOver() {
        let sizeH =  sizeOfString(string: descriptionTextView.text ?? "", constrainedToWidth: Double(self.view.frame.width - 40), font: UIFont(name: "Roboto-Regular", size: 15)!)
        let noOFLines = (sizeH.height/UIFont(name: "Roboto-Regular", size: 15)!.lineHeight)
        
        if objUserView != nil {
            objUserView.removeFromSuperview()
        }
        objUserView = Bundle.main.loadNibNamed("CustomUserView", owner: self, options: nil)?.first as? CustomUserView
        objUserView.delegate = self
        objUserView.loadView(idArray: idToSendApi) { (height) -> Void in
            self.objUserView.tag = 900
            
            let objY = self.descriptionTextView.frame.maxY
    
            self.objUserView.frame = CGRect(x: 50.0, y: objY, width: self.view.frame.width - 80, height: CGFloat(height ?? 0 * 45))
             self.view.insertSubview(self.objUserView, aboveSubview: self.view)
            self.view.bringSubviewToFront(self.objUserView)
            
            self.objUserView.layer.cornerRadius = 0
            self.objUserView.layer.shadowOffset = CGSize(width:0, height:2.5)
            self.objUserView.layer.shadowColor =  UIColor.darkGray.cgColor
            self.objUserView.layer.shadowOpacity = 0.5
            self.objUserView.layer.shadowRadius = 5.0
            self.objUserView.layer.borderColor = UIColor(red: 170.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 0.5).cgColor
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handle(sender:)))
            tapGesture.delegate = self
            self.view.addGestureRecognizer(tapGesture)
        }
    } */
    
}



extension CreatePostVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return petList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let mediaCell : PetAddPostCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! PetAddPostCell
        
        let pet = petList[indexPath.item]
        mediaCell.setPet(pet: pet)
        mediaCell.petDelegate = self
        return mediaCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //let height =  self.bounds.size.height - (self.view.safeAreaInsets.top + self.view.safeAreaInsets.bottom + 101)
        return CGSize(width: self.view.frame.size.width - 40 , height: 65)
    }
       // return CGSize(width: 100, height: 100)
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print(indexPath.item)
    }
    
}


extension CreatePostVC : PetAddPostCellProtocol {
    func petAdded(pet: PetList) {
        self.SelectedPetList.append(pet)
        print(pet.petName)
        print(self.SelectedPetList)
    }
    
    func petRemoved(pet: PetList) {
        
        if let index = self.SelectedPetList.index(where: { $0.petID == pet.petID }) {
            print(pet.petName)
            self.SelectedPetList.remove(at: index)
        }
        print(self.SelectedPetList)
    }
    
    
}



extension CreatePostVC : EditLocationVCProtocol {
    func locationDetails(lat: Double, long: Double, address: String) {
        self.currentLat = lat
        self.currentLong = long
        self.currentAddress = address
        self.locationLabel.text = address
    }
    
    
}


extension CreatePostVC {
   func facebookSignUp(_ sender : UISwitch){
       let login = LoginManager()
       login.logIn(permissions: ["public_profile","email"], from: self) { (result, error) in
           if (error != nil) {
              sender.setOn(false, animated: false)
           } else {
               if AccessToken.current != nil {
                   let accessToken = AccessToken.current
                   let tokenString = (accessToken?.tokenString) ?? ""
                    print(tokenString)
                self.facebookToken = tokenString
//                   self.obj.facebookAccessToken="EAAHna20683sBAHWq8pp9pEM3G7ZBcJSAnfYHHk5NK2exSybLH3IPtnQWY70tzzeFFaZBDZCVJ0idkFlXPrsASj1XZBbOoot2RTyaL1XaV9TElkIpjpinc7pZAcbgQckSGZCSS42Om17HFwkMxmddEtweQG8wpUQELpU1EZCjnUzuaXvaHLbgoI17PA89CIvzRkK5BmdCn8NSl623dQTaAkK"
//                       self.obj.isShare = "1"
                   //login.logOut()
               }
               else{
                   sender.setOn(false, animated: false)
                self.facebookToken = nil
               }
           }
       }
   }
   
}



extension  CreatePostVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isUserTable{
        return self.userList.count
        }else{
            return self.hashTagList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isUserTable{
            let cell : UserSearchCell =  tableView.dequeueReusableCell(withIdentifier: "UserSearchCell", for: indexPath) as! UserSearchCell
            //        cell.homeCellDelegate = self
            // cell.indexNumber = indexPath
            let user = self.userList[indexPath.row]
            //        let post = postList[indexPath.row]
            //
            //        cell.setPost(post: post)
            cell.setUI(user: user)
            return cell
        }else{
            let cell : TagCell =  tableView.dequeueReusableCell(withIdentifier: tagCellIdentifier, for: indexPath) as! TagCell
            //cell.homeCellDelegate = self
            let hashTag = self.hashTagList[indexPath.row]
            cell.setHashTag(hashTag:hashTag)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isUserTable{
            let user = self.userList[indexPath.row]
            self.taggedUserList.append(user)
            //self.userList.removeAll()
            self.setIdAndNameUser(id: "\(user.id!)", name: "\(user.username!)")
        }else{
            // for hashTag
            
            var str : NSMutableString = NSMutableString(string:captionTV.text)
            let newString = "#" + hashTagList[indexPath.row].name! + " "
            str = str.replacingCharacters(in: rangeToAppend, with: newString) as! NSMutableString
            captionTV.text = str.copy() as! String
            self.showUserTableView(show: false)
            captionTV.becomeFirstResponder()
            
        }
    }
}
