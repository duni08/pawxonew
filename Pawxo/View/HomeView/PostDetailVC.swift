//
//  PostDetailVC.swift
//  Pawxo
//
//  Created by 42works on 07/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import IQKeyboardManager
import ActiveLabel
import AVKit


protocol PostDetailVCProtocol {
    func setNavigationDelegate()
}

class PostDetailVC: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var navigationLabel: SemiBoldLabel!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var cuddlLogo: UIImageView!
    @IBOutlet weak var cuddlLeadingConstraint: NSLayoutConstraint!
    var fromView = ""
    var delegate : PostDetailVCProtocol?
    var tempLabel : ActiveLabel!
    @IBOutlet weak var postDetailTableView: UITableView!
    
    @IBOutlet weak var suggestionsTableView: UITableView!
    
    @IBOutlet weak var suggestionHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var optionsButton: UIButton!
    
    @IBOutlet weak var descriptionBottomconstraint: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    var staticCount = 1
    
    var isFromProfileOrPet = false
    var postCommentIdentifier = "CommentCell"
    var postHeaderIdentifier = "ForumHeaderCell"
    
    
    @IBOutlet weak var placeHolderLabel: RegularLabel!
    
    @IBOutlet weak var boxViewHeightconstraint: NSLayoutConstraint!
    @IBOutlet weak var inputTextView: UITextView!
    
    @IBOutlet weak var centerImageView: UIImageView!
    
    @IBOutlet weak var cView: UIView!
    
    var ticketTop : NSLayoutConstraint?
    var aTop : NSLayoutConstraint?
    let aView = UIView()
    var imageCenter : UIImage?
    var imageFrame : CGRect?
    var showHeader = false
    var text = "Jason Stathom buy four new dogs for their petclub @Feelinghappy This is a test comment"
    
    var post : PostList?
    
    var tapGesture : UITapGestureRecognizer?
    var globalRange = NSRange()
    var mutString = NSMutableString()
    var isupdateTable = false
    var globalInt : Int = 1
    var addName = NSMutableArray()
    var idToSendApi = NSMutableArray()
    var currentWord : String = ""
    var updatedWord : String = ""
    var rangeToAppend = NSRange()
    var removeCount : Int = 0
     var taggedUserList = [Userlist]()
    var userList = [Userlist]()
    var postId: String = String()
    var hashTagList = [HashTag]()
    var isUserTable = false
    var tagCellIdentifier = "TagCell"
    var createEditVM : CreateEditPost_VM?
    
    var comment_VM : Comment_VM?
    var commentList = [CommentList]()
    
    @IBOutlet weak var ownView: UIView!
    
    @IBOutlet weak var outerOptionsView: UIView!
    
    @IBOutlet weak var reportAbuseView: UIView!
    
    
    var postList = [PostList]()
    
    var deletedPost : (()->())?
    
    var setPost : ((PostList)->())?
    
    var refresh = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ViewDi post details")
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 76, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = 0
        tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        tempLabel.font = UIFont.JosefinSansRegular(size: 14.0)
        
        
        if let imageCenter = imageCenter{
            self.centerImageView.image = imageCenter
        }
        
        if isFromProfileOrPet {
          //  self.cuddlLogo.isHidden = true
            let rect = self.cuddlLogo.frame
            self.cuddlLogo.frame = CGRect(x: (self.view.frame.width/2) - 56, y: rect.origin.y, width: rect.width, height:rect.height)
        }
        //self.postDetailTableView.contentInset = UIEdgeInsets(top: (self.imageFrame?.size.height)! -  CGFloat(35), left: 0, bottom: 0, right: 0)
        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
        postDetailTableView.register(UINib(nibName: postHeaderIdentifier, bundle: nil), forCellReuseIdentifier: postHeaderIdentifier)
        postDetailTableView.register(UINib(nibName: postCommentIdentifier, bundle: nil), forCellReuseIdentifier: postCommentIdentifier)
        if inputTextView.text.count > 0 {
            placeHolderLabel.alpha = 0
        }else{
            placeHolderLabel.alpha = 1
        }
        
        self.inputTextView.textColor = AppColor.colorGreySubHeading
        placeHolderLabel.textColor = AppColor.colorGreyTime
//       // inputTextView.maxLength = 140
//        inputTextView.trimWhiteSpaceWhenEndEditing = false
//        inputTextView.placeholder = "Type your message.."
//        inputTextView.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
//        inputTextView.minHeight = 25.0
//        inputTextView.maxHeight = 70.0
//        inputTextView.backgroundColor = UIColor.clear
//        inputTextView.layer.cornerRadius = 4.0
        
        suggestionsTableView.register(UINib(nibName: tagCellIdentifier, bundle: nil), forCellReuseIdentifier: tagCellIdentifier)
        
        
        refresh.tintColor = UIColor.clear
        refresh.backgroundColor = UIColor.clear
        refresh.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refresh.addTarget(self, action:#selector(refreshTable), for: UIControl.Event.valueChanged)
        // Add Custom Loader
        var loader = UIImageView()
        let gif = UIImage.gifImageWithName("gif")
        loader.center = self.view.center
        loader = UIImageView(frame: CGRect(x: self.view.frame.size.width/2 - 20, y: refresh.center.y, width: 40, height: 40))
        loader.image = gif
        refresh.insertSubview(loader, at: 0)
        
        self.postDetailTableView.addSubview(refresh)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.getPostDetails(postId: "\((self.post?.postID)!)")
        }
    }
    
    
    @objc private func refreshTable() {
        self.getPostDetails(postId: "\((self.post?.postID)!)")
        getCommentsList()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    func getHeightForDescription() -> Int {
        return 136
    }
    func getPostCommentList()
    {
        comment_VM = Comment_VM()
                        let userLogin = CommonFunctions.getuserFromDefaults()
                        comment_VM?.userId = userLogin.userId
                        comment_VM?.postId = "\(postId)"
                        
                        
                        comment_VM?.responseRecieved = {
                //            self.postList = self.homeListVM.postList
                //            // refresh content
                //            self.homeTableView.reloadData()
                            self.commentList = self.comment_VM!.commentList
                            self.postDetailTableView.reloadData()
                            
                        }
                        
                        comment_VM?.showAlertClosure = {[weak self] () in
                            DispatchQueue.main.async {
                                if let message = self?.comment_VM?.alertMessage {
                                    self?.showAlert(self ?? UIViewController(), message: message)
                                }
                            }
                        }
                        comment_VM?.callFetchCommentsService = true
    }
    func getCommentsList(){
                comment_VM = Comment_VM()
                let userLogin = CommonFunctions.getuserFromDefaults()
                comment_VM?.userId = userLogin.userId
                comment_VM?.postId = "\((self.post?.postID)!)"
                
                
//                comment_VM?.updateLoadingStatus = { [weak self] () in
//                    DispatchQueue.main.async {
//                        if self?.comment_VM?.isLoading ?? false {
//                            LoaderView.shared.showLoader()
//                        } else {
//                            LoaderView.shared.hideLoader()
//                        }
//                    }
//                }
                
                comment_VM?.responseRecieved = {
                    self.refresh.endRefreshing()
        //            self.postList = self.homeListVM.postList
        //            // refresh content
        //            self.homeTableView.reloadData()
                    self.commentList = self.comment_VM!.commentList
                    self.postDetailTableView.reloadData()
                    
                }
                
                comment_VM?.showAlertClosure = {[weak self] () in
                    self!.refresh.endRefreshing()
                    DispatchQueue.main.async {
                        if let message = self?.comment_VM?.alertMessage {
                            self?.showAlert(self ?? UIViewController(), message: message)
                        }
                    }
                }
                comment_VM?.callFetchCommentsService = true
            
    }
    
    
    func getPostDetails(postId:String){
        let comment_VM = Comment_VM()
        let userLogin = CommonFunctions.getuserFromDefaults()
        comment_VM.userId = userLogin.userId
        comment_VM.postId = "\(postId)"
        comment_VM.responseRecieved = {
             DispatchQueue.main.async {
            self.post = comment_VM.postObj
            print("post==\(self.post!)")
            self.postDetailTableView.reloadData()
                self.setPost?(self.post!)
            //                                self.post = self.postList[0]
            // self.postDetailTableView.reloadData()
            }
        }
        
        comment_VM.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.comment_VM?.alertMessage {
                    //self!.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        comment_VM.callFetchPostDetailService = true
        print("jnk")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewwilla[ppear")
        
        if self.isFromProfileOrPet{
            
            self.centerImageView.isHidden = true
            self.showHeader = true
            self.postDetailTableView.reloadData()
        }else{
            self.setUpConstraintForImageView()
        }
       if(fromView == "Notification")
       {
            getPostCommentList()
        }
        else
       {
            getCommentsList()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setUpConstraintForImageView(){
        self.centerImageView.translatesAutoresizingMaskIntoConstraints = false
         self.centerImageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
         self.centerImageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
         //self.centerImageView.heightAnchor.constraint(equalToConstant: (self.imageFrame?.size.height)!).isActive = true
         self.centerImageView.heightAnchor.constraint(equalToConstant: 254).isActive = true
         
         let cons = CGFloat(254)
         //self.centerImageView.topAnchor.constraint(equalTo: self.postDetailTableView.topAnchor, constant:cons).isActive = true
         
        ticketTop =  self.centerImageView.bottomAnchor.constraint(equalTo: self.postDetailTableView.topAnchor ,constant: cons)
         ticketTop!.isActive = true
        //self.descriptionView.isHidden = true
        /// descriptionView top constraint
       // self.descriptionBottomconstraint.constant = 440//self.imageFrame!.size.height - CGFloat(35)
        
        
//        self.view.addSubview(aView)
//        aView.backgroundColor = UIColor.clear
//        aView.isUserInteractionEnabled = false
//        aView.translatesAutoresizingMaskIntoConstraints = false
//        aView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
//        aView.heightAnchor.constraint(equalToConstant: 35).isActive = true
//        aView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
//        aTop = aView.bottomAnchor.constraint(equalTo: self.centerImageView.bottomAnchor, constant:  0)
//        aTop?.isActive = true
        
//        let bView = UIView()
//        aView.addSubview(bView)
//        bView.backgroundColor = .white
//        bView.translatesAutoresizingMaskIntoConstraints = false
//        bView.layer.cornerRadius = 35
//        bView.layer.masksToBounds = true
//        aView.clipsToBounds = true
//        bView.leadingAnchor.constraint(equalTo: aView.leadingAnchor).isActive = true
//        bView.heightAnchor.constraint(equalToConstant: 100).isActive = true
//        bView.trailingAnchor.constraint(equalTo: aView.trailingAnchor).isActive = true
//        bView.topAnchor.constraint(equalTo: aView.topAnchor).isActive = true
//        aView.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
        //self.view.bringSubviewToFront(self.descriptionView)
            //self.view.bringSubviewToFront(self.aView)
            //self.view.bringSubviewToFront(self.topView)
//            //self.aView.isHidden = false
//            self.centerImageView.isHidden = true
//            let cellView = self.tableView(self.postDetailTableView, viewForHeaderInSection: 0)!.superview as! HeaderViewCell
//            cellView.centerImageView.isHidden = false
//            cellView.setCenterImage(show: true)
//            cellView.setNeedsDisplay()
//
//
//            self.showHeader = true
//            self.postDetailTableView?.reloadSections([0], with: .none)
//            self.postDetailTableView?.layoutSubviews()
            
            self.centerImageView.isHidden = true
            self.showHeader = true
            self.postDetailTableView.reloadData()
            
           // print(cellView)
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        //self.bottomConstraint?.constant =  -10
    }
    
    
    
    @IBAction func optionsButtonPressed(_ sender: Any) {
        outerOptionsView.isHidden = false
        let userLogin = CommonFunctions.getuserFromDefaults()
        
        if self.post?.userID == Int(userLogin.userId!) {
            self.ownView.isHidden = false
            self.reportAbuseView.isHidden = true
        }else{
            self.ownView.isHidden = true
            self.reportAbuseView.isHidden = false
        }
    }
    
    
    @IBAction func cancelOptionsView(_ sender: Any) {
         outerOptionsView.isHidden = true
    }
    
    
    
    @IBAction func editButtonPressed(_ sender: Any) {
        outerOptionsView.isHidden = true
        let postStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = postStoryboard.instantiateViewController(withIdentifier: "CreatePostVC") as! CreatePostVC
        vc.isEdit = true
        vc.post = self.post
        vc.thumbnailImage = self.centerImageView.image
        vc.editPostFinished = {
            DispatchQueue.main.async {
                self.getPostDetails(postId: "\((self.post?.postID)!)")
                NotificationCenter.default.post(name: Notification.Name("RefreshHomeList"), object: nil, userInfo: nil)
            }
        }
        self.navigationController?.delegate = nil
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func deleteButtonPressed(_ sender: Any) {
        
        outerOptionsView.isHidden = true
        
        let alertController = UIAlertController(title: AppMessages.deletePost, message: nil, preferredStyle: UIAlertController.Style.alert)
        
        
        
        let somethingAction = UIAlertAction(title: "OK", style: .default, handler: {(alert: UIAlertAction!) in print("Report API call")
            
            let userLogin = CommonFunctions.getuserFromDefaults()
            
            let homeListVM = HomeList_VM()
            homeListVM.userId = userLogin.userId
            let pid = self.post?.postID!
            homeListVM.postId = "\(pid!)"
            
            
            homeListVM.userId = userLogin.userId
            
            homeListVM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if homeListVM.isLoading  {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            homeListVM.responseRecieved = {
                // refresh button like
                self.deletedPost?()
                self.backButtonPressed(UIButton())
            }
            
            homeListVM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = homeListVM.alertMessage {
                        self?.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            homeListVM.deletePostService = true
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
        
        alertController.addAction(somethingAction)
        alertController.addAction(cancelAction)
        alertController.view.tintColor = UIColor.black
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
    }
    
    
    @IBAction func reportAbuseButtonPressed(_ sender: Any) {
        outerOptionsView.isHidden = true
        let userLogin = CommonFunctions.getuserFromDefaults()
        
        var homeListVM = HomeList_VM()
        let pid = self.post?.postID!
        
        let key = "\(pid!)&\((userLogin.userId)!)"
        if let val = UserDefaults.standard.value(forKey: key){
            
            self.showAlert(self, message: AppMessages.reportAbuseSecond)
            
            return
        }
        
        
        let alertController = UIAlertController(title: AppMessages.reportAbuse, message: nil, preferredStyle: UIAlertController.Style.alert)

        let somethingAction = UIAlertAction(title: "Report", style: .default, handler: {(alert: UIAlertAction!) in print("Report API call")
            
            
            homeListVM = HomeList_VM()
            homeListVM.userId = userLogin.userId
            let pid = self.post?.postID!
            homeListVM.postId = "\(pid!)"
            
            
            homeListVM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if homeListVM.isLoading {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            homeListVM.responseRecieved = {
                // refresh button like
                self.showAlert(self, message: AppMessages.reportAbuseFirst)
                UserDefaults.standard.setValue(true, forKey: key)
            }
            
            homeListVM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = homeListVM.alertMessage {
                        self?.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            
            homeListVM.reportPostService = true
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})

        alertController.addAction(somethingAction)
        alertController.addAction(cancelAction)
        alertController.view.tintColor = UIColor.black
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            var scroll = false
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.bottomConstraint?.constant = 0.0
            } else {
                self.bottomConstraint?.constant = -((endFrame?.size.height)! - self.view.safeAreaInsets.bottom) ?? 0.0
                scroll = true
            }
            
            
            UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations: {
                self.view.layoutIfNeeded()
            }) { (success) in
                if scroll{
                    self.postDetailTableView.scrollToBottom()
                }
            }
        }
        
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.view.endEditing(true)
        
        
        if self.isFromProfileOrPet{
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        self.showHeader = false
        //self.postDetailTableView?.reloadSections([0], with: .none)
        
        if let cell = self.postDetailTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ForumHeaderCell {
            cell.forumCollectionView.isHidden = true
        }
        
        self.centerImageView.isHidden = false
        delegate?.setNavigationDelegate()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func goToProfileView(userId:Int){
        let storyboardProfile = UIStoryboard(name: "Profile", bundle: nil)
        let vc_Profile = storyboardProfile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc_Profile.isFromOtherScreen = true
        
        if userId == Int(CommonFunctions.getuserFromDefaults().userId!){
                vc_Profile.isMyProfile = true
        }else{
            vc_Profile.isMyProfile = false
        }
        
        
        vc_Profile.userId = "\(userId)"
        self.navigationController?.delegate = nil
        self.navigationController?.pushViewController(vc_Profile, animated: true)
    }
    
    
    func likeButtonPressed() {
        // Go to Like views
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_LikesList = storyboard.instantiateViewController(withIdentifier: "PostLikesListVC") as! PostLikesListVC
        vc_LikesList.post = self.post
        self.navigationController?.delegate = nil
        self.navigationController?.pushViewController(vc_LikesList, animated: true)
    }
    
    
    
    
    func deleteCommentPopUp(cell:CommentCell,indexPath:IndexPath){
        let alertController = UIAlertController(title: AppMessages.deleteComment, message: nil, preferredStyle: UIAlertController.Style.alert)
        
        let indexPathNew = self.postDetailTableView.indexPath(for: cell)
       
        let somethingAction = UIAlertAction(title: "OK", style: .default, handler: {(alert: UIAlertAction!) in print("Report API call")
             // Delete comment API
                
            
            let comment_VM = Comment_VM()
            let userLogin = CommonFunctions.getuserFromDefaults()
            comment_VM.userId = userLogin.userId
            comment_VM.commentId = "\((cell.comment?.id)!)"
            
            
            comment_VM.updateLoadingStatus = { [weak self] () in
                DispatchQueue.main.async {
                    if comment_VM.isLoading ?? false {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            comment_VM.responseRecieved = {
                self.commentList.remove(at: indexPathNew!.row - 1)
                self.postDetailTableView.deleteRows(at: [indexPathNew!], with: .automatic)
            }
            
            comment_VM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = comment_VM.alertMessage {
                        self?.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            comment_VM.callDeletecommentService = true
            
            
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
            
            alertController.addAction(somethingAction)
            alertController.addAction(cancelAction)
            alertController.view.tintColor = UIColor.black
            DispatchQueue.main.async {
                self.present(alertController, animated: true, completion:{})
            }
    }
    
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        
        
       
        
        if inputTextView.text!.count == 0 {
            return
        }
        
        comment_VM = Comment_VM()
        let userLogin = CommonFunctions.getuserFromDefaults()
        comment_VM?.userId = userLogin.userId
        comment_VM?.comment = inputTextView.text!.trim()
        comment_VM?.postId = "\((self.post?.postID)!)"
        var arr = [String]()
        if idToSendApi.count != 0{
            for obj in idToSendApi {
                arr.append(obj as? String ?? "")
            }
        }
        let taggedUserIdList = (arr.map{String($0)}).joined(separator: ",")
        
        comment_VM?.taggedUsers = taggedUserIdList
        
        let description = self.inputTextView.text?.components(separatedBy: " ")
        var hashArr = [String]()
        
        for obj in description ?? [String]() {
            if obj.first == "#" {
                var temp = obj
                temp.remove(at: temp.startIndex)
                hashArr.append(temp)
            }
        }
        if hashArr.count != 0 {
            let hashArrList = (hashArr.map{String($0)}).joined(separator: ",")
            comment_VM?.hashTags = hashArrList
        }
        
        
        
        comment_VM?.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if self?.comment_VM?.isLoading ?? false {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        comment_VM?.responseRecieved = {

            self.inputTextView.text = ""
            self.placeHolderLabel.alpha = 1
            self.idToSendApi.removeAllObjects()
            
            if let comment = self.comment_VM?.commentObj {
                self.commentList.insert(comment, at: 0)
//                self.postDetailTableView.insertRows(at: [IndexPath(row: 1, section: 0)], with: .middle)
                self.postDetailTableView.reloadData()
            }
            
            self.postDetailTableView.setContentOffset(.zero, animated:false)
            // Todo attachComment
            
            
            
        }
        
        comment_VM?.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = self?.comment_VM?.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        
        self.view.endEditing(true)
        
        comment_VM?.callAddcommentService = true
        
    }
    
}


extension PostDetailVC : UITableViewDelegate , UITableViewDataSource {
    
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let cell : HeaderViewCell =  tableView.dequeueReusableCell(withIdentifier: postHeaderIdentifier) as! HeaderViewCell
//        cell.setCenterImage(show: showHeader)
//        return cell.contentView
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 373
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        tempLabel.text = text
        //        tempLabel.sizeToFit()
        //
        //        let calculatedHeight = tempLabel.frame.height + 34
        //        return calculatedHeight
        
        if tableView == suggestionsTableView{
            return 50
        }else{
            if indexPath.row == 0 {
                return UITableView.automaticDimension
                tempLabel.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width - 40, height: CGFloat.greatestFiniteMagnitude)
                tempLabel.text = self.post?.postListDescription ?? ""
                tempLabel.font = UIFont.JosefinSansSemiBold(size: 15.0)
                tempLabel.sizeToFit()
                let calculatedHeight = tempLabel.frame.height + 279
                return calculatedHeight
            }else{
                tempLabel.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width - 76, height: CGFloat.greatestFiniteMagnitude)
                tempLabel.font = UIFont.JosefinSansRegular(size: 14.0)
                let comment = self.commentList[indexPath.row - 1]
                tempLabel.text = comment.name!
                tempLabel.sizeToFit()
                let calculatedHeight = tempLabel.frame.height + 36
                return calculatedHeight
                return 80.0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == suggestionsTableView {
            if isUserTable{
                return self.userList.count
            }else{
                return self.hashTagList.count
            }
        }
        else{
            return self.commentList.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == suggestionsTableView {
            if isUserTable{
                let cell : UserSearchCell =  tableView.dequeueReusableCell(withIdentifier: "UserSearchCell", for: indexPath) as! UserSearchCell
                //        cell.homeCellDelegate = self
                // cell.indexNumber = indexPath
                let user = self.userList[indexPath.row]
                //        let post = postList[indexPath.row]
                //
                //        cell.setPost(post: post)
                cell.setUI(user: user)
                return cell
            }else{
                let cell : TagCell =  tableView.dequeueReusableCell(withIdentifier: tagCellIdentifier, for: indexPath) as! TagCell
                //cell.homeCellDelegate = self
                let hashTag = self.hashTagList[indexPath.row]
                cell.setHashTag(hashTag:hashTag)
                return cell
            }
        }else{
            if indexPath.row == 0 {
                let cell : ForumHeaderCell =  tableView.dequeueReusableCell(withIdentifier: postHeaderIdentifier) as! ForumHeaderCell
                print(self.post!)
                cell.setUpPost(post: self.post!)
                //cell.setUpData(count: 0, currentPage: 0)
                //cell.setTextDescription(text: text)
                cell.setCollectionView(show: showHeader)
                cell.delegate = self
                cell.postDelegate = self
                cell.userNotFound = {
                    self.showAlert(self, message: "User not found.")
                }
                return cell
            }else{
                let cell : CommentCell =  tableView.dequeueReusableCell(withIdentifier: postCommentIdentifier, for: indexPath) as! CommentCell
                cell.delegate = self
                let comment = self.commentList[indexPath.row - 1]
                cell.setComment(comment: comment)
                cell.delegate = self
                cell.longPressedForDelete = {
                    if comment.userID == Int((CommonFunctions.getuserFromDefaults().userId)!){
                        print(comment.name)
                        print(indexPath.row)
                        self.deleteCommentPopUp(cell: cell, indexPath: indexPath)
                    }
                }
                cell.userNotFound = {
                    self.showAlert(self, message: "User not found.")
                }
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == suggestionsTableView {
            if isUserTable{
                let user = self.userList[indexPath.row]
                self.taggedUserList.append(user)
                //self.userList.removeAll()
                self.setIdAndNameUser(id: "\(user.id!)", name: "\(user.username!)")
            }else{
                // for hashTag
                
                var str : NSMutableString = NSMutableString(string:inputTextView.text)
                let newString = "#" + hashTagList[indexPath.row].name! + " "
                str = str.replacingCharacters(in: rangeToAppend, with: newString) as! NSMutableString
                inputTextView.text = str.copy() as! String
                self.showUserTableView(show: false)
                inputTextView.becomeFirstResponder()
                
            }
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_PostDetail = storyboard.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
        self.view.endEditing(true)
        // self.navigationController?.pushViewController(vc_PostDetail, animated: true)
    }
  
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
       
    }
}




extension UITableView {
    
    func scrollToBottom(){
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(
                row: self.numberOfRows(inSection:  self.numberOfSections - 1) - 1,
                section: self.numberOfSections - 1)
            self.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    func scrollToTop() {
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
}



extension PostDetailVC : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
                    if newText.count > 0 {
                        placeHolderLabel.alpha = 0
                    }else{
                        placeHolderLabel.alpha = 1
                    }
            
            globalRange = range
            if text == "\n" {
                textView.resignFirstResponder()
                mutString = NSMutableString()
            } else {
                if text == "" {
                    var str:String = mutString as String
                    if str != "" {
                        str.remove(at: str.index(before: str.endIndex))
                        mutString = NSMutableString()
                        mutString.append(str)
                    }
                }
                if inputTextView.text.length == 0
                {
                    isupdateTable = false
                }
                if text == " " {
                    mutString = ""
                    isupdateTable = false
                } else {
                    mutString.append(text)
                }
                let isBackSpace = strcmp(text, "\\b")
                if (isBackSpace == -92) {
                    globalInt = 0
                    
                    //removeCount = removeCount-1
                }else
                {
                    globalInt = 1
                    //removeCount = removeCount+1
                }
                
                if isupdateTable == true {
                    showTaggedUserSuggestion(mutString)
                }
                
                if text == "@" {
                    mutString = NSMutableString()
                    isupdateTable = true
                }
                
                var isExist = false
                var index = 0
                for i in 0 ..< addName.count {
                    index = i
                    isExist = false
                    if inputTextView.text!.range(of: addName[i] as! String) != nil {
                        isExist = true
                    }
                    
                    if isExist == false {
                        if idToSendApi.count <= index+1 {
                            idToSendApi.removeObject(at: index)
                            addName.removeObject(at: index)
                        }
                    }
                }
            }
            
            return true
        }
        
        func showTaggedUserSuggestion(_ search : NSMutableString){
            print("search", search)
            if search.length <= 1
            {
                return
            }
            else{
                setPopOver(textToSeach: search as String)
                print(" query for users")
    //            objUserView.search(search: mutString as String, idArray:idToSendApi, oncompletion: { (height) -> Void in
    //                if height ?? 0 >= 9 {
    //                    self.objUserView.frame = CGRect(x: 50, y: self.descriptionTextView.frame.maxY + 60, width: self.view.frame.width - 80, height: 260)
    //                }else {
    //                    self.objUserView.frame = CGRect(x: 50, y: self.descriptionTextView.frame.maxY + 60, width: self.view.frame.width - 80, height: CGFloat(height! * 45))
    //                }
    //            })

            }
        }
        
        
        func textViewDidChange(_ textView: UITextView) {
            let text = textView.text as NSString
            if text == ""
            {
                return
            }
            else{
           let substring = text as String
            currentWord = substring.components(separatedBy: " ").last!
            var wordWithSpace = substring.components(separatedBy: " ").last!
            wordWithSpace = " "+wordWithSpace
            let lastChar = String(currentWord.suffix(1))
            
            
           
            if (wordWithSpace.contains(" #")) && !(lastChar==" ")
            {
                var textWidth = textView.frame.inset(by: textView.textContainerInset).width
                textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
                let boundingRect =  substring.rectForText(withConstrainedWidth: textWidth, font: textView.font!)   //sizeOfString(string: substring, constrainedToWidth: Double(textWidth), font: textView.font!)
                let numberOfLines = boundingRect.height / textView.font!.lineHeight;
                
                updatedWord =  currentWord
                print("==== HashTag",updatedWord)
                
               // self.setPopOverForHashTag(textToSeach: updatedWord)
                //self.hashSearchApi(currentWord: updatedWord, numberOfLines: Int(numberOfLines))
                
                if let range = textView.selectedTextRange {
                    if range.start == range.end {
                        let beginning = textView.beginningOfDocument
                        let selectedRange = textView.selectedTextRange
                        let selectionStart = selectedRange?.start
                        let selectionEnd = selectedRange?.end
                        let location = textView.offset(from: beginning, to: selectionStart!)
                        let length = textView.offset(from: selectionStart!, to: selectionEnd!)
                        print(length)
                        if removeCount<0
                        {
                            removeCount = 0
                        }
                        //print("currentWord======",currentWord)
                        rangeToAppend =  NSMakeRange((location-(currentWord.count)), (currentWord.count))
                        print("rangeToAppend location ==",rangeToAppend.location)
                        print("rangeToAppend length ==",rangeToAppend.length)
                        if rangeToAppend.length == 1{
                            self.showUserTableView(show: false)
                        }
                        
                        
                    }
                }
            } else if (wordWithSpace.contains(" @")) && !(lastChar==" ")
            {
                updatedWord =  currentWord.replacingOccurrences(of: "@", with: "")
             
                if let range = textView.selectedTextRange {
                    if range.start == range.end {
                        let beginning = textView.beginningOfDocument
                        let selectedRange = textView.selectedTextRange
                        let selectionStart = selectedRange?.start
                        let selectionEnd = selectedRange?.end
                        let location = textView.offset(from: beginning, to: selectionStart!)
                        let length = textView.offset(from: selectionStart!, to: selectionEnd!)
                        print(length)
                        if removeCount<0
                        {
                            removeCount = 0
                        }
                        print("currentWord======",currentWord)
                        rangeToAppend =  NSMakeRange((location-(currentWord.count)), (currentWord.count))
                        print("rangeToAppend location ==",rangeToAppend.location)
                        print("rangeToAppend length ==",rangeToAppend.length)
                        if rangeToAppend.length == 1{
                            self.showUserTableView(show: false)
                        }
                    }
                }
              }
            }
        }
        
        func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        // change    transparentBGView.isHidden = false
        // change    postButton.isSelected = true
            return true
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
           // change transparentBGView.isHidden = true
           // change postButton.isSelected = false
        }
        
        func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
            if touch.view?.isDescendant(of: self.suggestionsTableView) == true {
                return false
            }
            return true
        }
    
    
    
    @objc func handle(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            showUserTableView(show: false)
        }
        sender.cancelsTouchesInView = false
    }
    
    
    
    func setIdAndNameUser(id:String, name:String) {
            
            isupdateTable = false
            
            var str : NSMutableString = NSMutableString(string:inputTextView.text)
            let newString = "@" + name + " "
            str.replaceCharacters(in: rangeToAppend, with: newString)
            //str = str.replacingCharacters(in: rangeToAppend, with: newString) as! NSMutableString
            inputTextView.text = str.copy() as? String ?? ""
            
    //        let commentTemp = self.descriptionTextView.text.replacingOccurrences(of: "@", with: "@")
           // let comment =  self.descriptionTextView.text.replacingOccurrences(of: mutString as String, with:"")
            
            let name = name.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            print(name)
            print(id)
            
            if self.addName.contains(name) {
                
            } else {
                self.addName.add(name)
                self.idToSendApi.add(id)
            }
            
    //        self.descriptionTextView.text = ""
    //        //self.commentTextView.textColor = UIColor.black
    //        self.descriptionTextView.text = "\(comment)\(name)"
            //setRedColor(text: "\(comment)\(name)")
            self.showUserTableView(show: false)
        }
    
    func showUserTableView(show:Bool){
        self.suggestionsTableView.isHidden = !show
        
        if show {
            tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handle(sender:)))
            tapGesture!.delegate = self
            self.view.addGestureRecognizer(tapGesture!)
        }else{
            if tapGesture != nil {
                self.view.removeGestureRecognizer(tapGesture!)
            }
            if self.userList.count > 0 {
                self.userList.removeAll()
            }
            if self.hashTagList.count > 0 {
                self.hashTagList.removeAll()
            }
        }
        
        self.suggestionsTableView.reloadData()
        
        if isUserTable {
            
            if self.userList.count > 5 {
                self.suggestionHeightConstraint.constant = 250
            }else{
                if self.userList.count < 4 {
                    self.suggestionHeightConstraint.constant = 150.0
                }else{
                    self.suggestionHeightConstraint.constant = CGFloat(self.userList.count * 50)
                }
                
            }
        }else{
            if self.hashTagList.count > 5 {
                self.suggestionHeightConstraint.constant = 250
            }else{
                if self.hashTagList.count < 4 {
                    self.suggestionHeightConstraint.constant = 150.0
                }else{
                    self.suggestionHeightConstraint.constant = CGFloat(self.hashTagList.count * 50)
                }
                
            }
        }
    }
    
    //MARK: POPover
    func setPopOver(textToSeach :String){
        let userLogin = CommonFunctions.getuserFromDefaults()
                createEditVM = CreateEditPost_VM()
                
                createEditVM?.userId = userLogin.userId
                createEditVM?.perPage = "10"
                createEditVM?.page = "1"
                createEditVM?.nameStr = textToSeach
                createEditVM?.callGetUserSearchListService = true
                createEditVM?.responseRecieved = {
                    self.userList = self.createEditVM!.userList
                    // refresh content
                    self.isUserTable = true
                    if self.userList.count == 0 {
                        self.showUserTableView(show: false)
                    }else{
                        self.showUserTableView(show: true)
                    }
                    //self.setPetViewHeight()
                }
                
                createEditVM?.showAlertClosure = {[weak self] () in
                    DispatchQueue.main.async {
                        if let message = self?.createEditVM?.alertMessage {
                            self?.showAlert(self ?? UIViewController(), message: message)
                            self?.userList.removeAll()
                            self?.showUserTableView(show: false)
                        }
                    }
                }
    }
    
    //MARK: POPover
    func setPopOverForHashTag(textToSeach :String){
        
        if currentWord.length < 1 {
            return
        }
        var searchText = currentWord
        searchText.remove(at: searchText.startIndex)
        
        
        let userLogin = CommonFunctions.getuserFromDefaults()
                createEditVM = CreateEditPost_VM()
                
                createEditVM?.userId = userLogin.userId
                createEditVM?.nameStr = searchText
                createEditVM?.callGetHashTAGListService = true
                createEditVM?.responseRecieved = {
                    self.hashTagList = self.createEditVM!.hashTagList
                    // refresh content
                    self.isUserTable = false
                    
                    
                    if self.hashTagList.count == 0 {
                        self.showUserTableView(show: false)
                    }else{
                        self.showUserTableView(show: true)
                    }
                    //self.setPetViewHeight()
                }
                
                createEditVM?.showAlertClosure = {[weak self] () in
                    DispatchQueue.main.async {
                        if let message = self?.createEditVM?.alertMessage {
                            self?.showAlert(self ?? UIViewController(), message: message)
                            self?.userList.removeAll()
                            self?.showUserTableView(show: false)
                        }
                    }
                }
    }
    
    
    
    
}


extension PostDetailVC : HashTagUserMentionProtocol {
   
    
    func hashTagPressed(hashTag:String){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_PostHashTag = storyboard.instantiateViewController(withIdentifier: "HashTagPostsVC") as! HashTagPostsVC
        vc_PostHashTag.hashTag = hashTag
        self.navigationController?.delegate = nil
        self.navigationController?.pushViewController(vc_PostHashTag, animated: true)
    }
    
    func profilePressed(userId:Int) {
        self.goToProfileView(userId: userId)
    }
        
}

extension PostDetailVC : ForumHeaderCellPostProtocol {
    
    
    func petPressed(pet:PetList,isMYPet:Bool) {
        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
        let vc_feed = storyboardPet.instantiateViewController(withIdentifier: "FeedVC") as! FeedVC
        vc_feed.isFromOtherScreen = true
        vc_feed.petObject = pet
        vc_feed.isMYPet = isMYPet
        self.navigationController?.delegate = nil
        self.navigationController?.pushViewController(vc_feed, animated: true)
    }
    
    
    
    func petListPressed(petlist :[PetList],isMYPet:Bool){
        let storyboardPet = UIStoryboard(name: "Main", bundle: nil)
        let vc_PetList = storyboardPet.instantiateViewController(withIdentifier: "PetListVC") as! PetListVC
        vc_PetList.petlist = petlist
        vc_PetList.isMYPet = isMYPet
        self.navigationController?.delegate = nil
        self.navigationController?.pushViewController(vc_PetList, animated: true)
    }
    
    func videoButtonPressed(){
        
        
        if let videoURL = self.post?.media?.first{
            let player = AVPlayer(url: URL(string:videoURL)!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        }
    }
    
    func likeButtonPressed(cell: ForumHeaderCell) {
        self.likeButtonPressed()
    }
    
    func shareButtonPressed(cell: ForumHeaderCell) {
            var description = ""
             description = cell.descriptionLabel.text ?? ""
            let mediaLink =  cell.post?.media?.first ?? ""
        
            let text = "Checkout this post!! \n \(description)\n \(mediaLink)\n\n Let’s make this world a happier and better place for innocent beings, who do the same for us! \n Download https://play.google.com/ http://itunes.apple.com/"
            //let shareimage = UIImage(named: "Pet")
          
            let imageToShare = [text] as [Any]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.setValue("Cuddle | Check out", forKey: "subject")
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            //        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
    }
    
    func doubleTAPLikePressed(cell: ForumHeaderCell) {
        let post = self.post!
        
        if post.isLike! == 0{
            // like api
            let userLogin = CommonFunctions.getuserFromDefaults()
            createEditVM = CreateEditPost_VM()
            
            
            createEditVM?.userId = userLogin.userId
            createEditVM?.postId = "\(post.postID!)"
            
            createEditVM?.updateLoadingStatus = { [weak self] () in
                DispatchQueue.main.async {
                    if self?.createEditVM?.isLoading ?? false {
                      //  LoaderView.shared.showLoader()
                    } else {
                      //  LoaderView.shared.hideLoader()
                    }
                }
            }
            
            createEditVM?.responseRecieved = {
                // refresh cell button
                post.isLike = 1
                post.likeCount! += 1
                cell.setUpPost(post: post)
                self.setPost?(post)
            }
            
            createEditVM?.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = self?.createEditVM?.alertMessage {
                        self?.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            
            createEditVM?.callUnLikePostService = true
        }else{
            //unlike api
            let userLogin = CommonFunctions.getuserFromDefaults()
            createEditVM = CreateEditPost_VM()
            
            
            createEditVM?.userId = userLogin.userId
            createEditVM?.postId = "\(post.postID!)"
            
            createEditVM?.updateLoadingStatus = { [weak self] () in
                DispatchQueue.main.async {
                    if self?.createEditVM?.isLoading ?? false {
                       // LoaderView.shared.showLoader()
                    } else {
                       // LoaderView.shared.hideLoader()
                    }
                }
            }
            
            createEditVM?.responseRecieved = {
                // refresh button like
                post.isLike = 0
                post.likeCount! -= 1
                cell.setUpPost(post: post)
            }
            
            createEditVM?.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = self?.createEditVM?.alertMessage {
                        self?.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            createEditVM?.callUnLikePostService = true
        }
    }
    
    
}
