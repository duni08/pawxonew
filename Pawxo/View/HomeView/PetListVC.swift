//
//  PetListVC.swift
//  Pawxo
//
//  Created by 42works on 05/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class PetListVC: UIViewController {
    
    
    var petlist = [PetList]()
    
    var cellIdentifier = "PetListingCell"
    
    var isMYPet = false
    
    @IBOutlet weak var petListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}



extension PetListVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return petlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PetListingCell =  tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PetListingCell
        let pet = self.petlist[indexPath.row]
        cell.setPet(pet:pet)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let pet = self.petlist[indexPath.row]
        let storyboardPet = UIStoryboard(name: "Pet", bundle: nil)
        let vc_feed = storyboardPet.instantiateViewController(withIdentifier: "FeedVC") as! FeedVC
        vc_feed.petObject = pet
        vc_feed.isFromOtherScreen = true
        vc_feed.isMYPet = self.isMYPet
        self.navigationController?.pushViewController(vc_feed, animated: true)
        
        //self.navigationController?.pushViewController(vc_PostDetail, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
}
