//
//  PostLikesListVC.swift
//  Pawxo
//
//  Created by 42works on 07/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class PostLikesListVC: UIViewController {

    
    @IBOutlet weak var likesTableView: UITableView!
    
    
    @IBOutlet weak var noResultsView: UIView!
    
    @IBOutlet weak var noInternetView: UIView!
    
    var userlist = [Userlist]()
    var usersListCellIdentifier = "UsersListCell"
    
    var likelist_VM = LikesList_VM()
    
    var unFollowRequest: (()->())?
    
    var post : PostList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        likesTableView.register(UINib(nibName: usersListCellIdentifier, bundle: nil), forCellReuseIdentifier: usersListCellIdentifier)
        // Do any additional setup after loading the view.
        likesTableView.contentInset = UIEdgeInsets(top: 7.5, left: 0, bottom: 0, right:0)
    }
    
    
    
    @IBAction func tryAgainPressed(_ sender: Any) {
        self.getLikeList()
        
    }
    
    
    
    
    func getLikeList(){
        
        if !(WebServices().isInternetWorking()){
            self.noInternetView.isHidden = false
            self.userlist.removeAll()
            self.likesTableView.addSubview(self.noInternetView)
            self.likesTableView.reloadData()
            return
        }else{
            self.noInternetView.isHidden = true
        }
        
        let pid = post?.postID!
        
        likelist_VM.postId = "\(pid!)"
        let userLogin = CommonFunctions.getuserFromDefaults()
        likelist_VM.userId = userLogin.userId
        likelist_VM.perPage = "50"
        likelist_VM.page = "1"
        likelist_VM.callGetUserListService = true
        
        likelist_VM.responseRecieved = {
            self.userlist = self.likelist_VM.userList
            // refresh content
            if self.userlist.count > 0{
                self.noResultsView.isHidden = true
            }else{
                self.noResultsView.isHidden = false
            }
            self.likesTableView.addSubview(self.noResultsView)
            self.likesTableView.reloadData()
        }
        
        likelist_VM.showAlertClosure = {
            DispatchQueue.main.async {
                if let message = self.likelist_VM.alertMessage {
                    //self.showAlert(self, message: message)
                    if self.userlist.count > 0{
                        self.noResultsView.isHidden = true
                    }else{
                        self.noResultsView.isHidden = false
                    }
                    self.likesTableView.addSubview(self.noResultsView)
                    self.likesTableView.reloadData()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getLikeList()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func goToProfileView(userId:Int){
        let storyboardProfile = UIStoryboard(name: "Profile", bundle: nil)
        let vc_Profile = storyboardProfile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc_Profile.isFromOtherScreen = true
        vc_Profile.userId = "\(userId)"
        self.navigationController?.pushViewController(vc_Profile, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension PostLikesListVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UsersListCell =  tableView.dequeueReusableCell(withIdentifier: usersListCellIdentifier, for: indexPath) as! UsersListCell
        cell.delegate = self
        //cell.homeCellDelegate = self
        let user = self.userlist[indexPath.row]
        cell.setUpUser(user: user)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc_PostDetail = storyboard.instantiateViewController(withIdentifier: "PostDetailVC") as! PostDetailVC
        //self.navigationController?.pushViewController(vc_PostDetail, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
}



extension PostLikesListVC : UsersListCellProtocol {
    func followbuttonPressed(cell:UsersListCell){
        var follow_VM = Follow_VM()
        follow_VM.userId = CommonFunctions.getuserFromDefaults().userId
        follow_VM.followerId = "\((cell.user?.id)!)"
        
        let status = cell.profileStatus
        
        if status == .following {
            //Show Alert
            let user = cell.user!
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let unfollow = storyboard.instantiateViewController(withIdentifier: "UnFollowAlertVC") as! UnFollowAlertVC
            unfollow.nameStr = user.name ?? ""
            unfollow.imageStr = user.userImage ?? ""
            unfollow.postLikeDelegate = self
            unfollow.modalPresentationStyle = .overCurrentContext
            unfollow.modalTransitionStyle = .crossDissolve
            self.present(unfollow, animated: true, completion: nil)
            
            
            self.unFollowRequest = {
                
                print("UnFollow Accepted")
                
                follow_VM.responseRecieved = {
                    let user = cell.user!
                    user.isFollowed = 0
                    cell.setUpUser(user: user)
                }
                follow_VM.showAlertClosure = {
                    DispatchQueue.main.async {
                        if let message = follow_VM.alertMessage {
                            self.showAlert(self, message: message)
                        }
                    }
                }
                
                follow_VM.updateLoadingStatus = {
                    DispatchQueue.main.async {
                        if follow_VM.isLoading {
                            LoaderView.shared.showLoader()
                        } else {
                            LoaderView.shared.hideLoader()
                        }
                    }
                }
                
                follow_VM.callFollowerService = true
                
                
            }
            
        }else if status == .requested{
            
            follow_VM.responseRecieved = {
                let user = cell.user!
                 user.isRequested = "0"
                cell.setUpUser(user: user)
            }
            
            follow_VM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = follow_VM.alertMessage {
                        self!.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            
            follow_VM.callFollowerService = true
            
        }
        else
        {
            
            follow_VM.responseRecieved = {
                let user = cell.user!
                
                if let isPrivate = user.isProfilePrivate, isPrivate.boolValue == true {
                    user.isRequested = "1"
                }else{
                    // public check for follow unfoolow
                    user.isFollowed = 1
                }
                cell.setUpUser(user: user)
            }
            
            follow_VM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = follow_VM.alertMessage {
                        self!.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            
            follow_VM.callFollowerService = true
        }
    }
    
   func profilePressed(cell:UsersListCell) {
        self.goToProfileView(userId:cell.user!.id!)
    }
    
    
}
