//
//  EditLocationVC.swift
//  Pawxo
//
//  Created by Apple on 17/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces


protocol EditLocationVCProtocol {
    func locationDetails(lat:Double,long:Double,address:String)
}


class EditLocationVC: UIViewController {

    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var isEdit = false
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var currentLocationView: UIView!
    @IBOutlet weak var changeLocationView: UIView!
    @IBOutlet weak var confirmLocationBtn: UIButton!
    @IBOutlet weak var searchLocation_tf: UITextField!
    @IBOutlet weak var recentList_tbl: UITableView!
    @IBOutlet weak var googleAutoComplete_tbl: UITableView!
    
    var delegate: EditLocationVCProtocol?
    
    @IBOutlet weak var contentHeight: NSLayoutConstraint!
    
    var googleAutoCompleteArray = [GoogleAutoComplete]()
    
    var isChoosedCurrentLocation = false
    
    @IBOutlet weak var locationNameLabel: UILabel!
    
    var currentLocationNameString = ""
    var currentLongitude : Double = 0
    var currentLatitude : Double = 0
    
    var locationSelected :((Double,Double,String)->())?
    
    @IBOutlet weak var googleTableHeightConnstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //setUpView()
        self.titleLabel.text = (self.isEdit == true) ? "Edit Location" :"Choose Location"
       // setUpView(latitude: -33.86, longitude: 151.20)
        self.locationNameLabel.text = "Select Your Location"
        setupMapView()
        LocationManager.sharedManager.initializeLocationManager()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.locationAvailable(notification:)), name: Notification.Name("LocationAvailable"), object: nil)
    }
    
    
    func getLocationPermissionStatus(){
        let locationManager = CLLocationManager()
        let locStatus = CLLocationManager.authorizationStatus()
        switch locStatus {
           case .notDetermined:
              locationManager.requestWhenInUseAuthorization()
           return
           case .denied, .restricted:
              let alert = UIAlertController(title: "Location Services are disabled", message: "Please enable Location Services in your Settings", preferredStyle: .alert)
              let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
              alert.addAction(okAction)
              present(alert, animated: true, completion: nil)
           return
           case .authorizedAlways, .authorizedWhenInUse:
           break
        }
    }
    
    @objc private func locationAvailable(notification: Notification) {
        // No need to refresh the screen automatically if data is already present.
        
        if self.isChoosedCurrentLocation == true || true {
            self.currentLatitude = LocationManager.sharedManager.latitude
            self.currentLongitude = LocationManager.sharedManager.longitude
            self.setupMapView()
            //self.setUpView(latitude: self.currentLatitude, longitude: self.currentLongitude)
            self.getAddressFromLatLon(pdblLatitude: self.currentLatitude, withLongitude: self.currentLongitude) { (address) in
                self.locationNameLabel.text = address
                self.currentLocationNameString = address
                LocationManager.sharedManager.stopUpdating()
                
            }
        }
    }
    
    
    
    func setupMapView(){
            
            
        if  self.currentLongitude == 0 {
                print("empty live Device lat long")
                mapView.clear()
                return
            }
            
            mapView.clear()
            
        let latitude : CLLocationDegrees = currentLatitude
            
        let longitude : CLLocationDegrees = currentLongitude
            
            let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 16.0)
            
            let m = GMSMarker(position: camera.target)
            
        
            
            if self.isMarkerWithinScreen(marker: m) == false{
                let deadline = DispatchTime.now() + 1.0
                        DispatchQueue.main.asyncAfter(deadline: deadline) { [weak self] in
                            self?.mapView.animate(toLocation: m.position)
                            self?.mapView.animate(toZoom: 16)
                        }
            }
          //  mapView.animate(toLocation: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
    //        let deadline = DispatchTime.now() + 2.0
    //        DispatchQueue.main.asyncAfter(deadline: deadline) { [weak self] in
    //            self?.mapView.animate(toLocation: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
    //        }
            
            //custom marker image
            let pulseRingImg = UIImageView(frame: CGRect(x: -30, y: -30, width: 78, height: 78))
            pulseRingImg.image = UIImage(named: "Pulse")
            pulseRingImg.isUserInteractionEnabled = false
            CATransaction.begin()
            CATransaction.setAnimationDuration(3.5)
            
            //transform scale animation
            var theAnimation: CABasicAnimation?
            theAnimation = CABasicAnimation(keyPath: "transform.scale.xy")
            theAnimation?.repeatCount = Float.infinity
            theAnimation?.autoreverses = false
            theAnimation?.fromValue = Float(0.0)
            theAnimation?.toValue = Float(2.0)
            theAnimation?.isRemovedOnCompletion = false
            
            pulseRingImg.layer.add(theAnimation!, forKey: "pulse")
            pulseRingImg.isUserInteractionEnabled = false
            CATransaction.setCompletionBlock({() -> Void in
                
                //alpha Animation for the image
                let animation = CAKeyframeAnimation(keyPath: "opacity")
                animation.duration = 3.5
                animation.repeatCount = Float.infinity
                animation.values = [Float(2.0), Float(0.0)]
                m.iconView?.layer.add(animation, forKey: "opacity")
            })
            
            CATransaction.commit()
            //m.iconView = pulseRingImg
            //m.layer.addSublayer(pulseRingImg.layer)
            m.map = mapView
            //m.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        }
        
        
        
        
        func isMarkerWithinScreen(marker: GMSMarker) -> Bool {
            let region = self.mapView.projection.visibleRegion()
            let bounds = GMSCoordinateBounds(region: region)
            return bounds.contains(marker.position)
        }
    
    
    
    func workHard(enterDoStuff: (Bool) -> Void) {
     // Replicate Downloading/Uploading
     for _ in 1...1000 {
      print("👷🏻‍👷🏻👷🏽👷🏽️👷🏿‍️👷🏿")
     }
     enterDoStuff(true)
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double, completionGet: @escaping (String) -> Void){
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        //21.228124
        let lon: Double = pdblLongitude
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon

        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)


        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]

                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
                    print(pm.subThoroughfare)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }

                    print(addressString)
                    completionGet(addressString)
              }
        })

    }
    
    
    @IBAction func confirmLocationPressed(_ sender: Any) {
        print(currentLongitude)
        print(currentLatitude)
        print(currentLocationNameString)
        
        if self.locationNameLabel.text == "Select Your Location"{
            let locationManager = CLLocationManager()
            let locStatus = CLLocationManager.authorizationStatus()
            switch locStatus {
               case .notDetermined:
                  locationManager.requestWhenInUseAuthorization()
               return
               case .denied, .restricted:
                  let alert = UIAlertController(title: "Location Services are disabled", message: "Please enable Location Services in your Settings", preferredStyle: .alert)
                  let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                  alert.addAction(okAction)
                  present(alert, animated: true, completion: nil)
               return
               case .authorizedAlways, .authorizedWhenInUse:
               break
            }
        }
        
        if self.locationNameLabel.text == "Select Your Location"{
            return
        }
        self.locationSelected?(currentLatitude,currentLongitude,currentLocationNameString)
        delegate?.locationDetails(lat: currentLatitude, long: currentLongitude, address: currentLocationNameString)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    
    
    
    
    @IBAction func useCurrentLocationPressed(_ sender: Any) {
        self.isChoosedCurrentLocation = true
        
        self.locationNameLabel.text = "Select Your Location"
        self.closeTappedButton(UIButton())
        self.view.endEditing(true)
        let locationManager = CLLocationManager()
        let locStatus = CLLocationManager.authorizationStatus()
        switch locStatus {
           case .notDetermined:
              locationManager.requestWhenInUseAuthorization()
           return
           case .denied, .restricted:
              let alert = UIAlertController(title: "Location Services are disabled", message: "Please enable Location Services in your Settings", preferredStyle: .alert)
              let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
              alert.addAction(okAction)
              present(alert, animated: true, completion: nil)
           return
           case .authorizedAlways, .authorizedWhenInUse:
           break
        }
        
        LocationManager.sharedManager.initializeLocationManager()
        
        
        self.setupMapView()
        
    }
    
    
    
    func useCurrentLocation(){
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("LocationAvailable"), object: nil)
    }
    
    
    func setUpView(latitude:Double,longitude:Double) {
        
        confirmLocationBtn.layer.cornerRadius = confirmLocationBtn.frame.height / 2
        confirmLocationBtn.layer.masksToBounds = true
        
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 6.0)
         
         mapView.camera = camera
         
         let myView = UIView.init(frame: CGRect(x: 0, y: 0, width: 40 , height: 40))
         myView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
         myView.cornerRadius = myView.frame.height / 2
         myView.layer.masksToBounds = true
        
         
         let imageViewSelect = UIImageView.init(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
         imageViewSelect.image = #imageLiteral(resourceName: "selectionIcon")
         //addRippleEffect(to: myView)
         myView.addSubview(imageViewSelect)
        
         let marker = GMSMarker()
         marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
         marker.title = "Sydney"
         marker.snippet = "Australia"
         
         marker.iconView = myView
         marker.map = mapView
    }
    
    
    @IBAction func changeTappedButton(_ sender:UIButton) {
        
        self.contentHeight.constant = self.view.frame.size.height - self.view.safeAreaInsets.top - 44
        currentLocationView.isHidden = true
        changeLocationView.isHidden = false
    
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func closeTappedButton(_ sender:UIButton) {
        self.contentHeight.constant = 250
        currentLocationView.isHidden = false
        changeLocationView.isHidden = true
        self.searchLocation_tf.text = ""
        self.googleAutoComplete_tbl.isHidden = true
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
//    func addRippleEffect(to referenceView: UIView) {
//        /*! Creates a circular path around the view*/
//        let path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: referenceView.bounds.size.height))
//        /*! Position where the shape layer should be */
//        let shapePosition = CGPoint(x: referenceView.bounds.size.width / 2.0, y: referenceView.bounds.size.height / 2.0)
//        let rippleShape = CAShapeLayer()
//        rippleShape.bounds = CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: referenceView.bounds.size.height)
//        rippleShape.path = path.cgPath
//        rippleShape.fillColor = UIColor.red.cgColor
//        rippleShape.strokeColor = UIColor.yellow.cgColor
//        rippleShape.lineWidth = 1
//        rippleShape.position = shapePosition
//        rippleShape.opacity = -10//-10
//
//        /*! Add the ripple layer as the sublayer of the reference view */
//        referenceView.layer.addSublayer(rippleShape)
//        /*! Create scale animation of the ripples */
//        let scaleAnim = CABasicAnimation(keyPath: "transform.scale")
//        scaleAnim.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
//        scaleAnim.toValue = NSValue(caTransform3D: CATransform3DMakeScale(2, 2, 1))
//
//        /*! Create animation for opacity of the ripples */
//        let opacityAnim = CABasicAnimation(keyPath: "opacity")
//        opacityAnim.fromValue = 1
//        opacityAnim.toValue = nil
//        /*! Group the opacity and scale animations */
//        let animation = CAAnimationGroup()
//        animation.animations = [scaleAnim, opacityAnim]
//        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
//        animation.duration = CFTimeInterval(1.12)
//        animation.repeatCount = 200
//        animation.isRemovedOnCompletion = true
//        rippleShape.add(animation, forKey: "rippleEffect")
//    }
    
    
    func googleAutoCompleteApi(qurey:String) {
        
        var searchQurey = qurey
        searchQurey = searchQurey.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
        let googleAutoCompleteUrl = "\(ApiServiceName.googleAutoComplete)?input=\(searchQurey )&key=AIzaSyBa-LT2cQTQwg-xo5OFl7RQ5z0OxOo38as&types=(cities)"
        
        let param = ["input":"\(qurey)","key":"AIzaSyBa-LT2cQTQwg-xo5OFl7RQ5z0OxOo38as","types":"(cities)"]
        
        
        WebServices().googleAutoComplete(methodName: googleAutoCompleteUrl, params: param as NSDictionary, oncompletion: { (status, message, response) in
            
            if status == "OK" {
                self.googleAutoCompleteArray.removeAll()
                let googleArray = response?.object(forKey: "predictions") as? NSArray
                for googleUser in googleArray! {
                    let googleQurey = GoogleAutoComplete.init(googleInfo: googleUser as! NSDictionary)
                    self.googleAutoCompleteArray.append(googleQurey)
                }
                
                print("googleAutoCompleteArray--- \(self.googleAutoCompleteArray.count)")
                DispatchQueue.main.async {
                    self.googleAutoComplete_tbl.reloadData()
                }
            }else{
                
            }
            
        })
        
    }
    
    func getGooglePlaceLocationPont(searchQurey:String,placeId:String) {
        
        var finalQuery = searchQurey
        finalQuery = finalQuery.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let googleAutoCompleteUrl = "\(ApiServiceName.googleLocationPoint)?input=\(finalQuery )&placeid=\(placeId)&key=AIzaSyBa-LT2cQTQwg-xo5OFl7RQ5z0OxOo38as"
        
        let param = ["input":"\(finalQuery)","key":"AIzaSyBa-LT2cQTQwg-xo5OFl7RQ5z0OxOo38as","placeid":"\(placeId)"]
        
        WebServices().googleAutoComplete(methodName: googleAutoCompleteUrl, params: param as NSDictionary, oncompletion: { (status, message, response) in
            
            if status == "OK" {
                
                var latitudeFinal:Double?
                var longitudeFinal:Double?
                
                if let resultDic  = response?.object(forKey: "result") as? NSDictionary {
                    
                    if let geometryDic = resultDic.object(forKey: "geometry") as? NSDictionary {
                        
                        if let locationDic = geometryDic.object(forKey: "location") as? NSDictionary {
                        
                            if let latitude = locationDic.object(forKey: "lat") as? Double {
                                latitudeFinal = latitude
                            }
                            
                            if let longitude = locationDic.object(forKey: "lng") as? Double {
                                longitudeFinal = longitude
                            }
                        
                        }
                        
                        
                        //self.setUpView(latitude: latitudeFinal!, longitude: longitudeFinal!)
                        self.currentLongitude = longitudeFinal!
                        self.currentLatitude = latitudeFinal!
                        self.setupMapView()
                        self.searchLocation_tf.text = ""
                        self.contentHeight.constant = 250
                        self.currentLocationView.isHidden = false
                        self.changeLocationView.isHidden = true
                        if let addressStr = resultDic.object(forKey: "formatted_address") as? String{
                            self.locationNameLabel.text = addressStr
                            self.currentLocationNameString = addressStr
                        }
                        
                        UIView.animate(withDuration: 0.5) {
                            self.view.layoutIfNeeded()
                        }
                    }
                }
                
            }else{
                
            }
            
        })
        
    }
    
    
}

extension EditLocationVC:UITableViewDataSource,UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == googleAutoComplete_tbl {
            
            if self.googleAutoCompleteArray.count > 5 {
                self.googleTableHeightConnstraint.constant = 205
            }else{
                self.googleTableHeightConnstraint.constant = CGFloat(41 * self.googleAutoCompleteArray.count)
            }
            
            return self.googleAutoCompleteArray.count
            
        }else{
          return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == googleAutoComplete_tbl {
           let cell:GoogleAutocompleteCell = (tableView.dequeueReusableCell(withIdentifier: "GoogleAutocompleteCell") as? GoogleAutocompleteCell)!
            
            cell.locationTitle_lbl.text = self.googleAutoCompleteArray[indexPath.row].title
            
           return cell
        }
        
        let cell:RecentLocationCell = (tableView.dequeueReusableCell(withIdentifier: "RecentLocationCell") as? RecentLocationCell)!
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == googleAutoComplete_tbl {
            self.searchLocation_tf.text = self.googleAutoCompleteArray[indexPath.row].title
            googleAutoComplete_tbl.isHidden = true
        
            let googlePlaceId = self.googleAutoCompleteArray[indexPath.row].placeId
            self.view.endEditing(true)
            self.getGooglePlaceLocationPont(searchQurey:self.searchLocation_tf.text! , placeId: googlePlaceId!)
            
        }
    }
    
}


extension EditLocationVC:UITextFieldDelegate {
 
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        let acController = GMSAutocompleteViewController()
//                   acController.delegate = self
//                   present(acController, animated: true, completion: nil)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
           let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
        
            if updatedText.isEmpty {
                self.googleAutoComplete_tbl.isHidden = true
                self.recentList_tbl.isHidden = false
            }else{
                self.googleAutoComplete_tbl.isHidden = false
                self.recentList_tbl.isHidden = true
            }
            
            
            googleAutoCompleteApi(qurey: updatedText)
        }
        return true
    }
    
    
    
}

extension EditLocationVC: GMSAutocompleteViewControllerDelegate {
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    // Get the place name from 'GMSAutocompleteViewController'
    // Then display the name in textField
    
    searchLocation_tf.text = place.name
    
    // Dismiss the GMSAutocompleteViewController when something is selected
    dismiss(animated: true, completion: nil)
  }
  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // Handle the error
    print("Error: ", error.localizedDescription)
  }
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    // Dismiss when the user canceled the action
    dismiss(animated: true, completion: nil)
  }
}
