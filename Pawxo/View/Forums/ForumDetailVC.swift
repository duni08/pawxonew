//
//  PostDetailVC.swift
//  Pawxo
//
//  Created by 42works on 07/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import IQKeyboardManager
import ActiveLabel
import AVKit

class UserNew:Codable
{
    var firstName:String?
    var lastName:String?
    var country:String?

    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case country
    }
}

protocol ForumDetailVCProtocol {
    func setNavigationDelegate()
}

class ForumDetailVC: UIViewController,UIGestureRecognizerDelegate {
    
    
    
    
    @IBOutlet weak var optionsButton: UIButton!
    
    var isFromProfileOrPet = false
    
    @IBOutlet weak var optionsOuterView: UIView!
    
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var navigationLabel: SemiBoldLabel!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var cuddlLogo: UIImageView!
    @IBOutlet weak var cuddlLeadingConstraint: NSLayoutConstraint!
    
    var delegate : ForumDetailVCProtocol?
    
    @IBOutlet weak var postDetailTableView: UITableView!
    
    var tempLabel : ActiveLabel!
    
    @IBOutlet weak var descriptionBottomconstraint: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    var staticCount = 15
    
    var postCommentIdentifier = "CommentCell"
    var forumHeaderIdentifier = "ForumDetailsHeaderCell"
    
    @IBOutlet weak var placeHolderLabel: LightLabel!
    
    @IBOutlet weak var boxViewHeightconstraint: NSLayoutConstraint!
    @IBOutlet weak var inputTextView: UITextView!
    
    @IBOutlet weak var centerImageView: UIImageView!
    
    @IBOutlet weak var cView: UIView!
    
    var forumList : ForumList?
    
    var categories = [Category]()
    @IBOutlet weak var suggestionHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var suggestionsTableView: UITableView!
    var selectedIndex = 0
    var ticketTop : NSLayoutConstraint?
    var aTop : NSLayoutConstraint?
    let aView = UIView()
    var imageCenter : UIImage?
    var imageFrame : CGRect?
    var showHeader = false
    var text = "Jason Stathom buy four new dogs for their petclub @Feelinghappy This is a test comment and this is a ne wtest comeent agaainaadsfs fasfkjf lsfj aslfjslfj al"
    
    var commentList = [ForumCommentList]()
    
    var userList = [Userlist]()
    var taggedUserList = [Userlist]()
    var tapGesture : UITapGestureRecognizer?
    var globalRange = NSRange()
    var mutString = NSMutableString()
    var isupdateTable = false
    var globalInt : Int = 1
    var addName = NSMutableArray()
    var idToSendApi = NSMutableArray()
    var currentWord : String = ""
    var updatedWord : String = ""
    var rangeToAppend = NSRange()
    var removeCount : Int = 0
    
    var page = 1
    var pageLimit = 25
    var refresh = UIRefreshControl()
    var hasMore = false
    
    var IsloadedComments = true
    
    
    var deletedForum : (()->())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ViewDi post details")
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
        tempLabel.numberOfLines = 0
        tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        self.centerImageView.image = imageCenter
        //self.postDetailTableView.contentInset = UIEdgeInsets(top: (self.imageFrame?.size.height)! -  CGFloat(35), left: 0, bottom: 0, right: 0)
        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
        postDetailTableView.register(UINib(nibName: forumHeaderIdentifier, bundle: nil), forCellReuseIdentifier: forumHeaderIdentifier)
        postDetailTableView.register(UINib(nibName: postCommentIdentifier, bundle: nil), forCellReuseIdentifier: postCommentIdentifier)
        if inputTextView.text.count > 0 {
            placeHolderLabel.alpha = 0
        }else{
            placeHolderLabel.alpha = 1
        }
        
        self.inputTextView.textColor = AppColor.colorGreySubHeading
        placeHolderLabel.textColor = AppColor.colorGreyTime
//       // inputTextView.maxLength = 140
//        inputTextView.trimWhiteSpaceWhenEndEditing = false
//        inputTextView.placeholder = "Type your message.."
//        inputTextView.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
//        inputTextView.minHeight = 25.0
//        inputTextView.maxHeight = 70.0
//        inputTextView.backgroundColor = UIColor.clear
//        inputTextView.layer.cornerRadius = 4.0
        postDetailTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right:0)
        refresh.tintColor = UIColor.clear
        refresh.backgroundColor = UIColor.clear
        refresh.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refresh.addTarget(self, action:#selector(refreshTable), for: UIControl.Event.valueChanged)
        // Add Custom Loader
        var loader = UIImageView()
        let gif = UIImage.gifImageWithName("gif")
        loader.center = self.view.center
        loader = UIImageView(frame: CGRect(x: self.view.frame.size.width/2 - 20, y: refresh.center.y, width: 40, height: 40))
        loader.image = gif
        refresh.insertSubview(loader, at: 0)
        
        setOptionsView()
        
        self.postDetailTableView.addSubview(refresh)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.getForumDetails(forumId:"\((self.forumList?.id)!)")
        }
        
         if isFromProfileOrPet {
           //  self.cuddlLogo.isHidden = true
             let rect = self.cuddlLogo.frame
             self.cuddlLogo.frame = CGRect(x: (self.view.frame.width/2) - 56, y: rect.origin.y, width: rect.width, height:rect.height)
         }
       
    }
    
    
    
    @objc private func refreshTable() {
        page = 1
        getCommentsList()
        self.getForumDetails(forumId:"\((self.forumList?.id)!)")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewwilla[ppear")
        if self.isFromProfileOrPet{
            
            self.centerImageView.isHidden = true
            self.showHeader = true
            self.postDetailTableView.reloadData()
        }else{
            self.setUpConstraintForImageView()
        }
//        self.setUpConstraintForImageView()
        self.getCommentsList()
    }
    
    func videoButtonPressed(url:String?){
        
        if let videoURL = url{
            let player = AVPlayer(url: URL(string:videoURL)!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let _ = scrollView as? UITableView {
            bottomRefresh(scrollView)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            if let _ = scrollView as? UITableView {
                bottomRefresh(scrollView)
            }
        }
    }
    
    func bottomRefresh(_ scrollView : UIScrollView)
    {
        let scrollViewHeight = scrollView.frame.size.height;
        let scrollContentSizeHeight = scrollView.contentSize.height;
        let scrollOffset = scrollView.contentOffset.y;
        if (scrollOffset >  scrollContentSizeHeight - scrollViewHeight)
        {
            if IsloadedComments {
                page = page + 1
                
                if self.hasMore{
                    self.getCommentsList()
                }
            }
        }
    }
    
    
    @IBAction func optionsButtonPressed(_ sender: Any) {
        
        self.optionsOuterView.isHidden = false
    }
    
    func setOptionsView(){
        if self.forumList?.user?.id == Int(CommonFunctions.getuserFromDefaults().userId!){
            self.optionsButton.isHidden = false
        }else{
            self.optionsButton.isHidden = true
        }
    }
    
    
    @IBAction func hideOptionsViewPressed(_ sender: Any) {
        self.optionsOuterView.isHidden = true
    }
    
    
    @IBAction func editForumPressed(_ sender: Any) {
         self.optionsOuterView.isHidden = true
        let storyboard = UIStoryboard(name: "Forum", bundle: nil)
               let vc_AddForum = storyboard.instantiateViewController(withIdentifier: "AddForumVC") as! AddForumVC
            vc_AddForum.categories = self.categories
            vc_AddForum.forumList = self.forumList
            vc_AddForum.refreshData = {
                self.getForumDetails(forumId:"\((self.forumList?.id)!)")
                NotificationCenter.default.post(name: Notification.Name("RefreshForumList"), object: nil, userInfo: nil)
                self.postDetailTableView.reloadData()
        }
        self.navigationController?.delegate = nil
        self.navigationController?.pushViewController(vc_AddForum, animated: true)
        // Show edit view
    }
    
    
    @IBAction func deleteForumPressed(_ sender: Any) {
        
        
        
        
        self.optionsOuterView.isHidden = true
        
        
        let alertController = UIAlertController(title: AppMessages.deleteForum, message: nil, preferredStyle: UIAlertController.Style.alert)
        
        let somethingAction = UIAlertAction(title: "OK", style: .default, handler: {(alert: UIAlertAction!) in print("Report API call")
            // Delete Forum API and UI actions
            var forumVM = Forum_VM()
            forumVM.userId = CommonFunctions.getuserFromDefaults().userId
            forumVM.forumId = "\((self.forumList!.id)!)"
            
            
            forumVM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if forumVM.isLoading ?? false {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            forumVM.responseRecieved = {
                // refresh button like
                self.deletedForum?()
                self.navigationController?.popViewController(animated: true)
                //  self.postList.remove(at: indexPath!.row)
                //  self.homeTableView.deleteRows(at: [indexPath!], with: .automatic)
            }
            
            forumVM.showAlertClosure = {
                DispatchQueue.main.async {
                    if let message = forumVM.alertMessage {
                        // self.showAlert(self , message: message)//
                    }
                }
            }
            
            
            forumVM.callDeleteForumService = true
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
        
        alertController.addAction(somethingAction)
        alertController.addAction(cancelAction)
        alertController.view.tintColor = UIColor.black
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
    }
    
    
    
    
    
    
    
    func getForumDetails(forumId:String){
        
        let  forum = Forum_VM()
        let userLogin = CommonFunctions.getuserFromDefaults()
        forum.userId = userLogin.userId
        forum.forumId = "\(forumId)"
        
        
        forum.responseRecieved = {
           
            self.forumList?.setNewValues(forumList:(forum.forumLists?[0])!)
            self.postDetailTableView.reloadData()
           
        }
        
        forum.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = forum.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        forum.callshowforumList = true
        print("jnk")
    }

    



    func getCommentsList(){
                    let comment_VM = Comment_VM()
                    let userLogin = CommonFunctions.getuserFromDefaults()
                    comment_VM.userId = userLogin.userId
                    comment_VM.forumId = "\((self.forumList?.id)!)"
                    comment_VM.page = page
                    comment_VM.limit = pageLimit
                    
    //                comment_VM?.updateLoadingStatus = { [weak self] () in
    //                    DispatchQueue.main.async {
    //                        if self?.comment_VM?.isLoading ?? false {
    //                            LoaderView.shared.showLoader()
    //                        } else {
    //                            LoaderView.shared.hideLoader()
    //                        }
    //                    }
    //                }
                    
                    comment_VM.responseRecieved = {
            //            self.postList = self.homeListVM.postList
            //            // refresh content
            //            self.homeTableView.reloadData()
                        self.refresh.endRefreshing()
                        self.IsloadedComments = true
                        if self.page == 1{
                        self.commentList = comment_VM.forumCommentList
                        }else{
                            self.commentList.append(contentsOf: comment_VM.forumCommentList)
                        }
                        if comment_VM.forumCommentList.count < self.pageLimit{
                            self.hasMore = false
                        }else{
                            self.hasMore = true
                        }
                        
                        self.postDetailTableView.reloadData()
                        
                    }
                    
                    comment_VM.showAlertClosure = {
                        self.IsloadedComments = true
                        self.refresh.endRefreshing()
                        DispatchQueue.main.async {
                            if let message = comment_VM.alertMessage {
                                self.showAlert(self, message: message)
                            }
                        }
                    }
                    self.IsloadedComments = false
                    comment_VM.callFetchForumCommentsService = true
                
        }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setUpConstraintForImageView(){
        self.centerImageView.translatesAutoresizingMaskIntoConstraints = false
        self.centerImageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.centerImageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        //self.centerImageView.heightAnchor.constraint(equalToConstant: (self.imageFrame?.size.height)!).isActive = true
        self.centerImageView.heightAnchor.constraint(equalToConstant: 254).isActive = true
        
        let cons = CGFloat(254)
        //self.centerImageView.topAnchor.constraint(equalTo: self.postDetailTableView.topAnchor, constant:cons).isActive = true
        
       ticketTop =  self.centerImageView.bottomAnchor.constraint(equalTo: self.postDetailTableView.topAnchor ,constant: cons)
        ticketTop!.isActive = true
        //self.descriptionView.isHidden = true
        /// descriptionView top constraint
       // self.descriptionBottomconstraint.constant = 440//self.imageFrame!.size.height - CGFloat(35)
        
        
//        self.view.addSubview(aView)
//        aView.backgroundColor = UIColor.clear
//        aView.isUserInteractionEnabled = false
//        aView.translatesAutoresizingMaskIntoConstraints = false
//        aView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
//        aView.heightAnchor.constraint(equalToConstant: 35).isActive = true
//        aView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
//        aTop = aView.bottomAnchor.constraint(equalTo: self.centerImageView.bottomAnchor, constant:  0)
//        aTop?.isActive = true
        
//        let bView = UIView()
//        aView.addSubview(bView)
//        bView.backgroundColor = .white
//        bView.translatesAutoresizingMaskIntoConstraints = false
//        bView.layer.cornerRadius = 35
//        bView.layer.masksToBounds = true
//        aView.clipsToBounds = true
//        bView.leadingAnchor.constraint(equalTo: aView.leadingAnchor).isActive = true
//        bView.heightAnchor.constraint(equalToConstant: 100).isActive = true
//        bView.trailingAnchor.constraint(equalTo: aView.trailingAnchor).isActive = true
//        bView.topAnchor.constraint(equalTo: aView.topAnchor).isActive = true
//        aView.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
        //self.view.bringSubviewToFront(self.descriptionView)
            //self.view.bringSubviewToFront(self.aView)
            //self.view.bringSubviewToFront(self.topView)
            //self.aView.isHidden = false
            self.centerImageView.isHidden = true
            
            self.showHeader = true
            self.postDetailTableView.reloadData()
//            self.postDetailTableView?.reloadSections([0], with: .none)
//            self.postDetailTableView?.layoutSubviews()
           // print(cellView)
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        //self.bottomConstraint?.constant =  -10
    }
    
    
    
    
    
    
    
    
    @objc func keyboardNotification(notification: NSNotification) {
        
        
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            var scroll = false
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.bottomConstraint?.constant = 0.0
            } else {
                self.bottomConstraint?.constant = -((endFrame?.size.height)! - self.view.safeAreaInsets.bottom) ?? 0.0
                scroll = true
            }
            
            
            UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations: {
                self.view.layoutIfNeeded()
            }) { (success) in
                if scroll{
                    self.postDetailTableView.scrollToBottom()
                }
            }
        }
        
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        
        self.showHeader = false
        //self.postDetailTableView?.reloadSections([0], with: .none)
        
        if self.isFromProfileOrPet{
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        if let cell = self.postDetailTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ForumDetailsHeaderCell {
            cell.forumCollectionView.isHidden = true
        }
        
        self.centerImageView.isHidden = false
        delegate?.setNavigationDelegate()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func goToProfileView(userId:Int){
        let storyboardProfile = UIStoryboard(name: "Profile", bundle: nil)
        let vc_Profile = storyboardProfile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc_Profile.isFromOtherScreen = true
        vc_Profile.isMyProfile = false
        vc_Profile.userId = "\(userId)"
        self.navigationController?.delegate = nil
        self.navigationController?.pushViewController(vc_Profile, animated: true)
    }
    
    
    func deleteCommentPopUp(cell:CommentCell,indexPath:IndexPath){
        let alertController = UIAlertController(title: AppMessages.deleteComment, message: nil, preferredStyle: UIAlertController.Style.alert)
        
         let indexPathNew = self.postDetailTableView.indexPath(for: cell)
        
        let somethingAction = UIAlertAction(title: "OK", style: .default, handler: {(alert: UIAlertAction!) in print("Report API call")
             // Delete comment API
                
            
            let comment_VM = Comment_VM()
            let userLogin = CommonFunctions.getuserFromDefaults()
            comment_VM.userId = userLogin.userId
            comment_VM.commentId = "\((cell.forumComment?.id)!)"
            
            
            comment_VM.updateLoadingStatus = { [weak self] () in
                DispatchQueue.main.async {
                    if comment_VM.isLoading ?? false {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            comment_VM.responseRecieved = {
                self.commentList.remove(at: indexPathNew!.row - 1)
                if let count = self.forumList?.stats?.threads {
                    var newCount = count - 1
                    self.forumList?.stats?.threads = newCount
                }
                self.postDetailTableView.deleteRows(at: [indexPathNew!], with: .automatic)
            }
            
            comment_VM.showAlertClosure = {[weak self] () in
                DispatchQueue.main.async {
                    if let message = comment_VM.alertMessage {
                        self?.showAlert(self ?? UIViewController(), message: message)
                    }
                }
            }
            comment_VM.callDeleteForumCommentService = true
            
            
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
            
            alertController.addAction(somethingAction)
            alertController.addAction(cancelAction)
            alertController.view.tintColor = UIColor.black
            DispatchQueue.main.async {
                self.present(alertController, animated: true, completion:{})
            }
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        
        if inputTextView.text!.count == 0 {
            return
        }
        
        var comment_VM = Comment_VM()
        let userLogin = CommonFunctions.getuserFromDefaults()
        comment_VM.userId = userLogin.userId
        comment_VM.content = inputTextView.text!.trim()
        comment_VM.forumId = "\((self.forumList?.id)!)"
        var arr = [String]()
        if idToSendApi.count != 0{
            for obj in idToSendApi {
                arr.append(obj as? String ?? "")
            }
        }
        let taggedUserIdList = (arr.map{String($0)}).joined(separator: ",")
        
        comment_VM.taggedUsers = taggedUserIdList
        
        
        comment_VM.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                if comment_VM.isLoading ?? false {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        comment_VM.responseRecieved = {

            self.inputTextView.text = ""
            self.placeHolderLabel.alpha = 1
            self.idToSendApi.removeAllObjects()
            
            if let comment = comment_VM.forumCommentObj {
                if comment.approved == false{
                    self.showAlert(self, message: comment_VM.alertMessage!)
                }
                self.commentList.insert(comment, at: 0)
                if let count = self.forumList?.stats?.threads {
                    var newCount = count + 1
                    self.forumList?.stats?.threads = newCount
                }
//                self.postDetailTableView.insertRows(at: [IndexPath(row: 1, section: 0)], with: .middle)
                self.postDetailTableView.reloadData()
            }
            // Todo attachComment
            self.postDetailTableView.setContentOffset(.zero, animated:false)
            
            
        }
        
        comment_VM.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = comment_VM.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        
        self.view.endEditing(true)
        comment_VM.callAddcommentForumService = true
        
    }
    
    
    func shareButtonPressed(forumlist:ForumList){
        
        var title = ""
        title = forumlist.title ?? ""
        
        
            var descriptionPost = ""
            descriptionPost = forumlist.content ?? ""
            //let mediaLink =  cell.post?.media?.first ?? ""
            var content = ""
            
        for mediaItem in forumlist.mediaItems!{
            content.append(contentsOf: mediaItem.imageVideoUrl!)
            content.append(contentsOf: "\n")
        }
        
        
            let text = "Checkout this post!! \n \(title)\n \(content)\n \(descriptionPost) Let’s make this world a happier and better place for innocent beings, who do the same for us! \n Download https://play.google.com/ http://itunes.apple.com/"
            //let shareimage = UIImage(named: "Pet")
          
            let imageToShare = [text] as [Any]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.setValue("Cuddle | Check out", forKey: "subject")
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            //        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
    }
    
    
}


extension ForumDetailVC : UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == suggestionsTableView{
            return 50
        }else{
            if indexPath.row == 0 {
                
                tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
                tempLabel.font = UIFont.JosefinSansSemiBold(size: 16.0)
                tempLabel.numberOfLines = 0
                tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                tempLabel.text = self.forumList!.title ?? ""
                tempLabel.sizeToFit()
                
                let calculatedHeight = tempLabel.frame.height
                
                
                tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
                tempLabel.font = UIFont.JosefinSansSemiBold(size: 16.0)
                tempLabel.numberOfLines = 0
                tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                tempLabel.text = self.forumList!.content ?? ""
                tempLabel.sizeToFit()
                
                let calculatedHeightDescription = tempLabel.frame.height
                
                if tempLabel.text != "" {
                    
                    return calculatedHeight + calculatedHeightDescription + 279
                }else{
                    return calculatedHeight + 279
                }
            }else{
                
                let comment = self.commentList[indexPath.row - 1]
                
                tempLabel.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width - 76, height: CGFloat.greatestFiniteMagnitude)
                tempLabel.font = UIFont.JosefinSansRegular(size: 14.0)
                tempLabel.text = comment.content ?? ""
                tempLabel.sizeToFit()
                let calculatedHeight = tempLabel.frame.height + 36
                return calculatedHeight
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == suggestionsTableView {
            return self.userList.count
        }
        else{
            return commentList.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == suggestionsTableView {
            
            let cell : UserSearchCell =  tableView.dequeueReusableCell(withIdentifier: "UserSearchCell", for: indexPath) as! UserSearchCell
            let user = self.userList[indexPath.row]

            cell.setUI(user: user)
            return cell
            
        }else{
            
            if indexPath.row == 0 {
                let cell : ForumDetailsHeaderCell =  tableView.dequeueReusableCell(withIdentifier: forumHeaderIdentifier) as! ForumDetailsHeaderCell
                //cell.setUpPost(post: self.post!)
                //cell.setUpData(count: 0, currentPage: 0)
                //cell.setTextDescription(text: text)
                cell.setUpForumList(forumlist: self.forumList!, selectedIndex: self.selectedIndex)
                cell.setCollectionView(show: showHeader)
                cell.delegate = self
                cell.forumDelegate = self
                cell.showAlertClosure = { message in
                    self.showAlert(self, message: message)
                }
                cell.playVideoUrl = { videoUrl in
                    self.videoButtonPressed(url: videoUrl)
                }
                return cell
            }else{
                let cell : CommentCell =  tableView.dequeueReusableCell(withIdentifier: postCommentIdentifier, for: indexPath) as! CommentCell
                cell.delegate = self
                let forumComment = self.commentList[indexPath.row - 1]
                
                
                cell.setComment(forumComment: forumComment)
                cell.longPressedForDelete = {
                    if forumComment.user?.id == Int((CommonFunctions.getuserFromDefaults().userId)!){
                        self.deleteCommentPopUp(cell: cell, indexPath: indexPath)
                    }
                }
                
                cell.userNotFound = {
                    self.showAlert(self, message: "User not found.")
                }
                //cell.setTextDescription(text: text)
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if tableView == suggestionsTableView {
                let user = self.userList[indexPath.row]
                self.taggedUserList.append(user)
                //self.userList.removeAll()
                self.setIdAndNameUser(id: "\(user.id!)", name: "\(user.username!)")
        }
        
        self.view.endEditing(true)
        // self.navigationController?.pushViewController(vc_PostDetail, animated: true)
    }
  
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
}


extension ForumDetailVC : HashTagUserMentionProtocol {
   func hashTagPressed(hashTag:String){
        let storyboard = UIStoryboard(name: "Forum", bundle: nil)
        let vc_favForum = storyboard.instantiateViewController(withIdentifier: "FavouriteForumsVC") as! FavouriteForumsVC
        vc_favForum.hashTag = hashTag
        vc_favForum.categories = self.categories
        self.navigationController?.delegate = nil
        self.navigationController?.pushViewController(vc_favForum, animated: true)
    }
    
    func profilePressed(userId:Int) {
        self.goToProfileView(userId: userId)
    }
    
    
}


extension ForumDetailVC : ForumDetailsHeaderCellPosProtocol {
    func favouriteButtonPressed(cell: ForumDetailsHeaderCell) {
        
    }
    
    func shareButtonPressed(cell: ForumDetailsHeaderCell) {
        self.shareButtonPressed(forumlist: cell.forumList!)
    }
    
    func videoButtonPressed(cell: ForumDetailsHeaderCell) {
        
    }
    
   
    
    
}


extension ForumDetailVC : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
                    if newText.count > 0 {
                        placeHolderLabel.alpha = 0
                    }else{
                        placeHolderLabel.alpha = 1
                    }
            
            globalRange = range
            if text == "\n" {
                textView.resignFirstResponder()
                mutString = NSMutableString()
            } else {
                if text == "" {
                    var str:String = mutString as String
                    if str != "" {
                        str.remove(at: str.index(before: str.endIndex))
                        mutString = NSMutableString()
                        mutString.append(str)
                    }
                }
                if inputTextView.text.length == 0
                {
                    isupdateTable = false
                }
                if text == " " {
                    mutString = ""
                    isupdateTable = false
                } else {
                    mutString.append(text)
                }
                let isBackSpace = strcmp(text, "\\b")
                if (isBackSpace == -92) {
                    globalInt = 0
                    
                    //removeCount = removeCount-1
                }else
                {
                    globalInt = 1
                    //removeCount = removeCount+1
                }
                
                if isupdateTable == true {
                    showTaggedUserSuggestion(mutString)
                }
                
                if text == "@" {
                    mutString = NSMutableString()
                    isupdateTable = true
                }
                
                var isExist = false
                var index = 0
                for i in 0 ..< addName.count {
                    index = i
                    isExist = false
                    if inputTextView.text!.range(of: addName[i] as! String) != nil {
                        isExist = true
                    }
                    
                    if isExist == false {
                        if idToSendApi.count <= index+1 {
                            idToSendApi.removeObject(at: index)
                            addName.removeObject(at: index)
                        }
                    }
                }
            }
            
            return true
        }
        
        func showTaggedUserSuggestion(_ search : NSMutableString){
            print("search", search)
            if search.length <= 1
            {
                return
            }
            else{
                setPopOver(textToSeach: search as String)
                print(" query for users")
    //            objUserView.search(search: mutString as String, idArray:idToSendApi, oncompletion: { (height) -> Void in
    //                if height ?? 0 >= 9 {
    //                    self.objUserView.frame = CGRect(x: 50, y: self.descriptionTextView.frame.maxY + 60, width: self.view.frame.width - 80, height: 260)
    //                }else {
    //                    self.objUserView.frame = CGRect(x: 50, y: self.descriptionTextView.frame.maxY + 60, width: self.view.frame.width - 80, height: CGFloat(height! * 45))
    //                }
    //            })

            }
        }
        
        
        func textViewDidChange(_ textView: UITextView) {
            let text = textView.text as NSString
            if text == ""
            {
                return
            }
            else{
            let substring = text as String
            currentWord = substring.components(separatedBy: " ").last!
            var wordWithSpace = substring.components(separatedBy: " ").last!
            wordWithSpace = " "+wordWithSpace
            let lastChar = String(currentWord.suffix(1))
            
            
           
            if (wordWithSpace.contains(" #")) && !(lastChar==" ")
            {
                var textWidth = textView.frame.inset(by: textView.textContainerInset).width
                textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
                let boundingRect =  substring.rectForText(withConstrainedWidth: textWidth, font: textView.font!)   //sizeOfString(string: substring, constrainedToWidth: Double(textWidth), font: textView.font!)
                let numberOfLines = boundingRect.height / textView.font!.lineHeight;
                
                updatedWord =  currentWord
                print("==== HashTag",updatedWord)
                
               // self.setPopOverForHashTag(textToSeach: updatedWord)
                //self.hashSearchApi(currentWord: updatedWord, numberOfLines: Int(numberOfLines))
                
                if let range = textView.selectedTextRange {
                    if range.start == range.end {
                        let beginning = textView.beginningOfDocument
                        let selectedRange = textView.selectedTextRange
                        let selectionStart = selectedRange?.start
                        let selectionEnd = selectedRange?.end
                        let location = textView.offset(from: beginning, to: selectionStart!)
                        let length = textView.offset(from: selectionStart!, to: selectionEnd!)
                        print(length)
                        if removeCount<0
                        {
                            removeCount = 0
                        }
                        //print("currentWord======",currentWord)
                        rangeToAppend =  NSMakeRange((location-(currentWord.count)), (currentWord.count))
                        print("rangeToAppend location ==",rangeToAppend.location)
                        print("rangeToAppend length ==",rangeToAppend.length)
                        if rangeToAppend.length == 1{
                            self.showUserTableView(show: false)
                        }
                        
                        
                    }
                }
            } else if (wordWithSpace.contains(" @")) && !(lastChar==" ")
            {
                updatedWord =  currentWord.replacingOccurrences(of: "@", with: "")
             
                if let range = textView.selectedTextRange {
                    if range.start == range.end {
                        let beginning = textView.beginningOfDocument
                        let selectedRange = textView.selectedTextRange
                        let selectionStart = selectedRange?.start
                        let selectionEnd = selectedRange?.end
                        let location = textView.offset(from: beginning, to: selectionStart!)
                        let length = textView.offset(from: selectionStart!, to: selectionEnd!)
                        print(length)
                        if removeCount<0
                        {
                            removeCount = 0
                        }
                        print("currentWord======",currentWord)
                        rangeToAppend =  NSMakeRange((location-(currentWord.count)), (currentWord.count))
                        print("rangeToAppend location ==",rangeToAppend.location)
                        print("rangeToAppend length ==",rangeToAppend.length)
                        if rangeToAppend.length == 1{
                            self.showUserTableView(show: false)
                        }
                    }
                }
              }
            }
        }
        
        func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        // change    transparentBGView.isHidden = false
        // change    postButton.isSelected = true
            return true
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
           // change transparentBGView.isHidden = true
           // change postButton.isSelected = false
        }
        
        func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
            if touch.view?.isDescendant(of: self.suggestionsTableView) == true {
                return false
            }
            return true
        }
    
    
    
    @objc func handle(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            showUserTableView(show: false)
        }
        sender.cancelsTouchesInView = false
    }
    
    
    
    func setIdAndNameUser(id:String, name:String) {
            
            isupdateTable = false
            
            var str : NSMutableString = NSMutableString(string:inputTextView.text)
            let newString = "@" + name + " "
            str.replaceCharacters(in: rangeToAppend, with: newString)
            //str = str.replacingCharacters(in: rangeToAppend, with: newString) as! NSMutableString
            inputTextView.text = str.copy() as? String ?? ""
            
    //        let commentTemp = self.descriptionTextView.text.replacingOccurrences(of: "@", with: "@")
           // let comment =  self.descriptionTextView.text.replacingOccurrences(of: mutString as String, with:"")
            
            let name = name.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            print(name)
            print(id)
            
            if self.addName.contains(name) {
                
            } else {
                self.addName.add(name)
                self.idToSendApi.add(id)
            }
            
    //        self.descriptionTextView.text = ""
    //        //self.commentTextView.textColor = UIColor.black
    //        self.descriptionTextView.text = "\(comment)\(name)"
            //setRedColor(text: "\(comment)\(name)")
            self.showUserTableView(show: false)
        }
    
    func showUserTableView(show:Bool){
        self.suggestionsTableView.isHidden = !show
        
        if show {
            tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handle(sender:)))
            tapGesture!.delegate = self
            self.view.addGestureRecognizer(tapGesture!)
        }else{
            if tapGesture != nil {
                self.view.removeGestureRecognizer(tapGesture!)
            }
            if self.userList.count > 0 {
                self.userList.removeAll()
            }
        }
        
        self.suggestionsTableView.reloadData()
        
            if self.userList.count > 5 {
                self.suggestionHeightConstraint.constant = 250
            }else{
                if self.userList.count < 4 {
                    self.suggestionHeightConstraint.constant = 150.0
                }else{
                    self.suggestionHeightConstraint.constant = CGFloat(self.userList.count * 50)
                }
            }
        
    }
    
    //MARK: POPover
    func setPopOver(textToSeach :String){
        let userLogin = CommonFunctions.getuserFromDefaults()
               var createEditVM = CreateEditPost_VM()
                
                createEditVM.userId = userLogin.userId
                createEditVM.perPage = "10"
                createEditVM.page = "1"
                createEditVM.nameStr = textToSeach
                createEditVM.callGetUserSearchListService = true
                createEditVM.responseRecieved = {
                    self.userList = createEditVM.userList
                    // refresh content
                    if self.userList.count == 0 {
                        self.showUserTableView(show: false)
                    }else{
                        self.showUserTableView(show: true)
                    }
                    //self.setPetViewHeight()
                }
                
                createEditVM.showAlertClosure = {[weak self] () in
                    DispatchQueue.main.async {
                        if let message = createEditVM.alertMessage {
                            self?.showAlert(self ?? UIViewController(), message: message)
                            self?.userList.removeAll()
                            self?.showUserTableView(show: false)
                        }
                    }
                }
    }
    

    
    
}
