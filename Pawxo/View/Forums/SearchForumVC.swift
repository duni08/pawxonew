//
//  SearchForumVC.swift
//  Pawxo
//
//  Created by 42works on 21/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class SearchForumVC: UIViewController {

    @IBOutlet weak var searchTF: RegularTextField!
    
    
    var staticCount = 10
    
    var cellType = 0
    
    var forumCellTypeOne = "ForumListTypeOneCell"
    
    var forumCellTypeTwo = "ForumListTypeTwoCell"
    
    @IBOutlet weak var searchForumTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchForumTableView.register(UINib(nibName: forumCellTypeOne, bundle: nil), forCellReuseIdentifier: forumCellTypeOne)
        cellType = 0
        //staticCount = 0
        searchForumTableView.register(UINib(nibName: forumCellTypeTwo, bundle: nil), forCellReuseIdentifier: forumCellTypeTwo)
        // Do any additional setup after loading the view.
    }
    

    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension SearchForumVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return staticCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ForumListTypeOneCell =  tableView.dequeueReusableCell(withIdentifier: forumCellTypeOne, for: indexPath) as! ForumListTypeOneCell
        
        
        if cellType == 0{
            cell.staticCount = 3
        }else if cellType == 1{
            cell.staticCount = 4
        }else if cellType == 2 {
            cell.staticCount = 2
        }else if cellType == 3{
            cell.staticCount = 1
        }else if cellType == 4 {
            cell.staticCount = 3
        }
        cell.setUpLayout()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 460
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return UIView()
    }
}
