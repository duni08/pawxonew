//
//  AddForumVC.swift
//  Pawxo
//
//  Created by 42works on 09/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import YPImagePicker
import AVFoundation



class ForumMediaItem{
    var type : String?
    var data : Data?
    var thumbImage : UIImage?
    var imageVideoUrl : String?
    var imageVideoThumbUrl : String?
}

class AddForumVC: UIViewController {

    @IBOutlet weak var mainTableView: UITableView!
    
    var staticCount = 2
    
    var cellType = 0
    var cellHeight : CGFloat = 500
    var forumAddCell = "AddForumCell"
    let cellMediaIdentifier = "AddMediaForumCell"
   // var forumCellTypeTwo = "ForumListTypeTwoCell"
    var mediaArray = [UIImage]()
    
    var categories = [Category]()
    
    @IBOutlet weak var submitButton: RegularButton!
    @IBOutlet weak var titleLabel: SemiBoldLabel!
    var selectedCategoryId = ""
    
    var tapGesture : UITapGestureRecognizer?
    
    var forumList : ForumList?
    var media = [ForumMediaItem]()
    
    var refreshData :(()->())?
    
    var refreshForumList :(()->())?
    
    var currentLat : Double?
    var currentLong : Double?
    var currentAddress : String = ""
    var mediaItems = [ForumMediaItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let forumList = self.forumList{
            // edit forum
            self.titleLabel.text = "Edit Forum"
            self.submitButton.setTitle("Submit", for: .normal)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.getForumDetails(forumId:"\((self.forumList?.id)!)")
            }
        }else{
            self.titleLabel.text = "Add Forum"
            self.submitButton.setTitle("Share", for: .normal)
        }
        // Do any additional setup after loading the view.
    }
    
    
    func setUpMediaItems(){
        
        self.mediaItems.removeAll()
        
        if let image_0 = self.forumList?.image0{
            let mediaItem = ForumMediaItem()
            mediaItem.imageVideoUrl = image_0
            
            if let image_0_type = self.forumList?.image0_Type{
                mediaItem.type = image_0_type
            }else{
                mediaItem.type = "image"
            }
            
            if let image_0_thumb = self.forumList?.image0_Thumb{
                mediaItem.imageVideoThumbUrl = image_0_thumb
            }
            
            self.mediaItems.append(mediaItem)
        }
        
        
        if let image_1 = self.forumList?.image1{
            let mediaItem = ForumMediaItem()
            mediaItem.imageVideoUrl = image_1
            
            if let image_1_type = self.forumList?.image1_Type{
                mediaItem.type = image_1_type
            }else{
                mediaItem.type = "image"
            }
            
            if let image_1_thumb = self.forumList?.image1_Thumb{
                mediaItem.imageVideoThumbUrl = image_1_thumb
            }
            
            self.mediaItems.append(mediaItem)
        }
        
        
        if let image_2 = self.forumList?.image2{
            let mediaItem = ForumMediaItem()
            mediaItem.imageVideoUrl = image_2
            
            if let image_2_type = self.forumList?.image2_Type{
                mediaItem.type = image_2_type
            }else{
                mediaItem.type = "image"
            }
            
            if let image_2_thumb = self.forumList?.image2_Thumb{
                mediaItem.imageVideoThumbUrl = image_2_thumb
            }
            
            self.mediaItems.append(mediaItem)
        }
        
        if let image_3 = self.forumList?.image3{
            let mediaItem = ForumMediaItem()
            mediaItem.imageVideoUrl = image_3
            
            if let image_3_type = self.forumList?.image3_Type{
                mediaItem.type = image_3_type
            }else{
                mediaItem.type = "image"
            }
            
            if let image_3_thumb = self.forumList?.image3_Thumb{
                mediaItem.imageVideoThumbUrl = image_3_thumb
            }
            
            self.mediaItems.append(mediaItem)
        }
        
    }
    
    func getForumDetails(forumId:String){
        
        let  forum = Forum_VM()
        let userLogin = CommonFunctions.getuserFromDefaults()
        forum.userId = userLogin.userId
        forum.forumId = "\(forumId)"
        
        
        forum.responseRecieved = {
           
            self.forumList?.setNewValues(forumList:(forum.forumLists?[0])!)
            self.mainTableView.reloadData()
           
        }
        
        forum.showAlertClosure = {[weak self] () in
            DispatchQueue.main.async {
                if let message = forum.alertMessage {
                    self?.showAlert(self ?? UIViewController(), message: message)
                }
            }
        }
        forum.callshowforumList = true
        print("jnk")
    }
   
    @IBAction func shareButtonPressed(_ sender: Any) {
        
        if let cell = self.mainTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? AddForumCell{
        
            if cell.titleTF.text == ""{
                self.showAlert(self, message: ValidationMessage.emptyForumTitle)
                return
            }
            if cell.categoryTF.text == "" {
                self.showAlert(self, message: ValidationMessage.emptyForumCategory)
                return
            }
            if cell.descriptionTV.text == "" {
                self.showAlert(self, message: ValidationMessage.emptyForumDescription)
                return
            }
            
            if cell.locationTF.text == "" {
                self.showAlert(self, message: ValidationMessage.emptyForumLocation)
                return
            }
            
            if self.forumList == nil{
                if self.media.count == 0 {
                    self.showAlert(self, message: ValidationMessage.emptyForumMedia)
                    return
                }
            }
        
        
        
            let forum_VM = Forum_VM()
            forum_VM.userId = CommonFunctions.getuserFromDefaults().userId
            if let forumList = self.forumList{
                forum_VM.forumId = "\(forumList.id!)"
            }else{
                forum_VM.mediaItems = self.media
            }
            forum_VM.title = cell.titleTF.text
            forum_VM.description = cell.descriptionTV.text
            forum_VM.category = self.selectedCategoryId
            
            forum_VM.latitude = "\(currentLat!)"
            forum_VM.longitude = "\(currentLong!)"
            forum_VM.currentAddress = currentAddress
            var description = cell.descriptionTV.text!
            var hashArr = [String]()
            let replaced = description.replacingOccurrences(of: " #", with: "#")
            let finalreplaced = replaced.replacingOccurrences(of: "#", with: " #")
            description = finalreplaced
            print(description)
            
            let descriptionNew = description.components(separatedBy: " ")
            for obj in descriptionNew ?? [String]() {
                if obj.first == "#" {
                    var temp = obj
                    temp.remove(at: temp.startIndex)
                    hashArr.append(temp)
                }
            }
            
            if hashArr.count != 0 {
                let hashArrList = (hashArr.map{String($0)}).joined(separator: ",")
                forum_VM.hashTag = hashArrList
            }
            
            
            
            forum_VM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if forum_VM.isLoading {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            forum_VM.responseRecieved = {
                // refresh content
               DispatchQueue.main.async {
                
                
                if let forumlist = self.forumList{
                    forumlist.title = cell.titleTF.text!
                    forumlist.content = cell.descriptionTV.text!
                    forumlist.category?.id = Int(self.selectedCategoryId)
                    forumlist.category?.name = cell.categoryTF.text
                    
                }
                    self.refreshData?()
                   if let message = forum_VM.alertMessage {
                       self.showAlert(self, message: message)
                   }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.navigationController?.popViewController(animated: true)
                }
               }
                    
            }
            
            forum_VM.showAlertClosure = {
                DispatchQueue.main.async {
                    if let message = forum_VM.alertMessage {
                        self.showAlert(self, message: message)
                    }
                }
            }
            
            if let forumList = self.forumList{
                forum_VM.callEditForumService = true
            }else{
                forum_VM.callAddForumService = true
            }
            
    }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension AddForumVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return staticCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell : AddForumCell =  tableView.dequeueReusableCell(withIdentifier: forumAddCell, for: indexPath) as! AddForumCell
            cell.delegate = self
            cell.categories = self.categories
            
            if let forumlist = self.forumList {
                cell.setEditView(forumlist: forumlist)
            }
            cell.showLocation = { cell in
               
                if let vc = UIStoryboard(name: StoryboardName.Settings, bundle: nil).instantiateViewController(withIdentifier: "EditLocationVC") as? EditLocationVC{
                    
                    if let forumList = self.forumList{
                        vc.isEdit = true
                    }
                    
                    vc.locationSelected = { lat,long,address in
                        self.currentLat = lat
                        self.currentLong = long
                        self.currentAddress = address
                        cell.locationTF.text = address
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            return cell
        }else if indexPath.row == 1{
            let cell : AddMediaForumCell =  tableView.dequeueReusableCell(withIdentifier: cellMediaIdentifier, for: indexPath) as! AddMediaForumCell
            cell.delegate = self
            if let forumlist = self.forumList{
                self.setUpMediaItems()
                self.forumList?.mediaItems = self.mediaItems
                cell.setUPMediaObject(mediaArray: forumlist.mediaItems!)
            }else{
                cell.setUPMediaObject(mediaArray: self.media)
            }
           // cell.setUpLayout()
            return cell
            
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 517
        }else{
            return cellHeight
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
}


extension AddForumVC : AddMediaForumCellProtocol {
    func setheight(height: CGFloat) {
        self.cellHeight = height
        self.mainTableView.reloadData()
    }
    
    
}


extension AddForumVC : AddForumCellProtocol ,UIGestureRecognizerDelegate{
    func mediaAddPressed() {
//        photoManager = PhotoManager(navigationController: self.navigationController!, allowEditing: true, videoType: false, callback: { (pickedImage,urlVideo)  in
//            //self.questionaireModel.profileImage = pickedImage ?? #imageLiteral(resourceName: "TempProfile")
//            if let image = pickedImage {
//
//                //if let cell = self.mainTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as?
//                  //  AddMediaForumCell {
//                self.mediaArray.append(image)
//               // cell.setUPMediaObject(mediaArray: self.mediaArray)
//                //cell.setUpLayout()
//                self.mainTableView.reloadData()
//                //cell.
//            }
//
//        //}
//
//    })
        
        if let forumlist = self.forumList {
            return
        }
        
        
        var config = YPImagePickerConfiguration()
        config.library.onlySquare = false
        config.bottomMenuItemSelectedTextColour = AppColor.baseColor!
        config.bottomMenuItemUnSelectedTextColour = AppColor.colorGreySubHeading
        config.colors.tintColor = AppColor.baseColor!
        
        config.isScrollToChangeModesEnabled = true
        config.onlySquareImagesFromCamera = true
        config.targetImageSize = .original
        config.library.mediaType = .photoAndVideo
        config.usesFrontCamera = false
        config.showsPhotoFilters = true
        config.shouldSaveNewPicturesToAlbum = true
        config.video.compression = AVAssetExportPresetMediumQuality
        config.albumName = "Pawxo"
        config.screens = [.library, .photo, .video]
        config.startOnScreen = .library
        config.video.recordingTimeLimit = 60
        config.video.libraryTimeLimit = 60
        config.video.fileType = .mp4
        config.library.maxNumberOfItems = 4
        // Build a picker with your configuration
        let picker = YPImagePicker(configuration: config)
        //  let picker = YPImagePicker()
        
        picker.didFinishPicking { [unowned picker] items, cancelled in
            
            if cancelled == true {
                picker.dismiss(animated: true, completion: nil)
            }
            
            var media = [ForumMediaItem]()
            
            
                
                
               for item in items {
                    switch item {
                    case .photo(let photo):
                        let mediaItem = ForumMediaItem()
                        mediaItem.type = "image"
                        mediaItem.data = photo.image.jpegData(compressionQuality: 0.5)
                        mediaItem.thumbImage = photo.image
                        media.append(mediaItem)
                    case .video(let video):
                        print(video)
                        let mediaItem = ForumMediaItem()
                        mediaItem.type = "video"
                        do {
                            let videoData = try Data(contentsOf: video.url)
                            mediaItem.data = videoData
                            mediaItem.thumbImage = video.thumbnail
                            media.append(mediaItem)
                        } catch {
                            print("Unable to load data: \(error)")
                        }
                       
                    }
                }
            
            
            
//            if let photo = items.singlePhoto {
//                print(photo.fromCamera) // Image source (camera or library)
//                print(photo.image) // Final image selected by the user
//                print(photo.originalImage) // original image selected by the user, unfiltered
//                print(photo.modifiedImage) // Transformed image, can be nil
//                print(photo.exifMeta) // Print exif meta data of original image.
//
//                // image picked
//                let data = photo.image.jpegData(compressionQuality: 0.5)
//                self.createPost(img: photo.image, isImage: true, data: data, url: nil, picker: picker)
//                //picker.dismiss(animated: true, completion: nil)
//            }else{
//
//            }
//
//            if let video = items.singleVideo {
//                print(video.fromCamera)
//                print(video.thumbnail)
//                print(video.url)
//
//                do {
//                    let videoData = try Data(contentsOf: video.url)
//                    self.createPost(img: video.thumbnail, isImage: false, data: videoData, url: video.url, picker: picker)
//                } catch {
//                    print("Unable to load data: \(error)")
//                }
//            }
            self.setMediaItems(media: media)
            
             picker.dismiss(animated: true, completion: nil)
        
            
        }
        
        self.present(picker, animated: true, completion: nil)
        
        
}
    
    func setMediaItems(media:[ForumMediaItem]){
        if media.count > 0 {
            // reload second Cell
            self.media = media
            self.mainTableView.reloadData()
        }
    }
    
    
    private func createPost(img: UIImage, isImage: Bool, data: Data?,url:URL?, picker: YPImagePicker?) {
        // self.mmPlayerLayer.player?.pause()
//        delay(0.1) { () -> () in
//            let postStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
//            let vc = postStoryboard.instantiateViewController(withIdentifier: "CreatePostVC") as! CreatePostVC
//
//            vc.isImage = isImage
//            vc.mediaData = data
//            vc.thumbnailImage = img
//            vc.mediaUrl = url
//            vc.delegate = self
//            picker?.navigationBar.isHidden = true
//            picker?.pushViewController(vc, animated: true)
//        }
        // self.presentedViewController?.present(vc, animated: false, completion: nil)
    }
    
    
    func categoryPressed(id:Int) {
        self.selectedCategoryId = "\(id)"
    }
    
    
    func shownHashTagTable(cell:AddForumCell,show:Bool){
        if show {
            tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handle(sender:)))
            tapGesture!.delegate = self
            self.view.addGestureRecognizer(tapGesture!)
        }else{
            if tapGesture != nil {
                self.view.removeGestureRecognizer(tapGesture!)
            }
        }
    }
    
    
    @objc func handle(sender: UITapGestureRecognizer) {
           if sender.state == .ended {
            if let cell = self.mainTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? AddForumCell{
            
                cell.showUserTableView(show: false)
            }
           }
           sender.cancelsTouchesInView = false
       }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if let cell = self.mainTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? AddForumCell{
        
            if touch.view?.isDescendant(of: cell.hashTagTableView) == true {
            return false
        }
        }
        return true
    }
    
}
