//
//  FilterVC.swift
//  Pawxo
//
//  Created by 42works on 21/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class FilterVC: UIViewController {

    
    
    @IBOutlet weak var filterWidthYConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var categoryTableView: UITableView!
    @IBOutlet weak var newestImageV: UIImageView!
    
    @IBOutlet weak var oldestImageV: UIImageView!
    
    @IBOutlet weak var popularImageV: UIImageView!
    
    
    var staticCount = 10
    
    var cellType = 0
    
    var cellIdentifier = "FilterCategoryCell"
    
    
    var selectedImageRadio = UIImage(named: "radioSelected")
    var unSelectedImageRadio = UIImage(named: "radioUnselected")
    
    var categories = [Category]()
    var selectedCategories = [Category](){
        didSet {
            for category in self.selectedCategories{
                category.isSelectedCategory = true
            }
        }
    }
    
    var sort = [String]()
    
    var selectedSort = ""
    
    @IBOutlet weak var clearFilterButton: RegularButton!
    var categorySelectedAndSort : ((String,[Category])->())?
    
    var clearFilters : (()->())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.filterWidthYConstraint.constant = 300
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
        UIView.animate(withDuration: 0.5) {
            self.filterWidthYConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
        }
        
        if selectedSort != "" {
            if selectedSort == "newest"{
                self.newestImageV.image = self.selectedImageRadio
                self.popularImageV.image = self.unSelectedImageRadio
                self.oldestImageV.image = self.unSelectedImageRadio
            }else if selectedSort == "oldest"{
                self.newestImageV.image = self.unSelectedImageRadio
                self.popularImageV.image = self.unSelectedImageRadio
                self.oldestImageV.image = self.selectedImageRadio
            }else if selectedSort == "popular"{
                self.newestImageV.image = self.unSelectedImageRadio
                self.popularImageV.image = self.selectedImageRadio
                self.oldestImageV.image = self.unSelectedImageRadio
            }
        }else{
            self.newestImageV.image = self.unSelectedImageRadio
            self.popularImageV.image = self.unSelectedImageRadio
            self.oldestImageV.image = self.unSelectedImageRadio
        }
        
        if selectedCategories.count > 0 {
            self.clearFilterButton.isHidden = false
            
            
        }else{
            self.clearFilterButton.isHidden = true
        }
        
      // Do any additional setup after loading the view.
    }
    
    @IBAction func popularButtonPressed(_ sender: Any) {
        
        self.newestImageV.image = self.unSelectedImageRadio
        self.popularImageV.image = self.selectedImageRadio
        self.oldestImageV.image = self.unSelectedImageRadio
        self.selectedSort = "popular"
    }
    
    
    @IBAction func newestButtonPressed(_ sender: Any) {
        self.newestImageV.image = self.selectedImageRadio
               self.popularImageV.image = self.unSelectedImageRadio
               self.oldestImageV.image = self.unSelectedImageRadio
        self.selectedSort = "newest"
    }
    
    
    @IBAction func oldestButtonPressed(_ sender: Any) {
        self.newestImageV.image = self.unSelectedImageRadio
        self.popularImageV.image = self.unSelectedImageRadio
        self.oldestImageV.image = self.selectedImageRadio
        self.selectedSort = "oldest"
    }
    
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        for category in self.selectedCategories{
            category.isSelectedCategory = false
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.filterWidthYConstraint.constant = 300
            self.view.layoutIfNeeded()
        }) { (success) in
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    func updateSelectedCategory(clear:Bool){
        self.selectedCategories.removeAll()
        
        for category in self.categories{
            if clear{
                if let isSelectedCategory = category.isSelectedCategory,isSelectedCategory == true{
                    category.isSelectedCategory = false
                    print(category.name!)
                }
            }else
                if let isSelectedCategory = category.isSelectedCategory,isSelectedCategory == true{
                    self.selectedCategories.append(category)
                    print(category.name!)
            }
        }
    }
    

    
    
    
    @IBAction func clearFiltersPressed(_ sender: Any) {
        self.updateSelectedCategory(clear: true)
        self.newestImageV.image = self.unSelectedImageRadio
        self.popularImageV.image = self.unSelectedImageRadio
        self.oldestImageV.image = self.unSelectedImageRadio
        self.selectedSort = ""
        self.categoryTableView.reloadData()
        self.clearFilters?()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func applyFiltersPressed(_ sender: Any) {
        self.categorySelectedAndSort!(self.selectedSort,self.selectedCategories)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension FilterVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : FilterCategoryCell =  tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FilterCategoryCell
        var category = self.categories[indexPath.row]
        cell.setCategory(category: category)
        
        cell.categorySelected = { isSelectedCategory in
                category.isSelectedCategory = isSelectedCategory
                cell.setCategory(category: category)
            self.updateSelectedCategory(clear: false)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 35
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return UIView()
    }
}
