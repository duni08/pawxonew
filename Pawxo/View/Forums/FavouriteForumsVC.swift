//
//  FavouriteForumsVC.swift
//  Pawxo
//
//  Created by 42works on 21/01/20.
//  Copyright © 2020 42works. All rights reserved.
//
import UIKit
import ActiveLabel

class FavouriteForumsVC: UIViewController {
    
    
    
    let simpleOver = ForumDetailsAnimation()
    
    var frameTest : CGRect?
    
    @IBOutlet weak var refreshButton: UIButton!
    
    @IBOutlet weak var outerMainView: UIView!
    
    @IBOutlet weak var viewTitleLabel: SemiBoldLabel!
    
    var forumCellTypeOne = "ForumListTypeOneCell"
    
    var hideContent = false
    
    var currentIndex: Int = 0
    
    @IBOutlet weak var noResulstsview: UIView!
    
    @IBOutlet weak var noInternetView: UIView!
    var requests = [ForumRequestModel]()
    
    var selectedRequest : ForumRequestModel?
    
    var forumList = [ForumList]()
    
    var categories = [Category]()
    var tempLabel : ActiveLabel!
    var tempLabel2 : ActiveLabel!
    
    @IBOutlet weak var favouriteTableView: UITableView!
    
    let gif = UIImage.gifImageWithName("gif")
    
    var refresh = UIRefreshControl()
    var loader = UIImageView()
    
    var hashTag = ""
    var requestModel = ForumRequestModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.setUpIndicatorView(index:0, animate: false, scroll: false, sender: lostLabel)
        
        favouriteTableView.register(UINib(nibName: forumCellTypeOne, bundle: nil), forCellReuseIdentifier: forumCellTypeOne)
        favouriteTableView.estimatedRowHeight = 464
       // self.refreshButton.isHidden = true
        
        if self.hashTag == ""{
            self.viewTitleLabel.text = "Favorites"
            self.getAPIData()
        }else{
            setHashTAG(hashTag: self.hashTag)
        }
        
        refresh.tintColor = UIColor.clear
                  refresh.backgroundColor = UIColor.clear
                  refresh.attributedTitle = NSAttributedString(string: "Pull to refresh")
                  refresh.addTarget(self, action:#selector(refreshTable), for: UIControl.Event.valueChanged)
                  // Add Custom Loader
                  
                  
                  loader.center = self.refresh.center
                   loader = UIImageView(frame: CGRect(x: kAppDelegate.window!.frame.size.width/2 - 20, y: refresh.center.y, width: 40, height: 40))
                   loader.image = gif
                  refresh.insertSubview(loader, at: 0)
                  
                  self.favouriteTableView.addSubview(refresh)

                  let emptyLabel = UILabel.init(frame: self.favouriteTableView.frame)
                  var msg = ""
               
                  msg = "No latest posts yet."
                  emptyLabel.isHidden = true
                  emptyLabel.text = msg
                  emptyLabel.textAlignment = .center
                  emptyLabel.numberOfLines = 0
                  emptyLabel.textColor = UIColor.darkGray
                  self.favouriteTableView.addSubview(emptyLabel)
        
        // Do any additional setup after loading the view.
    }
   
    
    func setHashTAG(hashTag:String){
        self.hashTag = hashTag
        self.viewTitleLabel.text = hashTag
        requestModel.hashTag = hashTag.replacingOccurrences(of: "#", with: "")
        self.getHashTagPosts()
    }
    
    
    func getHashTagPosts(){
        
        if !(WebServices().isInternetWorking()){
            self.noInternetView.isHidden = false
            self.refresh.endRefreshing()
            self.forumList.removeAll()
            self.favouriteTableView.addSubview(self.noInternetView)
            self.favouriteTableView.reloadData()
            return
        }else{
            self.noInternetView.isHidden = true
        }
        
        let forum_VM = Forum_VM()
        forum_VM.userId = CommonFunctions.getuserFromDefaults().userId!
        forum_VM.requests = [self.requestModel]
        forum_VM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if forum_VM.isLoading {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        forum_VM.responseRecieved = {
            // refresh content
            DispatchQueue.main.async {
                self.refresh.endRefreshing()
                print("Final Response recieved")
                self.requestModel = forum_VM.requests![0]
                self.forumList = self.requestModel.data
                if self.forumList.count == 0{
                    self.noResulstsview.isHidden = false
                    self.favouriteTableView.addSubview(self.noResulstsview)
                }else{
                    self.noResulstsview.isHidden = true
                }
                self.favouriteTableView.reloadData()
            }
        }
        
        forum_VM.showAlertClosure = {
            DispatchQueue.main.async {
                if let message = forum_VM.alertMessage {
                    self.refresh.endRefreshing()
                    self.showAlert(self, message: message)
                    
                }
            }
        }
        
        forum_VM.callGetForumListService = true
        
    }
    
    
   
    @IBAction func tryAgainPressed(_ sender: Any) {
        self.refreshTable()
    }
    
    
    
    
     @objc private func refreshTable() {
        self.forumList.removeAll()
        self.favouriteTableView.reloadData()
        
        if self.hashTag != ""{
            self.requestModel.page = 1
            self.getHashTagPosts()
            
        }else{
            self.getAPIData()
        }
//            self.request?.page = 1
//    //        if self.request?.page == 3 {
//    //            return
//    //        }
//
//            self.listToRefresh!(request!, self)
        }
    
    
    @IBAction func refreshButtonPressed(_ sender: Any) {
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func editForumButtonPressed(forumlist:ForumList){
        
        // Edit Forum Pressed
        let storyboard = UIStoryboard(name: "Forum", bundle: nil)
               let vc_AddForum = storyboard.instantiateViewController(withIdentifier: "AddForumVC") as! AddForumVC
            vc_AddForum.categories = self.categories
            vc_AddForum.forumList = forumlist
            vc_AddForum.refreshData = {
            self.refreshTable()
            NotificationCenter.default.post(name: Notification.Name("RefreshForumList"), object: nil, userInfo: nil)
        }
               self.navigationController?.pushViewController(vc_AddForum, animated: true)
    }
    
    func deleteForumButtonPressed(forumlist:ForumList){
       
        var forumVM = Forum_VM()
        forumVM.userId = CommonFunctions.getuserFromDefaults().userId
        forumVM.forumId = "\((forumlist.id)!)"
        
        
        forumVM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if forumVM.isLoading {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        forumVM.responseRecieved = {
            // refresh button like
            
            NotificationCenter.default.post(name: Notification.Name("RefreshForumList"), object: nil, userInfo: nil)
            
            self.refreshTable()
            
            
            let viewcontrollers = NSMutableArray(array: self.navigationController!.viewControllers)
            
            if let previousController = viewcontrollers[viewcontrollers.count - 2] as? ForumDetailVC{
                if previousController.forumList?.id ==  forumlist.id{
                    viewcontrollers.removeObject(at: (viewcontrollers.count - 2))
                    self.navigationController?.delegate = nil
                    self.navigationController?.setViewControllers(viewcontrollers as! [UIViewController], animated: false)
            }
            }
          //  self.postList.remove(at: indexPath!.row)
          //  self.homeTableView.deleteRows(at: [indexPath!], with: .automatic)
        }
        
        forumVM.showAlertClosure = {
            DispatchQueue.main.async {
                if let message = forumVM.alertMessage {
                   // self.showAlert(self , message: message)//
                }
            }
        }
        
        
        forumVM.callDeleteForumService = true
        
        
        
     //       NotificationCenter.default.post(name: Notification.Name("RefreshForumList"), object: nil, userInfo: nil)
        
    }
    
    func getAPIData(){
        
        
        if !(WebServices().isInternetWorking()){
            self.noInternetView.isHidden = false
            self.noResulstsview.isHidden = true
            self.refresh.endRefreshing()
            self.forumList.removeAll()
            self.favouriteTableView.addSubview(self.noInternetView)
            self.favouriteTableView.reloadData()
            return
        }else{
            self.noInternetView.isHidden = true
        }
        
        let forum_VM = Forum_VM()
        forum_VM.userId = CommonFunctions.getuserFromDefaults().userId!
        //forum_VM.requests = requests
        forum_VM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if forum_VM.isLoading {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        forum_VM.responseRecieved = {
            // refresh content
            self.refresh.endRefreshing()
            DispatchQueue.main.async {
            print("Final Response recieved")
                self.forumList = forum_VM.forumLists!
                if self.forumList.count == 0{
                    self.noResulstsview.isHidden = false
                    self.favouriteTableView.addSubview(self.noResulstsview)
                }else{
                    self.noResulstsview.isHidden = true
                }
                self.favouriteTableView.reloadData()
            }
        }
        
        forum_VM.showAlertClosure = {
            DispatchQueue.main.async {
                if let message = forum_VM.alertMessage {
                    self.refresh.endRefreshing()
                    self.showAlert(self, message: message)
                }
            }
        }
        
        forum_VM.callGetFavouriteListService = true
        
    }
    
    
    func bottomRefresh(_ scrollView : UIScrollView)
    {
        
        if self.hashTag != "" {
            let scrollViewHeight = scrollView.frame.size.height;
            let scrollContentSizeHeight = scrollView.contentSize.height;
            let scrollOffset = scrollView.contentOffset.y;
            if (scrollOffset >  scrollContentSizeHeight - scrollViewHeight)
            {
                if self.requestModel.isLoaded {
                    self.requestModel.page += 1
                    if self.requestModel.hasMore {
                        self.getHashTagPosts()
                    }
                }
            }
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let cells = self.favouriteTableView.visibleCells as? [ForumListTypeOneCell]{
            for cell in cells{
                if cell.optionsOuterView.isHidden == false{
                    cell.optionsOuterView.isHidden = true
                    cell.setOptionsView()
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let _ = scrollView as? UITableView {
            bottomRefresh(scrollView)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            if let _ = scrollView as? UITableView {
                bottomRefresh(scrollView)
            }
        }
    }
    
    
    func refreshForumList(request:ForumRequestModel,cell:ForumListCollectionCell?){
        let index = self.requests.lastIndex{$0.categoryId == request.categoryId}
        let forum_VM = Forum_VM()
        forum_VM.userId = CommonFunctions.getuserFromDefaults().userId!
        forum_VM.requests = [request]
        forum_VM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if forum_VM.isLoading {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        forum_VM.responseRecieved = {
            // refresh content
            DispatchQueue.main.async {
            print("Pull To Response recieved")
                cell?.refresh.endRefreshing()
                self.requests[index!] = forum_VM.requests![0]
                let forumList = self.requests[index!].data
                cell?.request = self.requests[index!]
                cell?.setForumList(forumList:forumList)
                
            }
        }
        
        forum_VM.showAlertClosure = {
            DispatchQueue.main.async {
                if let message = forum_VM.alertMessage {
                    self.showAlert(self, message: message)
                }
            }
        }
        
        forum_VM.callGetForumListService = true
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           self.navigationController?.delegate = nil
            self.favouriteTableView.reloadData()
       }
    
    
    func shareButtonPressed(forumlist:ForumList){
        
        var title = ""
        title = forumlist.title ?? ""
        
        
            var descriptionPost = ""
            descriptionPost = forumlist.content ?? ""
            //let mediaLink =  cell.post?.media?.first ?? ""
            var content = ""
            
        for mediaItem in forumlist.mediaItems!{
            content.append(contentsOf: mediaItem.imageVideoUrl!)
            content.append(contentsOf: "\n")
        }
        
        
            let text = "Checkout this post!! \n \(title)\n \(content)\n \(descriptionPost) Let’s make this world a happier and better place for innocent beings, who do the same for us! \n Download https://play.google.com/ http://itunes.apple.com/"
            //let shareimage = UIImage(named: "Pet")
          
            let imageToShare = [text] as [Any]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.setValue("Cuddle | Check out", forKey: "subject")
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            //        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
    }
    
    func hashTagPressed(hashTag:String){
        self.forumList.removeAll()
        self.favouriteTableView.reloadData()
        self.setHashTAG(hashTag: hashTag)
//        let storyboard = UIStoryboard(name: "Forum", bundle: nil)
//        let vc_favForum = storyboard.instantiateViewController(withIdentifier: "FavouriteForumsVC") as! FavouriteForumsVC
//        vc_favForum.hashTag = hashTag
//        vc_favForum.categories = self.categories
//        self.navigationController?.pushViewController(vc_favForum, animated: true)
    }
    
    func goToProfileView(userId:Int){
        let storyboardProfile = UIStoryboard(name: "Profile", bundle: nil)
        let vc_Profile = storyboardProfile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc_Profile.isFromOtherScreen = true
        vc_Profile.userId = "\(userId)"
        self.navigationController?.pushViewController(vc_Profile, animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

/*
extension ForumsListVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let forumCell : ForumListCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ForumListCollectionCell
        forumCell.cellType = indexPath.item
        forumCell.delegate = self
        let forumList = self.requests[indexPath.item].data
        forumCell.request = self.requests[indexPath.item]
        forumCell.setForumList(forumList:forumList)
//        forumCell.endRefreshing()
        forumCell.showAlertClosure = { message in
            self.showAlert(self, message: message)
        }
        
        forumCell.shareForumPressed = { forumList in
            self.shareButtonPressed(forumlist: forumList)
        }
        
        forumCell.listToRefresh = { request,refresh in
            self.refreshForumList(request: request, cell: refresh)
        }
        
        
        
        return forumCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height =  self.view.bounds.size.height - (self.view.safeAreaInsets.top + self.view.safeAreaInsets.bottom + 101)
        return CGSize(width: self.view.frame.width, height: height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let visibleRect = CGRect(origin: forumCollectionView.contentOffset, size: forumCollectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = forumCollectionView.indexPathForItem(at: visiblePoint)
        // print(visibleIndexPath?.item)
        
        self.selectedRequest = self.requests[visibleIndexPath!.item]
        self.menuTABSView.collView.scrollToItem(at: visibleIndexPath!, at: .centeredHorizontally, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            
            self.menuTABSView.collView.selectItem(at: visibleIndexPath!, animated: true, scrollPosition: .centeredVertically)
            
        }
//        if(visibleIndexPath?.item ==  0){
//           // self.setUpIndicatorView(index:0, animate: true, scroll: false, sender: lostLabel)
//        }else if(visibleIndexPath?.item ==  1){
//           // self.setUpIndicatorView(index:1, animate: true, scroll: false, sender: rescueLabel)
//        }else{
//            //self.setUpIndicatorView(index:2, animate: true, scroll: false, sender: healthLabel)
//        }
        
    }
}
*/
/*
extension ForumsListVC: MenuBarDelegate {

    func menuBarDidSelectItemAt(menu: MenuTabsView, index: Int) {

        // If selected Index is other than Selected one, by comparing with current index, page controller goes either forward or backward.
            
            self.scrollToIndex(index: index)
            self.selectedRequest = self.requests[index]
            menuTABSView.collView.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: true)

    }

}

*/
extension FavouriteForumsVC : ForumListTypeOneCellProtocol {
    func cellClicked(frame: CGRect, index: Int, forumlist: ForumList,image:UIImage?) {
        print(frame)
        
        self.frameTest = frame
        let storyboard = UIStoryboard(name: "Forum", bundle: nil)
        let vc_ForumDetail = storyboard.instantiateViewController(withIdentifier: "ForumDetailVC") as! ForumDetailVC
        vc_ForumDetail.imageCenter = image ?? DefaultImage.defaultPostImage
        vc_ForumDetail.imageFrame = self.frameTest
        vc_ForumDetail.delegate = self
        vc_ForumDetail.forumList = forumlist
        vc_ForumDetail.selectedIndex = index
        vc_ForumDetail.categories = self.categories
        self.navigationController?.delegate = self
        self.navigationController?.pushViewController(vc_ForumDetail, animated: true)
    }
    
    func profileClicked(userId:Int){
        self.goToProfileView(userId: userId)
    }
    
    func unFavouritePressed(){
        if self.hashTag == ""{
            self.viewTitleLabel.text = "Favourite"
            self.getAPIData()
        }else{
            setHashTAG(hashTag: self.hashTag)
        }
        
        NotificationCenter.default.post(name: Notification.Name("RefreshForumList"), object: nil, userInfo: nil)
    }
    
}


extension FavouriteForumsVC: ForumDetailVCProtocol {
     func setNavigationDelegate() {
        self.navigationController?.delegate = self
    }
}






extension FavouriteForumsVC: UIViewControllerTransitioningDelegate,UINavigationControllerDelegate {

    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//            return FlipPresentAnimationController(originFrame: frameTest!)
        simpleOver.popStyle = (operation == .pop)
        if !simpleOver.popStyle {
            simpleOver.originFrame = self.frameTest
        }else{
            
        }
        return simpleOver
    }
    
}


extension FavouriteForumsVC : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.forumList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ForumListTypeOneCell =  tableView.dequeueReusableCell(withIdentifier: forumCellTypeOne, for: indexPath) as! ForumListTypeOneCell
        
        let forumList = self.forumList[indexPath.row]
        cell.delegate = self
        cell.setUpForumList(forumlist: forumList)
        cell.showAlertClosure = { message in
            self.showAlert(self, message: message)
        }
        
        cell.shareForumPressed = { forumList in
            self.shareButtonPressed(forumlist:forumList)
        }
        
        cell.hashTagPressed = { hashTag in
            self.hashTagPressed(hashTag: hashTag)
        }
        
        cell.editForumPressed = { forumList in
            self.editForumButtonPressed(forumlist: forumList)
        }
        
        cell.deleteForumPressed = { forumList in
            self.deleteForumButtonPressed(forumlist: forumList)
        }
        
        cell.unFavouritePressed = { forumCell in
            self.unFavouritePressed()
        }
        
        //cell.setTextDescription(text: text, descriptionText: descriptionText)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return UITableView.automaticDimension
        
        let forumList = self.forumList[indexPath.row]
         
         
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
         tempLabel.font = UIFont.JosefinSansSemiBold(size: 16.0)
         tempLabel.numberOfLines = 0
         tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
         tempLabel.text = forumList.title ?? ""
         tempLabel.sizeToFit()
         
        // let calculatedHeight = tempLabel.frame.height
         
         let height = tempLabel.frame.height
         
         tempLabel.text = ""
         tempLabel.text = "A"
         tempLabel.sizeToFit()
         
         let twoLineTextHeight = tempLabel.frame.height
         
         var calculatedHeight : CGFloat = 0
         if height > twoLineTextHeight {
             calculatedHeight = twoLineTextHeight
         }else{
             calculatedHeight = height
         }
         
         
         
         
        tempLabel = ActiveLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 40, height: CGFloat.greatestFiniteMagnitude))
         tempLabel.font = UIFont.JosefinSansSemiBold(size: 15.0)
         tempLabel.numberOfLines = 0
         tempLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
         tempLabel.text = forumList.content ?? ""
         tempLabel.sizeToFit()
         
         //let calculatedHeightDescription = tempLabel.frame.height
         
         let heightDescription = tempLabel.frame.height
         
         tempLabel.text = ""
         tempLabel.text = "A \n B"
         tempLabel.sizeToFit()
         
         let twoLineTextHeightDescription = tempLabel.frame.height
         
         var calculatedHeightDescription : CGFloat = 0
         if heightDescription > twoLineTextHeightDescription {
             calculatedHeightDescription = twoLineTextHeightDescription
         }else{
             calculatedHeightDescription = heightDescription
         }
         
         if tempLabel.text != "" {
             
             return calculatedHeight + calculatedHeightDescription + 420
         }else{
             return calculatedHeight + 420
         }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return UIView()
    }
}
