//
//  ForumsListVC.swift
//  Pawxo
//
//  Created by 42works on 08/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class ForumsListVC: UIViewController {
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var forumCollectionView: UICollectionView!
    
    @IBOutlet weak var searchTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var indicatorViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var healthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var rescueConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lostFoundConstraint: NSLayoutConstraint!
    let simpleOver = ForumDetailsAnimation()
    
    var frameTest : CGRect?
    
    @IBOutlet weak var refreshButton: UIButton!
    
    @IBOutlet weak var outerMainView: UIView!
    
    
    @IBOutlet weak var cuddlLogo: UIImageView!
    
    @IBOutlet weak var menuTABSView: MenuTabsView!
    
    @IBOutlet weak var searchOuterView: UIView!
    
    @IBOutlet weak var lostLabel: RegularLabel!
    
    @IBOutlet weak var rescueLabel: RegularLabel!
    
    var selectedindexCategory = 0
    
    @IBOutlet weak var healthLabel: RegularLabel!
    
    @IBOutlet weak var searchTextField: UITextField!
    var categories = [Category]()
    var sort = [String]()
    
    let cellIdentifier = "ForumListCollectionCell"
    
    var hideContent = false
    
    var currentIndex: Int = 0
    var tabs = [String]()
    
    var originalCategories = [Category]()
    var selectedSort = ""
    var isAppliedFilter = false
    
    var requests = [ForumRequestModel]()
    
    var isSearchShown = false
    var selectedRequest : ForumRequestModel?
    
    var searchtext = ""
    
    var currentLat : Double?
    var currentLong : Double?
    
    
    
    @IBOutlet weak var searchOuterTrailingConstraintNew: NSLayoutConstraint!
    
    
    @IBOutlet weak var filterButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.setUpIndicatorView(index:0, animate: false, scroll: false, sender: lostLabel)
        forumCollectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        
        
        self.refreshButton.isHidden = true
        self.setSearchConstraint(show: false, animate: false)
        getCategories()
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(self.refreshContent),
        name: Notification.Name("RefreshForumList"),
        object: nil)
        // Do any additional setup after loading the view.
    }
    
    
    @objc func refreshContent(){
        self.isAppliedFilter = false
        self.selectedSort = ""
        self.setUpDefaultCategories()
        self.setTabsView()
        self.getAPIData()
        
        self.setUpFilterButton()
        self.menuTABSView.collView.selectItem(at: IndexPath.init(item: self.selectedindexCategory, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        self.menuTABSView.collView.scrollToItem(at: IndexPath.init(item: self.selectedindexCategory, section: 0), at: .centeredHorizontally
            , animated: false)
        
    }
    
    
    @IBAction func searchCrossButtonPressed(_ sender: Any) {
        self.searchTextField.text = ""
        self.searchtext = ""
        if self.isSearchShown{
            self.getAPIData()
        }
        self.searchTextField.resignFirstResponder()
        self.setSearchConstraint(show: false, animate: true)
    }
    
    
    
    func setSearchConstraint(show:Bool,animate:Bool){
        
        if show{
            self.cuddlLogo.isHidden = show
            self.searchOuterView.borderWidth = 1
            self.searchOuterView.cornerRadius = 17.5
            var duration = 0.0
            if animate{
                duration = 0.6
            }
            UIView.animate(withDuration: duration, animations: {
                self.searchLeadingConstraint.constant = 10
                self.searchTrailingConstraint.constant = 35
                self.searchOuterTrailingConstraintNew.constant = 5
                self.view.layoutIfNeeded()
                
            }) { (completed) in
                self.searchTextField.becomeFirstResponder()
            }
        }else{
            
            self.searchOuterView.cornerRadius = 0
            var duration = 0.0
            if animate{
                duration = 0.6
            }else{
                self.searchOuterView.borderWidth = 0
            }
            UIView.animate(withDuration: duration, animations: {
                self.searchLeadingConstraint.constant = (kAppDelegate.window?.frame.size.width)! - 133
                self.searchTrailingConstraint.constant = 0
                self.searchOuterTrailingConstraintNew.constant = 0
                self.view.layoutIfNeeded()
                
            }) { (completed) in
                self.cuddlLogo.isHidden = show
                self.searchOuterView.borderWidth = 0
            }
        }
    }
    
    
    func setTabsView(){
        
        self.tabs.removeAll()
        
        for category in self.categories {
            self.tabs.append(category.name!)
        }
        
        menuTABSView.dataArray = self.tabs
        menuTABSView.isSizeToFitCellsNeeded = true
        menuTABSView.collView.backgroundColor = UIColor.init(white: 0.97, alpha: 0.97)
        
        //presentPageVCOnView()
        menuTABSView.menuDelegate = self
        //For Intial Display
        menuTABSView.collView.selectItem(at: IndexPath.init(item: 0, section: 0), animated: true, scrollPosition: .centeredVertically)
        
    }
    
    
    @IBAction func refreshButtonPressed(_ sender: Any) {
        self.getCategories()
    }
    
    func getCategories(){
        let forum_VM = Forum_VM()
        forum_VM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if forum_VM.isLoading {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        forum_VM.responseRecieved = {
            // refresh content
            
            
            if let categories = forum_VM.categories {
                
                self.originalCategories = categories
                
                self.setUpDefaultCategories()
                
                
            }else{
                self.hideContent = true
            }
            if let sort = forum_VM.sort {
                    self.sort = sort
            }else{
                self.hideContent = true
            }
            
            
            if self.hideContent {
                // Show Error Viw
                self.refreshButton.isHidden = false
                
            }else{
                // Show view and proceed
                self.outerMainView.isHidden = true
                self.setTabsView()
                self.getAPIData()
            }
            
                
        }
        
        forum_VM.showAlertClosure = {
            DispatchQueue.main.async {
                if let message = forum_VM.alertMessage {
                    self.refreshButton.isHidden = false
                    self.showAlert(self, message: message)
                }
            }
        }
        
        forum_VM.callGetFiltersService = true
    }
    
    
    func getForumsModels(categories:[Category],sort:String) -> [ForumRequestModel]{
        
        var forumRequestModels = [ForumRequestModel]()
        if categories.count > 0 {
            for category in categories{
                let requestModel = ForumRequestModel()
                requestModel.name = category.name!
                requestModel.categoryId = category.id!
                requestModel.cat_area = category.cat_area!
                if let lat = self.currentLat,let long = self.currentLong{
                    requestModel.lat = "\(lat)"
                    requestModel.long = "\(long)"
                }
                requestModel.filter = sort
                if self.searchtext != ""{
                    requestModel.searchText = self.searchtext
                }else{
                    requestModel.searchText = ""
                }
                forumRequestModels.append(requestModel)
            }
            return forumRequestModels
        }else{
            return forumRequestModels
        }
    }
    
    
    
    func getAPIData(){
        let requests = self.getForumsModels(categories: self.categories, sort: self.selectedSort)
        let forum_VM = Forum_VM()
        forum_VM.userId = CommonFunctions.getuserFromDefaults().userId!
        forum_VM.requests = requests
        forum_VM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if forum_VM.isLoading {
                    LoaderView.shared.showLoader()
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        forum_VM.responseRecieved = {
            // refresh content
            DispatchQueue.main.async {
            print("Final Response recieved")
            self.requests = forum_VM.requests!
            self.selectedRequest = self.requests[0]
                if self.selectedRequest?.searchText != ""{
                    self.isSearchShown = true
                }else{
                    self.isSearchShown = false
                }
            self.forumCollectionView.reloadData()
            }
        }
        
        forum_VM.showAlertClosure = {
            DispatchQueue.main.async {
                if let message = forum_VM.alertMessage {
                    self.showAlert(self, message: message)
                }
            }
        }
        
        forum_VM.callGetForumListService = true
        
    }
    
    
    func refreshForumList(request:ForumRequestModel,cell:ForumListCollectionCell){
        let index = self.requests.lastIndex{$0.categoryId == request.categoryId}
        let forum_VM = Forum_VM()
        forum_VM.userId = CommonFunctions.getuserFromDefaults().userId!
        request.internetConnected = true
        forum_VM.requests = [request]
        forum_VM.updateLoadingStatus = {
            DispatchQueue.main.async {
                if forum_VM.isLoading {
                    if !cell.refresh.isRefreshing{
                    LoaderView.shared.showLoader()
                    }
                } else {
                    LoaderView.shared.hideLoader()
                }
            }
        }
        
        forum_VM.responseRecieved = {
            // refresh content
            DispatchQueue.main.async {
            print("Pull To Response recieved")
                cell.refresh.endRefreshing()
                self.requests[index!] = forum_VM.requests![0]
                let forumList = self.requests[index!].data
                cell.request = self.requests[index!]
                cell.setForumList(forumList:forumList)
                
            }
        }
        
        forum_VM.showAlertClosure = {
            DispatchQueue.main.async {
                if let message = forum_VM.alertMessage {
                    self.showAlert(self, message: message)
                }
            }
        }
        
        forum_VM.callGetForumListService = true
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.forumCollectionView.reloadData()
        
        
        let visibleRect = CGRect(origin: forumCollectionView.contentOffset, size: forumCollectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = forumCollectionView.indexPathForItem(at: visiblePoint){
        
        if let cell = forumCollectionView.cellForItem(at: visibleIndexPath) as? ForumListCollectionCell{
            cell.forumTableView.reloadData()
        }
        }
        
        self.navigationController?.delegate = nil
        if self.categories.count > 0 {
            self.menuTABSView.collView.scrollToItem(at: IndexPath(item: self.selectedindexCategory, section: 0), at: .centeredHorizontally, animated: false)
        }
    }
    
    
    @IBAction func addForumPressed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Forum", bundle: nil)
        let vc_AddForum = storyboard.instantiateViewController(withIdentifier: "AddForumVC") as! AddForumVC
        vc_AddForum.categories = self.categories
        
        vc_AddForum.refreshData = {
            
            let request = self.requests[self.selectedindexCategory]
            request.page = 1
            let cell = self.forumCollectionView.cellForItem(at: IndexPath(item: self.selectedindexCategory, section: 0)) as! ForumListCollectionCell
            self.refreshForumList(request: request, cell: cell)
        }
        self.navigationController?.pushViewController(vc_AddForum, animated: true)
        
        
    }
    
    
    @IBAction func favouritePressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Forum", bundle: nil)
        let vc_favForum = storyboard.instantiateViewController(withIdentifier: "FavouriteForumsVC") as! FavouriteForumsVC
        vc_favForum.categories = self.originalCategories
        self.navigationController?.pushViewController(vc_favForum, animated: true)
    }
    
    func hashTagPressed(hashTag:String){
        let storyboard = UIStoryboard(name: "Forum", bundle: nil)
        let vc_favForum = storyboard.instantiateViewController(withIdentifier: "FavouriteForumsVC") as! FavouriteForumsVC
        vc_favForum.hashTag = hashTag
        vc_favForum.categories = self.originalCategories
        self.navigationController?.pushViewController(vc_favForum, animated: true)
    }
    
    
    @IBAction func filterPressed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Forum", bundle: nil)
        let filter = storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        filter.modalPresentationStyle = .overCurrentContext
        filter.modalTransitionStyle = .crossDissolve
        filter.categories = self.originalCategories
        filter.selectedCategories =  self.isAppliedFilter ? self.categories : [Category]()
        filter.selectedSort = self.selectedSort
        
        filter.categorySelectedAndSort = { sort, categories in
            if sort != "" {
                self.selectedSort = sort
                self.isAppliedFilter = true
            }
            
            if categories.count > 0 {
                self.isAppliedFilter = true
                self.categories = categories
                self.setTabsView()
                self.getAPIData()
            }else{
                
            }
            self.setUpFilterButton()
        }
        
        filter.clearFilters = {
            self.refreshContent()
        }
        self.present(filter, animated: true, completion: nil)
    }
    
    func setUpDefaultCategories(){
        self.categories.removeAll()
        for category in self.originalCategories{
            self.categories.append(category.copy())
        }
    }
    
    func setUpFilterButton(){
        if self.isAppliedFilter {
            self.filterButton.setImage(UIImage(named: "filterSelected"), for: .normal)
        }else{
            self.filterButton.setImage(UIImage(named: "filterIcon"), for: .normal)
        }
    }
    
    @IBAction func searchButtonPressed(_ sender: Any) {
       
        self.setSearchConstraint(show: true, animate: true)
        
        
//        let storyboard = UIStoryboard(name: "Forum", bundle: nil)
//        let vc_searchForum = storyboard.instantiateViewController(withIdentifier: "SearchForumVC") as! SearchForumVC
//        self.navigationController?.pushViewController(vc_searchForum, animated: true)
    }
    
    
    
    func shareButtonPressed(forumlist:ForumList){
        
        var title = ""
        title = forumlist.title ?? ""
        
        
            var descriptionPost = ""
            descriptionPost = forumlist.content ?? ""
            //let mediaLink =  cell.post?.media?.first ?? ""
            var content = ""
            
        for mediaItem in forumlist.mediaItems!{
            content.append(contentsOf: mediaItem.imageVideoUrl!)
            content.append(contentsOf: "\n")
        }
        
        
            let text = "Checkout this post!! \n \(title)\n \(content)\n \(descriptionPost) Let’s make this world a happier and better place for innocent beings, who do the same for us! \n Download https://play.google.com/ http://itunes.apple.com/"
            //let shareimage = UIImage(named: "Pet")
          
            let imageToShare = [text] as [Any]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.setValue("Cuddle | Check out", forKey: "subject")
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            //        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    func editForumButtonPressed(forumlist:ForumList){
        
        // Edit Forum Pressed
        let storyboard = UIStoryboard(name: "Forum", bundle: nil)
               let vc_AddForum = storyboard.instantiateViewController(withIdentifier: "AddForumVC") as! AddForumVC
               vc_AddForum.categories = self.categories
                vc_AddForum.forumList = forumlist
        vc_AddForum.refreshData = {
            self.refreshContent()
        }
               self.navigationController?.pushViewController(vc_AddForum, animated: true)
    }
    
    func deleteForumButtonPressed(forumlist: ForumList,indexPath:IndexPath,cell:ForumListCollectionCell){
        
        
        let alertController = UIAlertController(title: AppMessages.deleteForum, message: nil, preferredStyle: UIAlertController.Style.alert)
        
        let somethingAction = UIAlertAction(title: "OK", style: .default, handler: {(alert: UIAlertAction!) in print("Report API call")
            
            var forumVM = Forum_VM()
            forumVM.userId = CommonFunctions.getuserFromDefaults().userId
            forumVM.forumId = "\((forumlist.id)!)"
            
            forumVM.updateLoadingStatus = {
                DispatchQueue.main.async {
                    if forumVM.isLoading ?? false {
                        LoaderView.shared.showLoader()
                    } else {
                        LoaderView.shared.hideLoader()
                    }
                }
            }
            
            forumVM.responseRecieved = {
                // refresh button like
                
                 self.requests[self.selectedindexCategory].data.remove(at: indexPath.row)
                
                cell.forumList.remove(at: indexPath.row)
                cell.forumTableView.deleteRows(at: [indexPath], with: .automatic)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    cell.setForumList(forumList: cell.forumList)
                }
                //  self.postList.remove(at: indexPath!.row)
                //  self.homeTableView.deleteRows(at: [indexPath!], with: .automatic)
            }
            
            forumVM.showAlertClosure = {
                DispatchQueue.main.async {
                    if let message = forumVM.alertMessage {
                        // self.showAlert(self , message: message)//
                    }
                }
            }
            
            forumVM.callDeleteForumService = true
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
        
        alertController.addAction(somethingAction)
        alertController.addAction(cancelAction)
        alertController.view.tintColor = UIColor.black
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
    }
    
    
    func goToProfileView(userId:Int){
        let storyboardProfile = UIStoryboard(name: "Profile", bundle: nil)
        let vc_Profile = storyboardProfile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        vc_Profile.isFromOtherScreen = true
        vc_Profile.userId = "\(userId)"
        self.navigationController?.pushViewController(vc_Profile, animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setUpIndicatorView(index:Int, animate:Bool,scroll:Bool,sender:UILabel){
        self.indicatorViewWidthConstraint.constant = sender.bounds.size.width
        if index == 0 {
            
            self.lostLabel.textColor = AppColor.orangeColor
            self.rescueLabel.textColor = AppColor.colorGrey
            self.healthLabel.textColor = AppColor.colorGrey
            self.rescueConstraint.isActive = false
            self.healthConstraint.isActive = false
            self.lostFoundConstraint.isActive = true
            
        }else if index == 1 {
            self.lostLabel.textColor = AppColor.colorGrey
            self.rescueLabel.textColor = AppColor.orangeColor
            self.healthLabel.textColor = AppColor.colorGrey
            self.lostFoundConstraint.isActive = false
            self.healthConstraint.isActive = false
            self.rescueConstraint.isActive = true
            if scroll {
                //self.scrollToIndex(index: 1)
            }
        }else{
            self.lostLabel.textColor = AppColor.colorGrey
            self.rescueLabel.textColor = AppColor.colorGrey
            self.healthLabel.textColor = AppColor.orangeColor
            self.lostFoundConstraint.isActive = false
            self.rescueConstraint.isActive = false
            self.healthConstraint.isActive = true
            if scroll {
                //self.scrollToIndex(index: 2)
            }
        }
        if animate {
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
                if scroll {
                    self.scrollToIndex(index: index)
                }
            }
        }
        
    }
    
    
    func scrollToIndex(index:Int){
       // DispatchQueue.main.async {
            let indexPath = IndexPath(
                row:index, section: 0)
            self.forumCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
            self.forumCollectionView.reloadData()
       // }
    }
    
    @IBAction func lostAndFoundPressed(_ sender: UIButton) {
        self.indicatorViewWidthConstraint.constant = sender.bounds.size.width
        self.setUpIndicatorView(index: 0, animate: true, scroll: true, sender: lostLabel)
    }
    
    @IBAction func rescueButtonPressed(_ sender: UIButton) {
        self.indicatorViewWidthConstraint.constant = sender.bounds.size.width
        self.setUpIndicatorView(index: 1, animate: true, scroll: true, sender: rescueLabel)
    }
    
    @IBAction func healthButtonPressed(_ sender: UIButton) {
        self.setUpIndicatorView(index: 2, animate: true, scroll: true, sender: healthLabel)
    }
    
    
    
    
    func openAlertForLocation(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let locAlert = storyboard.instantiateViewController(withIdentifier: "LocationAlertVC") as! LocationAlertVC
        locAlert.modalPresentationStyle = .overCurrentContext
        locAlert.modalTransitionStyle = .crossDissolve
        locAlert.continuePressed = {
            if let vc = UIStoryboard(name: StoryboardName.Settings, bundle: nil).instantiateViewController(withIdentifier: "EditLocationVC") as? EditLocationVC{
                vc.isEdit = true
                self.navigationController?.pushViewController(vc, animated: true)
                vc.delegate = self
            }
        }
        self.present(locAlert, animated: true, completion: nil)
    }
    
}


extension ForumsListVC : EditLocationVCProtocol {
    func locationDetails(lat: Double, long: Double, address: String) {
        self.currentLat = lat
        self.currentLong = long
        NotificationCenter.default.post(name: Notification.Name("RefreshForumList"), object: nil, userInfo: nil)
    }
    
}
extension ForumsListVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let forumCell : ForumListCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ForumListCollectionCell
        forumCell.cellType = indexPath.item
        forumCell.delegate = self
        let forumList = self.requests[indexPath.item].data
        forumCell.request = self.requests[indexPath.item]
        forumCell.setForumList(forumList:forumList)
//        forumCell.endRefreshing()
        forumCell.showAlertClosure = { message in
            self.showAlert(self, message: message)
        }
        
        forumCell.shareForumPressed = { forumList in
            self.shareButtonPressed(forumlist: forumList)
        }
        
        forumCell.listToRefresh = { request,refresh in
            self.refreshForumList(request: request, cell: refresh)
        }
        
        forumCell.hashTagPressed  = { hashTag in
            self.hashTagPressed(hashTag: hashTag)
        }
        
        forumCell.editForumPressed = { forumList in
            self.editForumButtonPressed(forumlist: forumList)
        }
        
        forumCell.deleteForumPressed = { forumList,indexPaths,cell in
            self.deleteForumButtonPressed(forumlist: forumList,indexPath: indexPaths,cell: cell)
        }
        
        forumCell.tryAgainPressed = { request,refresh in
            self.refreshForumList(request: request, cell: refresh)
        }
        return forumCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height =  self.view.bounds.size.height - (self.view.safeAreaInsets.top + self.view.safeAreaInsets.bottom + 101)
        return CGSize(width: self.view.frame.width, height: collectionView.frame.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let visibleRect = CGRect(origin: forumCollectionView.contentOffset, size: forumCollectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = forumCollectionView.indexPathForItem(at: visiblePoint)
        // print(visibleIndexPath?.item)
        
        self.selectedindexCategory = visibleIndexPath!.item
        self.selectedRequest = self.requests[visibleIndexPath!.item]
        if self.selectedRequest?.cat_area != 3 && self.currentLat == nil{
                       self.openAlertForLocation()
                   }
        self.menuTABSView.collView.scrollToItem(at: visibleIndexPath!, at: .centeredHorizontally, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            
            self.menuTABSView.collView.selectItem(at: visibleIndexPath!, animated: true, scrollPosition: .centeredVertically)
            
        }
//        if(visibleIndexPath?.item ==  0){
//           // self.setUpIndicatorView(index:0, animate: true, scroll: false, sender: lostLabel)
//        }else if(visibleIndexPath?.item ==  1){
//           // self.setUpIndicatorView(index:1, animate: true, scroll: false, sender: rescueLabel)
//        }else{
//            //self.setUpIndicatorView(index:2, animate: true, scroll: false, sender: healthLabel)
//        }
        
    }
}


extension ForumsListVC: MenuBarDelegate {

    func menuBarDidSelectItemAt(menu: MenuTabsView, index: Int) {

        // If selected Index is other than Selected one, by comparing with current index, page controller goes either forward or backward.
            
            self.scrollToIndex(index: index)
            self.selectedRequest = self.requests[index]
            self.selectedindexCategory = index
            menuTABSView.collView.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: true)
            if self.selectedRequest?.cat_area != 3 && self.currentLat == nil{
                self.openAlertForLocation()
            }

    }

}


extension ForumsListVC : ForumListCollectionCellProtocol {
    func cellClicked(frame: CGRect,index:Int,forumList:ForumList,image:UIImage?,cell:ForumListCollectionCell) {
        print(frame)
        
        self.frameTest = frame
        let storyboard = UIStoryboard(name: "Forum", bundle: nil)
        let vc_ForumDetail = storyboard.instantiateViewController(withIdentifier: "ForumDetailVC") as! ForumDetailVC
        vc_ForumDetail.imageCenter = image ?? DefaultImage.defaultPostImage
        vc_ForumDetail.imageFrame = self.frameTest
        vc_ForumDetail.delegate = self
        vc_ForumDetail.forumList = forumList
        vc_ForumDetail.selectedIndex = index
        vc_ForumDetail.categories = self.originalCategories
        vc_ForumDetail.deletedForum = {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                
                self.requests[self.selectedindexCategory].data.remove(at:index)
                cell.forumList.remove(at: index)
                cell.forumTableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    cell.setForumList(forumList:cell.forumList)
                }
            }
        }
        
        self.navigationController?.delegate = self
        self.navigationController?.pushViewController(vc_ForumDetail, animated: true)
    }
    
    func profileClicked(userId:Int){
        self.goToProfileView(userId: userId)
    }
    
}

extension ForumsListVC: ForumDetailVCProtocol {
     func setNavigationDelegate() {
        self.navigationController?.delegate = self
    }
}

class ForumDetailsAnimation: NSObject, UIViewControllerAnimatedTransitioning {

    var popStyle: Bool = false
    
    var originFrame: CGRect?
    
//    init(originFrame: CGRect) {
//      self.originFrame = originFrame
//    }
    
    func transitionDuration(
        using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        if popStyle {

            animatePop(using: transitionContext)
            return
        }

        let fz = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let tz = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        let tzN = tz as! ForumDetailVC
        
        let f = transitionContext.finalFrame(for: tz)

        let fOff = f.offsetBy(dx: f.width, dy: f.height)
       // tz.view.frame = fOff
//        tzN.heightConstraint.isActive = false
//        tzN.widthConstraint.isActive = false
//        tzN.topConstraint.isActive = false
//        tzN.leftConstraint.isActive = false
//        tzN.rightConstraint.isActive = false
//        tzN.topConstraint.isActive = false
//        tzN.heightConstraint.isActive = false
        
       // tzN.imgView.frame = originFrame!
        tzN.view.bringSubviewToFront(tzN.boxView)
        let imageView = tzN.centerImageView
        imageView?.frame = self.originFrame!
        //tzN.topView.alpha = 0
        tzN.postDetailTableView.alpha = 0
        tzN.backButton.alpha = 0
        let rect = tzN.cuddlLogo.frame
        //tzN.boxView.alpha = 0
        imageView?.layer.cornerRadius = 12
        transitionContext.containerView.insertSubview(tz.view, aboveSubview: fz.view)
        tzN.aView.isHidden = true
        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            animations: {
                imageView?.frame = CGRect(x: 0, y: (tzN.view.safeAreaInsets.top + tzN.topView.frame.size.height), width: tzN.view.frame.width , height: 254)
                tzN.cuddlLogo.frame = CGRect(x: (tzN.view.frame.width/2) - 56, y: rect.origin.y, width: rect.width, height:rect.height)
                tzN.topView.alpha = 1.0
                tzN.boxView.alpha = 1.0
                //tzN.cuddlLeadingConstraint.constant = (tzN.view.frame.width/2) - 56
                imageView?.layer.cornerRadius = 0
                tzN.postDetailTableView.alpha = 1.0
                tzN.backButton.alpha = 1.0
              //  tzN.imgView.layer.cornerRadius = 0
        }, completion: {_ in
//            tzN.leftConstraint.isActive = true
//            tzN.rightConstraint.isActive = true
//            tzN.topConstraint.isActive = true
//            tzN.heightConstraint.isActive = true
          //  tzN.setUpConstraintForImageView()
            transitionContext.completeTransition(true)
        })
    }
    
    
    func animatePop(using transitionContext: UIViewControllerContextTransitioning) {

        let fz = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let tz = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!

        let f = transitionContext.initialFrame(for: fz)
        let fOffPop = f.offsetBy(dx: f.width, dy: 55)
        
        
        let tzN = fz as! ForumDetailVC
        let imageView = tzN.centerImageView
        imageView?.isHidden = false
        imageView?.removeAllConstraints()
        tzN.aView.isHidden = true
        let rect = tzN.cuddlLogo.frame
        transitionContext.containerView.insertSubview(tz.view, belowSubview: fz.view)
        
        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            animations: {
                
                imageView?.frame = self.originFrame!
                tzN.cuddlLogo.frame = CGRect(x: 10, y: rect.origin.y, width: rect.width, height:rect.height)
                tzN.backButton.alpha = 0
                tzN.postDetailTableView.alpha = 0
                //tzN.boxView.alpha = 0
                imageView?.layer.cornerRadius = 12
        }, completion: {_ in
                transitionContext.completeTransition(true)
        })
    }
}


extension ForumsListVC: UIViewControllerTransitioningDelegate,UINavigationControllerDelegate {

//func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//    return FlipPresentAnimationController(originFrame: frameTest!)
//}
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//            return FlipPresentAnimationController(originFrame: frameTest!)
        simpleOver.popStyle = (operation == .pop)
        if !simpleOver.popStyle {
            simpleOver.originFrame = self.frameTest
        }else{
            
        }
        return simpleOver
    }
    
}


extension ForumsListVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("Return Pressed")
        
        if textField.text != ""{
            self.searchtext = textField.text!
            self.getAPIData()
        }
        return false
    }
}
