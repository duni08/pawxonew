//
//  LocationAlertVC.swift
//  Pawxo
//
//  Created by 42works on 12/03/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class LocationAlertVC: UIViewController {

    
    var continuePressed:(()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func continueButtonPressed(_ sender: Any) {
        self.continuePressed?()
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
