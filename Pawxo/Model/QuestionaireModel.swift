//
//  QuestionaireModel.swift
//  
//
//  Created by 42Works-Worksys2 on 05/01/18.
//

import UIKit

class QuestionaireModel : NSObject {
    var category: String
    var name: String
    var breed: String
    var gender: Int
    var ageYear: String
    var ageMonth: String
    var ageDate: String
    var profileUrl: String
    var location: String
    var lat: Float
    var long: Float
    var profileImage: UIImage?
    
    override init() {
        category = ""
        name = ""
        breed = ""
        gender = 0
        ageYear = ""
        ageMonth = ""
        ageDate = ""
        profileUrl = ""
        location = ""
        lat = 0.0
        long = 0.0
    }
    
    func resetQuestionaire() {
        category = ""
        name = ""
        breed = ""
        gender = 0
        ageYear = ""
        ageMonth = ""
        ageDate = ""
        profileUrl = ""
        location = ""
        lat = 0.0
        long = 0.0
    }
}


struct GoogleAutoComplete {
    
    var title:String?
    var subtitle:String?
    var placeId:String?
    
    init(googleInfo:NSDictionary) {
        
        if let titleStr = googleInfo.object(forKey: "description") as? String {
            title = titleStr
        }
        
        if let placeIdStr = googleInfo.object(forKey: "place_id") as? String {
            placeId = placeIdStr
        }
        
        subtitle = ""
    }
    

}


