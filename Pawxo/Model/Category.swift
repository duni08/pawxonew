//
//  Category.swift
//  Pawxo
//
//  Created by 42works on 11/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation


class Category: Codable {
    var id: Int?
    var name: String?
    var count: Int?
    var isSelectedCategory : Bool?
    var cat_area : Int?
    
    func copy(with zone: NSZone? = nil) -> Category {
        let copy = Category()
        copy.id = self.id
        copy.name = self.name
        copy.count = self.count
        copy.cat_area = self.cat_area
        copy.isSelectedCategory = self.isSelectedCategory
        return copy
    }
    
}



class FilterData: Codable {
    var options: [FilterOption]?
    var sort: [String]?

    init(options: [FilterOption], sort: [String]) {
        self.options = options
        self.sort = sort
    }
}


class FilterOption: Codable {
    var name: String?
    var collection: [Category]?

    init(name: String, collection: [Category]) {
        self.name = name
        self.collection = collection
    }
}
