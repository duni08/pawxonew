//
//  NotificationList.swift
//  Pawxo
//
//  Created by 42Works on 07/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit


class NotificationListReceivedResponse: Codable {
    var name: String?
    var id: Int?
    var notificationList: [NotificationList]?
    var total, totalPage: Int?
    var currentage: String?

    enum CodingKeys: String, CodingKey {
        case name, id
        case notificationList = "notification_list"
        case total
        case totalPage = "total_page"
        case currentage
    }

    init(name: String, id: Int, notificationList: [NotificationList], total: Int, totalPage: Int, currentage: String) {
        self.name = name
        self.id = id
        self.notificationList = notificationList
        self.total = total
        self.totalPage = totalPage
        self.currentage = currentage
    }
}
class NotificationList: Codable {
    var id, notificationID: Int?
    var notificationType: String?
    var isAccepted: Int?
    var sender_id: Int?
    var image: String?
    var postMedia, notification, name: String?
    var isRead, isFollowed: Int?
    var createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case notificationID = "notification_id"
        case notificationType = "notification_type"
        case isAccepted = "is_accepted"
        case sender_id = "sender_id"
        case image
        case postMedia = "post_media"
        case notification, name
        case isRead = "is_read"
        case isFollowed = "is_followed"
        case createdAt = "created_at"
    }

    init(id: Int, notificationID: Int, notificationType: String, isAccepted: Int, image: String, postMedia: String, notification: String, name: String, isRead: Int, isFollowed: Int, createdAt: String,sender_id: Int) {
        self.id = id
        self.notificationID = notificationID
        self.notificationType = notificationType
        self.isAccepted = isAccepted
        self.image = image
        self.postMedia = postMedia
        self.notification = notification
        self.name = name
        self.sender_id = sender_id
        self.isRead = isRead
        self.isFollowed = isFollowed
        self.createdAt = createdAt
    }
}
