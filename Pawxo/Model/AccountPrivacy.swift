//
//  AccountPrivacy.swift
//  Pawxo
//
//  Created by Macbook on 01/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation

class AccountPrivacy: Codable {
    var accountPrivateStatus: Int?

    enum CodingKeys: String, CodingKey {
        case accountPrivateStatus = "account_private_status"
    }

    init(accountPrivateStatus: Int) {
        self.accountPrivateStatus = accountPrivateStatus
    }
}
