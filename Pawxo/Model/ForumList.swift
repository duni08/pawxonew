//
//  ForumList.swift
//  Pawxo
//
//  Created by 42works on 11/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation




class ForumList: Codable {
    var id: Int?
    var title, content: String?
    var image0: String?
    var image1, image2, image3: String?
    var image0_Type: String?
    var image1_Type, image2_Type, image3_Type: String?
    var image0_Thumb: String?
    var image1_Thumb, image2_Thumb, image3_Thumb: String?
    var longitude,latitude,location_name : String?
    var category: Category?
    var tags: [HashTag]?
    var user: Userlist?
    var stats: Stats?
    var approved, hasLiked, hasFollowed: Bool?
    var createdAt, updatedAt: String?
    var mediaItems : [ForumMediaItem]?
    enum CodingKeys: String, CodingKey {
        case id, title, content
        case image0 = "image_0"
        case image1 = "image_1"
        case image2 = "image_2"
        case image3 = "image_3"
        case image0_Type = "image_0_type"
        case image1_Type = "image_1_type"
        case image2_Type = "image_2_type"
        case image3_Type = "image_3_type"
        case image0_Thumb = "image_0_thumb"
        case image1_Thumb = "image_1_thumb"
        case image2_Thumb = "image_2_thumb"
        case image3_Thumb = "image_3_thumb"
        case category, tags, user, stats, approved
        case hasLiked = "has_liked"
        case hasFollowed = "has_followed"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case longitude = "longitude"
        case latitude = "latitude"
        case location_name = "location_name"
    }
    
    
    
    func setNewValues(forumList:ForumList){
        self.id = forumList.id
        self.title = forumList.title
        self.content = forumList.content
        self.image0 = forumList.image0
        self.image1 =  forumList.image1
        self.image2 = forumList.image2
        self.image3 = forumList.image3
        self.image0_Type = forumList.image0_Type
        self.image1_Type = forumList.image1_Type
        self.image2_Type = forumList.image2_Type
        self.image3_Type = forumList.image3_Type
        self.image0_Thumb = forumList.image0_Thumb
        self.image1_Thumb = forumList.image1_Thumb
        self.image2_Thumb = forumList.image2_Thumb
        self.image3_Thumb = forumList.image3_Thumb
        self.category = forumList.category
        self.tags = forumList.tags
        self.user = forumList.user
        self.stats = forumList.stats
        self.approved = forumList.approved
        self.hasLiked = forumList.hasLiked
        self.hasFollowed = forumList.hasFollowed
        self.createdAt = forumList.createdAt
        self.updatedAt = forumList.updatedAt
        self.location_name = forumList.location_name
        self.latitude = forumList.latitude
        self.longitude = forumList.longitude
        self.mediaItems = forumList.mediaItems
    }
}


class Stats: Codable {
    var likes, threads, followers: Int?

    init(likes: Int, threads: Int, followers: Int) {
        self.likes = likes
        self.threads = threads
        self.followers = followers
    }
}






class ForumRequestModel {
    var name = ""
    var data = [ForumList]()
    var filter : String?
    var page = 1
    var limit = 25
    var hashTags = ""
    var categoryId : Int?
    var cat_area : Int?
    var hashTag : String?
    var sort : String?
    var isLoaded = false
    var hasMore = false
    var searchText = ""
    var internetConnected = true
    var lat : String?
    var long: String?
}
