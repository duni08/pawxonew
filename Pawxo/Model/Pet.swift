//
//  Pet.swift
//  PetConnet
//
//  Created by 42Works-Worksys2 on 16/01/18.
//  Copyright © 2018 42Works-Worksys2. All rights reserved.
//

import UIKit
import Foundation

class Pet:Codable {
    
    var petBreed : String?
    var petId : String?
    var petImages: String?
    var petName: String?
    var petType: String?
    var favCount:String?
    var isFav:Int?
    var isPoke:Int?
    var location:String?
    var petDob:String?
    var pokeCount:String?
    var petBio:String?
    var petGender:Int?
    
    init(petInfo:NSDictionary) {
        
        if let petBreedValue = petInfo.object(forKey: "pet_breed") as? String {
            self.petBreed = petBreedValue
        }
        
        if let petIdValue = petInfo.object(forKey: "pet_id") as? Int {
            self.petId = "\(petIdValue)"
        }
        
        if let petImagesValue = petInfo.object(forKey: "pet_images") as? String {
            self.petImages = petImagesValue
        }
        
        if let petNameValue = petInfo.object(forKey: "pet_name") as? String {
            self.petName = petNameValue
        }
        
        if let petTypeValue = petInfo.object(forKey: "pet_type") as? String {
            self.petType = petTypeValue
        }

        if let favCountValue = petInfo.object(forKey: "fav_count") as? Int {
            self.favCount = "\(favCountValue)"
        }
        
        if let isFavValue = petInfo.object(forKey: "is_fav") as? Int {
            self.favCount = "\(isFavValue)"
        }
        
        if let isPokeValue = petInfo.object(forKey: "is_poke") as? Int {
            self.isPoke = isPokeValue
        }
        
        if let locationValue = petInfo.object(forKey: "location") as? String {
            self.location = locationValue
        }
        
        if let petDobValue = petInfo.object(forKey: "pet_dob") as? String {
            self.petDob = petDobValue
        }
        
        if let pokeCountValue = petInfo.object(forKey: "poke_count") as? Int {
            self.pokeCount = "\(pokeCountValue)"
        }
        
        if let petBioValue = petInfo.object(forKey: "pet_bio") as? String {
            self.petBio = petBioValue
        }
        
        if let petGenderValue = petInfo.object(forKey: "pet_gender") as? String {
            self.petGender = (petGenderValue as NSString).integerValue
        }
        
    }
    
}




class PetRecievedResponse: NSObject, Codable {
    let name: String?
    let id: Int?
    let petList: [PetList]?
    let total, totalPage: Int?
    let currentage: String?

    enum CodingKeys: String, CodingKey {
        case name, id
        case petList = "pet_list"
        case total
        case totalPage = "total_page"
        case currentage
    }
    
}




class PetList: Codable {
    var petName: String?
    let petID: Int?
    var petType, petBreed, petGender, petDob: String?
    var petBio: String?
    var location: String?
    var isFav, isPoke, favCount, pokeCount: Int?
    var petImages: String?
    
    var isSelectedPet = false
    
    enum CodingKeys: String, CodingKey {
        case petName = "pet_name"
        case petID = "pet_id"
        case petType = "pet_type"
        case petBreed = "pet_breed"
        case petGender = "pet_gender"
        case petDob = "pet_dob"
        case petBio = "pet_bio"
        case location
        case isFav = "is_fav"
        case isPoke = "is_poke"
        case favCount = "fav_count"
        case pokeCount = "poke_count"
        case petImages = "pet_images"
    }

    
    
    init(petName: String, petID: Int, petType: String, petBreed: String, petGender: String, petDob: String, petBio: String?, location: String, isFav: Int, isPoke: Int, favCount: Int, pokeCount: Int, petImages: String) {
        self.petName = petName
        self.petID = petID
        self.petType = petType
        self.petBreed = petBreed
        self.petGender = petGender
        self.petDob = petDob
        self.petBio = petBio
        self.location = location
        self.isFav = isFav
        self.isPoke = isPoke
        self.favCount = favCount
        self.pokeCount = pokeCount
        self.petImages = petImages
    }
}



//class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}

