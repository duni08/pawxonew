//
//  Notification.swift
//  Pawxo
//
//  Created by Macbook on 01/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation



// MARK: - DataClass
class NotificationSetting: Codable {
    var id, likePush, commentPush, tagCommentPush,comment_forum_push,like_forum_push,receive_follow_request_push,mention_comment: Int?
    var tagPostPush, receiveFollowPush, pokePush, favouritePush: Int?
    var userID: Int?
    var createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case comment_forum_push
        case like_forum_push
        case mention_comment
        case receive_follow_request_push
        case likePush = "like_push"
        case commentPush = "comment_push"
        case tagCommentPush = "tag_comment_push"
        case tagPostPush = "tag_post_push"
        case receiveFollowPush = "receive_follow_push"
        case pokePush = "poke_push"
        case favouritePush = "favourite_push"
        case userID = "user_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}


