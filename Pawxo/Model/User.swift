//
//  User.swift
//  AutoForce
//
//  Created by Apple2 on 21/08/19.
//  Copyright © 2019 Apple2. All rights reserved.
//

import UIKit
import Foundation

class User: Codable {
    
    var bio:String?
    var email:String?
    var gender:String?
    var id:String?
    var latitude:String?
    var location:String?
    var name:String?
    var userId:String?
    var phoneNumber:String?
    var userImage:String?
    var userName:String?
    var agreement_Status : Int?
    var is_signup : Int?
    var hasPets = false
    
    var pets : [PetList]?
    
    init(userInfo:NSDictionary) {
        
        if let nameValue = userInfo.object(forKey: "name") as? String {
            self.name = nameValue
        }
        
        if let userIdValue = userInfo.object(forKey: "id") as? Int {
            self.userId = "\(userIdValue)"
        }
        
        if let bioValue = userInfo.object(forKey: "bio") as? String {
            self.bio = bioValue
        }
        
        if let agreement_Status = userInfo.object(forKey: "agreement_status") as? Int {
            self.agreement_Status = agreement_Status
        }
        
        if let pets = userInfo.object(forKey: "pets") as? [NSDictionary] {
            
            if pets.count > 0 {
                self.hasPets = true
            }
            self.pets = [PetList]()
            
        }
        
        if let usernameValue = userInfo.object(forKey: "username") as? String {
            self.userName = usernameValue
        }
        
        if let userImageValue = userInfo.object(forKey: "user_image") as? String {
            self.userImage = userImageValue
        }
        
        if let emailValue = userInfo.object(forKey: "email") as? String {
            self.email = emailValue
        }
        
        if let genderValue = userInfo.object(forKey: "gender") as? String {
            self.gender = genderValue
        }
        
        if let latitudeValue = userInfo.object(forKey: "latitude") as? String {
            self.latitude = latitudeValue
        }
        
        if let locationValue = userInfo.object(forKey: "location") as? String {
            self.location = locationValue
        }
        
        if let is_signup = userInfo.object(forKey: "is_signup") as? Int {
            self.is_signup = is_signup
        }
    }
    
    
//    var username: String = ""
//    var name: String = ""
//    var id: String = ""
//    var userId: Int = 0
//    var profileImage: String = ""
//    var dummyImage: UIImage? = #imageLiteral(resourceName: "demoUser")
//    var following: Bool = false
//    var followedCount = 0
//    var followingCount = 0
//    var location = ""
//    var bio = ""
//    var phone = ""
//    var gender = ""
//    var email = ""
//    var isSelected : Bool = false
//    var userPets : NSArray = []
    

}



class Userlist: Codable {
    var id: Int?
    var chatid: Int?
    var name, email, username, image: String?
    var dob, location, latitude,phone,status,gender, longitude: String?
    var bio, userImage: String?
    var isFollowed: Int?
    var isRequested: String?
    var following: [UserNew]?
    var followingCount, followerCount,total_post,isProfilePrivate,block : Int?
    var petList: [PetList]?
    var pets : [PetList]?
    var is_user_blocked : Int?
    
    
    enum CodingKeys: String, CodingKey {
        case id, name, email, username, dob, location, latitude, longitude, bio
        case image = "image"
        case userImage = "user_image"
        case isFollowed = "is_followed"
        case isProfilePrivate = "is_profile_private"
        case isRequested = "is_requested"
        case followingCount, followerCount , following, total_post, phone, status
        case petList = "pet_list"
        case pets = "pets"
        case gender = "gender"
        case chatid = "chat_id"
        case block = "block"
        case is_user_blocked = "is_user_blocked"
    }

}
