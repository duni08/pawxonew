//
//  Comment.swift
//  Pawxo
//
//  Created by 42works on 04/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation


class CommentList: Codable {
    var id: Int?
    var name, createdAt: String?
    var userID: Int?
    var userName: String?
    var userImage: String?
    var commentTag: [HashTag]?
    var taggedUser: [Userlist]?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case createdAt = "created_at"
        case userID = "user_id"
        case userName = "user_name"
        case userImage = "user_image"
        case commentTag = "comment_tag"
        case taggedUser = "tagged_user"
    }
}

class CommentResponseList: Codable {
    var userImage: String?
    var commentList: [CommentList]?

    enum CodingKeys: String, CodingKey {
        case userImage = "user_image"
        case commentList
    }

    init(userImage: String, commentList: [CommentList]) {
        self.userImage = userImage
        self.commentList = commentList
    }
}


class CommentAddResponse: Codable {
    var comment: ForumCommentList?

    enum CodingKeys: String, CodingKey {
        case comment = "comment"
    }
}


class ForumCommentList: Codable {
    var id: Int?
    var content: String?
    var user: Userlist?
    var stats: Stats?
    var taggedUsers: [Userlist]?
    var approved, hasLiked: Bool?
    var createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, content, user, stats
        case taggedUsers = "tagged_users"
        case approved
        case hasLiked = "has_liked"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
    
}


