//
//  HashTag.swift
//  Pawxo
//
//  Created by 42works on 31/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation


class HashTag: Codable {
    var id: Int?
    var name, createdAt: String?
    var postCount: Int?

    enum CodingKeys: String, CodingKey {
        case id, name
        case createdAt = "created_at"
        case postCount = "post_count"
    }

    init(id: Int, name: String, createdAt: String, postCount: Int) {
        self.id = id
        self.name = name
        self.createdAt = createdAt
        self.postCount = postCount
    }
}

