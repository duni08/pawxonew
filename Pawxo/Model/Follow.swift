//
//  Follow.swift
//  Pawxo
//
//  Created by 42works on 03/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation


class FollowListResponse: Codable {
    var follower : [Userlist]?
    var following : [Userlist]?
    var followerCount,followingCount: Int?

    enum CodingKeys: String, CodingKey {
        
        case follower = "follower"
        case following = "following"
        case followerCount = "followerCount"
        case followingCount = "followingCount"
    }
   
}
