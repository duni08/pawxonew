//
//  Breed.swift
//  Pawxo
//
//  Created by Apple on 24/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation

import UIKit
import Foundation

class Breed {
    
    var userId : String?
    var name: String?
    var petBreedArray:NSArray?
    
    init(breedInfo:NSDictionary) {
        
        if let idValue = breedInfo.object(forKey: "id") as? Int {
            self.userId = "\(idValue)"
        }
        
        if let typeValue = breedInfo.object(forKey: "type") as? String {
            self.name = typeValue
        }
        
        if let petBreedArr = breedInfo.object(forKey: "pet_breeds") as? NSArray {
            self.petBreedArray = petBreedArr
        }
        
    }
    
}
