//
//  Utility.swift
//  Pawxo
//
//  Created by 42Works on 24/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation
import UIKit


extension UIImageView {
    public func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        layer.mask = shape
    }
}
