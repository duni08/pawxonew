//
//  PetBreed.swift
//  Pawxo
//
//  Created by Apple on 24/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import Foundation

class PetBreed:Codable {
    
    var id : String?
    var name: String?
    var typeId: String?
    
    init(petInfo:NSDictionary) {
        
        if let idValue = petInfo.object(forKey: "id") as? Int {
            self.id = "\(idValue)"
        }
        
        if let petIdValue = petInfo.object(forKey: "breed") as? String {
            self.name = petIdValue
        }
        
        if let petIdValue = petInfo.object(forKey: "type_id") as? Int {
            self.typeId = "\(petIdValue)"
        }
        
    }
    
}
