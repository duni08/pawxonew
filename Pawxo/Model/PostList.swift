//
//  HomeList.swift
//  Pawxo
//
//  Created by 42works on 28/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation
import UIKit

class PostList: Codable {
    var type: String?
    let postID: Int?
    var title: String?
    var postListDescription: String?
    var link, linkTitle, linkImage: String?
    var longitude, latitude, location, userName: String?
    var userImage: String?
    var userID: Int?
    var createdDate: String?
    var commentCount, isReport, isLike, likeCount: Int?
    var usertagList: [Userlist]?
    var media: [String]?
    var thumbnail: [String]?
    var petsList: [PetList]?
    var postsHashtags: [HashTag]?
    
    var height : CGFloat = 0
    
    enum CodingKeys: String, CodingKey {
        case type
        case postID = "post_id"
        case title
        case postListDescription = "description"
        case link
        case linkTitle = "link_title"
        case linkImage = "link_image"
        case longitude, latitude, location
        case userName = "user_name"
        case userImage = "user_image"
        case userID = "user_id"
        case createdDate = "created_date"
        case commentCount = "comment_count"
        case isReport = "is_report"
        case isLike = "is_like"
        case likeCount = "like_count"
        case usertagList = "usertag_list"
        case media, thumbnail
        case petsList = "pets_list"
        case postsHashtags = "posts_hashtags"
    }
}


class HomeRecievedResponse: NSObject, Codable {
    let name: String?
    let id: Int?
    let postList: [PostList]?
    let notificationCount, total, totalPage: Int?
    let currentage: String?

    enum CodingKeys: String, CodingKey {
        case name, id
        case postList = "post_list"
        case notificationCount = "notification_count"
        case total
        case totalPage = "total_page"
        case currentage
    }

    init(name: String, id: Int, postList: [PostList], notificationCount: Int, total: Int, totalPage: Int, currentage: String) {
        self.name = name
        self.id = id
        self.postList = postList
        self.notificationCount = notificationCount
        self.total = total
        self.totalPage = totalPage
        self.currentage = currentage
    }
    
}



