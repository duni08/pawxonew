//
//  SocialIntegration.swift
//  Pawxo
//
//  Created by Apple on 22/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import TwitterKit
import TwitterCore
import Foundation

class SocialIntegration: NSObject {

    
    static let shared = SocialIntegration()
    
    private override init() {
        
    }
    
    func getFacebookUserInfo(viewController : UIViewController,completion: @escaping (_ status: Bool,_ response:NSMutableDictionary,_ message:String)->()) {
        
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [ .publicProfile, .email ], viewController: viewController) { loginResult in
            
            DispatchQueue.main.async {
                
            switch loginResult {
            case .failed(let error):
                //self.showAlert(self, message: error.localizedDescription)
                completion(false,NSMutableDictionary(),error.localizedDescription)
                print(error)
            case .cancelled:
                print("User cancelled login.")
                completion(false,NSMutableDictionary(),"User cancelled login.")
            case .success(_, _, _)://let grantedPermissions, let declinedPermissions, let accessToken
                
                
                
                    
                
                print("Logged in!")
                
                    LoaderView.shared.showLoader()
                
                let responseDic = NSMutableDictionary()
                print(responseDic)
                
                let request = GraphRequest(graphPath: "/me")
                        request.parameters = ["fields": "id, name, email, picture.type(large)"]//user_birthday
                        
                        let connection = GraphRequestConnection()
                        connection.add(request) { httpResponse, result, error   in
                            LoaderView.shared.hideLoader()
                            if error != nil {
                                NSLog(error.debugDescription)
                                return
                            }
                            
                            guard let response = result as? [String:Any] else {
                                return
                            }
                            
                            if let picture = response["picture"] as? [String:Any] ,
                                let imgData = picture["data"] as? [String:Any] ,
                                let imgUrl = imgData["url"] as? String {
                                if let url = URL(string: imgUrl) {

                                    if let data = try? Data(contentsOf: url) { //here you get image data from url
                                       //generate image from data and assign it to your profile imageview
                                       let newImage = UIImage(data: data)
                                        responseDic.setValue(data, forKey: "profilepicData")
                                    }
                                }
                                //responseDic.setValue(imgUrl, forKey: "profilepic")
                                
                            }
                            
                            if let userId = response["id"] as? String {
                                responseDic.setValue(userId, forKey: "id")
                            }
                            
                            if let name = response["name"] as? String {
                                responseDic.setValue(name, forKey: "name")
                            }
                            
                            if let email = response["email"] as? String {
                                responseDic.setValue(email, forKey: "email")
                            }
                            completion(true,responseDic, "Sucess Login")
//                            self.getImageFromFacebook { (imageUrl) in
//                                print(imageUrl)
//                            }

                        }
                        connection.start()
            }
            }
        }
        
    }
    
    
    func getImageFromFacebook(completion: @escaping (_ url:String)->()){
//        NSDictionary *params = @{
//          @"redirect": @NO,
//          @"height": @200,
//          @"width": @200,
//          @"type": @"normal",
//        };
//        /* make the API call */
//        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
//                                       initWithGraphPath:@"/me/picture"
//                                              parameters:params
//                                              HTTPMethod:@"GET"];
//        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
//                                              id result,
//                                              NSError *error) {
//            // Handle the result
//        }];
        
        
        
        let params = ["redirect":false,"height":"200","width":"200","type":"normal"] as [String : Any]
        
        let request = GraphRequest(graphPath: "/me/picture")
        request.parameters = params
        let connection = GraphRequestConnection()
        connection.add(request) { httpResponse, result, error   in
            if error != nil {
                NSLog(error.debugDescription)
                return
            }
            guard let response = result as? [String:Any] else {
                return
            }
            print(response)
            completion("nothing")
        }
        
        connection.start()
    }
    
    
    func getTwitterUserInfo(viewController : UIViewController,completion: @escaping (_ status: Bool,_ response:NSMutableDictionary,_ message:String)->()) {
        
                TWTRTwitter.sharedInstance().logIn { session, error in
                    if session != nil { // Log in succeeded
                        
                        let reponseDic = NSMutableDictionary()
                        let client = TWTRAPIClient.withCurrentUser()
                        
                        client.requestEmail { email, error in
                            if (email != nil) {
                              
                                reponseDic.setValue(email, forKey: "email")
                                
                                if let userID = session?.userID {

                                    let client = TWTRAPIClient.withCurrentUser()
                                    client.loadUser(withID: userID) { (user, error) in
                                        
                                    reponseDic.setValue(user?.userID, forKey: "id")
                                    reponseDic.setValue(user?.screenName, forKey: "username")
                                    reponseDic.setValue(user?.name, forKey: "name")
                                    reponseDic.setValue(user?.profileImageURL, forKey: "profileImageURL")
                                    reponseDic.setValue(user?.profileImageLargeURL, forKey: "profileImageLargeURL")
                                    completion(true,reponseDic,"Scuess Login")
                                    
                                  }
                                }
                            }else {
                                completion(false,NSMutableDictionary(),error!.localizedDescription)
                            }
                        }
                        
                    } else {
                        completion(false,NSMutableDictionary(),"No Twitter Accounts Available")
                        
                    }
                }
        
        
    }
    
    
}
