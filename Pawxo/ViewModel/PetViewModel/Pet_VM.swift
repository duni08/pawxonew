//
//  Pet_VM.swift
//  Pawxo
//
//  Created by Apple on 23/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation
import UIKit

class Pet_VM: BaseViewModel {
    
    //MARK:- Variables
    var userId: String?
    var currentUserId: String?
    var perPage: String?
    var page: String?
    
    var postList = [PostList]()
    
    var pets_post : Int?
    var postId :String?
    
    
    
    var callGetPetPostListService:Bool? {
        didSet {
            callGetPetPostListAPI()
        }
    }
    
    
    
    //MARK:- Register Api Call
    private func callGetPetPostListAPI() {
        self.isLoading = true
        
        
        var param = [String : Any]()
        if let userIdValue = userId{
            param["like_user_id"] = userIdValue
        }
        
        //                if let currentUserIdValue = userId{
        //                    param["current_user_id"] = currentUserIdValue
        //                }
        
        if let perPageValue = perPage{
            param["per_page"] = perPageValue
        }
        
        if let pageValue = page{
            param["page"] = pageValue
        }
        
        if let pets_post = pets_post {
            param["pet_posts"] = pets_post
        }
        
        let getHomeListRequest = HomeListRequest(parameters:param)
        
        WebServiceManager.getHomeListWithRequest(request: getHomeListRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                print(responseData.data)
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    self.postList = responseData.data!.postList!
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
    }
    
}
