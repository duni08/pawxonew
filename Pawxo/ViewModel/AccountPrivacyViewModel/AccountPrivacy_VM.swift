//
//  AccountPrivacy_VM.swift
//  Pawxo
//
//  Created by Macbook on 01/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class AccountPrivacy_VM: BaseViewModel {
    
    //MARK:- Variables
    var userId: String?
    
    var account_private_status : Int?
    
    var callGetAccountStatusService: Bool? {
        didSet {
            callGettingAccountPrivacySettingAPI()
        }
    }
    
    var callUpdatePrivacyService: Bool? {
        didSet {
            callUpdatePrivacyAPI()
        }
    }
    
    
    
    func callGettingAccountPrivacySettingAPI(){
        
        self.isLoading = true
        
        
        var accountPrivacyUrl = WebServiceConstants.getAccountPrivacySettingAPI
        accountPrivacyUrl = accountPrivacyUrl.replacingOccurrences(of: "{user_id}", with: userId!)
        let settingListRequest = AcountPrivacyRequest(urlString:accountPrivacyUrl)
        
        WebServiceManager.accountPrivacySettingWithRequest(request: settingListRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    self.account_private_status = responseData.data?.accountPrivateStatus
                 self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
        
        
    }
    
    func callUpdatePrivacyAPI(){
        self.isLoading = true
        var param = [String : Any]()
        
        
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        if let account_private_status = account_private_status{
            param["private_status"] = account_private_status
        }
        
        let updateSettingRequest = AcountPrivacyUpdateRequest(parameters:param)
        
        WebServiceManager.accountPrivacyUpdateSettingWithRequest(request: updateSettingRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                 self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
        
    }
}
