//
//  ChangePassword_VM.swift
//  Pawxo
//
//  Created by Macbook on 01/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class ChangePassword_VM: BaseViewModel {
    
    //MARK:- Variables
    var userId: String?
    var email: String?
    var oldpassword: String?
    var password: String?
    var password_confirmation: String?
    
    var callChangePasswordService: Bool? {
        didSet {
            changePasswordAPI()
        }
    }
    
    
    func changePasswordAPI(){
        self.isLoading = true
        var param = [String : Any]()
        
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        if let email = email{
            param["email"] = email
        }
        
        if let oldpassword = oldpassword{
            param["oldpassord"] = oldpassword
            if oldpassword.count == 0{
                self.alertMessage = ValidationMessage.emptyPwd
                self.isLoading = false
                self.showAlertClosure!()
                
                return
            }
        }
        
        if let password = password{
            param["password"] = password
            if password.count == 0{
                self.alertMessage = ValidationMessage.emptyNewPwd
                self.isLoading = false
                self.showAlertClosure!()
                return
            }
            if password.count <= 5 {
                self.alertMessage = ValidationMessage.incorrectPwdLength
                self.isLoading = false
                self.showAlertClosure!()
                return
            }
        }
        
        if let password_confirmation = password_confirmation{
            param["password_confirmation"] = password_confirmation
            if password_confirmation.count == 0{
                self.alertMessage = ValidationMessage.emptyConfirmPwd
                self.isLoading = false
                self.showAlertClosure!()
                return
            }
            
            if password_confirmation.count <= 5 {
                self.alertMessage = ValidationMessage.incorrectPwdLength
                self.isLoading = false
                self.showAlertClosure!()
                return
            }
        }
        
        
        if let password = password ,password != password_confirmation {
            self.alertMessage = ValidationMessage.confirmPwdAlertMsg
            self.isLoading = false
            self.showAlertClosure!()
            return
        }
        
        let updateSettingRequest = PasswordUpdateRequest(parameters:param)
        
        WebServiceManager.passwordUpdateSettingWithRequest(request: updateSettingRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                self.alertMessage = responseData.message!
                self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
    }
}
