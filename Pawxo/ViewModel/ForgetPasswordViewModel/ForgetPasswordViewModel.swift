

import UIKit

class ForgetPasswordViewModel: BaseViewModel {
    
    //MARK:- Variables
    var email: String?
    
    var callForgetPasswordService : Bool?{
        didSet{
            callFogetPasswordUserApi()
        }
    }
    
    //MARK:- validation for login
    func isFormValidForgotPassword() -> (Bool, String) {
        return (isValidForgotData().0, isValidForgotData().1)
    }
    
    func isValidForgotData() -> (Bool, String) {
        var message = ""
        if email == "" {
            message = ValidationMessage.emptyEmail
        } else if !(email?.isValidEmail())! {
            message = ValidationMessage.invalidEmail
        }
        return message == "" ? (true, message) : (false, message)
    }
    
    
    //MARK:- Login Api Call

    private func callFogetPasswordUserApi(){
        self.isLoading = true
        var param = [String : Any]()
        
        if let email = email{
            param["email"] = email
        }
        WebServices().postRequest(methodName: ApiServiceName.forgotUrl, params: param as NSDictionary, oncompletion: { (status, message, response) in
            self.isLoading = false
            if status ?? false{
               // let user = Mapper<User>().map(JSONObject: response?.object(forKey: "data"))
               // UserDefaults.setObjectModel(user, forKey: UserDefault.user)
               // UserDefaults.standard.synchronize()
                self.responseRecieved?()
            }else{
                
            }
            self.alertMessage = message
        })
    }
}
