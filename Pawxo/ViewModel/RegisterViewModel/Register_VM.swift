//
//  Register_VM.swift
//  Rolld
//
//  Created by Apple2 on 23/04/19.
//  Copyright © 2019 Apple2. All rights reserved.
//

import UIKit

class Register_VM: BaseViewModel {

    //MARK:- Variables
    var name: String?
    var username: String?
    var email: String?
    var password: String?
    var deviceType: String? = kDeviceType
    var phoneNo: String?
    var deviceId: String? = kDeviceId
      //Facebook
    var facebookId: String?
    var coverPicUrl: String?
    var profilePicUrl: String?
    var dateOfBirth: String?
    
    var fbImageData : Data?
    
    var pwd: String?
    var newPwd: String?
    var confirmPwd: String?
    var deviceToken:String?
    var accountType:String?
    var pets : [PetList]?
    //MARK:- Validation
    func isFormValidForRegister() -> (Bool, String) {
        return (isValidRegisterData().0, isValidRegisterData().1)
    }
    
    //MARK:- Form Validations
    func isValidRegisterData() -> (Bool, String) {
        var message = ""
        if name == "" {
            message = ValidationMessage.yourName
        }else if email == "" {
            message = ValidationMessage.emptyEmail
        } else if !(email?.isValidEmail())! {
            message = ValidationMessage.invalidEmail
        }else if username == "" {
            message = ValidationMessage.emptyUserName
        }else if pwd == "" {
            message = ValidationMessage.emptyPwd
        }else if pwd?.count ?? 0 < kMinPasswordLimit {
            message = ValidationMessage.incorrectPwdLength
        }else if confirmPwd == "" {
            message = ValidationMessage.emptyConfirmPwd
        }else if confirmPwd?.count ?? 0 < kMinPasswordLimit {
            message = ValidationMessage.incorrectPwdLength
        }
        else if pwd != confirmPwd{
            message = ValidationMessage.confirmPwdAlertMsg
        }
        return message == "" ? (true, message) : (false, message)
    }
    
    var callRegisterUserService: Bool? {
        didSet{
            callRegisterUserApi()
        }
    }
    
    var callSocialLoginService: Bool? {
        didSet{
            callSocialLoginApi()
        }
    }
    
    //MARK:- Register Api Call
    private func callRegisterUserApi(){
        self.isLoading = true
        var param = [String : Any]()
        if let nameValue = name{
            param["name"] = nameValue
        }
        if let emailValue = email{
            param["email"] = emailValue
        }
        if let passwordValue = password{
            param["password"] = passwordValue
        }
        if let password_confirmation = confirmPwd{
            param["password_confirmation"] = password_confirmation
        }
        if let deviceType = deviceType{
            param["device_type"] = deviceType
        }
        
        if let phoneValue = phoneNo{
            param["phone"] = phoneValue
        }
        
       
        if let username = username{
            param["username"] = username
        }
        
        if let device_id = deviceId{
            param["device_id"] = device_id
        }
        
        if let device_token = deviceToken{
            param["device_token"] = device_token
        }
       
        WebServices().postRequest(methodName: ApiServiceName.registerUrl, params: param as NSDictionary, oncompletion: { (status, message, response) in
            self.isLoading = false
            if status ?? false{//Success
                
                if let userInfo = response?.object(forKey: "data") as? NSDictionary {
                    UserDefaults.setObjectModel(userInfo, forKey: UserDefault.user)
                    UserDefaults.standard.synchronize()
                    self.responseRecieved?()
                }
                
            }else{
                self.alertMessage = message
            }
        })
    }
    
    //MARK:- Social Login Api Call
    private func callSocialLoginApi(){
        self.isLoading = true
        var param = [String : Any]()
        
        if let username = username{
            param["username"] = username
        }
        
        if let nameValue = name{
            param["name"] = nameValue
        }
        if let emailValue = email{
            param["email"] = emailValue
        }
        if let deviceId = deviceId{
            param["device_id"] = deviceId
        }
        if let deviceType = deviceType{
            param["device_type"] = deviceType
        }
//        if let phoneValue = phoneNo{
//            param["phone"] = ""
//        }
        
        if let imageData = self.fbImageData{
            param["photo"] = imageData
            param["profile_image"] = ""
        }else{
            param["profile_image"] = ""
            param["photo"] = ""
        }
        
//
//        param["profile_image"] = "https://cuddl.app/admin/public/uploads/user_image/421_user_image.jpg"
//        param["photo"] = ""
        
        param["phone"] = ""
        
        if let device_Token = deviceToken{
            param["device_token"] = device_Token
        }
        else {
            param["device_token"] = "1234567890"
        }
        
        if let facebookId = facebookId{
            param["social_token"] = facebookId
        }
        
        if let social_Type = accountType {
            param["social_type"] = social_Type
        }
        
//        if let profilePicUrl = profilePicUrl{
//            param["profile_image"] = profilePicUrl
//        }
        
        
        print(param)
     /*   WebServices().postRequest(methodName: ApiServiceName.socialLoginUrl, params: param as NSDictionary, oncompletion: { (status, message, response) in
            self.isLoading = false
            if status ?? false{//Success
             
                if let userInfo = response?.object(forKey: "data") as? NSDictionary {
                    print(userInfo)
                   UserDefaults.setObjectModel(userInfo, forKey: UserDefault.user)
                    UserDefaults.standard.synchronize()
                   self.responseRecieved?()
                             }
            }else{
                self.alertMessage = message
            }
        })
 */
        
        
        WebServices().socialMediaAPI(methodName: ApiServiceName.socialLoginUrl, params: param as NSDictionary) { (status, message, response) in
            self.isLoading = false
            if status ?? false{//Success
             
                if let userInfo = response?.object(forKey: "data") as? NSDictionary {
                    print(userInfo)
                   UserDefaults.setObjectModel(userInfo, forKey: UserDefault.user)
                    UserDefaults.standard.synchronize()
                   self.responseRecieved?()
                             }
            }else{
                self.alertMessage = message
            }
        }
        
    }
}
