//
//  Search_VM.swift
//  Pawxo
//
//  Created by 42works on 03/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class Search_VM: BaseViewModel {
    //MARK:- Variables
    var userId: String?
    var currentUserId: String?
    var perPage: String?
    var page: String?
    var petList = [PetList]()
    
    var selectedPetList = [PetList]()
    var postId :String?
    var userList = [Userlist]()
    var hashTagList = [HashTag]()
    var type : String?
    var title :String?
    var description : String?
    var link : String?
    var link_title : String?
    var link_image :String?
    var hashTags : String?
    var tagged_pets : String?
    var tagged_users : String?
    var thumbnail : String?
    var isShare : Int?
    var location: String?
    var latitude : String?
    var longitude : String?
    var twitter_access_token : String?
    var twitter_access_token_secret : String?
    var facebook_access_token : String?
    var mediaData : Data?
    var thumbnailData :Data?
    var fileName :String?
    var mimeType : String?
    
    lazy var addName = NSMutableArray()
    lazy var idToSendApi = NSMutableArray()
    
    var nameStr : String?
    
    var isTag : String?
    
    var callGetSearchListService:Bool? {
        didSet {
            getSearchListAPI()
        }
    }
    
    
    
    private func getSearchListAPI(){
        self.isLoading = true
        
        /// For search users
        
        
        let dispatchGroup = DispatchGroup()

        dispatchGroup.enter()
        
        
        var param = [String : Any]()
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        if let nameStr = nameStr{
            param["name"] = nameStr
        }
        
        param["is_tag"] = 0
        
        if let perPageValue = perPage{
            param["per_page"] = perPageValue
        }
        
        if let pageValue = page{
            param["page"] = pageValue
        }
        
        let getDevicesRequest = UserSearchListRequest(parameters:param)
        
        print(param)
        WebServiceManager.getUserSearchListWithRequest(request: getDevicesRequest) { (response) in
            
            switch response {
            case .success(let responseData):
                print(responseData.data)
                if responseData.success!{
                    self.userList = responseData.data!
                    DispatchQueue.main.async {
                        dispatchGroup.leave()   // <<----
                    }
                }else{
                   // self.petList = responseData.data!.petList!
                self.alertMessage = responseData.message
                DispatchQueue.main.async {
                    dispatchGroup.leave()   // <<----
                }
                    
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                DispatchQueue.main.async {
                    dispatchGroup.leave()   // <<----
                }
                //self.callFailure(error)
            }
        }
        
        
        
        // for Search hashtag
        dispatchGroup.enter()
        
        var paramSecond = [String : Any]()
        if let userIdValue = userId{
            paramSecond["user_id"] = userIdValue
        }
        
        
        if let nameStr = nameStr{
            paramSecond["name"] = nameStr
        }
        
        paramSecond["is_tag"] = 0
        
        if let perPageValue = perPage{
            paramSecond["per_page"] = perPageValue
        }
        
        if let pageValue = page{
            paramSecond["page"] = pageValue
        }
        
        print(paramSecond)
        let getHashTagListRequest = HashTagListRequest(parameters:paramSecond)
        
        WebServiceManager.getHashTagListWithRequest(request: getHashTagListRequest) { (response) in
            switch response {
            case .success(let responseData):
                print(responseData.data)
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    DispatchQueue.main.async {
                        dispatchGroup.leave()   // <<----
                    }
                }else{
                    self.hashTagList = responseData.data!
                    DispatchQueue.main.async {
                        dispatchGroup.leave()   // <<----
                    }
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                DispatchQueue.main.async {
                    dispatchGroup.leave()   // <<----
                }
                //self.callFailure(error)
            }
        }
        
        
        
        
        dispatchGroup.notify(queue: .main) {
            self.isLoading = false
            self.responseRecieved!()
            // whatever you want to do when both are done
        }
        
    }
    
}
