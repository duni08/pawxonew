//
//  HomeList_VM.swift
//  Pawxo
//
//  Created by 42works on 28/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class HomeList_VM: BaseViewModel {
    
    var userId: String?
    var currentUserId: String?
    var perPage: String?
    var page: String?
    var postList = [PostList]()
    var total_pages = 0
    
    var hashTag : String?
    var postId :String?
    var callGetHomeService: Bool? {
        didSet {
            callGetHomePostAPI()
        }
    }
    
    var deletePostService:Bool?{
        didSet {
            callDeletePostAPI()
        }
    }
    
    var reportPostService:Bool?{
        didSet {
            callReportPostAPI()
        }
    }
    
    func callGetHomePostAPI(){
        self.isLoading = true
                
        //        let headers :[String:String] = ["Authorization":"Bearer \((User.sharedInstance?.accessToken)!)"]
                var param = [String : Any]()
                if let userIdValue = userId{
                    param["like_user_id"] = userIdValue
                }
                
//                if let currentUserIdValue = userId{
//                    param["current_user_id"] = currentUserIdValue
//                }
                
                if let perPageValue = perPage{
                    param["per_page"] = perPageValue
                }
                
                if let pageValue = page{
                    param["page"] = pageValue
                }
                
                if let hashTag = hashTag {
                    param["filter"] = hashTag
                }
        
                let getHomeListRequest = HomeListRequest(parameters:param)
                
                WebServiceManager.getHomeListWithRequest(request: getHomeListRequest) { (response) in
                    self.isLoading = false
                    switch response {
                    case .success(let responseData):
                        print(responseData.data)
                        if  responseData.success! == false{
                            self.alertMessage = responseData.message!
                            self.showAlertClosure!()
                        }else{
                            self.postList = responseData.data!.postList!
                            self.total_pages = (responseData.data?.totalPage!)!
                            self.responseRecieved!()
                        }
                    case .baseError (let error):
                        print(error)
                        self.alertMessage = error.errorBase?.message
                        self.showAlertClosure!()
                        //self.callFailure(error)
                    }
                }
    }
    
    func callDeletePostAPI(){
            self.isLoading = true
                    
            //        let headers :[String:String] = ["Authorization":"Bearer \((User.sharedInstance?.accessToken)!)"]
                    var param = [String : Any]()
                    
    //                if let currentUserIdValue = userId{
    //                    param["current_user_id"] = currentUserIdValue
    //                }
                   
                    
                    if let postId = postId{
                        param["post_id"] = Int(postId)
                    }
                    
            
                    let deletePostRequest = DeletePostRequest(parameters:param)
                    
                    WebServiceManager.deletePostWithRequest(request: deletePostRequest) { (response) in
                        self.isLoading = false
                        switch response {
                        case .success(let responseData):
                            if  responseData.success! == false{
                                self.alertMessage = responseData.message!
                                self.showAlertClosure!()
                            }else{
                                
                                self.responseRecieved!()
                            }
                        case .baseError (let error):
                            print(error)
                            self.alertMessage = error.errorBase?.message
                            self.showAlertClosure!()
                            //self.callFailure(error)
                        }
                    }
        }
    
    func callReportPostAPI(){
        self.isLoading = true
        
        var param = [String : Any]()

        if let postId = postId{
            param["post_id"] = Int(postId)
        }
        
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        var reportUrl = WebServiceConstants.reportPostAPI
        reportUrl = reportUrl.replacingOccurrences(of: "{id}", with: "\(postId!)")
            
        let reportPostRequest = ReportPostRequest(parameters:param, urlString: reportUrl)
        
        WebServiceManager.reportPostWithRequest(request: reportPostRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
    }
}
