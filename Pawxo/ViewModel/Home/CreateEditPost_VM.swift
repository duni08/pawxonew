//
//  CreateEditPost_VM.swift
//  Pawxo
//
//  Created by 42works on 27/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit
import Foundation

class CreateEditPost_VM: BaseViewModel {
    
    //MARK:- Variables
    var userId: String?
    var currentUserId: String?
    var perPage: String?
    var page: String?
    var petList = [PetList]()
    
    var selectedPetList = [PetList]()
    var postId :String?
    var userList = [Userlist]()
    var hashTagList = [HashTag]()
    var type : String?
    var title :String?
    var description : String?
    var link : String?
    var link_title : String?
    var link_image :String?
    var hashTags : String?
    var tagged_pets : String?
    var tagged_users : String?
    var thumbnail : String?
    var isShare : Int?
    var location: String?
    var latitude : String?
    var longitude : String?
    var twitter_access_token : String?
    var twitter_access_token_secret : String?
    var facebook_access_token : String?
    var mediaData : Data?
    var thumbnailData :Data?
    var fileName :String?
    var mimeType : String?
    
    var isForumHahTag = false
    
    lazy var addName = NSMutableArray()
    lazy var idToSendApi = NSMutableArray()
    
    var nameStr : String?
    
    
    var callAddPostService: Bool? {
        didSet {
            callAddNewPostAPI()
        }
    }
    
    
    var callEditPostService: Bool? {
        didSet {
            callEditPostAPI()
        }
    }
    
    var callGetUserSearchListService:Bool? {
        didSet {
            callGetUserListAPI()
        }
    }
    
    
    var callGetHashTAGListService:Bool? {
        didSet {
            callGetHashTagListAPI()
        }
    }
    
    var callGetPetListService:Bool? {
        didSet {
            callGetPetListAPI()
        }
    }
    
    var callLikePostService:Bool? {
        didSet {
            callLikePostAPI()
        }
    }
    var callUnLikePostService:Bool? {
        didSet {
            callUnLikePostAPI()
        }
    }
    //MARK:- Register Api Call
    private func callGetPetListAPI() {
        self.isLoading = true
        
        
//        let headers :[String:String] = ["Authorization":"Bearer \((User.sharedInstance?.accessToken)!)"]
        
        var param = [String : Any]()
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        if let currentUserIdValue = userId{
            param["current_user_id"] = currentUserIdValue
        }
        
        if let perPageValue = perPage{
            param["per_page"] = perPageValue
        }
        
        if let pageValue = page{
            param["page"] = pageValue
        }
        
        var getPetUrl = WebServiceConstants.getPetListAPI
        getPetUrl = getPetUrl.replacingOccurrences(of: "{user_id}", with: "\(userId!)")
        let getDevicesRequest = PetListRequest(parameters:param,urlString:getPetUrl)
        
        WebServiceManager.getPetListWithRequest(request: getDevicesRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                print(responseData.data)
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    self.petList = responseData.data!.petList!
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
        
    }
    
    
    //MARK:- Unlike Post Api Call
        private func callUnLikePostAPI() {
                self.isLoading = true
                
                var param = [String : Any]()
                if let userIdValue = userId{
                    param["user_id"] = userIdValue
                }
               
                
                var likeUrl = WebServiceConstants.likePostAPI
                likeUrl = likeUrl.replacingOccurrences(of: "{id}", with: "\(postId!)")
                let likeUnlikeRequest = LikeUnlikePostRequest(parameters:param,urlString:likeUrl)
                
                WebServiceManager.likeUnlikePostRequest(request: likeUnlikeRequest) { (response) in
                    self.isLoading = false
                    switch response {
                    case .success(let responseData):
                        if  responseData.success! == false{
                            self.alertMessage = responseData.message!
                            self.showAlertClosure!()
                        }else{
                            
                            self.responseRecieved!()
                        }
                    case .baseError (let error):
                        print(error)
                        self.alertMessage = error.errorBase?.message
                        self.showAlertClosure!()
                        //self.callFailure(error)
                    }
                }
                
            }
    
    //MARK:- Like Post Api Call
        private func callLikePostAPI() {
            self.isLoading = true
            
            
    //        let headers :[String:String] = ["Authorization":"Bearer \((User.sharedInstance?.accessToken)!)"]
            
            var param = [String : Any]()
            if let userIdValue = userId{
                param["user_id"] = userIdValue
            }
           
           
            
            var likeUrl = WebServiceConstants.likePostAPI
            likeUrl = likeUrl.replacingOccurrences(of: "{id}", with: "\(postId!)")
            let likeUnlikeRequest = LikeUnlikePostRequest(parameters:param,urlString:likeUrl)
            
            WebServiceManager.likeUnlikePostRequest(request: likeUnlikeRequest) { (response) in
                self.isLoading = false
                switch response {
                case .success(let responseData):
                    if  responseData.success! == false{
                        self.alertMessage = responseData.message!
                        self.showAlertClosure!()
                    }else{
                        
                        self.responseRecieved!()
                    }
                case .baseError (let error):
                    print(error)
                    self.alertMessage = error.errorBase?.message
                    self.showAlertClosure!()
                    //self.callFailure(error)
                }
            }
            
        }
    
    //MARK:- get HashTag List Api Call
        private func callGetHashTagListAPI() {
            self.isLoading = true
            
            
    //        let headers :[String:String] = ["Authorization":"Bearer \((User.sharedInstance?.accessToken)!)"]
            
            var param = [String : Any]()
            if let userIdValue = userId{
                param["user_id"] = userIdValue
            }
            
            
            if let nameStr = nameStr{
                param["name"] = nameStr
            }
            
            
            
            
            var getHashTagListRequest = HashTagListRequest(parameters:param)
            
            if self.isForumHahTag {
                getHashTagListRequest.urlString = WebServiceConstants.getHashTagListForumSearchAPI
            }
            
            WebServiceManager.getHashTagListWithRequest(request: getHashTagListRequest) { (response) in
                switch response {
                case .success(let responseData):
                    print(responseData.data)
                    if  responseData.success! == false{
                        self.alertMessage = responseData.message!
                        self.showAlertClosure!()
                    }else{
                        self.hashTagList = responseData.data!
                        self.responseRecieved!()
                    }
                case .baseError (let error):
                    print(error)
                    self.alertMessage = error.errorBase?.message
                    self.showAlertClosure!()
                    //self.callFailure(error)
                }
            }
            
        }
    
    //MARK:- Register Api Call
        private func callGetUserListAPI() {
            self.isLoading = true
            
            
    //        let headers :[String:String] = ["Authorization":"Bearer \((User.sharedInstance?.accessToken)!)"]
            
            var param = [String : Any]()
            if let userIdValue = userId{
                param["user_id"] = userIdValue
            }
            
            if let nameStr = nameStr{
                param["name"] = nameStr
            }
            
            param["is_tag"] = 1
            
            if let perPageValue = perPage{
                param["per_page"] = perPageValue
            }
            
            if let pageValue = page{
                param["page"] = pageValue
            }
            
            let getDevicesRequest = UserSearchListRequest(parameters:param)
            
            WebServiceManager.getUserSearchListWithRequest(request: getDevicesRequest) { (response) in
                switch response {
                case .success(let responseData):
                    print(responseData.data)
                    if responseData.success!{
                        self.userList = responseData.data!
                        self.responseRecieved!()
                    }else{
                       // self.petList = responseData.data!.petList!
                    self.alertMessage = responseData.message
                    self.showAlertClosure!()
                        
                    }
                case .baseError (let error):
                    print(error)
                    self.alertMessage = error.errorBase?.message
                    self.showAlertClosure!()
                    //self.callFailure(error)
                }
            }
            
        }
    
    
    //MARK:- Register Api Call
    private func callAddNewPostAPI() {
        self.isLoading = true
        var param = [String : Any]()
        
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        if let type = type{
            param["type"] = type
        }
        
        if let title = title{
            param["title"] = title
        }
        if let description = description{
            param["description"] = description
        }
        if let link = link{
            param["link"] = link
        }
        if let link_title = link_title{
            param["link_title"] = link_title
        }
        
        if let link_image = link_image{
            param["link_image"] = link_image
        }
        
        if let latitude = latitude{
            param["latitude"] = latitude
        }
        
        if let longitude = longitude{
            param["longitude"] = longitude
        }
        
        if let hashTags = hashTags{
            param["hash_tags"] = hashTags
        }
        
        if let tagged_pets = tagged_pets{
            param["tagged_pets"] = tagged_pets
        }
        
        if let thumbnail = thumbnailData{
            param["thumbnail"] = thumbnail
        }
        
        if let tagged_users = tagged_users{
            param["tagged_users"] = tagged_users
        }
        
        if let isShare = isShare{
            param["isShare"] = isShare
        }
        
        if let location = location{
            param["location"] = location
        }
        
        if let data = mediaData {
            param["data"] = data
        }
        
        param["is_share"] = "0"
        
        if let twitter_access_token = twitter_access_token{
            param["twitter_access_token"] = twitter_access_token
            param["is_share"] = "1"
        }
        if let twitter_access_token_secret = twitter_access_token_secret{
            param["twitter_access_token_secret"] = twitter_access_token_secret
            param["is_share"] = "1"
        }
        if let facebook_access_token = facebook_access_token{
            param["facebook_access_token"] = facebook_access_token
        }
        if let location = location{
            param["location"] = location
        }
        
        WebServices().createPost(methodName: ApiServiceName.addPostUrl, params: param as NSDictionary) { (check, message, response) in
            self.isLoading = false
            if response == nil{
                 //self.postUploadedCompletion?(false)
                print(response)
                self.alertMessage = message
                self.showAlertClosure!()
            }
            else{
                
                if let dataVal = response?["data"] as? NSDictionary,let isBlocked = dataVal["is_blocked"] as? Bool,isBlocked == true{
                    self.alertMessage = dataVal["message"] as? String
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        kAppDelegate.logoutFromApp()
                    }
                }
//            self.postUploadedCompletion?(true)
                self.responseRecieved!()
            }
        }
        
//        WebServices().AddPost(methodName: ApiServiceName.addPostUrl, imageType: "media[]",imageUpload: mediaData, mimeType: self.mimeType!, fileName: self.fileName!, params: param as NSDictionary) { (status, msg, response) in
//
//            if status == true {
//
//                print("response-- \(String(describing: response))")
//                self.responseRecieved?()
//            }else{
//                self.alertMessage = msg
//            }
//
//        }

    }
    
    
    //MARK:- Register Api Call
        private func callEditPostAPI() {
            self.isLoading = true
            var param = [String : Any]()
            
            if let userIdValue = userId{
                param["user_id"] = userIdValue
            }
            
            if let postId = postId{
                param["post_id"] = postId
            }
            
            if let type = type{
                param["type"] = type
            }
            
            if let title = title{
                param["title"] = title
            }
            if let description = description{
                param["description"] = description
            }
            if let link = link{
                param["link"] = link
            }
            if let link_title = link_title{
                param["link_title"] = link_title
            }
            
            if let link_image = link_image{
                param["link_image"] = link_image
            }
            
            if let latitude = latitude{
                param["latitude"] = latitude
            }
            
            if let longitude = longitude{
                param["longitude"] = longitude
            }
            
            if let hashTags = hashTags{
                param["hash_tags"] = hashTags
            }
            
            if let tagged_pets = tagged_pets{
                param["tagged_pets"] = tagged_pets
            }
            
            if let thumbnail = thumbnailData{
                param["thumbnail"] = thumbnail
            }
            
            if let tagged_users = tagged_users{
                param["tagged_users"] = tagged_users
            }
            
            if let isShare = isShare{
                param["isShare"] = isShare
            }
            
            if let location = location{
                param["location"] = location
            }
            
            if let data = mediaData {
                param["data"] = data
            }
            
            if let twitter_access_token = twitter_access_token{
                param["twitter_access_token"] = twitter_access_token
                param["is_share"] = "1"
            }
            if let twitter_access_token_secret = twitter_access_token_secret{
                param["twitter_access_token_secret"] = twitter_access_token_secret
                param["is_share"] = "1"
            }
            if let facebook_access_token = facebook_access_token{
                param["facebook_access_token"] = facebook_access_token
            }
            if let location = location{
                param["location"] = location
            }
            
            WebServices().createPost(methodName: ApiServiceName.editPostUrl, params: param as NSDictionary) { (check, message, response) in
                self.isLoading = false
                if response == nil{
                     //self.postUploadedCompletion?(false)
                    print(response)
                    self.alertMessage = message
                    self.showAlertClosure!()
                }
                else{
                    
                    if let dataVal = response?["data"] as? NSDictionary,let isBlocked = dataVal["is_blocked"] as? Bool,isBlocked == true{
                        self.alertMessage = dataVal["message"] as? String
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            kAppDelegate.logoutFromApp()
                        }
                    }
    //            self.postUploadedCompletion?(true)
                    self.responseRecieved!()
                }
            }
            
    //        WebServices().AddPost(methodName: ApiServiceName.addPostUrl, imageType: "media[]",imageUpload: mediaData, mimeType: self.mimeType!, fileName: self.fileName!, params: param as NSDictionary) { (status, msg, response) in
    //
    //            if status == true {
    //
    //                print("response-- \(String(describing: response))")
    //                self.responseRecieved?()
    //            }else{
    //                self.alertMessage = msg
    //            }
    //
    //        }

        }
  

}
