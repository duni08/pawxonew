//
//  LikesList_VM.swift
//  Pawxo
//
//  Created by 42works on 31/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class LikesList_VM: BaseViewModel {
    
    var userId: String?
    var perPage: String?
    var page: String?
    var postId : String?
    
    var userList = [Userlist]()
    
    var callGetUserListService:Bool? {
        didSet {
            callGetUserListAPI()
        }
    }
    
    //MARK:- get Users who likes post Api Call
        private func callGetUserListAPI() {
            self.isLoading = true
            
            
    //        let headers :[String:String] = ["Authorization":"Bearer \((User.sharedInstance?.accessToken)!)"]
            
            var param = [String : Any]()
            if let userIdValue = userId{
                param["user_id"] = userIdValue
            }
            
            
            if let perPageValue = perPage{
                param["per_page"] = perPageValue
            }
            
            if let pageValue = page{
                param["page"] = pageValue
            }
            
            
            var likeUrl = WebServiceConstants.likeListAPI
                           likeUrl = likeUrl.replacingOccurrences(of: "{id}", with: "\(postId!)")
                           let likeListRequest = LikeListRequest(parameters:param,urlString:likeUrl)
            
            
            WebServiceManager.likeListRequest(request: likeListRequest) { (response) in
                switch response {
                case .success(let responseData):
                    print(responseData.data)
                    if responseData.success!{
                        self.userList = responseData.data!
                        self.responseRecieved!()
                    }else{
                       // self.petList = responseData.data!.petList!
                    self.alertMessage = responseData.message
                    self.showAlertClosure!()
                        
                    }
                case .baseError (let error):
                    print(error)
                    self.alertMessage = error.errorBase?.message
                    self.showAlertClosure!()
                    //self.callFailure(error)
                }
            }
            
        }
    
}
