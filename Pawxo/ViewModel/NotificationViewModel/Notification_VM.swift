//
//  Notification_VM.swift
//  Pawxo
//
//  Created by 42Works on 07/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class Notification_VM: BaseViewModel {
    var userId: String?
       var perPage: String?
       var page: String?
       var postId : String?
       var request_id : String?
        var requests_status : String?
    var total_pages = 0
       var notification_list = [NotificationList]()
    var callGetNotificationListService:Bool? {
           didSet {
               callGetNotificationListAPI()
           }
       }
    var callAcceptNotificationRequestService:Bool?
    {
        didSet{
            callAccpetNotificationAPI()
        }
    }
       //MARK:- Register Api Call
       private func callGetNotificationListAPI() {
           self.isLoading = true
           
           
           var param = [String : Any]()
           if let userIdValue = userId{
               param["user_id"] = userIdValue
           }
           
           if let perPageValue = perPage{
               param["per_page"] = perPageValue
           }
           
           if let pageValue = page{
               param["page"] = pageValue
           }
           print(param)
        var notificationUrl = WebServiceConstants.getNotificationAPI
         notificationUrl = notificationUrl.replacingOccurrences(of: "{user_id}", with: "\(userId!)")
           let getNotificationListRequest = NotificationListRequest(parameters:param,urlString:notificationUrl)
           print(notificationUrl)
           WebServiceManager.getNotificationListWithRequest(request: getNotificationListRequest) { (response) in
               self.isLoading = false
               switch response {
               case .success(let responseData):
                   if  responseData.success! == false{
                       self.alertMessage = responseData.message!
                       self.showAlertClosure!()
                   }else{
                  //  self.notification_list.append(responseData.data!.notificationList!)
                   print(responseData.data!.name!)
                       self.notification_list = responseData.data!.notificationList!
                    self.total_pages = (responseData.data?.totalPage!)!
                       self.responseRecieved!()
                   }
               case .baseError (let error):
                   print(error)
                   self.alertMessage = error.errorBase?.message
                   self.showAlertClosure!()
                   //self.callFailure(error)
               }
           }
       }
    private func callAccpetNotificationAPI()
    {
        self.isLoading = true
        var param = [String : Any]()
        //           if let userIdValue = userId{
        //               param["user_id"] = userIdValue
        //           }
                   
                   if let req_id = request_id{
                       param["request_id"] = req_id
                   }
            if let req_status = requests_status
            {
                param["request_status"] = req_status
        }
        let acceptFollowRequest = AcceptFollowRequest(parameters: param)
        WebServiceManager.acceptFollowWithRequest(request: acceptFollowRequest)//(urlStr:,params:param as NSDictionary) { (check, message, response) in
        { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
}
}
