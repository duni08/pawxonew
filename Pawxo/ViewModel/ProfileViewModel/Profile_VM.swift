//
//  Profile_VM.swift
//  Pawxo
//
//  Created by Macbook on 01/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class Profile_VM: BaseViewModel {

    var userId: String?
    
    var currentUserId: String?
    var perPage: String?
    var page: String?
    var postList = [PostList]()
    
    var postId :String?
    
    var alertSecond : String?
    
    
    
    var name : String?
    var phone : String?
    var location : String?
    var latitude : String?
    var longitude : String?
    var bio : String?
    var gender : String?
    var user_image : String?
    
    var imageData : Data?
    
    var blocker_id : String?
    
    
    var logoutError = false
    
    
    var callBlockUserProfileService:Bool?{
        didSet{
            callUserBlockAPI()
        }
    }
    
    
     var callGetUserProfileService: Bool? {
        didSet {
            callUserProfileAPI()
        }
    }
    
    var callUpdateProfileService: Bool? {
        didSet {
            callUserUpdateProfileAPI()
        }
    }
    
    var userList : Userlist?
    
    //MARK:- Like Post Api Call
        private func callUserProfileAPI() {
            self.isLoading = true
            
            
    //        let headers :[String:String] = ["Authorization":"Bearer \((User.sharedInstance?.accessToken)!)"]
            
            let dispatchGroup = DispatchGroup()

            dispatchGroup.enter()
            
            var param = [String : Any]()
            let myId = CommonFunctions.getuserFromDefaults().userId!
            
            if let userId = userId{
                param["user_id"] = myId
            }
           
            
            var profileUrl = WebServiceConstants.getUserPorfileAPI
            profileUrl = profileUrl.replacingOccurrences(of: "{id}", with: userId!)
            let userProfileRequest = UserProfileRequest(parameters:param,urlString:profileUrl)
            
            WebServiceManager.userProfileWithRequest(request: userProfileRequest) { (response) in
                switch response {
                case .success(let responseData):
                    if  responseData.success! == false{
                        self.alertMessage = responseData.message!
                        DispatchQueue.main.async {
                            dispatchGroup.leave()   // <<----
                        }
                    }else{
                    self.userList = responseData.data
                        DispatchQueue.main.async {
                            dispatchGroup.leave()   // <<----
                        }
                    }
                case .baseError (let error):
                    print(error)
                    self.alertMessage = error.errorBase?.message
                    if error.errorBase?.errorCode == 999{
                        self.logoutError = true
                    }
                    DispatchQueue.main.async {
                        dispatchGroup.leave()   // <<----
                    }                    //self.callFailure(error)
                }
            }
            
            
            
            dispatchGroup.enter()
            
            var paramForPost = [String : Any]()
            
            if let userIdValue = userId{
                paramForPost["like_user_id"] = userIdValue
            }
            
            if let perPageValue = perPage{
                paramForPost["per_page"] = perPageValue
            }
            
            if let pageValue = page{
                paramForPost["page"] = pageValue
            }
            
            //let uId = CommonFunctions.getuserFromDefaults().userId
            if let userId = userId{
                paramForPost["id"] = userId
            }
            
            
            if page == "1"{
                self.postList.removeAll()
            }
            
            let getHomeListRequest = HomeListRequest(parameters:paramForPost)
            
            print(getHomeListRequest.parameters)
            
            WebServiceManager.getHomeListWithRequest(request: getHomeListRequest) { (response) in
                switch response {
                case .success(let responseData):
                    print(responseData.data)
                    if  responseData.success! == false{
                        self.alertSecond = responseData.message!
                        DispatchQueue.main.async {
                            dispatchGroup.leave()   // <<----
                        }
                    }else{
                        self.postList = responseData.data!.postList!
                        DispatchQueue.main.async {
                            dispatchGroup.leave()   // <<----
                        }                    }
                case .baseError (let error):
                    print(error)
                    self.alertSecond = error.errorBase?.message
                    if error.errorBase?.errorCode == 999{
                        self.logoutError = true
                    }
                    DispatchQueue.main.async {
                        dispatchGroup.leave()   // <<----
                    }                    //self.callFailure(error)
                }
            }
            
            
            
            
            dispatchGroup.notify(queue: .main) {
                self.isLoading = false
                if self.logoutError{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        kAppDelegate.logoutFromApp()
                    }
                }else{
                    self.responseRecieved!()
                }
                // whatever you want to do when both are done
            }

        }
    
    
    private func callUserUpdateProfileAPI(){
        
        self.isLoading = true
        var param = [String : Any]()
        if let nameValue = name{
            param["name"] = nameValue
        }
        if let phone = phone{
            param["phone"] = phone
        }
        if let location = location{
            param["location"] = location
        }
        if let latitude = latitude{
            param["latitude"] = latitude
        }
        if let longitude = longitude{
            param["longitude"] = longitude
        }
        if let bio = bio{
            param["bio"] = bio
        }
        
        if let gender = gender{
            param["gender"] = gender
        }
        
        if let user_image = user_image{
            param["user_image"] = user_image
        }
        
        if let imageData = imageData {
            param["image_data"] = imageData
        }
        
        
        var editProfileUrl = WebServiceConstants.editUserAPI
        editProfileUrl = editProfileUrl.replacingOccurrences(of: "{id}", with: userId!)
        
        WebServices().edituserProfile(urlStr:editProfileUrl,params:param as NSDictionary) { (check, message, response) in
            self.isLoading = false
                    if response == nil{
                         //self.postUploadedCompletion?(false)
                        print(response)
                        self.alertMessage = message
                        self.showAlertClosure!()
                    }
                    else{
        //            self.postUploadedCompletion?(true)
                        self.alertMessage = response?["message"] as? String
                        self.responseRecieved!()
                    }
                }
    }
    
    
    private func callUserBlockAPI(){
        
        self.isLoading = true
        var param = [String : Any]()
        
        
        if let blocker_id = self.blocker_id{
            param["blocker_id"] = blocker_id
        }
        var blockProfileUrl = WebServiceConstants.blockUserAPI
        blockProfileUrl = blockProfileUrl.replacingOccurrences(of: "{id}", with: userId!)
        
        let requestBlockUserRequest = RequestUserBlockRequest(parameters:param,urlString:blockProfileUrl)
        
       WebServiceManager.requestBlockUserRequest(request: requestBlockUserRequest) { (response) in
           self.isLoading = false
           switch response {
           case .success(let responseData):
               if  responseData.success! == false{
                   self.alertMessage = responseData.message!
                   self.showAlertClosure!()
               }else{
                   
                   self.responseRecieved!()
               }
           case .baseError (let error):
               print(error)
               self.alertMessage = error.errorBase?.message
               self.showAlertClosure!()
               //self.callFailure(error)
           }
       }
    }
}



