//
//  Follow_VM.swift
//  Pawxo
//
//  Created by 42works on 03/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class Follow_VM: BaseViewModel {
    
    var userId: String?
    var followerId : String?
    
    var perPage: String?
    var page: String?
    var postId : String?
    
    var userList = [Userlist]()
    
    var isFollowing = false
    
    var callFollowerService:Bool? {
        didSet {
            callFollowAPI()
        }
    }
    
    var callRequestUserFollowService:Bool? {
        didSet {
            callRequestUserFollowAPI()
        }
    }
    
    var callFollowerUsersService:Bool? {
        didSet {
            callGetFollowerUserListAPI()
        }
    }
    
    private func callFollowAPI(){
        self.isLoading = true
        var followUrl = WebServiceConstants.followAPI
        followUrl = followUrl.replacingOccurrences(of: "{id}", with: "\(userId!)")
        
        var param = [String : Any]()
        
        if let follower_id = followerId {
            param["follower_id"] = follower_id
        }
        
        let followUnFollowRequest = FollowUnFollowPostRequest(parameters:param,urlString:followUrl)
        
        WebServiceManager.followUnfollowRequest(request: followUnFollowRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
    }
    
    
    private func callRequestUserFollowAPI(){
        self.isLoading = true
        
        
        var param = [String : Any]()
        
        if let userId = userId {
            param["sender_id"] = userId
        }
        
        if let followerId = followerId {
            param["receiver_id"] = followerId
        }
        
        
        let requestfollowUserRequest = RequestUserFollowRequest(parameters:param)
        
        WebServiceManager.requestfollowUserRequest(request: requestfollowUserRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
    }
    
    //MARK:- get Users who likes post Api Call
        private func callGetFollowerUserListAPI() {
            self.isLoading = true
            
            
    //        let headers :[String:String] = ["Authorization":"Bearer \((User.sharedInstance?.accessToken)!)"]
            
            
            var param = [String : Any]()
            if let userIdValue = userId{
                param["user_id"] = userIdValue
            }
            
            
            if let perPageValue = perPage{
                param["per_page"] = perPageValue
            }
            
            if let pageValue = page{
                param["page"] = pageValue
            }
            
            
            var followUsersListUrl = WebServiceConstants.followersUserAPI
                           followUsersListUrl = followUsersListUrl.replacingOccurrences(of: "{id}", with: "\(followerId!)")
                           let followUserRequest = FollowUsersListRequest(parameters:param,urlString:followUsersListUrl)
            
            
            WebServiceManager.followListRequest(request: followUserRequest) { (response) in
                self.isLoading = false
                switch response {
                case .success(let responseData):
                    print(responseData.data)
                    if responseData.success!{
                        if self.isFollowing {
                            if let following = responseData.data!.following{
                                self.userList = following
                            }
                            
                        }else{
                            
                            if let followersList = responseData.data!.follower{
                                self.userList = followersList
                            }
                            
                        }
                        self.responseRecieved!()
                    }else{
                       // self.petList = responseData.data!.petList!
                    self.alertMessage = responseData.message
                    self.showAlertClosure!()
                        
                    }
                case .baseError (let error):
                    print(error)
                    self.alertMessage = error.errorBase?.message
                    self.showAlertClosure!()
                    //self.callFailure(error)
                }
            }
            
        }
    
}
