//
//  Breed_VM.swift
//  Pawxo
//
//  Created by Apple on 24/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation


class Breed_VM: BaseViewModel {
    
    
    var callGetBreedListService:Bool? {
        didSet {
            callGetBreedListAPI()
        }
    }
    
    //MARK:- Register Api Call
    private func callGetBreedListAPI() {
        self.isLoading = true
        let param = [String : Any]()
        WebServices().getRequest(methodName: ApiServiceName.getBreedList, params: param as NSDictionary, oncompletion: { (status, message, response) in
            self.isLoading = false
            if status ?? false{//Success
                
                var breedArr = [Breed]()
                
                if let breedArray = response?.object(forKey: "data") as? NSArray {
                    
                    for breedDic in breedArray {
                    
                        let breed = Breed(breedInfo: breedDic as! NSDictionary)
                        breedArr.append(breed)
                    }
                    
                    self.responseRecievedBreed?(breedArr)
                }
                
            }else{
                self.alertMessage = message
                self.showAlertClosure!()
            }
        })

    }
}
