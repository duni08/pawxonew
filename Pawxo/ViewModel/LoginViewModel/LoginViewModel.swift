

import UIKit

class LoginViewModel: BaseViewModel {
    
    //MARK:- Variables
    var password: String?
    var email: String?
    var deviceId = kDeviceId
    var deviceType = "Ios"
    var userId : String?
    
    var callLoginService : Bool?{
        didSet{
            callLoginUserApi()
        }
    }
    
    
    var callAcceptEulaService : Bool?{
        didSet{
            callAcceptEulaAPI()
        }
    }
    
    //MARK:- validation for login
    func isFormValidForLogin() -> (Bool, String) {
        return (isValidLoginData().0, isValidLoginData().1)
    }
    
    func isValidLoginData() -> (Bool, String) {
        var message = ""
        if email == "" {
            message = ValidationMessage.emptyEmail
        } else if !(email?.isValidEmail())! {
            message = ValidationMessage.invalidEmail
        }else  if password == "" {
            message = ValidationMessage.emptyPwd
        }
        return message == "" ? (true, message) : (false, message)
    }
    
    
    //MARK:- Login Api Call

    private func callLoginUserApi(){
        self.isLoading = true
        var param = [String : Any]()
        
        if let password = password{
            param["password"] = password
        }
        if let email = email{
            param["email"] = email
        }
        
        param["device_id"] = deviceId
        param["device_type"] = deviceType
        
        let deviceToken = UserDefaults.standard.value(forKey: "deviceToken") as? String
        
        if let deviceTokenStr = deviceToken {
            param["device_token"] = deviceTokenStr
        }else{
            param["device_token"] = "qmc62wo3bjRTRyuHm1Haqmc62wo3bjRTdssfs"
        }
        
        
        WebServices().postRequest(methodName: ApiServiceName.loginUrl, params: param as NSDictionary, oncompletion: { (status, message, response) in
            self.isLoading = false
            if status ?? false{
                
                if let userInfo = response?.object(forKey: "data") as? NSDictionary {
                    UserDefaults.setObjectModel(userInfo, forKey: UserDefault.user)
                    UserDefaults.standard.synchronize()
                    self.responseRecieved?()
                }
               
            }else{
                self.alertMessage = message
            }
        })
    }
    
    
    
    private func callAcceptEulaAPI(){
        self.isLoading = true
        var acceptUrl = WebServiceConstants.acceptEulaAPI
        acceptUrl = acceptUrl.replacingOccurrences(of: "{id}", with: "\(userId!)")
        
        var param = [String : Any]()
        
        param["status"] = "1"
        
        let acceptEulaRequest = AcceptEulaRequest(parameters:param,urlString:acceptUrl)
        
        WebServiceManager.acceptEulaWithRequest(request: acceptEulaRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
    }
}
