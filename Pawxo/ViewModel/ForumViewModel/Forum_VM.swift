//
//  Forum_VM.swift
//  Pawxo
//
//  Created by 42works on 11/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class Forum_VM: BaseViewModel {
    
    
    var userId: String?
    var currentUserId: String?
    var perPage: String?
    var page: String?
    var postList = [PostList]()
    var total_pages = 0
    var forumId,isFavourite : String?
    
    var hashTag,search,tag_name,latitude,longitude,currentAddress : String?
    var postId :String?
    var callGetFiltersService: Bool? {
        didSet {
            callGetFilterCategoryAPI()
        }
    }
    
    
    var requests : [ForumRequestModel]?
    
    var forumLists : [ForumList]?
    
    var callAddForumService: Bool? {
        didSet {
            callAddForumAPI()
        }
    }
    
    var callDeleteForumService: Bool? {
        didSet {
            calldeleteForumAPI()
        }
    }
    
    
    var callEditForumService: Bool? {
        didSet {
            callEditForumAPI()
        }
    }
    
    var callGetForumListService: Bool? {
        didSet {
            callGetforumsListAPI()
        }
    }
    
    
    var callGetFavouriteListService: Bool? {
        didSet {
            callGetFavouriteForumsListAPI()
        }
    }
    
    var callFavouriteUnfavouriteService:Bool? {
        didSet{
            callFavouriteAPI()
        }
    }
    
    var callshowforumList:Bool? {
          didSet{
              callshowFormlistnotification()
          }
      }
    
    var categories : [Category]?
    var sort : [String]?
    
    var title,description,category,sortby :String?
    
    var mediaItems = [ForumMediaItem]()
    
    
    
    func callGetFilterCategoryAPI(){
            self.isLoading = true
                    
            //        let headers :[String:String] = ["Authorization":"Bearer \((User.sharedInstance?.accessToken)!)"]
                    
                    let categoryRequest = FilterRequest()
                    
                    WebServiceManager.getForumCategoryWithRequest(request: categoryRequest) { (response) in
                        self.isLoading = false
                        switch response {
                        case .success(let responseData):
                            print(responseData.data)
                            if  responseData.success! == false{
                                self.alertMessage = responseData.message!
                                self.showAlertClosure!()
                            }else{
                                self.categories = responseData.data?.options?.first?.collection
                                self.sort = responseData.data?.sort
                                self.responseRecieved!()
                            }
                        case .baseError (let error):
                            print(error)
                            self.alertMessage = error.errorBase?.message
                            self.showAlertClosure!()
                            //self.callFailure(error)
                        }
                    }
        }
    
    
    //MARK:- Register Api Call
    private func callAddForumAPI() {
        self.isLoading = true
        var param = [String : Any]()
        
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        if let title = title{
            param["title"] = title
        }
        if let description = description{
            param["description"] = description
        }
        if let category = category{
            param["category"] = category
        }
        
        
        if let latitude = latitude{
            param["latitude"] = latitude
        }
        
        if let longitude = longitude{
            param["longitude"] = longitude
        }
        
        if let currentAddress = currentAddress{
            param["location_name"] = currentAddress
        }
        
        if let hashTag = hashTag{
            param["tags"] = hashTag
        }
        
        for  i in 0...mediaItems.count - 1 {
            let item = mediaItems[i]
            let strKey = "image_\(i)_\(item.type!)"
            param[strKey] = item.data!
            let secondKey = "image_\(i)_type"
            param[secondKey] = item.type!
        }
        
        
        WebServices().addForum(urlStr: WebServiceConstants.createForumAPI, params: param as NSDictionary) { (check, message, response) in
            self.isLoading = false
            if response == nil{
                    //self.postUploadedCompletion?(false)
                print(response)
                self.alertMessage = message
                self.showAlertClosure!()
            }
            else{
//            self.postUploadedCompletion?(true)
                
                if let dataVal = response?["data"] as? NSDictionary,let isBlocked = dataVal["is_blocked"] as? Bool,isBlocked == true{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        kAppDelegate.logoutFromApp()
                    }
                }
                
                if response?["success"] as? Bool == true {
                self.alertMessage = response?["message"] as? String
                self.responseRecieved!()
                }else{
                    self.alertMessage = response?["message"] as? String
                    self.showAlertClosure!()
                }
            }
        }
        
//        WebServices().AddPost(methodName: ApiServiceName.addPostUrl, imageType: "media[]",imageUpload: mediaData, mimeType: self.mimeType!, fileName: self.fileName!, params: param as NSDictionary) { (status, msg, response) in
//
//            if status == true {
//
//                print("response-- \(String(describing: response))")
//                self.responseRecieved?()
//            }else{
//                self.alertMessage = msg
//            }
//
//        }

    }
    
    
    private func callEditForumAPI() {
            self.isLoading = true
            var param = [String : Any]()
            
            if let userIdValue = userId{
                param["user_id"] = userIdValue
            }
            
        if let forumId = forumId {
            param["id"] = forumId
        }
            if let title = title{
                param["title"] = title
            }
            if let description = description{
                param["description"] = description
            }
            if let category = category{
                param["category"] = category
            }
            
            if let hashTag = hashTag{
                param["tags"] = hashTag
            }
        
        if let latitude = latitude{
                   param["latitude"] = latitude
               }
               
               if let longitude = longitude{
                   param["longitude"] = longitude
               }
               
               if let currentAddress = currentAddress{
                   param["location_name"] = currentAddress
               }
            
            WebServices().addForum(urlStr: WebServiceConstants.editForumAPI, params: param as NSDictionary) { (check, message, response) in
                self.isLoading = false
                if response == nil{
                     //self.postUploadedCompletion?(false)
                    print(response)
                    self.alertMessage = message
                    self.showAlertClosure!()
                }
                else{
    //            self.postUploadedCompletion?(true)
                    
                    
                    if let dataVal = response?["data"] as? NSDictionary,let isBlocked = dataVal["is_blocked"] as? Bool,isBlocked == true{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        kAppDelegate.logoutFromApp()
                        }
                    }
                    
                    
                    if response?["success"] as? Bool == true {
                    self.alertMessage = response?["message"] as? String
                    self.responseRecieved!()
                    }else{
                       self.alertMessage = response?["message"] as? String
                        self.showAlertClosure!()
                    }
                }
            }
            
    //        WebServices().AddPost(methodName: ApiServiceName.addPostUrl, imageType: "media[]",imageUpload: mediaData, mimeType: self.mimeType!, fileName: self.fileName!, params: param as NSDictionary) { (status, msg, response) in
    //
    //            if status == true {
    //
    //                print("response-- \(String(describing: response))")
    //                self.responseRecieved?()
    //            }else{
    //                self.alertMessage = msg
    //            }
    //
    //        }

        }
    
    func callGetforumsListAPI(){
        self.isLoading = true
        
        //        let headers :[String:String] = ["Authorization":"Bearer \((User.sharedInstance?.accessToken)!)"]
        
        let dispatchGroup = DispatchGroup()

        
        
        for request in self.requests! {
            
            dispatchGroup.enter()
            
            var param = [String : Any]()
            if let userIdValue = userId{
                param["user_id"] = userIdValue
            }
           
            param["limit"] = request.limit
            
            param["page"] = request.page
            

            if let category = request.categoryId {
                param["category"] = category
            }
            
            if let tag_name = request.hashTag {
                param["tag_name"] = tag_name
            }
            
            if let sortby = request.filter {
                param["sortby"] = sortby
            }
            
            
            if request.searchText != "" {
                param["search"] = request.searchText
            }
            
            
            if let area = request.cat_area{
                if area != 3{
                    if let lat = request.lat,let long = request.long{
                        param["latitude"] = lat
                        param["longitude"] = long
                    }else{
                        DispatchQueue.main.async {
                            if request.page == 1{
                                request.data.removeAll()
                            }
                            dispatchGroup.leave()   // <<----
                        }
                        continue
                    }
                }
            }
            
            
            var forumListUrl = WebServiceConstants.forumListAPI
            forumListUrl = forumListUrl.replacingOccurrences(of: "{user_id}", with: "\(userId!)")
            
            let getForumListRequest = ForumListRequest(parameters:param,urlString: forumListUrl)
            
            WebServiceManager.getForumListWithRequest(request: getForumListRequest) { (response) in
                request.isLoaded = true
                switch response {
                case .success(let responseData):
                    print(responseData.data)
                    if  responseData.success! == false{
                        DispatchQueue.main.async {
                            if request.page == 1{
                                request.data.removeAll()
                            }
                            dispatchGroup.leave()   // <<----
                        }
                    }else{
                        
                        DispatchQueue.main.async {
                            
                            
                            if let dataCount = responseData.data, dataCount.count < request.limit{
                                request.hasMore = false
                            }else{
                                request.hasMore = true
                            }
                            if request.page == 1{
                                request.data = responseData.data!
                            }else{
                                request.data.append(contentsOf: responseData.data!)
                            }
                            dispatchGroup.leave()   // <<----
                        }
                    }
                case .baseError (let error):
                    print(error)
                    
                    DispatchQueue.main.async {
                        if error.errorBase?.errorCode == -1009 {
                            request.internetConnected = false
                        }
                        if request.page == 1{
                            request.data.removeAll()
                        }
                        dispatchGroup.leave()   // <<----
                    }
                    //self.callFailure(error)
                }
            }
            
        }
        
        dispatchGroup.notify(queue: .main) {
            self.isLoading = false
            self.responseRecieved!()
            // whatever you want to do when both are done
        }
        
        
        
    }
    
    
    func callGetFavouriteForumsListAPI(){
        self.isLoading = true
        
        
        var param = [String : Any]()
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        
        if let limit = perPage {
            param["limit"] = limit
        }
        
        if let page = page{
            param["page"] = page
        }
        
        
        var forumListUrl = WebServiceConstants.favouriteForumListAPI
        forumListUrl = forumListUrl.replacingOccurrences(of: "{user_id}", with: "\(userId!)")
        
        let getForumListRequest = ForumListRequest(parameters:param,urlString: forumListUrl)
        
        WebServiceManager.getForumListWithRequest(request: getForumListRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                print(responseData.data)
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    self.forumLists = responseData.data!
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
        
    }

            
    
    private func callFavouriteAPI(){
        self.isLoading = true
        var favourUrl = WebServiceConstants.favouriteForumAPI
        favourUrl = favourUrl.replacingOccurrences(of: "{id}", with: "\(forumId!)")
        
        var param = [String : Any]()
        
        if let userId = userId {
            param["user_id"] = userId
        }
        
        if let isFavourite = isFavourite{
            param["fav"] = isFavourite
        }
        let favourRequest = FavouritePostRequest(parameters:param,urlString:favourUrl)
        
        WebServiceManager.favouriteUnFavouriteRequest(request: favourRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
    }
    
    
    func calldeleteForumAPI(){
        self.isLoading = true
        
        var param = [String : Any]()
       
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        var deleteForumUrl = WebServiceConstants.deleteForumAPI
        deleteForumUrl = deleteForumUrl.replacingOccurrences(of: "{id}", with: "\(forumId!)")
            
        let deleteForumRequest = DeleteForumRequest(parameters:param, urlString: deleteForumUrl)
        
        WebServiceManager.deleteForumWithRequest(request: deleteForumRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
    }
    
    
    func callshowFormlistnotification(){
        self.isLoading = true
        
        
        var param = [String : Any]()
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        
        if let limit = perPage {
            param["limit"] = limit
        }
        
        if let page = page{
            param["page"] = page
        }
        print(param)
        
        var forumListUrl = WebServiceConstants.forumShowlist
        forumListUrl = forumListUrl.replacingOccurrences(of: "{id}", with: "\(forumId!)")
         forumListUrl = forumListUrl.replacingOccurrences(of: "{user_id}", with: "\(userId!)")
        print(forumListUrl)
        
        let getForumListRequest = ForumListRequest(parameters:param,urlString: forumListUrl)
        print(getForumListRequest)
        
        WebServiceManager.getForumListWithRequest(request: getForumListRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                print(responseData.data)
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    self.forumLists = responseData.data!
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                if error.errorBase?.errorCode == 999{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        kAppDelegate.logoutFromApp()
                    }
                }
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
        
    }

}
