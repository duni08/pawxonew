//
//  Comment_VM.swift
//  Pawxo
//
//  Created by 42works on 04/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit

class Comment_VM: BaseViewModel {

    
    var userId: String?
    var postId,forumId: String?
    var hashTags: String?
    var taggedUsers: String?
    var comment,commentId,content : String?
    
    
    var page = 1
    var limit = 25
    
    
    var postObj : PostList?
    var postArr = [PostList]()
    
    var commentObj : CommentList?
    
    var commentList = [CommentList]()
    
    
    var forumCommentObj : ForumCommentList?
    
    var forumCommentList = [ForumCommentList]()
    
    var callAddcommentService:Bool? {
        didSet {
            addCommentAPI()
        }
    }
    
    
    var callDeletecommentService:Bool? {
        didSet {
            deleteCommentAPI()
        }
    }
    
    
    var callDeleteForumCommentService:Bool? {
        didSet {
            deleteForumCommentAPI()
        }
    }
    
    
    var callAddcommentForumService:Bool? {
        didSet {
            addCommentForumsAPI()
        }
    }
    
     var callFetchCommentsService:Bool? {
           didSet {
               getCommentsAPI()
           }
       }
    var callFetchPostDetailService:Bool? {
        didSet {
            getPostDetail()
        }
    }
    private func getPostDetail()
    {
        self.isLoading = true
        var param = [String : Any]()
        
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        if let postId = postId{
            param["id"] = postId
        }
        
        var commentListUrl = WebServiceConstants.getPostDetailAPI
        commentListUrl = commentListUrl.replacingOccurrences(of: "{id}", with: "\(postId!)")
        print(commentListUrl)
        let getCommentsRequest = PostDetailFetchRequest(parameters:param,urlString:commentListUrl)
        
        
        WebServiceManager.getPostDetailRequest(request: getCommentsRequest) { (response) in
            print(response)
                   self.isLoading = false
                   switch response {
                   case .success(let responseData):
                    print(responseData)
                       if  responseData.success! == false{
                           self.alertMessage = responseData.message!
                           self.showAlertClosure!()
                       }else{
                        
                       // if let postList  = responseData.data{
                        self.postObj = responseData.data
                        print("arr==\(self.postObj!)")
                       // }
                           self.responseRecieved!()
                       }
                   case .baseError (let error):
                       print(error)
                       self.alertMessage = error.errorBase?.message
                       if error.errorBase?.errorCode == 999{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            kAppDelegate.logoutFromApp()
                        }
                       }
                       self.showAlertClosure!()
                       //self.callFailure(error)
                   }
               }
    }
    
    
    
    
    var callFetchForumCommentsService:Bool? {
        didSet {
            getForumCommentsAPI()
        }
    }
    
    
    private func getCommentsAPI(){
        self.isLoading = true
        var param = [String : Any]()
        
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        if let postId = postId{
            param["post_id"] = postId
        }
        
        var commentListUrl = WebServiceConstants.getCommentAPI
        commentListUrl = commentListUrl.replacingOccurrences(of: "{post_id}", with: "\(postId!)")
        let getCommentsRequest = CommentListFetchRequest(parameters:param,urlString:commentListUrl)
        
        WebServiceManager.getCommentListPostRequest(request: getCommentsRequest) { (response) in
                   self.isLoading = false
                   switch response {
                   case .success(let responseData):
                       if  responseData.success! == false{
                           self.alertMessage = responseData.message!
                           self.showAlertClosure!()
                       }else{
                        
                        if let commentList  = responseData.data!.commentList{
                            self.commentList = commentList
                        }
                           self.responseRecieved!()
                       }
                   case .baseError (let error):
                       print(error)
                       self.alertMessage = error.errorBase?.message
                       self.showAlertClosure!()
                       //self.callFailure(error)
                   }
               }
        
    }
    
    
    private func getForumCommentsAPI(){
        self.isLoading = true
        var param = [String : Any]()
        
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        
            param["page"] = page
        
             param["limit"] = limit
       
        
        
        var commentListUrl = WebServiceConstants.getForumCommentAPI
        commentListUrl = commentListUrl.replacingOccurrences(of: "{id}", with: "\(forumId!)")
        let getCommentsRequest = CommentListFetchRequest(parameters:param,urlString:commentListUrl)
        
        WebServiceManager.getForumCommentListPostRequest(request: getCommentsRequest) { (response) in
                   self.isLoading = false
                   switch response {
                   case .success(let responseData):
                       if  responseData.success! == false{
                           self.alertMessage = responseData.message!
                           self.showAlertClosure!()
                       }else{
                        
                        if let commentList  = responseData.data{
                            self.forumCommentList = commentList
                        }
                           self.responseRecieved!()
                       }
                   case .baseError (let error):
                       print(error)
                       self.alertMessage = error.errorBase?.message
                       self.showAlertClosure!()
                       //self.callFailure(error)
                   }
               }
        
    }
    
    private func addCommentAPI(){
        self.isLoading = true
        
        var param = [String : Any]()
        
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
        if let postId = postId{
            param["post_id"] = postId
        }
        
        if let hashTags = hashTags{
            param["hash_tags"] = hashTags
        }
        if let taggedUsers = taggedUsers{
            param["tagged_users"] = taggedUsers
        }
        if let comment = comment{
            param["comment"] = comment
        }
        
        
        let addCommentRequest = PostCommentAddRequest(parameters:param)
        
        WebServiceManager.addCommentPostRequest(request: addCommentRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    if let commentObject = responseData.data {
                        self.commentObj = commentObject
                    }
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
        
        
        
    }
    
    
    private func deleteForumCommentAPI(){
        
        self.isLoading = true
           var param = [String : Any]()
           
           if let userIdValue = userId{
               param["user_id"] = userIdValue
           }
           
         
           var commentListUrl = WebServiceConstants.deleteForumCommentAPI
           commentListUrl = commentListUrl.replacingOccurrences(of: "{id}", with: "\(commentId!)")
           let deleteForumCommentsRequest = ForumDeleteCommentRequest(parameters:param,urlString:commentListUrl)
        
        WebServiceManager.deleteForumCommentPostRequest(request: deleteForumCommentsRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
        
        
        
    }
   
    
    private func deleteCommentAPI(){
        self.isLoading = true
        
        var param = [String : Any]()
        
        if let commentId = commentId{
            param["comment_id"] = commentId
        }
        
       
        
        
        let deleteCommentRequest = PostDeleteCommentRequest(parameters:param)
        
        WebServiceManager.deletePostCommentPostRequest(request: deleteCommentRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
        
        
        
    }
    
    private func addCommentForumsAPI(){
        self.isLoading = true
        
        var param = [String : Any]()
        
        if let userIdValue = userId{
            param["user_id"] = userIdValue
        }
        
//        if let forumId = forumId{
//            param["forum_id"] = forumId
//        }
        
        
        if let taggedUsers = taggedUsers{
            param["tagged_users"] = taggedUsers
        }
        if let content = content{
            param["content"] = content
        }
        
        
        var addCommentUrl = WebServiceConstants.addForumCommentAPI
        addCommentUrl = addCommentUrl.replacingOccurrences(of: "{id}", with: "\(forumId!)")
        let addForumCommentsRequest = CommentAddForumRequest(parameters:param,urlString:addCommentUrl)
        
        WebServiceManager.addForumCommentPostRequest(request: addForumCommentsRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                    
                    self.alertMessage = responseData.message!
                    if let commentObject = responseData.data!.comment {
                        self.forumCommentObj = commentObject
                    }
                    self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
        
        
        
    }
}
