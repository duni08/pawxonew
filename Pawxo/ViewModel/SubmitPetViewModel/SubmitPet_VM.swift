//
//  SubmitPet_VM.swift
//  Pawxo
//
//  Created by Apple on 23/01/20.
//  Copyright © 2020 42works. All rights reserved.
//

import Foundation
import UIKit


class SubmitPet_VM: BaseViewModel {

    //MARK:- Variables
    var petType: String?
    var petName: String?
    var breed: String?
    var gender: String?
    var dateOfBirth: String?
    var location: String?
    var latitude: Float?
    var longitude: Float?
    var userIdFinal: String?
    var perImage:UIImage?
    var petId : String?
    var petBio : String?
    var imageData : Data?
    
    var callRegisterNewPetService:Bool? {
        didSet {
            callRegisterNewPetAPI()
        }
    }
    
    var callEditPetService:Bool? {
        didSet {
            callEditPetAPI()
        }
    }
    
    //MARK:- Register Api Call
    private func callRegisterNewPetAPI() {
        self.isLoading = true
        var param = [String : Any]()
        if let petTypeValue = petType{
            param["petType"] = petTypeValue
        }
        if let petNameValue = petName{
            param["petName"] = petNameValue
        }
        if let breedValue = breed{
            param["breed"] = breedValue
        }
        if let genderValue = gender{
            param["gender"] = genderValue
        }
        if let dobValue = dateOfBirth,dobValue != "" {
            param["dob"] = dobValue
        }
        
        if let locationValue = location{
            param["location"] = locationValue
        }
        
        if let latitudeValue = latitude{
            param["latitude"] = "\(latitudeValue)"
        }
        
        if let longitudeValue = longitude{
            param["longitude"] = "\(longitudeValue)"
        }
        
        if let userIdValue = userIdFinal{
            param["user_id"] = "\(userIdValue)"
        }
        
        WebServices().uploadImage(methodName: ApiServiceName.addPetUrl, image:true , imageUpload: perImage, params: param as NSDictionary) { (status, msg, response) in
            self.isLoading = false
            if status == true {
                
                print("response-- \(String(describing: response))")
                self.responseRecieved?()
            }else{
                self.alertMessage = msg
                self.showAlertClosure?()
            }
            
        }
        
        
        
        
        
        

    }
    
    
    //MARK:- Register Api Call
    private func callEditPetAPI() {
        self.isLoading = true
        var param = [String : Any]()
        if let petTypeValue = petType{
            param["pet_type"] = petTypeValue
        }
        if let petNameValue = petName{
            param["pet_name"] = petNameValue
        }
        if let breedValue = breed{
            param["pet_breed"] = breedValue
        }
        if let genderValue = gender{
            param["pet_gender"] = genderValue
        }
        if let dobValue = dateOfBirth{
            param["pet_dob"] = dobValue
        }
        
        if let petBio = petBio {
            param["pet_bio"] = petBio
        }
        if let locationValue = location{
            param["location"] = locationValue
        }
        
        if let latitudeValue = latitude{
            param["latitude"] = "\(latitudeValue)"
        }
        
        if let longitudeValue = longitude{
            param["longitude"] = "\(longitudeValue)"
        }
        
        if let userIdValue = userIdFinal{
            param["user_id"] = "\(userIdValue)"
        }
        
        if let imageData = imageData {
            param["image_data"] = imageData
        }
        
        
        var editPetUrl = WebServiceConstants.editPetAPI
        editPetUrl = editPetUrl.replacingOccurrences(of: "{id}", with: "\(petId!)")
        
        WebServices().editPetProfile(urlStr: editPetUrl, params: param as NSDictionary) { (status, msg, response) in
            self.isLoading = false
            if status == true {
                print("response-- \(String(describing: response))")
                self.responseRecieved?()
            }else{
                self.alertMessage = msg
            }
        }

    }
    
    
}
