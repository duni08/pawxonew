//
//  PushNotification_VM.swift
//  Pawxo
//
//  Created by Macbook on 01/02/20.
//  Copyright © 2020 42works. All rights reserved.
//

import UIKit


class PushSetting:NSObject {
    var title = ""
    var isEnabled = 0
}



class PushNotification_VM: BaseViewModel {
    //MARK:- Variables
    var userId: String?
    
    var settings : NotificationSetting?
    
    
    
    var likePush, commentPush, tagCommentPush,comment_forum_push,like_forum_push,receive_follow_request_push,mention_comment: String?
    var tagPostPush, receiveFollowPush, pokePush, favouritePush: String?
    
    
    
    
    
    
    
    
    
    
    
    
    
    var callGetSettingService: Bool? {
        didSet {
            callGettingNotificationSettingAPI()
        }
    }
    
    var callUpdateSettingService: Bool? {
        didSet {
            callUpdateNotificationSettingsAPI()
        }
    }
    
    var pushSettings = [PushSetting]()
    
     var titleArr = ["Likes your post","Comment on your post","Mention you in a post","Mention you in a post comment","Sends you follow request","Start following you","Likes your forum","Comment on your forum","Mention you in a forum comment"]
    
    
    func setUpSettings(settings:NotificationSetting){
        let likesSetting = PushSetting()
        likesSetting.title = titleArr[0]
        likesSetting.isEnabled = settings.likePush!
        self.pushSettings.append(likesSetting)
        
        let commentSetting = PushSetting()
        commentSetting.title = titleArr[1]
        commentSetting.isEnabled = settings.commentPush!
        self.pushSettings.append(commentSetting)
        
        let tagPostSetting = PushSetting()
        tagPostSetting.title = titleArr[2]
        tagPostSetting.isEnabled = settings.tagPostPush!
        self.pushSettings.append(tagPostSetting)
        
        let tagCommentPushSetting = PushSetting()
        tagCommentPushSetting.title = titleArr[3]
        tagCommentPushSetting.isEnabled = settings.tagCommentPush!
        self.pushSettings.append(tagCommentPushSetting)
        
        let sendsFollowRequestSetting = PushSetting()
        sendsFollowRequestSetting.title = titleArr[4]
        sendsFollowRequestSetting.isEnabled = settings.receive_follow_request_push!
        self.pushSettings.append(sendsFollowRequestSetting)
        
        let receiveFollowPushSetting = PushSetting()
        receiveFollowPushSetting.title = titleArr[5]
        receiveFollowPushSetting.isEnabled = settings.receiveFollowPush!
        self.pushSettings.append(receiveFollowPushSetting)
        
        let like_forum_pushSetting = PushSetting()
        like_forum_pushSetting.title = titleArr[6]
        like_forum_pushSetting.isEnabled = settings.like_forum_push!
        self.pushSettings.append(like_forum_pushSetting)
        
        let comment_forum_pushSetting = PushSetting()
        comment_forum_pushSetting.title = titleArr[7]
        comment_forum_pushSetting.isEnabled = settings.comment_forum_push!
        self.pushSettings.append(comment_forum_pushSetting)
        
        let mention_commentSetting = PushSetting()
        mention_commentSetting.title = titleArr[8]
        mention_commentSetting.isEnabled = settings.mention_comment!
        self.pushSettings.append(mention_commentSetting)
        
        
    }
    
    //MARK:- Like Post Api Call
           private func callGettingNotificationSettingAPI() {
               self.isLoading = true
               
              
               var notificationUrl = WebServiceConstants.notificationSettingAPI
               notificationUrl = notificationUrl.replacingOccurrences(of: "{user_id}", with: userId!)
               let settingListRequest = NotificationSettingRequest(urlString:notificationUrl)
               
               WebServiceManager.notificationSettingWithRequest(request: settingListRequest) { (response) in
                self.isLoading = false
                   switch response {
                   case .success(let responseData):
                       if  responseData.success! == false{
                           self.alertMessage = responseData.message!
                           self.showAlertClosure!()
                       }else{
                        self.settings = responseData.data!
                        self.responseRecieved!()
                       }
                   case .baseError (let error):
                       print(error)
                       self.alertMessage = error.errorBase?.message
                       self.showAlertClosure!()
                       //self.callFailure(error)
                   }
               }
               
           }
    
    
    
    
    
    
    //MARK:- update setting Api Call
        private func callUpdateNotificationSettingsAPI() {
            self.isLoading = true
            var param = [String : Any]()
            
            
            if let userIdValue = userId{
                param["user_id"] = userIdValue
            }
            
            if let likePush = likePush{
                param["like_push"] = likePush
            }
            
            if let commentPush = commentPush{
                param["comment_push"] = commentPush
            }
            if let tagCommentPush = tagCommentPush{
                param["tag_comment_push"] = tagCommentPush
            }
            if let tagPostPush = tagPostPush{
                param["tag_post_push"] = tagPostPush
            }
            if let receiveFollowPush = receiveFollowPush{
                param["receive_follow_push"] = receiveFollowPush
            }
            
            if let pokePush = pokePush{
                param["poke_push"] = pokePush
            }
            
            if let favouritePush = favouritePush{
                param["favourite_push"] = favouritePush
            }
            
            
            if let comment_forum_push = comment_forum_push{
                param["comment_forum_push"] = comment_forum_push
            }
            
            if let like_forum_push = like_forum_push{
                param["like_forum_push"] = like_forum_push
            }
            
            if let mention_comment = mention_comment{
                param["mention_comment"] = mention_comment
            }
            
            if let receive_follow_request_push = receive_follow_request_push{
                param["receive_follow_request_push"] = receive_follow_request_push
            }
            
            
        var notificationUrl = WebServiceConstants.notificationUpdateSettingAPI
        notificationUrl = notificationUrl.replacingOccurrences(of: "{user_id}", with: userId!)
            let settingListRequest = NotificationUpdateRequest(parameters:param,urlString:notificationUrl)
        
        WebServiceManager.notificationUpdateSettingWithRequest(request: settingListRequest) { (response) in
            self.isLoading = false
            switch response {
            case .success(let responseData):
                if  responseData.success! == false{
                    self.alertMessage = responseData.message!
                    self.showAlertClosure!()
                }else{
                 self.responseRecieved!()
                }
            case .baseError (let error):
                print(error)
                self.alertMessage = error.errorBase?.message
                self.showAlertClosure!()
                //self.callFailure(error)
            }
        }
            
    

        }
    
}
