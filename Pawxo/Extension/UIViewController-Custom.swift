//
//  UIViewController-Custom.swift
//  XpeDash
//
//  Created by PBS9 on 18/11/19.
//  Copyright © 2019 Dinesh Raja. All rights reserved.
//

import UIKit

var vSpinner : UIView?

extension UIViewController {
    
    func setBackButton() {
        var backBtn = UIImage(named: "back-icon")!.paddingImage(UIEdgeInsets.init(top: 4, left: 8, bottom: 4, right: 0))!
        backBtn = backBtn.withRenderingMode(.alwaysOriginal)
        
        self.navigationController?.navigationBar.backIndicatorImage = backBtn
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backBtn
        //self.navigationController?.navigationBar.backItem?.title = ""
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
//        self.navigationController?.navigationBar.backgroundColor = .appBlue
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
    }
    
    func setNavigationBar(){
        
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//        self.navigationController?.navigationBar.barTintColor = .appBlue
    
        let btn1 = UIButton()
        btn1.setImage(UIImage(named: "bell"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //btn1.addTarget(self, action: Selector("action1:"), forControlEvents: .TouchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = btn1

        let btn2 = UIButton()
        btn2.setImage(UIImage(named: "cart"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //btn2.addTarget(self, action: Selector("action2:"), forControlEvents: .TouchUpInside)
        let item2 = UIBarButtonItem()
        item2.customView = btn2

        let btn3 = UIButton()
        btn3.setImage(UIImage(named: "search"), for: .normal)
        btn3.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //btn2.addTarget(self, action: Selector("action2:"), forControlEvents: .TouchUpInside)
        let item3 = UIBarButtonItem()
        item3.customView = btn3

        self.navigationItem.rightBarButtonItems = [item1,item2,item3]
    }
    @objc func ShowMenu(){
//        sideMenuController?.showLeftViewAnimated()
    }
    
    func setBackButtonTitle(_ title: String = "") {
        let backButton = UIBarButtonItem()
        backButton.title = title
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    func getVC(id: String, storyboard: String) -> UIViewController {
        let s = UIStoryboard.init(name: storyboard, bundle: nil)
        return s.instantiateViewController(withIdentifier: id)
    }
    
    func pushVC(_ vC: UIViewController) {
        self.navigationController?.pushViewController(vC, animated: true)
    }
    
    func popVC() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func popToRoot() {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    func presentVC(_ vC: UIViewController) {
        self.present(vC, animated: true, completion: nil)
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    func popBack<T: UIViewController>(toControllerType: T.Type) {
        if var viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            viewControllers = viewControllers.reversed()
            for currentViewController in viewControllers {
                if currentViewController .isKind(of: toControllerType) {
                    self.navigationController?.popToViewController(currentViewController, animated: true)
                    break
                }
            }
        }
    }
    
    
    func showNoConnection(_ message: String = "The Internet connection appears to be offline.",completion: @escaping (Bool) -> ()) {
        let alertVC = UIAlertController(title: "Oops, something went wrong. Please try again.", message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alertAction) in
            completion(false)
        }))
        alertVC.addAction(UIAlertAction(title: "Try again", style: .default, handler: { (alertAction) in
            completion(true)
        }))
        self.present(alertVC, animated: true)
    }
    
    
    func showMessage(_ message: String) {
        let alertController = UIAlertController(title: message, message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        alertController.view.tintColor = UIColor.black
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(_ message: String, handler: ((UIAlertAction) -> Void)?) {
        let alertController = UIAlertController(title: message, message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: handler))
        alertController.view.tintColor = UIColor.black
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(_ title: String, _ message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        alertController.view.tintColor = UIColor.black
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(_ title: String, _ message: String, handler: ((UIAlertAction) -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: handler))
        alertController.view.tintColor = UIColor.black
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func hideKeyboardTapped() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    // MARK: - Spinner
//
    func showSpinner(onView : UIView,_ color: UIColor = .SpinnerColor) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = color//.appGray//UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.6)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center

        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }

        vSpinner = spinnerView
    }
    
    func hideSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}

