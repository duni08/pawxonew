//
//  UserDefaults+Additions.swift
//
//  AutoForce
//
//  Created by Apple2 on 20/08/19.
//  Copyright © 2019 Apple2. All rights reserved.
//

import Foundation

// MARK: - NSUserDefaults Extension
extension UserDefaults {

    // MARK: - User Defaults
    /**
     sets/adds object to NSUserDefaults

     - parameter aObject: object to be stored
     - parameter defaultName: key for object
     */
    class func setObjectModel(_ value: NSDictionary, forKey defaultName: String) {
        //let obj = NSKeyedArchiver.archivedData(withRootObject: value)
        
        let userLogin = User(userInfo: value)
        let encoder = JSONEncoder()
        
        if let encoded = try? encoder.encode(userLogin) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: defaultName)
        }
    }

    /**
     gives stored object in NSUserDefaults for a key

     - parameter defaultName: key for object

     - returns: stored object for key
     */
    class func objectModelForKey(_ defaultName: String) -> User {
        let defaults = UserDefaults.standard
        
        var loadPersonValue:Decodable?
        
        if let savedPerson = defaults.object(forKey: defaultName) as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(User.self, from: savedPerson) {
                print(loadedPerson.name)
                loadPersonValue = loadedPerson
            }
        }
        return loadPersonValue as! User
    }

    /**
     sets/adds object to NSUserDefaults
     
     - parameter aObject: object to be stored
     - parameter defaultName: key for object
     */
    class func setObject<T>(_ value: T?, forKey defaultName: String) {
        UserDefaults.standard.set(value, forKey: defaultName)
        UserDefaults.standard.synchronize()
    }
    
    /**
     gives stored object in NSUserDefaults for a key
     
     - parameter defaultName: key for object
     
     - returns: stored object for key
     */
    class func objectForKey<T>(_ defaultName: String) -> T? {
        return UserDefaults.standard.object(forKey: defaultName) as? T
    }

    
    /**
     removes object from NSUserDefault stored for a given key

     - parameter defaultName: key for object
     */
    class func removeObjectForKey(_ defaultName: String) {
        UserDefaults.standard.removeObject(forKey: defaultName)
        UserDefaults.standard.synchronize()
    }
}
