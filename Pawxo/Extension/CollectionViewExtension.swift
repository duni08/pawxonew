//
//  CollectionViewExtension.swift
//  AutoForce
//
//  Created by Apple2 on 01/09/19.
//  Copyright © 2019 Apple2. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    var centerPoint : CGPoint {
        
        get {
            return CGPoint(x: self.center.x + self.contentOffset.x, y: self.center.y + self.contentOffset.y);
        }
    }
    
    var centerCellIndexPath: IndexPath? {
        
        
        if let centerIndexPath: IndexPath  = self.indexPathForItem(at: self.centerPoint) {
            
            return centerIndexPath
        }
        return nil
    }
}
