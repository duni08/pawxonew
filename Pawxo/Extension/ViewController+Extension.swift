//
//  ViewController+Extension.swift
//  AutoForce
//
//  Created by Apple2 on 20/08/19.
//  Copyright © 2019 Apple2. All rights reserved.
//

import UIKit
import Foundation

extension UIViewController {
    
    func logController()
    {
        
    }
    
    func alertMethod(message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- Show Alerts
    func showAlert(_ viewController: UIViewController, message: String) {
        CommonFunctions.showAlert(viewController, message: message)
    }
    
    
    func alertMethodWithButton(title: String,message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
