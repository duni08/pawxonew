//
//  UIFont+CustomFonts.swift
//  AutoForce
//
//  Created by Apple2 on 20/08/19.
//  Copyright © 2019 Apple2. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    class func JosefinSansBold(size:CGFloat) -> UIFont{
        if let font = UIFont(name: "JosefinSans-Bold", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    class func JosefinSansRegular(size:CGFloat) -> UIFont{
        if let font = UIFont(name: "JosefinSans-Regular", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    class func JosefinSansLight(size:CGFloat) -> UIFont{
        if let font = UIFont(name: "JosefinSans-Light", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    class func JosefinSansSemiBold(size:CGFloat) -> UIFont{
        if let font = UIFont(name: "JosefinSans-SemiBold", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    
    class func poppinsBoldFont(size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Poppins-Bold", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }

    class func poppinsRegularFont(size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Poppins-Regular", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }

    class func poppinsMediumFont(size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Poppins-Medium", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }

    class func poppinsSemiBoldFont(size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Poppins-SemiBold", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    class func poppinsLightFont(size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Poppins-Light", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
}
