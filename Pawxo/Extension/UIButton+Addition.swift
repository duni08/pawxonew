//
//  UIButton+Addition.swift
//  AutoForce
//
//  Created by Apple2 on 20/08/19.
//  Copyright © 2019 Apple2. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIButton {
    
    
    func setButtonImageColor(color:UIColor){
        let image = self.image(for: .normal)?.withRenderingMode(.alwaysTemplate)
        self.setImage(image, for: .normal)
        self.tintColor = color
    }
    
    
    func setImage(fromUrl url:String, defaultImage image:UIImage?, isCircled:Bool = false, borderColor:UIColor? = .clear, borderWidth:CGFloat? = 1.0, isCache:Bool = true, showIndicator:Bool = true) -> Void {
        self.imageView?.contentMode = .scaleAspectFill
        var processer:ImageProcessor?
        if isCircled {
            layer.cornerRadius = frame.size.width / 2
            layer.borderColor = borderColor?.cgColor
            layer.borderWidth = borderWidth ?? 0.5
            clipsToBounds = true
            processer = RoundCornerImageProcessor(cornerRadius: self.bounds.size.width)
        }else {
            processer = DefaultImageProcessor()
        }
        guard let url = URL(string:url) else { self.setImage(image, for: .normal); return }
        if showIndicator {
            self.imageView?.kf.indicatorType = .activity
        }
//        if isCache {
//            self.kf.setImage(with: url, for: .normal, placeholder: image, options: [.processor(processer!)], progressBlock: nil, completionHandler: nil)
//        }else {
//            self.kf.setImage(with: url, for: .normal, placeholder: image, options: [.processor(processer!), .forceRefresh], progressBlock: nil, completionHandler: nil)
//        }
    }
}
