//
//  HelpingSingleton.swift
//  XpeDash
//
//  Created by PBS9 on 18/11/19.
//  Copyright © 2019 Dinesh Raja. All rights reserved.
//

import Foundation
import UIKit


struct Helper
{
    static var shared = Helper()
    
    // Mark: - Alert Message
    // =====================================
    static func Alertmessage(title:String, message:String , vc:UIViewController?){
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        if vc != nil{
            vc!.present(alert, animated: true, completion: nil)
            
        }else{
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate{
                appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    static func showNoConnection(vc:UIViewController?, completion: @escaping (Bool) -> ()) {
        let alertVC = UIAlertController(title: "Oops, something went wrong. Please try again.", message: "The Internet connection appears to be offline.", preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alertAction) in
            completion(false)
        }))
        alertVC.addAction(UIAlertAction(title: "Try again", style: .default, handler: { (alertAction) in
            completion(true)
        }))
        if vc != nil{
            vc!.present(alertVC, animated: true, completion: nil)
        }else{
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate{
                appDelegate.window?.rootViewController?.present(alertVC, animated: true, completion: nil)
            }
        }
    }
    
    
    struct Alert {
        static func errorAlert(title: String, message: String?, cancelButton: Bool = false, completion: (() -> Void)? = nil) -> UIAlertController {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let actionOK = UIAlertAction(title: "OK", style: .default) {
                _ in
                guard let completion = completion else { return }
                completion()
            }
            alert.addAction(actionOK)
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            cancel.setValue(#colorLiteral(red: 0.08235294118, green: 0.6509803922, blue: 0.3411764706, alpha: 1), forKey: "titleTextColor")
            if cancelButton { alert.addAction(cancel) }
            
            return alert
        }
        
    }
    
   static func showlogoutAlert(){
        
        let alert = UIAlertController(title: "Info:", message: "Your Session is expired", preferredStyle: UIAlertController.Style.alert);
        let action = UIAlertAction.init(title: "OK", style: .default) { (sction) in
            //  Helper.shared.appDelegate.loggout()
        }
        alert.addAction(action)
        if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
            window.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    // =========== UIview only top edges cornerradius ================
    static func Topedgeview(viewoutlet:UIView, radius: CGFloat)
    {
        viewoutlet.clipsToBounds = true
        viewoutlet.layer.cornerRadius = radius
        viewoutlet.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    static func Bottemedgeview(viewoutlet:UIView, radius: CGFloat)
    {
        viewoutlet.clipsToBounds = true
        viewoutlet.layer.cornerRadius = radius
        viewoutlet.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner]
    }
    static func leftSideEdgeview(viewoutlet:UIView, radius: CGFloat)
    {
        viewoutlet.clipsToBounds = true
        viewoutlet.layer.cornerRadius = radius
        viewoutlet.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
    }
    static func rightSideEdgeview(viewoutlet:UIView, radius: CGFloat)
    {
       
        viewoutlet.layer.cornerRadius = radius
        viewoutlet.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
    }
    
    static func BottemedgeviewCorner(viewoutlet:UIView, radius:CGFloat)
    {
        viewoutlet.clipsToBounds = true
        viewoutlet.layer.cornerRadius = radius
        viewoutlet.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    static func viewCornerRadius(viewoutlet:UIView)
    {
        viewoutlet.clipsToBounds = true
        viewoutlet.layer.cornerRadius = 5
        viewoutlet.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMinYCorner] 
    }
    
    static func topViewCornerRadius(viewoutlet:UIView)
    {
        viewoutlet.clipsToBounds = true
        viewoutlet.layer.cornerRadius = 5
        viewoutlet.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    static func instaGram (ID: String){
        let url = "instagram://user?username=" + ID
        let instagramURL = URL(string: url)
        if let instagramURL = instagramURL {
            if UIApplication.shared.canOpenURL(instagramURL) {
                UIApplication.shared.open(instagramURL, options: [:], completionHandler: nil)
            }else {
                //redirect to safari because the user doesn't have Instagram
                UIApplication.shared.open(NSURL(string: "http://instagram.com/\(ID)")! as URL, options: [:], completionHandler: nil)
            }
        }
    }
    
    // ============  UITextView CornerRadius =======================
    
    static func TextviewcornerRadius(textviewoutlet:UITextView)
    {
        textviewoutlet.layer.borderWidth = 1
        textviewoutlet.layer.borderColor = UIColor.init(red: 175/255, green: 178/255, blue: 197/225, alpha: 0.2).cgColor
        textviewoutlet.layer.cornerRadius = 15
    }
    
    static func dropShadowView(view: UIView, opacity: Float, color: CGColor){
        view.layer.shadowColor = color
        view.layer.shadowOpacity = opacity
        view.layer.shadowOffset = CGSize(width: 1, height: 1)
        view.layer.shadowRadius = 2
        
    }
    // Mark: - Valid Email or Not
    /*************************************************************/
    
    func isValidEmail(candidate: String) -> Bool {
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        var valid = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
        if valid {
            valid = !candidate.contains("..")
        }
        return !valid
    }
    
    // Mark: - Valid Phone or Not
    func isValidPhone(phone: String) -> Bool {
        
        let phoneRegex = "^[0-9]{10}$"
        var valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: phone)
        if valid {
            valid = !phone.contains("..")
        }
        return !valid
    }
    
    // Mark: - Field empty or not
    /*************************************************************/
    
    func isFieldEmpty(field: String) -> Bool {
        return field.count == 0
    }
}
    //Mark:- TextField padding
    class SharedClass: NSObject
    {
        static let sharedInstance = SharedClass()
        
        //This is my padding function.
        func textFieldLeftPadding(textFieldName: UITextField)
        {
            // Create a padding view
            textFieldName.leftView = UIView(frame: CGRect(x: 0, y: 0, width:10, height: textFieldName.frame.height))
            textFieldName.leftViewMode = .always//For left side padding
            textFieldName.rightViewMode = .always//For right side padding
        }
        
        
}


