//
//  Color.swift
//  XpeDash
//
//  Created by PBS9 on 18/11/19.
//  Copyright © 2019 Dinesh Raja. All rights reserved.
//

import UIKit

extension UIColor {
    
    @nonobjc class var darkGray: UIColor {
        return UIColor(red: 48.0 / 255.0, green: 48.0 / 255.0, blue: 48.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var lightGray: UIColor {
        return UIColor(red: 248.0 / 255.0, green: 246.0 / 255.0, blue: 251.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var shadowGray: UIColor {
        return UIColor(red: 235.0 / 255.0, green: 232.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var appGray: UIColor {
        return UIColor(red: 156.0 / 255.0, green: 156.0 / 255.0, blue: 162.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var appBlack: UIColor {
        return UIColor(red: 11.0 / 255.0, green: 12.0 / 255.0, blue: 13.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var appBlue: UIColor {
        return UIColor(red: 0 / 255.0, green: 0 / 255.0, blue: 255 / 255.0, alpha: 1.0)
        
    }
    @nonobjc class var SpinnerColor: UIColor {
        //return UIColor(red: 48.0 / 255.0, green: 47.0 / 255.0, blue: 180.0 / 255.0, alpha: 1.0)
        return UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.6)
        
    }
}

