//
//  ImageView+Extension.swift
//  AutoForce
//
//  Created by Apple2 on 20/08/19.
//  Copyright © 2019 Apple2. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

typealias CompletionHandlerIndicator = (_ success:Bool) -> Void

extension UIImageView {
    
    
     func setImageColor(color: UIColor) {
            
            if #available(iOS 13.0, *) {
                self.image =  self.image?.withTintColor(color, renderingMode: .alwaysTemplate)
            } else {
                // Fallback on earlier versions
                self.image  = self.image?.withRenderingMode(.alwaysTemplate)
                self.tintColor = color
            }
    //        self.setImage(imgView.image?.withRenderingMode(.alwaysTemplate) i )
        }
    
    
    func setImage(fromUrl url:String, defaultImage image:UIImage?, isCircled:Bool = false, borderColor:UIColor? = .clear, borderWidth:CGFloat? = 0.0, isCache:Bool = true, showIndicator:Bool = true) -> Void {
        var imageProcesser:ImageProcessor?
        if isCircled {
            layer.cornerRadius = frame.size.width / 2
            layer.borderColor = borderColor?.cgColor
            layer.borderWidth = borderWidth ?? 0.0
            clipsToBounds = true
            imageProcesser = RoundCornerImageProcessor(cornerRadius: self.bounds.size.width)
        }else {
            imageProcesser = DefaultImageProcessor.default
        }
        guard URL(string:url) != nil else { self.image = image; return }
        if showIndicator {
            self.kf.indicatorType = .activity
        }
      
        
        var url = URL(string: url)
        
       // let processor = DownsamplingImageProcessor(size: self.frame.size)
          //  |> RoundCornerImageProcessor(cornerRadius: 20)
      
        if isCache {
            self.kf.setImage(
                     with: url,
                     placeholder: image,
                     options: [
                       .processor(imageProcesser!),
                         .scaleFactor(UIScreen.main.scale),
                         .transition(.fade(1)),
                         .cacheOriginalImage,
                     ])
                 {
                     result in
                     switch result {
                     case .success(let value):
                      print("Task done for: \(value.source.url?.absoluteString ?? "")")
                     case .failure(let error):
                         print("Job failed: \(error.localizedDescription)")
                     }
                 }
        }else{
            self.kf.setImage(
                     with: url,
                     placeholder: image,
                     options: [
                       .processor(imageProcesser!),
                         .scaleFactor(UIScreen.main.scale),
                         .transition(.fade(1)),
                         .cacheOriginalImage,.forceRefresh
                     ])
                 {
                     result in
                     switch result {
                     case .success(let value):
                      print("Task done for: \(value.source.url?.absoluteString ?? "")")
                     case .failure(let error):
                         print("Job failed: \(error.localizedDescription)")
                     }
                 }
        }
        
        
     
        
        
//        if isCache {
//            self.kf.setImage(with: url, placeholder: image, options: [.processor(processer!)], progressBlock: nil, completionHandler: nil)
//        }else {
//            self.kf.setImage(with: url, placeholder: image, options: [.processor(processer!), .forceRefresh], progressBlock: nil, completionHandler: nil)
//        }
    }
    
    func setImageWithCustomIndicatorView(fromUrl url:String, defaultImage image:UIImage?, isCircled:Bool = false, borderColor:UIColor? = .clear, borderWidth:CGFloat? = 1.0, isCache:Bool = true, showIndicator:Bool = true, completion: @escaping CompletionHandlerIndicator){
        var processer:ImageProcessor?
        if isCircled {
            layer.cornerRadius = frame.size.width / 2
            layer.borderColor = borderColor?.cgColor
            layer.borderWidth = borderWidth ?? 0.5
            clipsToBounds = true
            processer = RoundCornerImageProcessor(cornerRadius: self.bounds.size.width)
        }else {
            processer = DefaultImageProcessor()
        }
        guard let url = URL(string:url) else { self.image = image; return }
        if showIndicator {
            self.kf.indicatorType = .none
        }
        if isCache {
            //self.kf.setImage(with: url, placeholder: image, options: [.processor(processer!)], progressBlock: nil, completionHandler: nil)
            self.kf.setImage(with: url, placeholder: image, options: [.processor(processer!)], progressBlock: nil) { (image, error, cache, url) in
                if error == nil{
                    completion(true)
                }else{
                    completion(false)
                }
            }
            
        }else {
           // self.kf.setImage(with: url, placeholder: image, options: [.processor(processer!), .forceRefresh], progressBlock: nil, completionHandler: nil)
            
            self.kf.setImage(with: url, placeholder: image, options: [.processor(processer!), .forceRefresh], progressBlock: nil) { (image, error, cache, url) in
                if error == nil{
                    completion(true)
                }else{
                    completion(false)
                }
            }
        }
    }
}

extension UIImageView {
    override open func awakeFromNib() {
        super.awakeFromNib()
        tintColorDidChange()
    }
}
