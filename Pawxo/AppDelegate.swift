//
//  AppDelegate.swift
//  Pawxo
//
//  Created by 42works on 24/12/19.
//  Copyright © 2019 42works. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import UserNotifications
import TwitterKit
import Firebase
import FirebaseCore
import FirebaseMessaging


let googleApiKey = "AIzaSyDYCNlu_VIk-61Z-dSsYX_PexC6o7kVOUc"
var breedVM : Breed_VM?
var notifyDta = true

var isChat = false

var notificationDictionary : NSDictionary?

var tabBar : MainTABViewController?

@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        TWTRTwitter.sharedInstance().start(withConsumerKey: "zAh6u4UHD7LUYa20NWl5mqgnj", consumerSecret: "eDal1Z6BG5XMIzBgPtkAUbo3Hxwnp4hbjDCB4M5UnXtMTuCZf0")
//        TWTRTwitter.sharedInstance().start(withConsumerKey: "H8VxITwT4mngKruQIdLBKk9Vk", consumerSecret: "ovFywCb2xrT81xfp429xp3KZvWgCHANOZ9GjApHQEgCPPhlW2H")
       
        GMSPlacesClient.provideAPIKey("AIzaSyBa-LT2cQTQwg-xo5OFl7RQ5z0OxOo38as")
        GMSServices.provideAPIKey(googleApiKey)
        registerForPushNotifications(application: application)
        application.applicationIconBadgeNumber = 0
        
//         if let remoteNotification = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? NSDictionary {
//
//             }
        
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            // Fallback on earlier versions
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.callgetBreedNameList()
        }
        
          NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: Notification.Name.InstanceIDTokenRefresh, object: nil)
        
         // Firebase config
              FirebaseApp.configure()

              
        if  notifyDta{
              
            self.registerPush()
        }
        
       

        return true
    }
    
    func registerPush(){
        if #available(iOS 10.0, *) {
              // For iOS 10 display notification (sent via APNS)
              UNUserNotificationCenter.current().delegate = self
              let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
              UNUserNotificationCenter.current().requestAuthorization(
                  options: authOptions,
                  completionHandler: {_, _ in })
              // For iOS 10 data message (sent via FCM
          } else {
              let settings: UIUserNotificationSettings =
                  UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
              UIApplication.shared.registerUserNotificationSettings(settings)
          }
          
          Messaging.messaging().delegate = self
            notifyDta = false
          
          UIApplication.shared.registerForRemoteNotifications()
        NotificationCenter.default.post(name: Notification.Name("ReceiveData"), object: nil)
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping(UNNotificationPresentationOptions) -> Void) {
           print(notification.description)
        
        if isChat{
            completionHandler([])
        }else{
           completionHandler([.alert,.badge,.sound])
        }
           
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nil)
           
       }
       
       func userNotificationCenter(_ center: UNUserNotificationCenter,didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
           let userInfo = response.notification.request.content.userInfo
           print(userInfo)
           print("Userinfo1 \(response.notification.request.content.userInfo)")
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nil)
           getDataFromNotification(userInfo: response.notification.request.content.userInfo)
           
           completionHandler()
       }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
//      if let messageID = userInfo[gcmMessageIDKey] {
//        print("Message ID: \(messageID)")
//      }

      // Print full message.
      print(userInfo)

      completionHandler(UIBackgroundFetchResult.newData)
    }

       func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
              Messaging.messaging().apnsToken = deviceToken
               Messaging.messaging().setAPNSToken(deviceToken, type: .sandbox)
               Messaging.messaging().setAPNSToken(deviceToken, type: .prod)
               let deviceTokenString = deviceToken.reduce("") { $0 + String(format: "%02X", $1) }
                   print("APNs device token: \(deviceTokenString)")
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nil)
        
       
       }
       
       func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
           print(fcmToken)
        NotificationCenter.default.post(name: Notification.Name("ReceiveData"), object: nil)
           UserDefaults.standard.set(fcmToken, forKey: "deviceToken")
       }
       
       @objc func tokenRefreshNotification(notification: NSNotification) {
           
           InstanceID.instanceID().instanceID { (result, error) in
               if let error = error {
                   print("Error fetching remote instange ID: \(error)")
               } else if let result = result {
                   print("Remote instance ID token: \(result.token)")
                   UserDefaults.standard.set(result.token, forKey: "FCMToken")
               }
           }
       }
       
       
       
       func getDataFromNotification(userInfo: [AnyHashable : Any])
       {
           let uInfo : NSDictionary! = userInfo as NSDictionary
           print(uInfo)
        notificationDictionary = uInfo
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushNotificationRecieved"), object: nil, userInfo: userInfo)
        }
        
           if let aps = uInfo["aps"] as? NSDictionary {
               print(aps)
               if let alert = aps["alert"] as? NSDictionary {
                   print(alert)
                   let message = alert["body"] as? String
                   print(message as Any)
                
                
                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nil)
                

               }
           }
           
           //            }
           //
           //        }
       }
       
    
    
    func callgetBreedNameList() {
        
        breedVM = Breed_VM()
        
        breedVM?.responseRecievedBreed = { breed in
            breedArray = breed
            //self.tableViewObj.reloadData()
            print("self.breedArray== \(breedArray.count)")
            
        }
        breedVM?.showAlertClosure = {
            
        }
        
        breedVM?.callGetBreedListService = true
        
        
    }
    
    
    
    
    func logoutFromApp(){
        UserDefaults.setObject(false, forKey: UserDefault.isLoggedIn)
        UserDefaults.standard.removeObject(forKey: "appleId")
        //UserDefaults.setObject(false, forKey: UserDefault.loginWithFb)
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let vc_TABBAR = storyboardMain.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let navigationController = UINavigationController(rootViewController: vc_TABBAR)
        navigationController.isNavigationBarHidden = true
        kAppDelegate.window?.rootViewController = navigationController
        tabBar = nil
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    
    
    func registerForPushNotifications(application: UIApplication) {
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                
            }
        } else {
            // Fallback on earlier versions
        }
        
        application.registerForRemoteNotifications()
    }
    
    
   
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register:", error.localizedDescription)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {

       if TWTRTwitter.sharedInstance().application(app, open: url, options: options) {
           return true
       }
       // Your other open URL handlers follow […]
       return true
    }

    
}

