
import Foundation
import UIKit
import Alamofire


let defaultHeaders  = ["X-custom-header":"7890abcdefghijkl"]

protocol BaseRequest {
    var urlString : String {get}
    var method:HTTPMethod{get}
    var parameters: Parameters! {get}
    var headers : HTTPHeaders! {get}
}

struct LoginRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters) {
        self.urlString =  WebServiceConstants.loginAdminAPI
        self.method = .post
        self.parameters = parameters
    }
}

struct SignUpRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters) {
        self.urlString =  WebServiceConstants.signUpAdminAPI
        self.method = .post
        self.parameters = parameters
    }
}

struct PetListRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct PostCommentAddRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters) {
        self.urlString =  WebServiceConstants.addCommentAPI
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct PostDeleteCommentRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters) {
        self.urlString =  WebServiceConstants.deletePostCommentAPI
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}



struct ForumDeleteCommentRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
     init(parameters:Parameters,urlString:String) {
           self.urlString =  urlString
           self.method = .get
           self.parameters = parameters
           self.headers = defaultHeaders
       }
}


struct CommentListFetchRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .get
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct CommentAddForumRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct PetFetchListFetchRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .get
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}

struct HomeListRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters) {
        self.urlString =  WebServiceConstants.getHomeListAPI
        self.method = .get
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct ForumListRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .get
        self.parameters = parameters
        self.headers = defaultHeaders
        }
}


struct NotificationListRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}
struct AcceptFollowRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters) {
        self.urlString =  WebServiceConstants.acceptFollowAPI
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct UserSearchListRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters) {
        self.urlString =  WebServiceConstants.getUserListSearchAPI
        self.method = .get
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct HashTagListRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters) {
        self.urlString =  WebServiceConstants.getHashTagListSearchAPI
        self.method = .get
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct DeletePostRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters) {
        self.urlString =  WebServiceConstants.deletePostAPI
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct RequestUserFollowRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters) {
        self.urlString =  WebServiceConstants.requstUserFollowAPI
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}

struct RequestUserBlockRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}

struct FilterRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init() {
        self.urlString =  WebServiceConstants.filterForumAPI
        self.method = .get
        self.headers = defaultHeaders
    }
}



struct ReportPostRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct DeleteForumRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .get
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct LikeUnlikePostRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct FavouritePostRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct FollowUnFollowPostRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}

struct AcceptEulaRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}

struct LikeListRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .get
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}

struct FollowUsersListRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .get
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}

struct NotificationSettingRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(urlString:String) {
        self.urlString =  urlString
        self.method = .get
        self.headers = defaultHeaders
    }
}


struct UserProfileRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.parameters = parameters
        self.urlString =  urlString
        self.method = .get
        self.headers = defaultHeaders
    }
}

struct NotificationUpdateRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct AcountPrivacyRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(urlString:String) {
        self.urlString =  urlString
        self.method = .get
        self.headers = defaultHeaders
    }
}

struct AcountPrivacyUpdateRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters) {
        self.urlString =  WebServiceConstants.updatePrivacySettingAPI
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}


struct PasswordUpdateRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters) {
        self.urlString =  WebServiceConstants.changePasswordSettingAPI
        self.method = .post
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}
struct PostDetailFetchRequest : BaseRequest {
    var urlString: String
    var method:HTTPMethod
    var parameters: Parameters!
    var headers: HTTPHeaders!
    init(parameters:Parameters,urlString:String) {
        self.urlString =  urlString
        self.method = .get
        self.parameters = parameters
        self.headers = defaultHeaders
    }
}
