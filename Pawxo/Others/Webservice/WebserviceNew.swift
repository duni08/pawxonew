

import UIKit
import Foundation
import Alamofire



public typealias ResponseCompletion<T> = (APIResponse<T>) -> Void where T: Codable

public enum APIResponse<T> where T: Codable {
    case success(T)
    case baseError(errorDetails)
}

class WebserviceNew: NSObject {
    
    static let shared = WebserviceNew()
    
    private override init() {
        //super.init()
    }
    
   fileprivate func handleResponseProvided<T>(responseData:DataResponse<Any>,completion : @escaping(ResponseCompletion<T>)){
        switch responseData.response?.statusCode {
        case 401:
            print("Error in Api Call \(responseData.result.error.debugDescription)")
            do {
                let decoded = try JSONSerialization.jsonObject(with: responseData.data!, options: [])
                // here "decoded" is of type `Any`, decoded from JSON data
                
                // you can now cast it with the right type
                if let dictFromJSON = decoded as? [String:Any] {
                    print(dictFromJSON)
                }
                let errorBasse = try JSONDecoder().decode(errorDetails.self, from: responseData.data!)
                print(errorBasse)
                let responseCompletion = APIResponse<T>.baseError(errorBasse)
                completion(responseCompletion)
            }catch let error as NSError {
                 print(error)
                let errorBase = errorDetails(errorBase: BaseError(errorCode: error.code, message: error.localizedDescription))
                let responseCompletion = APIResponse<T>.baseError(errorBase)
                completion(responseCompletion)
            }
        case 400:
        print("Error in Api Call \(responseData.result.error.debugDescription)")
        do {
            let decoded = try JSONSerialization.jsonObject(with: responseData.data!, options: [])
            // here "decoded" is of type `Any`, decoded from JSON data
            
            // you can now cast it with the right type
            
            
            if let dictFromJSON = decoded as? [String:Any] {
                print(dictFromJSON)
                
                if let dict = dictFromJSON["data"] as? NSDictionary {
                    if let isBlocked = dict["is_blocked"] as? Bool{
                        if isBlocked ==  true{
                            let errorBase = errorDetails(errorBase: BaseError(errorCode: 999, message:(dict["message"] as? String ?? "")))
                            let responseCompletion = APIResponse<T>.baseError(errorBase)
                            completion(responseCompletion)
                        }
                    }else{
                        let message = dictFromJSON["message"] as? String
                        let errorBase = errorDetails(errorBase: BaseError(errorCode: 888, message: message))
                        let responseCompletion = APIResponse<T>.baseError(errorBase)
                        completion(responseCompletion)
                    }
                }
            }
            
            
        }catch let error as NSError {
             print(error)
            let errorBase = errorDetails(errorBase: BaseError(errorCode: error.code, message: error.localizedDescription))
            let responseCompletion = APIResponse<T>.baseError(errorBase)
            completion(responseCompletion)
        }
        case 200:
            print("Sucess in Api Call")
            do {
                // if successfully decoded, completion. otherwise handle the error.
                let decoded = try JSONSerialization.jsonObject(with: responseData.data!, options: [])
                // here "decoded" is of type `Any`, decoded from JSON data
                
                // you can now cast it with the right type
                if let dictFromJSON = decoded as? [String:Any] {
                    print(dictFromJSON)
                }
                // let json2 = try JSONDecoder().decode(BaseResponse.self, from: data)
                let json = try JSONDecoder().decode(T.self, from: responseData.data!)
                print(json)
                //let dataError = TrackerError.serverError("response data is nil")
                let responseCompletion = APIResponse<T>.success(json)
                completion(responseCompletion)
                break
            } catch let error as NSError {
                print(error)
                let errorBase = errorDetails(errorBase: BaseError(errorCode: error.code, message: error.localizedDescription))
                let responseCompletion = APIResponse<T>.baseError(errorBase)
                completion(responseCompletion)
            }
         /*   case 400:
            print("Sucess in Api Call")
            do {
                // if successfully decoded, completion. otherwise handle the error.
                let decoded = try JSONSerialization.jsonObject(with: responseData.data!, options: [])
                // here "decoded" is of type `Any`, decoded from JSON data
                
                // you can now cast it with the right type
                if let dictFromJSON = decoded as? [String:Any] {
                    print(dictFromJSON)
                }
                // let json2 = try JSONDecoder().decode(BaseResponse.self, from: data)
                let json = try JSONDecoder().decode(T.self, from: responseData.data!)
                print(json)
                //let dataError = TrackerError.serverError("response data is nil")
                let responseCompletion = APIResponse<T>.success(json)
                completion(responseCompletion)
                break
            } catch let error as NSError {
                print(error)
                let errorBase = errorDetails(errorBase: BaseError(errorCode: error.code, message: error.localizedDescription))
                let responseCompletion = APIResponse<T>.baseError(errorBase)
                completion(responseCompletion)
            }
            */
        case 204:
            let resp :T = SuccessResponse(success: true, error: nil) as! T
            let responseCompletion = APIResponse<T>.success(resp)
            completion(responseCompletion)
        default:
            do {
                let decoded = try JSONSerialization.jsonObject(with: responseData.data!, options: [])
            // here "decoded" is of type `Any`, decoded from JSON data
            
            // you can now cast it with the right type
            if let dictFromJSON = decoded as? [String:Any] {
                print(dictFromJSON)
                
                if let dict = dictFromJSON["data"] as? NSDictionary {
                    if let isBlocked = dict["is_blocked"] as? Bool{
                        if isBlocked ==  true{
                            let errorBase = errorDetails(errorBase: BaseError(errorCode: 999, message:(dict["message"] as? String ?? "")))
                            let responseCompletion = APIResponse<T>.baseError(errorBase)
                            completion(responseCompletion)
                        }
                    }
                }
            }
                
            }catch let error as NSError{
                 print(error)
            }
            print("Nothing to print")
        }
    }
    
    
    func callWebAPINew<T:BaseResponse,S:BaseRequest>(requestObj:S,complete: @escaping(ResponseCompletion<T>))
    {
        
        
        let encoding = requestObj.method == .get ? URLEncoding.queryString : URLEncoding.default
        
        Alamofire.request(requestObj.urlString, method: requestObj.method, parameters: requestObj.parameters, encoding: encoding, headers: requestObj.headers).responseJSON
            { (response ) in
                switch response.result {
                case .success:
                    self.handleResponseProvided(responseData: response, completion: complete)
                case .failure(let error as NSError):
                    print(error)
                    let errorBase = errorDetails(errorBase: BaseError(errorCode: error.code, message: error.localizedDescription))
                    let responseCompletion = APIResponse<T>.baseError(errorBase)
                    complete(responseCompletion)
                }
        }
        
    }
    
    
    
    func callWebAPI<T,S:BaseRequest>(details:S,urlString:String,parameters:Parameters!,type:HTTPMethod,headers:[String:String]!,complete: @escaping(ResponseCompletion<T>))
    {
        
        print(details.urlString)
        Alamofire.request(urlString, method: type, parameters: parameters, encoding: JSONEncoding.default).responseJSON
            { (response ) in
                switch response.result {
                case .success:
                self.handleResponseProvided(responseData: response, completion: complete)
                case .failure(let error as NSError):
                    print(error)
                    let errorBase = errorDetails(errorBase: BaseError(errorCode: error.code, message: error.localizedDescription))
                    let responseCompletion = APIResponse<T>.baseError(errorBase)
                    complete(responseCompletion)
                }
            }
        
    }
    
    
    
   
  /*  func callWebAPINew(urlString:String,parameters:Parameters!,type:HTTPMethod,headers:[String:String]!,complete:@escaping(NSMutableDictionary)->Void)  {
        
        
            let urlStringArray =  urlString.split(separator: "?")
        print( "Inside API \(urlStringArray.first!))")
        
        let returnedDict = NSMutableDictionary()
        returnedDict["Error"] = false
        returnedDict["ErrorMsg"] = ""
        // Both calls are equivalent
        
        
        Alamofire.request(urlString , method: type, parameters: parameters, encoding: JSONEncoding.default).validate(statusCode: 200..<300).responseJSON
            { (response ) in
                switch response.result {
                case .success:
                    
                    do {
                                            // if successfully decoded, completion. otherwise handle the error.
                                            let decoded = try JSONSerialization.jsonObject(with: response.data!, options: [])
                                            // here "decoded" is of type `Any`, decoded from JSON data
                        
                                            // you can now cast it with the right type
                                            if let dictFromJSON = decoded as? [String:Any] {
                                                print(dictFromJSON)
                                            }
                                           // let json2 = try JSONDecoder().decode(BaseResponse.self, from: data)
                                            let json = try JSONDecoder().decode(.self, from: response.data!)
                                            print(json)
                                        } catch let error as NSError {

                                        }

                    print("getting data Successful")
                    let json = response.result.value as! NSDictionary

                    print(json)

                    if json["status"] as? String == "success"{

                        returnedDict["data"] = json

                    }else{
                        //Work to do
                        returnedDict["Error"] = true
                        returnedDict["ErrorMsg"] = json["reason"] as? String
                    }

                case .failure(let error):
                    print(error)
                    returnedDict["Error"] = true
                    returnedDict["ErrorMsg"] = error.localizedDescription
                }

        }

    } */
        
//      //  return Alamofire.request(urlString,
//                                 method: type,
//                                 parameters: parameters,
//                                 encoding: URLEncoding.default,
//                                 headers: headers).response { (dataResponse) in
//
//                                   // handleDataResponse(dataResponse, completion: complete)
//        }

//        return Alamofire.request(urlString,
//                                 method: type,
//                                 parameters: parameters,
//                                 encoding: URLEncoding.default,
//                                 headers: headers).response { (dataResponse) in
//
//                                   // self.handleDataResponse(dataResponse, completion: complete)
//
//
//        }
    }
       
                
//                switch response.result {
//                case .success:
//                    let json = response.result.value as! NSDictionary
//                    print(json)
//                    if json["sucess"] as? Bool == true{
//                        returnedDict["data"] = json
//                    }else{
//                        //Work to do
//                        returnedDict["Error"] = true
//                        returnedDict["ErrorMsg"] = json["reason"] as? String
//                    }
//                case .failure(let error):
//                    print(error)
//                    returnedDict["Error"] = true
//                    returnedDict["ErrorMsg"] = error.localizedDescription
//                }
               // complete(returnedDict)
       
    
    
//
//         func handleDataResponse<T>(_ dataResponse: DefaultDataResponse, completion: @escaping TrackerCompletion<T>) {
//            // HTTP Request failed. Handle the error.
//            if let error = dataResponse.error {
//                let responseError = TrackerError.responseError(dataResponse)
//                let res = TrackerResponse<T>.error(responseError)
//                completion(res)
//                let urlString = dataResponse.request?.url?.absoluteString ?? ""
//                //log("[GenericNetworking] Response Error:\(error.localizedDescription) at URLString:\(urlString)")
//                return;
//            }
//
//            // if we got data, try decode
//            if let data = dataResponse.data {
//                do {
//                    // if successfully decoded, completion. otherwise handle the error.
//                    let decoded = try JSONSerialization.jsonObject(with: data, options: [])
//                    // here "decoded" is of type `Any`, decoded from JSON data
//
//                    // you can now cast it with the right type
//                    if let dictFromJSON = decoded as? [String:Any] {
//                        print(dictFromJSON)
//                    }
//                   // let json2 = try JSONDecoder().decode(BaseResponse.self, from: data)
//                    let json = try JSONDecoder().decode(T.self, from: data)
//                    let res = TrackerResponse<T>.success(json)
//                    completion(res)
//                } catch let error as NSError {
//                    let responseText: String
//                    if let responseString = String(data: data, encoding: String.Encoding.utf8) {
//                        responseText = responseString
//                    } else {
//                        responseText = "can not convert response data to String"
//                    }
//                    let decodeError = TrackerError.decodeError(error, responseText)
//                    let res = TrackerResponse<T>.error(decodeError)
//                    completion(res)
//                   // let info = "[GenericNetworking] JSONDecoder error, origin response text is:\(responseText)"
//                   // log(info)
//                }
//                return;
//            }
//            // handle response data is empty
//            let dataError = TrackerError.serverError("response data is nil")
//            let res = TrackerResponse<T>.error(dataError)
//            completion(res)
//        }
//}
//



