
import Foundation


class WebServiceManager {
    
    public static func loginCallWithRequest(request:LoginRequest,completion: @escaping ResponseCompletion<LoginResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func signUpCallWithRequest(request:SignUpRequest,completion: @escaping ResponseCompletion<SignUpResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func getPetListWithRequest(request:PetListRequest,completion: @escaping ResponseCompletion<PetListResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func getUserSearchListWithRequest(request:UserSearchListRequest,completion: @escaping ResponseCompletion<UserSearchListResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func getHomeListWithRequest(request:HomeListRequest,completion: @escaping ResponseCompletion<HomeListResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func getForumListWithRequest(request:ForumListRequest,completion: @escaping ResponseCompletion<ForumListResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func getForumCategoryWithRequest(request:FilterRequest,completion: @escaping ResponseCompletion<FilterResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func getNotificationListWithRequest(request:NotificationListRequest,completion: @escaping ResponseCompletion<NotificationListResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    public static func acceptFollowWithRequest(request:AcceptFollowRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
           self.genericAPICall(request: request, completion: completion)
       }
    public static func deletePostWithRequest(request:DeletePostRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
           self.genericAPICall(request: request, completion: completion)
       }
    
    public static func reportPostWithRequest(request:ReportPostRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func deleteForumWithRequest(request:DeleteForumRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
           self.genericAPICall(request: request, completion: completion)
       }
    
    public static func getHashTagListWithRequest(request:HashTagListRequest,completion: @escaping ResponseCompletion<HashTagListResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func likeUnlikePostRequest(request:LikeUnlikePostRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func addCommentPostRequest(request:PostCommentAddRequest,completion: @escaping ResponseCompletion<AddCommentSuccessResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    
    public static func deletePostCommentPostRequest(request:PostDeleteCommentRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func deleteForumCommentPostRequest(request:ForumDeleteCommentRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func addForumCommentPostRequest(request:CommentAddForumRequest,completion: @escaping ResponseCompletion<AddForumCommentSuccessResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    
    public static func getCommentListPostRequest(request:CommentListFetchRequest,completion: @escaping ResponseCompletion<CommentListResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func getForumCommentListPostRequest(request:CommentListFetchRequest,completion: @escaping ResponseCompletion<ForumCommentListSuccessResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    
    public static func getPostDetailRequest(request:PostDetailFetchRequest,completion: @escaping ResponseCompletion<PostDetailResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func getPetsListPostRequest(request:PetFetchListFetchRequest,completion: @escaping ResponseCompletion<CommentListResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func followUnfollowRequest(request:FollowUnFollowPostRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func acceptEulaWithRequest(request:AcceptEulaRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func favouriteUnFavouriteRequest(request:FavouritePostRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func requestfollowUserRequest(request:RequestUserFollowRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
           self.genericAPICall(request: request, completion: completion)
       }
    
    
    public static func requestBlockUserRequest(request:RequestUserBlockRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
              self.genericAPICall(request: request, completion: completion)
          }
    
    public static func notificationSettingWithRequest(request:NotificationSettingRequest,completion: @escaping ResponseCompletion<NotificationResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    
    public static func userProfileWithRequest(request:UserProfileRequest,completion: @escaping ResponseCompletion<UserProfileResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    
    public static func notificationUpdateSettingWithRequest(request:NotificationUpdateRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    
    public static func accountPrivacySettingWithRequest(request:AcountPrivacyRequest,completion: @escaping ResponseCompletion<AccountPrivacyResponse>){
           self.genericAPICall(request: request, completion: completion)
       }
    
    public static func accountPrivacyUpdateSettingWithRequest(request:AcountPrivacyUpdateRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    public static func passwordUpdateSettingWithRequest(request:PasswordUpdateRequest,completion: @escaping ResponseCompletion<SuccessResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    
    public static func likeListRequest(request:LikeListRequest,completion: @escaping ResponseCompletion<LikeListResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    
    public static func followListRequest(request:FollowUsersListRequest,completion: @escaping ResponseCompletion<UsersFollowersListResponse>){
        self.genericAPICall(request: request, completion: completion)
    }
    
    // Generic API Call
    fileprivate static func genericAPICall<S:BaseRequest,T:BaseResponse>(request:S,completion:@escaping ResponseCompletion<T>){
        WebserviceNew.shared.callWebAPINew(requestObj: request, complete: completion)
    }
}








