

import Foundation
import UIKit
import Alamofire


protocol BaseResponse: Codable {
    
    var success: Bool? { get }
    var error : String? { get }
    
}

struct BaseError :Codable{
    let errorCode : Int?
    let message : String?
}

public struct errorDetails: Codable {
    
    let errorBase: BaseError?
    
    enum CodingKeys: String, CodingKey {
        case errorBase = "error"
    }
    
}


struct LoginResponse :  BaseResponse,Codable {
    var success: Bool?
    var error: String?
    var data : User?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case error = "error"
        case success = "success"
    }
}

struct SignUpResponse :  BaseResponse,Codable {
    var success: Bool?
    var error: String?
    var data : User?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case error = "error"
        case success = "success"
    }
}


struct PetListResponse :  BaseResponse,Codable {
    var success: Bool?
    var message: String?
    var error: String?
    var data : PetRecievedResponse?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case error = "error"
        case success = "success"
    }
}

struct HomeListResponse :  BaseResponse,Codable {
    var success: Bool?
    var message: String?
    var error: String?
    var data : HomeRecievedResponse?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case error = "error"
        case success = "success"
    }
}


struct ForumListResponse :  BaseResponse,Codable {
    var success: Bool?
    var message: String?
    var error: String?
    var data : [ForumList]?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case error = "error"
        case success = "success"
    }
}



struct NotificationListResponse: BaseResponse,Codable {
    var success: Bool?
    var data: NotificationListReceivedResponse?
    var message: String?
     var error: String?

    enum CodingKeys: String, CodingKey {
           case data = "data"
           case message = "message"
           case error = "error"
           case success = "success"
       }
}

struct AcceptyFollowRequestResponse: BaseResponse,Codable {
    var success: Bool?
    var data: NotificationListReceivedResponse?
    var message: String?
     var error: String?

    enum CodingKeys: String, CodingKey {
           case data = "data"
           case message = "message"
           case error = "error"
           case success = "success"
       }
}
    
    
    
class UserSearchRecievedResponse: NSObject, Codable {
    var userlist: [Userlist]?
    var total, totalPage: Int?
    var currentage: String?

    enum CodingKeys: String, CodingKey {
        case userlist, total
        case totalPage = "total_page"
        case currentage
    }

    init(userlist: [Userlist], total: Int, totalPage: Int, currentage: String) {
        self.userlist = userlist
        self.total = total
        self.totalPage = totalPage
        self.currentage = currentage
    }
}


struct UserSearchListResponse :  BaseResponse,Codable {
    var success: Bool?
    var message: String?
    var error: String?
    // server Change var data : UserSearchRecievedResponse?
    var data :[Userlist]?
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case error = "error"
        case success = "success"
    }
}


struct LikeListResponse :  BaseResponse,Codable {
    var success: Bool?
    var message: String?
    var error: String?
    var data : [Userlist]?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case error = "error"
        case success = "success"
    }
}



struct CommentListResponse :  BaseResponse,Codable {
    var success: Bool?
    var message: String?
    var error: String?
    var data : CommentResponseList?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case error = "error"
        case success = "success"
    }
}
struct PostDetailResponse :  BaseResponse,Codable {
    var success: Bool?
    var message: String?
    var error: String?
    var data : PostList?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case error = "error"
        case success = "success"
    }
}

//struct PetListResponse :  BaseResponse,Codable {
//    var success: Bool?
//    var message: String?
//    var error: String?
//    var data : [PetList]?
//
//    enum CodingKeys: String, CodingKey {
//        case data = "data"
//        case message = "message"
//        case error = "error"
//        case success = "success"
//    }
//}

struct UsersFollowersListResponse :  BaseResponse,Codable {
    var success: Bool?
    var message: String?
    var error: String?
    var data : FollowListResponse?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case error = "error"
        case success = "success"
    }
}

struct HashTagListResponse :  BaseResponse,Codable {
    var success: Bool?
    var message: String?
    var error: String?
    var data : [HashTag]?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case error = "error"
        case success = "success"
    }
}

class NotificationResponse: BaseResponse,Codable {
    var error: String?
    var success: Bool?
    var data: NotificationSetting?
    var message: String?
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case error = "error"
        case success = "success"
    }
}


class FilterResponse: BaseResponse,Codable {
    var error: String?
    var success: Bool?
    var data: FilterData?
    var message: String?
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case error = "error"
        case success = "success"
    }
}


class UserProfileResponse: BaseResponse,Codable {
    var error: String?
    var success: Bool?
    var data: Userlist?
    var message: String?
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case error = "error"
        case success = "success"
    }
}

class AccountPrivacyResponse: BaseResponse,Codable {
    var error: String?
    var success: Bool?
    var data: AccountPrivacy?
    var message: String?
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case message = "message"
        case error = "error"
        case success = "success"
    }
}


struct SuccessResponse :  BaseResponse,Codable {
    var success: Bool?
    var error: String?
    var message : String?
    enum CodingKeys: String, CodingKey {
        case error = "error"
        case success = "success"
        case message = "message"
    }
}


struct AddCommentSuccessResponse :  BaseResponse,Codable {
    var success: Bool?
    var error: String?
    var message : String?
    var data : CommentList?
    enum CodingKeys: String, CodingKey {
        case error = "error"
        case success = "success"
        case message = "message"
        case data = "data"
    }
}


struct AddForumCommentSuccessResponse :  BaseResponse,Codable {
    var success: Bool?
    var error: String?
    var message : String?
    var data : CommentAddResponse?
    enum CodingKeys: String, CodingKey {
        case error = "error"
        case success = "success"
        case message = "message"
        case data = "data"
    }
}



struct ForumCommentListSuccessResponse :  BaseResponse,Codable {
    var success: Bool?
    var error: String?
    var message : String?
    var data : [ForumCommentList]?
    enum CodingKeys: String, CodingKey {
        case error = "error"
        case success = "success"
        case message = "message"
        case data = "data"
    }
}
