//
//  LocationManager.swift
//  MyTracker
//
//  Created by MAC on 24/07/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    static let sharedManager = LocationManager()
    fileprivate var locationManager: CLLocationManager!
    var latitude: CLLocationDegrees = 0.0
    var longitude: CLLocationDegrees = 0.0
    var bgtimer = Timer()
    
    func initializeLocationManager() {
        locationManager = CLLocationManager()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.requestWhenInUseAuthorization()
        //locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
        
    }
    
    func startUpdating(){
        locationManager.startUpdatingLocation()
    }
    func stopUpdating(){
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        latitude = locValue.latitude
        longitude = locValue.longitude
        if latitude != 0.0 && longitude != 0 {
            print("lat = \(latitude) long = \(longitude) time = \(Date()) ")
            notifyLocationAvailability()
        }
    }
    
    func setUpGeofencing(fenceRegion:CLCircularRegion){
        self.locationManager.startMonitoring(for: fenceRegion)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
       print("Entered Region")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("Exit Region")
    }
    
    
    private func notifyLocationAvailability() {
        NotificationCenter.default.post(name: Notification.Name("LocationAvailable"), object: nil)
    }
    
    
    
    @objc func taskUpload(timer:Timer){
        print("Fired from Background ************************************")
    }
    
    
    @objc func bgtimerUpdate(timer:Timer!){
        
        print("Fired from Background ************************************")
        
       // updateLocation()
        
       // print("Position Report: \(pos)")
    }
    
}
