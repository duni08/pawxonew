//
//  FontLabels.swift
//  PetConnect
//
//  Created by 42Works-Worksys2 on 21/09/17.
//  Copyright © 2017 42works. All rights reserved.
//


import UIKit
import Foundation
//MARK:- Label classes

class SemiBoldLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "JosefinSans-SemiBold", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = UIColor.black
    }
}


class InputPlaceHolderLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "JosefinSans-SemiBold", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = AppColor.colorGreyTime
    }
}

class HomeScreenTitleLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "JosefinSans-SemiBold", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = AppColor.colorGreyHeading
    }
}


class HomeScreenTimeLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "JosefinSans-SemiBold", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = AppColor.colorGreyTime
    }
}


class HomeScreenLikeLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "JosefinSans-SemiBold", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = UIColor.black
    }
}


class HomeScreenDescriptionLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "JosefinSans-SemiBold", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = AppColor.colorGreySubHeading
    }
}

class LightLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "JosefinSans-Light", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = AppColor.colorGreyTime
    }
}


class RegularLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "JosefinSans-Regular", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = AppColor.colorGreyTime
    }
}

class RegularLabelChat: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "JosefinSans-Regular", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = AppColor.colorWhite
    }
}

class LightLabelWithColor: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "Roboto-Light", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = AppColor.lightTextColor
    }
}

class MediumLabelWithColor: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "Roboto-Medium", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = AppColor.darktextColor
    }
}

class BoldLabelWithColor: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "Roboto-Bold", size: self.font.pointSize) ?? UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = AppColor.darktextColor
    }
}

//MARK:- Button classes

class RegularButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.titleLabel?.font = UIFont.init(name: "JosefinSans-Regular", size: self.titleLabel?.font.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.titleLabel?.font.pointSize ?? 10)
        self.contentEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0);
    }
}


class SemiBoldButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.titleLabel?.font = UIFont.init(name: "JosefinSans-SemiBold", size: self.titleLabel?.font.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.titleLabel?.font.pointSize ?? 10)
        self.contentEdgeInsets = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0);
    }
}

class BoldButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.titleLabel?.font = UIFont.init(name: "JosefinSans-Bold", size: self.titleLabel?.font.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.titleLabel?.font.pointSize ?? 10)
    }
}

class MediumButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.titleLabel?.font = UIFont.init(name: "Roboto-Medium", size: self.titleLabel?.font.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.titleLabel?.font.pointSize ?? 10)
    }
}

class LightButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.titleLabel?.font = UIFont.init(name: "Roboto-Light", size: self.titleLabel?.font.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.titleLabel?.font.pointSize ?? 10)
    }
}

class RobotoMediumButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.backgroundColor = AppColor.gradientPinkColor
        self.titleLabel?.font = UIFont.init(name: "Roboto-Medium", size: self.titleLabel?.font.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.titleLabel?.font.pointSize ?? 10)
    }
}

//MARK:- Textfield classes
class RegularTextField: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "JosefinSans-Regular", size: 18) ?? UIFont.systemFont(ofSize: self.font?.pointSize ?? 14)
        self.textColor = AppColor.colorGreySubHeading
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: AppColor.colorGreyTime])

    }
}


class RegularTextView: UITextView {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "JosefinSans-Regular", size: self.font?.pointSize ?? 14) ?? UIFont.systemFont(ofSize: self.font?.pointSize ?? 14)
        self.textColor = AppColor.colorGreySubHeading
        //self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: AppColor.colorGrey])
    }
}


class RegularWithPaddingTextField: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "Roboto-Regular", size: self.font?.pointSize ?? 10) ?? UIFont.systemFont(ofSize: self.font?.pointSize ?? 10)
        self.textColor = UIColor.black
    }
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

class LoginTextField: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.font = UIFont.init(name: "Roboto-Light", size: 28) ?? UIFont.systemFont(ofSize: 25)
        }else {
            self.font = UIFont.init(name: "Roboto-Light", size: 17) ?? UIFont.systemFont(ofSize: 17)
        }
        self.textColor = UIColor.white
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
}



/*
class CustomTextField: SkyFloatingLabelTextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setFont_Color()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setFont_Color()
    }
    
    private func setFont_Color() {
        self.font = UIFont.init(name: "Roboto-Regular", size: 15) ?? UIFont.systemFont(ofSize: 15)
        self.textColor = UIColor.white
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.placeholderColor = UIColor.white
        self.titleColor = UIColor.white
        self.lineColor = UIColor.white
        self.selectedTitleColor = UIColor.white
        self.selectedLineColor = UIColor.white
       
    }
}

class DarkLabelTextField: SkyFloatingLabelTextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setFont_Color()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setFont_Color()
    }
    
    private func setFont_Color() {
        self.font = UIFont.init(name: "Roboto-Regular", size: 15) ?? UIFont.systemFont(ofSize: 15)
        self.textColor = AppColor.darktextColor
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.placeholderColor = AppColor.lightTextColor
        self.titleColor = AppColor.lightTextColor
        self.lineColor = AppColor.dividerColor
        self.selectedTitleColor = AppColor.lightTextColor
        self.selectedLineColor = AppColor.dividerColor
        self.errorColor = UIColor.red
    }
}

class UnselectedTextField: SkyFloatingLabelTextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setFont_Color()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setFont_Color()
    }
    
    private func setFont_Color() {
        self.font = UIFont.init(name: "Roboto-Regular", size: 15) ?? UIFont.systemFont(ofSize: 15)
        self.textColor = AppColor.lightTextColor
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.placeholderColor = AppColor.lightTextColor
        self.titleColor = AppColor.lightTextColor
        self.selectedTitleColor = AppColor.lightTextColor
        self.lineColor = AppColor.dividerColor
        self.selectedLineColor = AppColor.dividerColor
        self.errorColor = UIColor.red
    }
}

*/
