//
//  DividerView.swift
//  GoDolly
//
//  Created by 42Works-Worksys2 on 15/05/18.
//  Copyright © 2018 42works. All rights reserved.
//

import UIKit

class DividerView: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.colorFromHex("#E9E9E9")
        self.alpha = 1.0
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.colorFromHex("#E9E9E9")
        self.alpha = 1.0
    }    
}

class DividerViewNew: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = AppColor.colorGreyTime
        self.alpha = 0.5
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = AppColor.colorGreyTime
        self.alpha = 0.5
    }
}



class HeaderView: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = AppColor.primaryColor
        self.alpha = 1.0
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = AppColor.primaryColor
        self.alpha = 1.0
    }
}
