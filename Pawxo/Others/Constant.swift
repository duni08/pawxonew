//
//  Constant.swift
//  AutoForce
//
//  Created by Apple2 on 20/08/19.
//  Copyright © 2019 Apple2. All rights reserved.
//

import UIKit

//AppDelegate
let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
let kDeviceType = "Ios"
let kDeviceId = UIDevice.current.identifierForVendor?.uuidString

//MARK: Limits
let kMaxPasswordLimit = 15
let kMinPasswordLimit = 6

let kToastDuration = 2.0

var breedArray = [Breed]()
var petBreedArray = [PetBreed]()


struct StoryboardName{
    static let login = "Login"
    static let main = "Main"
    static let Settings = "Settings"
}

struct APIKey {
    static let googleKey = ""
}



struct WebServiceConstants {
    
    static let baseURL = "https://cuddl.app/admin/api/"
    static let loginAdminAPI = "\(baseURL)auth/login"
    static let signUpAdminAPI = "\(baseURL)auth/signup"
    static let getPetListAPI  = "\(baseURL)pet/{user_id}/list"
    static let getHomeListAPI  = "\(baseURL)post/list"
    static let getUserListSearchAPI  = "\(baseURL)user/list"
    static let getHashTagListSearchAPI  = "\(baseURL)tags"
    static let likePostAPI = "\(baseURL)post/{id}/like"
    static let followAPI = "\(baseURL)user/{id}/follow"
    static let acceptEulaAPI = "\(baseURL)user/status/{id}"
    static let notificationSettingAPI = "\(baseURL)setting/{user_id}"
    static let getUserPorfileAPI = "\(baseURL)user/detail/{id}"
    static let getAccountPrivacySettingAPI = "\(baseURL)account/private/get/{user_id}"
    static let updatePrivacySettingAPI = "\(baseURL)account/private"
    static let changePasswordSettingAPI = "\(baseURL)user/changePassword"
    static let notificationUpdateSettingAPI = "\(baseURL)setting/update/{user_id}"
    static let unLikePostAPI = "\(baseURL)post/{id}/Un-like"
    static let likeListAPI = "\(baseURL)post/{id}/likeUsers"
    static let reportPostAPI = "\(baseURL)post/{id}/report"
    static let forumListAPI = "\(baseURL)forums/list/{user_id}"
    static let editPetAPI = "\(baseURL)pet/update/{id}"
    static let deletePostAPI = "\(baseURL)post/delete"
    static let requstUserFollowAPI = "\(baseURL)initiate/request"
    static let filterForumAPI = "\(baseURL)forums/filters"
    static let editUserAPI = "\(baseURL)user/update/{id}"
    static let blockUserAPI = "\(baseURL)user/{id}/block"
    static let followersUserAPI = "\(baseURL)user/{id}/followers"
    static let addCommentAPI = "\(baseURL)comment/add"
    static let deletePostCommentAPI = "\(baseURL)comment/delete"
    static let deleteForumCommentAPI = "\(baseURL)threads/delete/{id}"
    static let getCommentAPI = "\(baseURL)comment/list/{post_id}"
    static let createForumAPI = "\(baseURL)forums/create"
    static let editForumAPI = "\(baseURL)forums/edit"
    static let favouriteForumAPI = "\(baseURL)forums/favorite/{id}"
    static let getHashTagListForumSearchAPI  = "\(baseURL)forums/tags"
    static let getNotificationAPI = "\(baseURL)notification/{user_id}/list"
     static let acceptFollowAPI = "\(baseURL)request/action"
    static let favouriteForumListAPI = "\(baseURL)forums/favorites/list/{user_id}"
    static let addForumCommentAPI = "\(baseURL)forums/threads/create/{id}"
    static let getForumCommentAPI = "\(baseURL)forums/threads/get/{id}"
    static let deleteForumAPI = "\(baseURL)forums/delete/{id}"
    static let getPostDetailAPI = "\(baseURL)post/detail/{id}"
    static let uploadimageFirebase = "\(baseURL)images/upload"
    static let notificationchat = "\(baseURL)chat/notification"
    static let forumShowlist = "\(baseURL)forums/show/{id}/{user_id}"
}


struct DefaultImage {
    static let defaultProfileImage = UIImage(named: "TempProfile")
    static let defaultPostImage = UIImage(named: "DefaultPostImage")
    static let defaultPetImage = UIImage(named: "DefaultPetImage")
    static let unlikeImage = UIImage(named: "likeButton")
    static let likeImage = UIImage(named: "likedButton")
    static let unFavouriteImage = UIImage(named: "star")
    static let favouriteImage = UIImage(named: "starFilled")
    static let defaultChatImage = UIImage(named: "Post Background_Default")
     static let DotImage = UIImage(named: "dots")
    
}


struct AppColor {
    
    
    static let baseColor = UIColor(named: "BaseColor")
    static let customGrey = UIColor.init(red: 102/255, green: 102/255, blue: 102/255, alpha: 1.0)
    static let statusBar = UIColor(red: 249/255.0, green: 250/255.0, blue: 250/255.0, alpha: 1.0)
    static let greenColor = UIColor.init(red: 48/255, green: 173/255, blue: 35/255, alpha: 1.0)
    static let customWhite = UIColor.init(red: 239/255, green: 239/255, blue: 244/255, alpha: 1.0)
    static let dividerColor = UIColor(red: 223/255.0, green: 223/255.0, blue: 223/255.0, alpha: 0.85)
    static let searchBtnsBackground = UIColor(red: 249/255.0, green: 250/255.0, blue: 250/255.0, alpha: 1.0)
    static let primaryColor = UIColor(red: 249/255.0, green: 250/255.0, blue: 250/255.0, alpha: 1.0)
    static let lightTextColor = UIColor(red: 91/255.0, green: 91/255.0, blue: 91/255.0, alpha: 1.0)
    static let darktextColor = UIColor(red: 8/255.0, green: 8/255.0, blue: 9/255.0, alpha: 1.0)
    static let placeHolderColor = UIColor(red: 165/255.0, green: 165/255.0, blue: 165/255.0, alpha: 1.0)
    static let background = UIColor(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1.0)
    static let topColor = UIColor(red: 255.0/255.0, green: 177.0/255.0, blue: 96.0/255.0, alpha: 1.0)
    static let checkoutUnselectedColor = UIColor(displayP3Red: 211/255.0, green: 211/255.0, blue: 211/255.0, alpha: 1.0)
    static let greyColor = UIColor(red: 156/255.0, green: 156/255.0, blue: 156/255.0, alpha: 1.0)
    static let lightGreen = UIColor(red: 90/255.0, green: 150/255.0, blue: 81/255.0, alpha: 1.0)
    static let gradientPinkColor = UIColor(red: 249/255.0, green: 80/255.0, blue: 138/255.0, alpha: 1.0)
    static let filterBackgroundColor = UIColor(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1.0)
    
    
    static let colorBackground =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.3)
    
    static let homeIconUnselected =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.3)
    
    // New colors
    static let dividerColorNew =  UIColor.colorFromHex("#4A4A4A")
    static let colorPrimary =  UIColor.colorFromHex("#FF930C")
    static let colorPrimaryDark =  UIColor.colorFromHex("#FF930C")
    static let colorAccent =  UIColor.colorFromHex("#000000")
    static let colorTransparent =  UIColor.colorFromHex("#00000000")
    static let colorBlueTags =  UIColor.colorFromHex("#1C4471")
    static let colorGreen =  UIColor.colorFromHex("#4A8892")
    static let colorGreenLight =  UIColor.colorFromHex("#F4FAFB")
    static let colorWhite =  UIColor.colorFromHex("#FFFFFF")
    static let colorBlack =  UIColor.colorFromHex("#00000")
    static let colorHintGrey =  UIColor.colorFromHex("#DBDBDB")
    static let colorGrey =  UIColor.colorFromHex("#9A9A9A")
    static let colorGreyHeading =  UIColor.colorFromHex("#2C2C2C")
    static let colorGreySubHeading =  UIColor.colorFromHex("#515151")
    static let colorGreyTime =  UIColor.colorFromHex("#9F9F9F")
    static let colorGreyIcon =  UIColor.colorFromHex("#333333")
    static let colorBubbleGrey =  UIColor.colorFromHex("#d6d6d6")
    static let colorBubbleBlue =  UIColor.colorFromHex("#3aaab9")
    static let colorBubbleOrange =  UIColor.colorFromHex("#df805c")
    static let orangeColor =  UIColor.orange
    static let hashTagColor =  UIColor.colorFromHex("#1C4471")
    static let userMentionColor =  UIColor.colorFromHex("#1C4471")
    static let userNameColor =  UIColor.black
    

}

struct ScreenSize {
    static let height = UIScreen.main.bounds.size.height
    static let width = UIScreen.main.bounds.size.width
}

//MARK:- AccountType
struct AccountType{
    static let email = "email"
    static let facebook = "Facebook"
    static let twitter = "Twitter"
    static let apple = "Apple"
}




struct BottomImages{
    static let selectedImages = [UIImage(named: "TABHomeSelected"),UIImage(named: "TABForumSelected"),UIImage(named: "TABChatSelected"),UIImage(named: "TABPawxoSelected"),UIImage(named: "TABProfileSelected")]
    static let unSelectedImages = [UIImage(named: "TABHome"),UIImage(named: "TABForum"),UIImage(named: "TABChat"),UIImage(named: "TABPawxo"),UIImage(named: "TABProfile")]
}
//MARK:- api services urls
struct ApiServiceName{
//    static let serverUrlTemp = "http://staging-api.autoforce.reviews/public/api/"
//    static let serverUrl = "https://staging-api.autoforce.reviews/public/api/"
    static let serverUrlTemp = "http://api.autoforce.io/public/api/"
    static let serverUrl = "https://cuddl.app/admin/api/"
    static let googleAutoComplete = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    static let googleLocationPoint = "https://maps.googleapis.com/maps/api/place/details/json"
    static let loginUrl = "user/login"
    static let registerUrl = "user"
    static let addPetUrl = "pet/add"
    static let getPetUrl = "pet/{user_id}/list"
    static let getBreedList = "pet/fetch/type_breeds"
    static let socialLoginUrl = "user/socialLogin"
    static let forgotUrl = "user/forgetPassword"
    static let getOverlaysUrl = "cam/get_overlays"
    static let postCapture = "cam/capture"
    static let postOverlaysUrl = "cam/post_overlay"
    static let postShare = "cam/share"
    static let getPlaylist = "cam/get_play_list"
    static let createPlaylist = "cam/create_play_list"
    static let addPostUrl = "post/add"
    static let editPostUrl = "post/edit"
    
}

//MARK:- messages
struct AppMessages {
    static let noInternetConnection = "No internet connection. Please try again."
    static let reportAbuse = "Are you sure you want to report this post?"
    static let deletePost = "Are you sure you want to delete this post?"
    static let deleteComment = "Are you sure you want to delete this comment?"
    static let retryAlert = "There some issue Please try again"
    static let unlinkAccount = "Are you sure you want to un-link this account?"
    
    static let deleteForum = "Are you sure you want to delete this forum?"
    static let reportAbuseSecond = "You've already given report for this post. Thanks for helping us keep Cuddl a safe and supportive community."
    static let blockUser = "Are you sure you want to block this user?"
    
    static let UnBlockUser = "Are you sure you want to unblock this user?"
    
    static let unblockUserText = "Please unblock the user in order to access this feature."
    
    static let isYouBlockedText = "You have been blocked by this user."
    static let reportAbuseFirst = "Thanks for reporting this post. Your feedback is important in helping us keep the Cuddl a safe community."
    
    static let myProfileNoPost = "When you share photos and videos, they'll appear on your profile."
    static let otherProfileNoPost = "When the user shares photos and videos, they'll appear here."
    static let profileUpdated = "Profile updated successfully."
    static let enableLocation = "We don't have access to location services on your device. Please go to settings and enable services to use this feature."
    static let foundSnag = "Something went wrong. Please try again."
    static let overlayError = "Error occured. Please try again later."
    
    static var uploadInProgress = "Uploading is in progress..."
    static var videoSegmentsAndOverlayMerge = "Video segments and overlay merging is in progress..."
    static var overlayMerge = "Overlay merging is in progress..."
    static var wait = "Please wait..."
    static var addingIntroToVideo = "Adding intro to video..."
    static var addingOutroToVideo = "Adding outro to video..."
    static var addingMusicToVideo =  "Adding music to video..."
    static var segmentsMerging = "Segments merging is in progress. Please be patient as this may take a few minutes."
    static var videoCropping = "Video cropping as per our resolution is in progress..."
    static var videoMerging = "Video merging is in progress..."
    static var addingBackgroundMusic = "Adding background music to video..."
    
    static var blueToothRequired = "Please connect Bluetooth audio source."
    static var blueToothPopUp = "Please disconnect your Bluetooth mic so you can listen to the audio on this video preview"
}



//MARK:- alert messages
struct ValidationMessage{
    static let yourName = "Please enter your name."
    static let firstName = "Please enter customer's first name."
    static let emptyEmail = "Please enter email address."
    static let invalidEmail = "Please enter valid email address."
    static let emptyUserName = "Please enter username."
    static let emptyCode = "Please enter unlock code."
    static let emptyPwd = "Please enter password."
    static let emptyCurrentPwd = "Please enter old password."
    static let emptyNewPwd = "Please enter new password."
    static let emptyConfirmPwd = "Please retype new password."
    static let confirmPwdAlertMsg = "Password and confirm password does not match."
    static let incorrectPwdLength = "Password should be min. 6 characters long."
    static let invalidPin = "Please enter 5 digit OTP to continue."
    static let emptyPhone = "Please enter phone number."
    static let emptyForumTitle = "Please enter title"
    static let emptyForumCategory = "Please enter category"
    static let emptyForumDescription = "Please enter description"
    static let emptyForumLocation = "Please enter Location"
    static let emptyForumMedia = "Please choose at least one image/video"
}

//alert popup messages
struct AlertMessages {
    //alert popup
    static let okStr = "OK"
    static let cancelStr = "Cancel"
    
    static let cancelRequestTitleStr = "Cancel Request"
   
    //logout confirmation alert title
    static let logoutTitleStr = "Logout User"
    static let logoutMsgStr = "Are you sure you want to logout?"

}

struct PushNotification {
    
    static var titleArr = ["Likes your post","Comment on your post","Mention you in a post","Mention you in a post comment","Sends you follow request","Start following you","Likes your forum","Comment on your forum","Mention you in a forum comment"]
}

struct CalendarP {
    static var monthArr = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    static var monthShortArr = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    static var yearArr = ["2020","2021","2022","2023","2024"]
}

struct Settings {

    static var titleArr = ["Push Notifications","Change Password","Linked Accounts","Privacy Policy","Account Privacy","Logout"]

    static var descriptionArr = ["Choose which mobile push notifications you recevie","Choose a unique password to protect your account","","","",""]

}


struct AnimalCategory {
    
    
    
    static var animalName = "Dog,Cat,Pig,Rodent,Reptile,Fish,Bird,Horse,Invertebrate,All Others"
    static var categoryDict : [String: [String]] = [
        "Cat": ["Abyssinian","Aegean,American Curl","American Bobtail","American Shorthair","American Wirehair","Abyssinian","Abyssinian","Aegean","American Curl","Bombay","Brazilian Shorthair","British Semi-longhair","British Shorthair","British Longhair","Burmese","Burmilla","Cheetoh","Colorpoint Shorthair","Cornish Rex","Cymric or Manx Longhair","Cyprus","Korn Ja","Kurilian Bobtail","Munchkin","Oriental Shorthair","Oriental Longhair","Pixie-bob","Russian White Black and Tabby","Sam Sawet","Savannah","Scottish Fold","Selkirk Rex","Serengeti","Serrade petit","Siamese","Siberian","Singapura","Snowshoe","Sokoke","Somali","Sphynx","Suphalak","Thai","Thai Lilac","Tonkinese","Toyger","Turkish Angora","Turkish Van","Ukrainian Levkoy","York Chocolate"],
        
        "Dog": ["Affenpinscher", "Afghan Hound", "Airedale Terrier", "Akita", "Alaskan Malamute", "American Cocker Spaniel", "American Eskimo Dog (Miniature)", "American Eskimo Dog (Standard)", "American Eskimo Dog (Toy)", "American Foxhound", "American Staffordshire Terrier", "American Water Spaniel", "Anatolian Shepherd", "Australian Cattle Dog", "Australian Shepherd", "Australian Terrier","Basenji", "Basset Hound", "Beagle", "Bearded Collie", "Beauceron", "Bedlington Terrier", "Belgian Malinois", "Belgian Sheepdog", "Belgian Tervuren", "Bernese Mountain Dog", "Bichon Frise", "Black and Tan Coonhound", "Black Russian Terrier", "Bloodhound", "Border Collie", "Border Terrier", "Borzoi", "Boston Terrier", "Bouvier des Flandres", "Boxer", "Briard", "Brittany", "Brussels Griffon", "Bull Terrier", "Bulldog", "Bullmastiff"," Cairn Terrier", "Canaan Dog", "Cardigan Welsh Corgi", "Cavalier King Charles Spaniel", "Chesapeake Bay Retriever", "Chihuahua", "Chinese Crested Dog", "Chinese Shar-Pei", "Chow Chow", "Clumber Spaniel", "Collie", "Curly-Coated Retriever","Dachshund (Miniature)", "Dachshund (Standard)", "Dalmatian", "Dandie Dinmont Terrier", "Doberman Pinscher", "Dutch Shephard", "Drever", "Drentsche Patrijshond", "Dogue de Bordeaux","English Cocker Spaniel", "English Foxhound", "English Setter", "English Springer Spaniel", "English Toy Spaniel","Field Spaniel", "Finnish Spitz", "Flat-Coated Retriever", "French Bulldog","German Pinscher", "German Shepherd Dog", "German Shorthaired Pointer", "German Wirehaired Pointer", "Giant Schnauzer", "Glen of Imaal Terrier", "Golden Retriever", "Gordon Setter", "Great Dane", "Great Pyrenees", "Greater Swiss Mountain Dog", "Greyhound", "Harrier", "Havanese", "Ibizan Hound", "Irish Setter", "Irish Terrier", "Irish Water Spaniel", "Irish Wolfhound", "Italian Greyhound","Japanese Chin","Keeshond", "Kerry Blue Terrier", "Komondor", "Kuvasz","Labrador Retriever", "Lakeland Terrier", "Lhasa Apso", "Lowchen"," Maltese", "Manchester Terrier (Standard)", "Manchester Terrier (Toy)", "Mastiff", "Miniature Bull Terrier", "Miniature Pinscher","Neapolitan Mastiff", "Newfoundland", "Norfolk Terrier", "Norwegian Elkhound", "Norwich Terrier", "Nova Scotia Duck Tolling Retriever","Old English Sheepdog", "Otterhound","Papillon", "Parson Russell Terrier", "Pekingese", "Pembroke Welsh Corgi", "Petit Basset Griffon Vendeen", "Pharaoh Hound", "Plott", "Pointer", "Polish Lowland Sheepdog", "Pomeranian", "Poodle (Miniature)", "Poodle (Standard)", "Poodle (Toy)", "Portuguese Water Dog", "Pug", "Puli","Rottweiler","Saint Bernard", "Saluki (or Gazelle Hound)", "Samoyed", "Schipperke", "Scottish Deerhound", "Scottish Terrier", "Sealyham Terrier", "Shetland Sheepdog", "Shiba Inu", "Shih Tzu", "Siberian Husky", "Silky Terrier", "Skye Terrier", "Smooth Fox Terrier", "Soft Coated Wheaten Terrier", "Spinone Italiano", "Staffordshire Bull Terrier", "Standard Schnauzer", "Sussex Spaniel","Tibetan Terrier","Toy Fox Terrier"],
        
        "Rodent" : ["Rats","Mouse","Guinea Pig","Hamster","Gerbil","Chinchilla","Squirrel","Chipmunk","All others"],
        
        "Reptile" : ["Lizard","Snakes","Turtle","Iguana","Gecko","Chamalion","Tortoise","All others"],
        "Bird": ["Canaries","Finches","Parrots","Doves","Pigeons","Mynah","Toucans","Sparrow","Titmouse","Owl","Quail","Eagle"],
        "Invertebrate": ["Crab","Grasshopper","Snails","Mantis","Beetles","Butterfly"]
        ]
}


struct Colour {
    static var primary = UIColor.init(red: 253/255.0, green: 155/255.0, blue: 70/255.0, alpha: 1.0)
    static var yellow = UIColor.init(red: 252/255.0, green: 204/255.0, blue: 76/255.0, alpha: 1.0)
    static var grey = UIColor.init(red: 154/255.0, green: 154/255.0, blue: 154/255.0, alpha: 1.0)
     static var backgroundColor = UIColor.init(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1.0)
}

//MARK:- UserDefaults keys
struct UserDefault {
    static let user = "user"
    static let isLoggedIn = "isLoggedIn"
    static let deviceToken = "deviceToken"
    static let deviceId = "deviceId"
    static let selectedLanguage = "selectedLanguage"
    static let appleLanguagesKey = "AppleLanguages"
    static let profileImg = "profileImage"
    static let loginWithFb  = "loggedInWithFB"
    static let isShowCheck1 = "isShowCheck1"
    static let isShowCheck2 = "isShowCheck2"
    
    static let photoOverlayLandscapeID = "photoOverlayLandscapeID"
    static let photoOverlayPortraitID = "photoOverlayPortraitID"
    static let videoOverlayID = "videoOverlayID"
    static let blueToothMicSelected = "bluetoothMicSelected"
    
    static let tokenId = "tokenId"
    static let userId = "userId"
    static let petList = "petList"
    static let savedPets = "savedPets"
    static let userName = "userName"
    static let name = "name"
    static let email = "email"
    static let notificationCount = "notificationCount"
    static let signUpType = "Fb"
    
}

//MARK: Placeholders
struct Placeholders {
    static let pullToRefresh = "Pull To Refresh"
    static let updating = "Updating..."
}



class KeyChain {

    class func save(key: String, data: Data) -> OSStatus {
        let query = [
            kSecClass as String       : kSecClassGenericPassword as String,
            kSecAttrAccount as String : key,
            kSecValueData as String   : data ] as [String : Any]

        SecItemDelete(query as CFDictionary)

        return SecItemAdd(query as CFDictionary, nil)
    }

    class func load(key: String) -> Data? {
        let query = [
            kSecClass as String       : kSecClassGenericPassword,
            kSecAttrAccount as String : key,
            kSecReturnData as String  : kCFBooleanTrue!,
            kSecMatchLimit as String  : kSecMatchLimitOne ] as [String : Any]

        var dataTypeRef: AnyObject? = nil

        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)

        if status == noErr {
            return dataTypeRef as! Data?
        } else {
            return nil
        }
    }

    class func createUniqueID() -> String {
        let uuid: CFUUID = CFUUIDCreate(nil)
        let cfStr: CFString = CFUUIDCreateString(nil, uuid)

        let swiftString: String = cfStr as String
        return swiftString
    }
}

extension Data {

    init<T>(from value: T) {
        var value = value
        self.init(buffer: UnsafeBufferPointer(start: &value, count: 1))
    }

    func to<T>(type: T.Type) -> T {
        return self.withUnsafeBytes { $0.load(as: T.self) }
    }
}
