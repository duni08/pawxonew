//
//  Geocoding.swift
//  AutoForce
//
//  Created by Apple2 on 20/08/19.
//  Copyright © 2019 Apple2. All rights reserved.
//

import UIKit
import CoreLocation

class Geocoding {
    
    var coordinates: CLLocationCoordinate2D
    var name: String?
    var sublocality_level_2: String?
    var sublocality_level_3: String?
    var sublocality_level_1: String?
    var locality : String?
    var formattedAddress: String?
    var boundNorthEast: CLLocationCoordinate2D?
    var boundSouthWest: CLLocationCoordinate2D?
    
    init(coordinates: CLLocationCoordinate2D) {
        self.coordinates = coordinates
    }
}
